<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\HostelPro\Models\RoomType;
use Auth;
use DB;

class SelectRoomComposer {

    public function compose(View $view)
    {
        $stay = $view->getData()['stay'];
        $booking = $view->getData()['booking'];

        $roomtypes = $booking->hostel->roomtypes->sortBy('type')->sortBy('number_of_guests');

        if (!is_null($stay->room)) {
            $number_of_guests = $stay->room->roomtype->number_of_guests;

            $roomtypes1 = $roomtypes->filter(function ($roomtype) use ($number_of_guests) {
                return $roomtype->number_of_guests <= $number_of_guests;
            })->sortByDesc('number_of_guests');

            $roomtypes2 = $roomtypes->filter(function ($roomtype) use ($number_of_guests) {
                return $roomtype->number_of_guests > $number_of_guests;
            });

            $roomtypes = $roomtypes1->merge($roomtypes2);

        }

        $allRooms = array();
        foreach ($roomtypes as $roomtype) {
            foreach($roomtype->rooms as $room) {
                $allRooms[$roomtype->name][$room->id] = $room->name;

            }
        }

        if (is_null($stay->room)) {
            $allRooms = array('' => '') + $allRooms;
        }

        $view->with('allRooms', $allRooms);
    }

}