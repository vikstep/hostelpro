<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\HostelPro\Models\ExpenseCategory;
use Auth;

class AddExpenseComposer {

    public function compose(View $view)
    {
        $categories = ExpenseCategory::where('hostel_id', '=', Auth::user()->currenthostel->id)->orderBy('name', 'ASC')->pluck('name', 'name');
        $view->with('categories', $categories);
    }

}