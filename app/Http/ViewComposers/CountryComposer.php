<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\HostelPro\Models\Country;
use Auth;

class CountryComposer {

    public function compose(View $view)
    {
        $countries = Country::orderBy('country_name')->pluck('country_name', 'id')->all();
        $countries = array('') + $countries;
        $view->with('countries', $countries)->with('id', $view->getData()['id']);
    }

}