<?php namespace App\Http\ViewComposers;

use App\HostelPro\Models\Board;
use App\HostelPro\Models\Booking;
use App\HostelPro\Models\DateHelper;
use App\HostelPro\Models\Hostel;
use Auth;
use Carbon;
use DB;
use Illuminate\Contracts\View\View;

class AlertsComposer {

    public function compose(View $view)
    {

        $notification_count = 0;

        return $view->with('notification_count', $notification_count);
    }

}