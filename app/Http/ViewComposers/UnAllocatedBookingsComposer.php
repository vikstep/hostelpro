<?php namespace App\Http\ViewComposers;

use App\HostelPro\Models\Board;
use App\HostelPro\Models\Booking;
use App\HostelPro\Models\DateHelper;
use App\HostelPro\Models\Hostel;
use Auth;
use Carbon;
use DB;
use Illuminate\Contracts\View\View;

class UnallocatedBookingsComposer {

    public function compose(View $view)
    {
        $hostel_id = Auth::user()->currenthostel->id;

        $bookings_that_need_attention = Booking::select('booking.id', 'guest.lastname',
                DB::raw('sum(board_unallocated.number_of_guests) DIV COUNT(distinct(board_unallocated.date)) as number_of_guests'),
                DB::raw('min(board_unallocated.date) as min_date'),
                DB::raw('max(board_unallocated.date) as max_date'),
                DB::raw('DATEDIFF(max(board_unallocated.date),min(board_unallocated.date)) + 1 as number_of_nights'))
            ->where('booking.hostel_id', '=', $hostel_id)
            ->where('status', '=', 'Unallocated')
            ->where('booking.show_notification', '=', true)
            ->join('guest', 'booking.guest_id', '=', 'guest.id')
            ->join('board_unallocated', 'booking.id', '=', 'board_unallocated.booking_id')
            ->groupBy('booking.id')
            ->orderBy('min_date')
            ->orderBy('number_of_guests', 'DESC')
            ->orderBy('guest.lastname')
            ->get();

        $hostel = Hostel::find($hostel_id);
        $possible_no_shows = Booking::select(
            'booking.id',
            DB::raw('COALESCE(label_types.type,\'Booking - Unpaid\') AS status'),
            'guest.firstname',
            'guest.lastname',
            'board.date',
            'board.room_id',
            DB::raw('DATEDIFF(max(board.date),min(board.date)) + 1 as number_of_nights'),
            DB::raw('room.name AS room_name'),
            DB::raw('GROUP_CONCAT(bed_number order by bed_number ASC) AS bed'),
            DB::raw('NULL as room_type_id'),
            DB::raw('COUNT(board.date) as number_of_guests'))
            ->where('booking.hostel_id', '=', $hostel_id)
            ->where(function ($q) {
                $q->whereIn('booking.label_type_id', [3, 6, 21])
                    ->orWhereNull('booking.label_type_id');
            })
            ->join('stay', 'stay.booking_id', '=', 'booking.id')
            ->join('board', 'board.stay_id', '=', 'stay.id')
            ->join('guest', 'guest.id', '=', 'booking.guest_id')
            ->join('room', 'board.room_id', '=', 'room.id')
            ->leftJoin('label_types', 'booking.label_type_id', '=', 'label_types.id')
            ->where('board.date', '=', $hostel->yesterday_at_hostel)
            ->groupBy('board.room_id', 'booking.id')
            ->orderBy('board.room_id', 'bed')
            ->get();

        $bookings_that_havent_checked_out = [];

        //Run the following only if it's past 12pm, ie past checkout time.
        if (Carbon::now($hostel->timezone->timezone)->format('H') > 12) {

            $booking_ids_yesterday = Board::select('stay.booking_id')
                ->where('hostel_id', '=', $hostel_id)
                ->where('date', '=', $hostel->yesterday_at_hostel)
                ->leftJoin('stay', 'stay.id', '=', 'board.stay_id')
                ->groupBy('stay.booking_id')
                ->pluck('booking_id')
                ->all();

            $booking_ids_today = Board::select('stay.booking_id')
                ->where('hostel_id', '=', $hostel_id)
                ->where('date', '=', $hostel->today_at_hostel)
                ->leftJoin('stay', 'stay.id', '=', 'board.stay_id')
                ->groupBy('stay.booking_id')
                ->pluck('booking_id')
                ->all();

            $departing_booking_ids = array_diff($booking_ids_yesterday, $booking_ids_today);

            $bookings_that_havent_checked_out = Booking::select(
                'booking.id',
                DB::raw('COALESCE(label_types.type,\'Booking - Unpaid\') AS status'),
                'guest.firstname',
                'guest.lastname',
                'board.date',
                'board.room_id',
                DB::raw('DATEDIFF(max(board.date),min(board.date)) + 1 as number_of_nights'),
                DB::raw('room.name AS room_name'),
                DB::raw('GROUP_CONCAT(bed_number order by bed_number ASC) AS bed'),
                DB::raw('NULL as room_type_id'),
                DB::raw('COUNT(board.date) as number_of_guests'))
                ->where('booking.hostel_id', '=', $hostel_id)
                ->whereIn('booking.id', $departing_booking_ids)
                ->where(function ($q) {
                    $q->whereIn('booking.label_type_id', [9, 12])
                        ->orWhereNull('booking.label_type_id');
                })
                ->join('stay', 'stay.booking_id', '=', 'booking.id')
                ->join('board', 'board.stay_id', '=', 'stay.id')
                ->join('guest', 'guest.id', '=', 'booking.guest_id')
                ->join('room', 'board.room_id', '=', 'room.id')
                ->leftJoin('label_types', 'booking.label_type_id', '=', 'label_types.id')
                ->where('board.date', '=', $hostel->yesterday_at_hostel)
                ->groupBy('board.room_id', 'booking.id')
                ->orderBy('board.room_id', 'bed')
                ->get();
        }

        $unallocated_bookings_count = $bookings_that_need_attention->count() + $possible_no_shows->count();

        $view->with('bookings_that_need_attention', $bookings_that_need_attention)
            ->with('unallocated_bookings_count', $unallocated_bookings_count)
            ->with('possible_no_shows', $possible_no_shows)
            ->with('bookings_that_havent_checked_out', $bookings_that_havent_checked_out);
    }

}