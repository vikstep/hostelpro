<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\HostelPro\Models\Country;
use Auth;

class AddBookingComposer {

    public function compose(View $view)
    {
        $countries = Country::orderBy('country_name')->pluck('country_name', 'id');
        $countries = Array('') + $countries;
        $roomtypes = Auth::user()->currenthostel->roomtypes->pluck('name', 'id');
        $view->with('countries', $countries)->with('roomtypes', $roomtypes);
    }

}