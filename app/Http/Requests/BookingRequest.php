<?php namespace App\Http\Requests;

use Auth;
use Carbon;

class BookingRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $timezone = Auth::user()->currenthostel->timezone->timezone;
        $yesterday =  Carbon::now($timezone)->subDay()->format("d-m-Y");

        return [
            #'guest_id' => 'required|exists:guest,id',
            'room_type_id' => 'required|exists:room_types,id',
            'start_date' => 'date_format:d-m-Y|after:' . $yesterday,
            'end_date' => 'date_format:d-m-Y',
            'number_of_guests' => 'required|integer|between:1,100',
            'price_per_night' => 'required|integer|between:1,999999',
        ];
    }

}
