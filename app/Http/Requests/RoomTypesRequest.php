<?php namespace App\Http\Requests;

class RoomTypesRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:50',
            'type' => 'required|in:Dorm,Private',
            'gender' => 'required|in:Female,Male,Mixed',
            'number_of_guests' => 'required|between:1,30',
        ];
    }

}
