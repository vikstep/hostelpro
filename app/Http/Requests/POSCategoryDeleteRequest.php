<?php namespace App\Http\Requests;

use Illuminate\Validation\Factory;
use Auth;
use App\HostelPro\Models\POSCategory;
use App\HostelPro\Models\POSProduct;

class POSCategoryDeleteRequest extends Request
{

    public function __construct(Factory $factory)
    {
        $factory->extendImplicit('categoryHasProducts', function ($attribute, $value, $parameters) {
            $categoryId = $this->route('POSCategory');
            if (POSProduct::where('category_id', $categoryId)->count() > 0) {
                return false;
            }
            return true;
        },
            'Before you can delete this, you must first delete all products associated with this category.'
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        $POSCategoryId = $this->route('POSCategory');
        if (!POSCategory::where('id', $POSCategoryId)->where('hostel_id', Auth::user()->currenthostel->id)->exists()) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'POSCategory' => 'categoryHasProducts'
        ];
    }

}
