<?php namespace App\Http\Requests;

class UserRegistrationRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'sometimes|required|min:2|max:20',
            'lastname' => 'sometimes|required|min:2|max:20',
            'hostelname' => 'required|min:2|max:50',
            'email' => 'sometimes|required|min:5|email|unique:user',
            'password' => 'sometimes|required|min:7',
            'country' => 'sometimes|required|exists:country,id',
            'terms_and_conditions' => 'accepted',
        ];
    }

}
