<?php namespace App\Http\Requests;

use Illuminate\Validation\Factory;
use Auth;
use App\HostelPro\Models\RoomType;
use App\HostelPro\Models\Room;

class RoomTypesDeleteRequest extends Request
{

    public function __construct(Factory $factory)
    {
        $factory->extendImplicit('roomTypeHasRooms', function ($attribute, $value, $parameters) {
            $roomTypesId = $this->route('roomtype');
            if (Room::where('room_type_id', '=', $roomTypesId)->count() > 0) {
                return false;
            }
            return true;
        },
            'Before you can delete this, you must first delete all rooms that use this room type.'
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return RoomType::where('id', '=', $this->route('roomtype'))->where('hostel_id', Auth::user()->currenthostel->id)->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roomtype' => 'roomTypeHasRooms'
        ];
    }

}
