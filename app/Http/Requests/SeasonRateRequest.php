<?php namespace App\Http\Requests;

use Auth;
use Carbon;
use Input;
use App\HostelPro\Models\SeasonRate;
use App\HostelPro\Models\DateHelper;
use Validator;

class SeasonRateRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $start_date = DateHelper::convertDateToYearMonthDay(Input::get('startdate'));
        $end_date = DateHelper::convertDateToYearMonthDay(Input::get('enddate'));
        $hostel_id = Auth::user()->currenthostel->id;

        Validator::extend('season_overlaps', function($attribute, $value, $parameters) use ($hostel_id, $start_date, $end_date)
        {
            $number_overlapping_seasons = SeasonRate::where('hostel_id', '=', $hostel_id)
                ->where('start_date', '<=', $end_date)
                ->where('end_date', '>=', $start_date)
                ->count();

            if ($number_overlapping_seasons >= 1) {
                return false;
            }

            return true;
        });

        return [
            'name' => 'required',
            #'room_type_id' => 'required|exists:room_types,id',
            'startdate' => 'date_format:d-m-Y|season_overlaps',
            'enddate' => 'date_format:d-m-Y|after:startdate'
        ];
    }

    public function messages() {
        return [
            'season_overlaps' => 'The dates you have entered overlap with another seasion. Seasons cannot overlap, please adjust your dates and try again.'
        ];
    }

}
