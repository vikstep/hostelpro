<?php namespace App\Http\Controllers;

use App\HostelPro\Models\Booking;
use App\HostelPro\Models\Email;
use Auth;
use Illuminate\Http\Request;
use View;
use Redirect;
use Swift_SmtpTransport;

class EmailController extends Controller {

    public function store(Request $request) {

        if ($request->exists('host')) {

            try{
                $transport = Swift_SmtpTransport::newInstance($request->input('host'), $request->input('port'), $request->input('encryption'));
                $transport->setUsername($request->input('username'));
                $transport->setPassword($request->input('password'));
                $mailer = \Swift_Mailer::newInstance($transport);
                $mailer->getTransport()->start();

                $hostel_id = Auth::user()->currenthostel->id;

                Email::where('hostel_id', '=', $hostel_id)->delete();

                $email = new Email();
                $email->fill($request->all());
                $email->hostel_id = $hostel_id;
                $email->save();

                //if successful save it and redirect
                return redirect()->back()->with('flash_message', 'Connection details saved successfully.')->with('alert-class', 'alert-success');
            } catch (\Exception $e) {
                return redirect()->back()->with('flash_message', 'Error: ' . $e->getMessage())->with('alert-class', 'alert-danger');
            }
        }

        if ($request->exists('pre_email_days') || $request->exists('post_email_days')) {

            $email = Email::where('hostel_id', '=', Auth::user()->currenthostel->id)->first();
            if (!$email) {
                return redirect()->back()->with('flash_message', 'You need to setup your SMTP configuration before you can set this.')->with('alert-class', 'alert-warning');
            }
            $email->fill($request->all());
            $email->save();

            return redirect()->back()->with('flash_message', 'Successfully set before arrival email.')->with('alert-class', 'alert-success');
        }

        return redirect()->back()->with('flash_message', 'Successfully set email.')->with('alert-class', 'alert-success');
    }

    public function sendTestEmail($emailtype) {

        $email = Email::where('hostel_id', '=', Auth::user()->currenthostel->id)->first();
        if (!$email) {
            return redirect()->back()->with('flash_message', 'You need to setup your SMTP configuration before you can set this.')->with('alert-class', 'alert-warning');
        }

        if ($emailtype == "pre") {
            $body = $email->pre_email_text;
        } else {
            $body = $email->post_email_text;
        }

        $to_email = Auth::user()->email;

        $transport = Swift_SmtpTransport::newInstance($email->host, $email->port, $email->encryption);
        $transport->setUsername($email->username);
        $transport->setPassword($email->password);
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $mailer = \Swift_Mailer::newInstance($transport);
        $mailer->getTransport()->start();
        $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

        $message = new \Swift_Message('Test Email');
        $message->setFrom([$email->username => Auth::user()->currenthostel->name]);
        $message->setTo($to_email);
        $message->setBody($body, 'text/html');
        $mailer->send($message);
        //dd($logger);

        return redirect()->back()->with('flash_message', 'Successfully sent test email to ' . $to_email)->with('alert-class', 'alert-success');
    }

    public function create()
    {
        $this->authorize('hostel_settings');

        $email = Email::where('hostel_id', '=', Auth::user()->currenthostel->id)->first();
        if (!$email) {
            $email = new Email();
            $email->encryption = 'ssl';
        }

        return View::make('pages.email')->with('email', $email);
    }

}