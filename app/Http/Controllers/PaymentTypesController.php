<?php namespace App\Http\Controllers;

use Auth;
use View;
use Input;
use Redirect;
use DB;
use App\HostelPro\Models\PaymentType;

class PaymentTypesController extends Controller {

    public function create() {
        $this->authorize('hostel_settings');
        $payment_types = Auth::user()->currenthostel->paymentTypes;
        return View::make('pages.paymenttypes')->with('payment_types', $payment_types);
    }

    public function store() {
        $this->authorize('hostel_settings');

        $newPaymentType = new PaymentType();
        $newPaymentType->hostel_id = Auth::user()->currenthostel->id;
        $newPaymentType->name = Input::get('name');
        $newPaymentType->kept_in_till = Input::get('keptintill');
        $newPaymentType->save();

        $payment_types = new \Illuminate\Database\Eloquent\Collection;
        $payment_types->add($newPaymentType);
        return View::make('partials.paymenttype')->with('payment_types', $payment_types);

    }

    public function destroy($id) {
        $this->authorize('hostel_settings');

        PaymentType::where('id', '=', $id)->where('hostel_id', '=', Auth::user()->currenthostel->id)->delete();

        $payment_types = PaymentType::where('hostel_id', '=', Auth::user()->currenthostel->id)->orderBy('name', 'ASC')->get();

        return View::make('partials.paymenttype')->with('payment_types', $payment_types);
    }

    public function sort()
    {
        $this->authorize('hostel_settings');

        $allowed_payment_type_ids = Auth::user()->currenthostel->paymentTypes->pluck('id')->all();
        $payment_type_ids = array_intersect(Input::get('paymenttypes'), $allowed_payment_type_ids);
        $payment_type_ids_with_commas = implode(',', $payment_type_ids);

        DB::beginTransaction();
        DB::statement('set @position = 0');
        DB::unprepared('UPDATE `payment_types` SET sort=@position:=@position+1 where `id` in (' . $payment_type_ids_with_commas . ') order by FIELD(id, ' . $payment_type_ids_with_commas . ')');
        DB::commit();
    }

}