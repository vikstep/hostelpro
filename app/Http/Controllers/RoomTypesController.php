<?php namespace App\Http\Controllers;

use Auth;
use Form;
use View;
use App\HostelPro\Models\RoomType;
use App\Http\Requests\RoomTypesRequest;
use App\Http\Requests\RoomTypesDeleteRequest;

class RoomTypesController extends Controller {

    public function index()
    {
        return Form::select("room_type_id", Auth::user()->currenthostel->roomtypes->pluck("name", "id"), "", array("class" => "form-control room_type_id"));
    }

    public function store(RoomTypesRequest $request) {

        $room = new RoomType();
        $room->fill($request->all());
        $room->hostel_id = Auth::user()->currenthostel->id;
        $room->save();

        return View::make('partials.roomtype')->with('room', $room);
    }

    public function destroy($roomtype, RoomTypesDeleteRequest $request) {

        RoomType::find($roomtype)->delete();

        $roomtypes = Auth::user()->currenthostel->roomtypes;
        return View::make('partials.roomtypes')->with('roomtypes', $roomtypes);
    }

}