<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{

    use ResetsPasswords;

    public $redirectTo = '/calendar';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
        //$row = \DB::table('password_resets')->where('token', '=', $token)->first();

        return view('auth.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
