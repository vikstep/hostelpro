<?php namespace App\Http\Controllers;

use App\HostelPro\Models\Booking;
use App\HostelPro\Models\Board;
use App\HostelPro\Models\MyAllocatorSyncAvailability;
use App\HostelPro\Models\Stay;
use Auth;
use View;
use App\HostelPro\Models\DayViewReport;
use App\HostelPro\Models\DateHelper;

class CalendarController extends Controller {

    public function create() {

        $timezone = Auth::user()->currenthostel->timezone->timezone;

        $start = DateHelper::getToday($timezone);
        $report = new DayViewReport(Auth::user()->currenthostel->id, $start);

        //$test = new MyAllocatorSyncAvailability(45, '2017-05-23', '2017-05-25');

        return View::make('pages.dashboard')->with('report', $report)->with('date', $start);
    }

}