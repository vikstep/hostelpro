<?php namespace App\Http\Controllers;

use App\HostelPro\Models\Calendar;
use App\HostelPro\Models\CompetitorRates;
use Auth;
use View;
use Input;
use Redirect;
use Carbon;

use App\HostelPro\Models\DateHelper;
use App\HostelPro\Models\MoneyHelper;
use App\HostelPro\Models\Rate;
use App\HostelPro\Models\DailyRate;
use App\HostelPro\Models\DailySeasonRate;
use App\HostelPro\Models\Board;
use App\HostelPro\Models\SeasonRate;
use App\HostelPro\Models\DiscountRate;
use App\Http\Requests\SeasonRateRequest;

class RatesController extends Controller {

    public function index() {
        $this->authorize('rates');
        $seasonrates = Auth::user()->currenthostel->seasonRates;
        $rateData = array();
        $days_of_week = array();
        $room_types = Auth::user()->currenthostel->roomtypes;
        $room_type_ids = Auth::user()->currenthostel->roomtypes->pluck('id');
        $seasonrate_ids = Auth::user()->currenthostel->seasonRates->pluck('id');

        //getting season rate for each day of week
        $dayOfWeekRatesData = DailySeasonRate::whereIn('room_type_id', $room_type_ids)
            ->whereIn('season_id', $seasonrate_ids)->get()->toArray();

        $dayOfWeekRates = array();

        foreach($dayOfWeekRatesData as $value)
        {
            $dayOfWeekRates[$value["season_id"] . '-' . $value["room_type_id"]] =
                ['rates' => unserialize($value["rates"]), 'nights' => unserialize($value["min_stay"])];

        }

        foreach ($seasonrates as $seasonrate) {

            $number_of_days_in_season = min(DateHelper::countNumberOfDays($seasonrate->start_date, $seasonrate->end_date),7);
            $dates = DateHelper::getArrayOfDatesByString($seasonrate->start_date, DateHelper::addDays($seasonrate->start_date, $number_of_days_in_season-1));

            //If we have a full week, start the day on Monday, otherwise start on whichever day the date falls on
            if ($number_of_days_in_season == 7) {
                $dates = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
            } else {
                foreach ($dates as $i => $date) {
                    $dates[$i] = DateHelper::getDayOfWeek($date);
                }
            }

            $days_of_week[$seasonrate->id] = $dates;

            //Initialize all values to 0 price and 1 night
            foreach($room_type_ids as $room_type_id) {
                foreach($dates as $date) {
                    $rateData[$seasonrate->id][$room_type_id][$date]['price'] = 0;
                    $rateData[$seasonrate->id][$room_type_id][$date]['nights'] = 1;
                }
            }

            //Get existing rate data for the given season
            $rates = Rate::whereIn('room_type_id', $room_type_ids)
                ->whereBetween('date', array($seasonrate->start_date, DateHelper::addDays($seasonrate->start_date, $number_of_days_in_season-1)))
                ->get();

            //Overwrite the array with rate data from the database (if found)
            foreach($rates as $rate) {
                $rate_day_of_week = DateHelper::getDayOfWeek($rate->date);

                $price = isset($dayOfWeekRates[$seasonrate->id . '-' . $rate->room_type_id]['rates'][$rate_day_of_week]) ?
                    $dayOfWeekRates[$seasonrate->id . '-' . $rate->room_type_id]['rates'][$rate_day_of_week] :
                        $rate->price;

                $min_stay = isset($dayOfWeekRates[$seasonrate->id . '-' . $rate->room_type_id]['nights'][$rate_day_of_week]) ?
                    $dayOfWeekRates[$seasonrate->id . '-' . $rate->room_type_id]['nights'][$rate_day_of_week] :
                    $rate->min_stay;

                $rateData[$seasonrate->id][$rate->room_type_id][$rate_day_of_week]['price'] =  
                    MoneyHelper::convertCentsToDollars($price);
                $rateData[$seasonrate->id][$rate->room_type_id][$rate_day_of_week]['nights'] = $min_stay;
            }

        }

        return View::make('pages.rates.seasonrates')
            ->with('seasonrates', $seasonrates)
            ->with('days_of_week', $days_of_week)
            ->with('roomtypes', $room_types)
            ->with('rateData', $rateData);
    }

    public function store(SeasonRateRequest $request) {
        $this->authorize('rates');
        $name = Input::get('name');
        $startdate = Input::get('startdate');
        $enddate = Input::get('enddate');

        $seasonRate = new SeasonRate();
        $seasonRate->name = $name;
        $seasonRate->hostel_id = Auth::user()->currenthostel->id;
        $seasonRate->start_date = DateHelper::convertDateToYearMonthDay($startdate);
        $seasonRate->end_date = DateHelper::convertDateToYearMonthDay($enddate);
        $seasonRate->save();

        return Redirect::route('settings.rates.index');
    }

    public function update($id) {
        $this->authorize('rates');
        $modify = Input::get('modify');

        if ($modify) {
            $startdate = Input::get('startdate');
            $enddate = Input::get('enddate');

            $seasonrate = SeasonRate::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('id', '=', $id)->first();
            $seasonrate->updateDates($startdate, $enddate);
        } else {
            $prices = Input::get('prices');
            $nights = Input::get('nights');

            $seasonrate = SeasonRate::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('id', '=', $id)->first();
            $seasonrate->updateRates($prices, $nights);
        }
        return Redirect::route('settings.rates.index');
    }

    public function destroy($id) {
        $this->authorize('rates');
        $seasonrate = SeasonRate::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('id', '=', $id)->delete();

        return Redirect::route('settings.rates.index');
    }

    public function competitor() {
        $this->authorize('rates');

        if (Auth::user()->currenthostel->competitorURLs()->count() == 0) {
            return Redirect::route('settings.competitor.index');
        }

        $rates = new CompetitorRates(Auth::user()->currenthostel->id, Carbon::now()->toDateString(), Carbon::now()->addDays(31)->toDateString());

        $calendar = new Calendar(Auth::user()->currenthostel->id, Carbon::now()->toDateString(), 32);
        $calendar->getAvailability();

        return View::make('pages.rates.competitorrates')->with('rates', $rates)->with('calendar', $calendar);
    }

    public function daily()
    {
        $roomtypes = Auth::user()->currenthostel->roomtypes;

        $occupancies = array();
        $datesFormatted = array();
        $roomTypes = array();

        foreach ($roomtypes as $roomtype)
        {
            $date = new \DateTime('now');

            $occupancies[$roomtype->name] = array();
            $roomTypes[$roomtype->name] = $roomtype->id;

            $countRooms = count($roomtype->rooms);
            $numberOfGuests = $roomtype->number_of_guests;
            $max_possible_bed_nights = $countRooms * $numberOfGuests;
 
            $room_ids = $roomtype->rooms->pluck('id')->all();

            //calculate for next 7 days
            for($i = 0; $i < 7; $i++)
            {
                $formattedDate = $date->format('Y-m-d');

                $rate =
                    DailyRate::where('date', $formattedDate)->whereIn('room_type_id', array($roomtype->id))->first();

                $total_bed_nights =
                    Board::where('date', $formattedDate)->whereIn('room_id', $room_ids)->count();

                $price = $rate ? MoneyHelper::convertCentsToDollars($rate->price) : 0;
                $percent = round(($total_bed_nights / $max_possible_bed_nights)*100,2);
                $color = Rate::getOccupancyColor($percent);

                $occupancies[$roomtype->name][$date->format('Y-m-d')] = array(
                    'percent' => $percent,
                    'available' => $max_possible_bed_nights - $total_bed_nights,
                    'rate' => $price,
                    'color' => $color
                );

                $datesFormatted[$formattedDate] = array(
                    'day' => $date->format('d'),
                    'dayOfWeek' => $date->format('D'),
                );

                $date->modify('+1 day');
            }
        }

        return View::make('pages.rates.dailyrates')
            ->with('occupancies', $occupancies)
            ->with('datesFormatted', $datesFormatted)
            ->with('roomTypes', $roomTypes);
    }

    public function discount() {
        $this->authorize('rates');

        $currenthostel = Auth::user()->currenthostel;
        $seasonrates = $currenthostel->seasonRates;
        $room_types = $currenthostel->roomtypes;

        $view = View::make('pages.rates.discountrates')
            ->with('seasonrates', $seasonrates)
            ->with('roomtypes', $room_types);

        if($seasonrates->count() > 0 && $room_types->count() > 0)
        {
            $currentSeasonId = $seasonrates->first()->id;
            $currentRoomTypeId = $room_types->first()->id;

            $discountRate = DiscountRate::where('roomtype_id', $currentRoomTypeId)
                ->where('season_rates', $currentSeasonId)->first();

            if($discountRate)
            {
                if($discountRate->progressive_low_price)
                {
                    $discountRate->progressive_low_price =
                        MoneyHelper::convertCentsToDollars($discountRate->progressive_low_price);
                }

                if($discountRate->progressive_high_price)
                {
                    $discountRate->progressive_high_price =
                        MoneyHelper::convertCentsToDollars($discountRate->progressive_high_price);
                }
                
                $view->with('currentDiscount', $discountRate);
            }
        }

        return $view;
    }

}