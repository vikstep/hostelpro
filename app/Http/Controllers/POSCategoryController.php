<?php namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use Input;
use Redirect;
use Form;
use App\HostelPro\Models\POSCategory;
use App\Http\Requests\POSCategoryRequest;
use App\Http\Requests\POSCategoryDeleteRequest;

class POSCategoryController extends Controller {

    public function index() {
        return Form::select("category_id", Auth::user()->currenthostel->posCategories->pluck("name", "id"), "", array("class" => "form-control category_id"));
    }

    public function store(POSCategoryRequest $request) {
        $this->authorize('hostel_settings');

        $category = new POSCategory();
        $category->name = Input::get('name');
        $category->hostel_id = Auth::user()->currenthostel->id;
        $category->save();
//        $categories = new \Illuminate\Database\Eloquent\Collection;
//        $categories->add($category);
        return View::make('partials.pos.settingscategory')->with('category', $category);
    }

    public function edit($id) {
        $this->authorize('hostel_settings');

        $category = Auth::user()->currenthostel->posCategories()->where('pos_categories.id', '=', $id)->firstOrFail();

        return View::make('partials.pos.settingscategoryedit')->with('category', $category);
    }

    public function update($id) {
        $this->authorize('hostel_settings');

        $category = Auth::user()->currenthostel->posCategories()->where('pos_categories.id', '=', $id)->firstOrFail();
        $category->name = Input::get('name');
        $category->save();

        return View::make('partials.pos.settingscategory')->with('category', $category);
    }

    public function destroy($categoryId, POSCategoryDeleteRequest $request) {
        $this->authorize('hostel_settings');

        POSCategory::find($categoryId)->delete();
        $categories = Auth::user()->currenthostel->posCategories;
        return View::make('partials.pos.settingscategories')->with('categories', $categories);
    }

    public function sort() {
        $this->authorize('hostel_settings');

        $allowed_category_ids = Auth::user()->currenthostel->posCategories->pluck('id')->all();
        $category_ids = array_intersect(Input::get('category'), $allowed_category_ids);
        $category_ids_with_commas = implode(',', $category_ids);

        DB::beginTransaction();
        DB::statement('set @position = 0');
        DB::unprepared('UPDATE `pos_categories` SET sort=@position:=@position+1 where `id` in (' . $category_ids_with_commas . ') order by FIELD(id, ' . $category_ids_with_commas . ')');
        DB::commit();
    }

}