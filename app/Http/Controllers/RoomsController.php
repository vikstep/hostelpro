<?php namespace App\Http\Controllers;

use App\HostelPro\Models\Board;
use Auth;
use DB;
use View;
use Input;
use Redirect;
use App\Http\Requests\RoomRequest;
use App\Http\Requests\RoomTypesRequest;
use App\HostelPro\Models\Room;
use App\HostelPro\Models\RoomType;

class RoomsController extends Controller {

    public function create()
    {

    }

    public function index() {
        /*
        $roomtypes = Auth::user()->currenthostel->roomtypes;
        //dd($roomtypes);
        $rooms = new \Illuminate\Database\Eloquent\Collection;
        foreach ($roomtypes as $roomtype) {
            if ($roomtype->rooms->count() > 0) {
                $temprooms = $roomtype->rooms;
                foreach($temprooms as $room) {
                    $rooms->add($room);
                }
            }
        }

        //$rooms = RoomType::find(14);
        //echo "total rooms: " . $rooms->count();
        //dd($rooms);
        return View::make('pages.rooms2')->with('roomtypes', $roomtypes)->with('rooms', $rooms);
        */
    }

    public function edit($id) {
        $this->authorize('hostel_settings');
        $room = Auth::user()->currenthostel->rooms()->where('room.id', '=', $id)->firstOrFail();
        $roomtypes = Auth::user()->currenthostel->roomtypes->pluck("name", "id");
        return View::make('partials.roomedit')->with('room', $room)->with('roomtypes', $roomtypes);
    }

    public function update($id) {
        $this->authorize('hostel_settings');
        $room = Auth::user()->currenthostel->rooms()->where('room.id', '=', $id)->firstOrFail();
        $room->name = Input::get('name');
        //$room_type_id = Input::get('room_type_id');
        //$room->room_type_id = $room_type_id;
        $room->save();
        return View::make('partials.room')->with('room', $room);
    }

    public function store(RoomRequest $request) {
        $this->authorize('hostel_settings');
        //
        //need to remember to add in check that current user is authorized to reference that hostel
        //
        //$values = array();
        $values['room_type_id'] = Input::get('room_type_id');
        $values['name'] = Input::get('name');
        $room = Room::create($values);
        return View::make('partials.room')->with('room', $room);

    }

    public function destroy($id) {
        $this->authorize('hostel_settings');

        $count = Board::where('room_id', '=', $id)
            ->where('hostel_id', '=', Auth::user()->currenthostel->id)
            ->where('date', '>=', Auth::user()->currenthostel->today_at_hostel)
            ->count();

        if ($count > 0) {
            return response('You must remove all future bookings (ie from today onwards) from this room before you can delete.', 500)->header('Content-Type', 'text/plain');
        }

        $room = Room::where('id', '=', $id)->whereIn('room_type_id', Auth::user()->currenthostel->roomtypes->pluck('id')->all())->delete();

        $rooms = Auth::user()->currenthostel->rooms;
        return View::make('partials.rooms')->with('rooms', $rooms);
    }

    public function sort() {
        $this->authorize('hostel_settings');

        $allowed_room_ids = Auth::user()->currenthostel->rooms->pluck('id')->all();
        $room_ids = array_intersect(Input::get('rooms'), $allowed_room_ids);
        $room_ids_with_commas = implode(',', $room_ids);

        DB::beginTransaction();
        DB::statement('set @position = 0');
        DB::unprepared('UPDATE `room` SET sort=@position:=@position+1 where `id` in (' . $room_ids_with_commas . ') order by FIELD(id, ' . $room_ids_with_commas . ')');
        DB::commit();

        /*
        DB::beginTransaction();
        Room::whereIn('id', $allowed_room_ids)->update(['sort' => null]);
        foreach ($rooms as $i => $room) {
            if (in_array($room, $allowed_room_ids)) {
                Room::where('id', '=', $room)->limit(1)->update(['sort' => $i]);
            }
        }
        DB::commit();
        */
    }

}