<?php namespace App\Http\Controllers;

use Auth;
use View;
use Input;
use Redirect;
use App\HostelPro\Models\Expense;
use App\HostelPro\Models\MoneyHelper;
use App\HostelPro\Models\DateHelper;

class ExpenseController extends Controller {

    public function store() {
        
        if (Input::get('date')) {
            $date = DateHelper::convertDateToYearMonthDay(Input::get('date'));
            $taken_from_cash_register = false;
        } else {
            $date = Auth::user()->currenthostel->today_at_hostel;
            $taken_from_cash_register = Input::get('taken_from_cash_register');
        }

        $expense = new Expense();
        $expense->hostel_id = Auth::user()->currenthostel->id;
        $expense->user_id = Auth::user()->id;
        $expense->date = $date;
        $expense->category = Input::get('category');
        $expense->name = Input::get('name');
        $expense->price = MoneyHelper::convertToCents(Input::get('price'));
        $expense->taken_from_cash_register = $taken_from_cash_register;
        $expense->save();

        return redirect()->back()->with('flash_message', 'Successfully added expense.')->with('alert-class', 'alert-success');
    }

    public function index() {
        $this->authorize('advanced_reports');
        $start = Input::get('startdate');
        $end = Input::get('enddate');
        if (!empty($start) && !empty($end)) {
            $start = DateHelper::convertDateToYearMonthDay($start);
            $end = DateHelper::convertDateToYearMonthDay($end);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-t');
        }
        $hostel_id = Auth::user()->currenthostel->id;

        $expenses = Expense::where('hostel_id', '=', $hostel_id)->whereBetween('date', [$start, $end])->orderBy('date', 'DESC')->get();

        $start = DateHelper::convertStringDateToCarbon($start)->format('d-m-Y');
        $end = DateHelper::convertStringDateToCarbon($end)->format('d-m-Y');

        return View::make('pages.reports.expenses')->with('expenses', $expenses)->with('startdate', $start)->with('enddate', $end);
    }

    public function destroy($expenseId) {
        $this->authorize('advanced_reports');
        Expense::where('id', '=', $expenseId)->where('hostel_id', '=', Auth::user()->currenthostel->id)->delete();
        #$money = Money::where('hostel_id', '=', Auth::user()->currenthostel->id)->orderBy('amount', 'ASC')->get();
        return "Success";
    }

}