<?php namespace App\Http\Controllers;

use App\HostelPro\Models\ActionLog;
use App\HostelPro\Models\ActionLogType;
use App\HostelPro\Models\Rate;
use App\HostelPro\Services\PackagedResponseService;
use App\HostelPro\Services\BookingModificationService;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use View;
use Redirect;
use Input;
use Session;
use App\HostelPro\Jobs\SyncAvailability;
use App\HostelPro\Models\BoardUnallocated;
use App\HostelPro\Models\Booking;
use App\HostelPro\Models\BoardTemp;
use App\HostelPro\Models\Calendar;
use App\HostelPro\Models\CapacityChecker;
use App\HostelPro\Models\DateHelper;
use App\HostelPro\Models\Guest;
use App\HostelPro\Models\LabelType;
use App\HostelPro\Models\MoneyHelper;
use App\HostelPro\Models\Stay;
use App\HostelPro\Models\Room;
use App\HostelPro\Models\User;
use App\HostelPro\Models\SmartAllocator;
use App\Http\Requests\BookingRequest;

class BookingController extends Controller {

    public function update($bookingid) {
        $hostel_id = Auth::user()->currenthostel->id;
        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', $hostel_id)->first();
        if (!$booking) {
            return response("Booking not found. The page will now reload in 5 seconds. Please try again afterwards.", "599")->header('Content-Type', 'text-plain');
        }

        $guestdata = Input::get('guestdata');
        $paymentdata = Input::get('paymentdata');
        $bookingdata = Input::get('bookingdata');
        $label = Input::get('label');

        DB::beginTransaction();
        $success = (new BookingModificationService($hostel_id, $bookingid, $guestdata, $paymentdata, $bookingdata, $label))->getStatus();
        if (!$success) {
            return response("Failed to update booking. The page will now reload in 5 seconds. Please try again afterwards.", "599")->header('Content-Type', 'text-plain');
        }

        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, $bookingid, $calendar_options);
        DB::commit();
        return response()->json($response);
    }

    public function updateLabel($bookingid) {
        $label = Input::get('label');
        $booking = Booking::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('id', '=', $bookingid)->first();

        if ((!$booking) || (empty($label))) {
            return response("Failed", "500")->header('Content-Type', 'text-plain');
        }

        if ($label == "checkin") {
            if ($booking->totalAmountOwing() > 0) {
                $labelType = LabelType::where('type', '=', 'Checked in - Unpaid')->first();
            } else {
                $labelType = LabelType::where('type', '=', 'Checked in - Paid')->first();
            }
        }

        if ($label == "checkout") {
            $labelType = LabelType::where('type', '=', 'Checked Out')->first();
        }

        $booking->label_type_id = $labelType->id;
        $booking->save();

        return response("Successfully changed label", "200")->header('Content-Type', 'text-plain');
    }

    public function block($bookingid) {
        $hostel_id = Auth::user()->currenthostel->id;

        DB::beginTransaction();
        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', $hostel_id)->first();

        if (!$booking->block()) {
            return response("Failed", "500")->header('Content-Type', 'text-plain');
        }

        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, null, $calendar_options);
        DB::commit();
        return response()->json($response);
    }

    public function unblock($bookingid) {
        $hostel_id = Auth::user()->currenthostel->id;

        DB::beginTransaction();
        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', $hostel_id)->first();

        if (!$booking->unblock()) {
            return response("Failed", "500")->header('Content-Type', 'text-plain');
        }
        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, null, $calendar_options);
        DB::commit();
        return response()->json($response);
    }

    public function lock($bookingid) {
        $hostel_id = Auth::user()->currenthostel->id;

        DB::beginTransaction();
        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', $hostel_id)->first();
        $booking->allocated_by_system = false;
        $booking->save();

        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, null, $calendar_options);
        DB::commit();
        return response()->json($response);
        return response("Successfully locked booking", "200")->header('Content-Type', 'text-plain');
    }

    public function unlock($bookingid) {
        $hostel_id = Auth::user()->currenthostel->id;

        DB::beginTransaction();
        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', $hostel_id)->first();
        $booking->allocated_by_system = true;
        $booking->save();

        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, null, $calendar_options);
        DB::commit();
        return response()->json($response);
        return response("Successfully locked booking", "200")->header('Content-Type', 'text-plain');
    }

    public function noshow($bookingid) {
        $hostel_id = Auth::user()->currenthostel->id;

        DB::beginTransaction();
        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', $hostel_id)->first();

        $start_date = $booking->nonTempStays()->min('start_date');
        $end_date = $booking->nonTempStays()->max('end_date');

        if (!$booking->noshow()) {
            return response("Failed", "500")->header('Content-Type', 'text-plain');
        }

        $actionLogType = ActionLogType::where('name', '=', 'No Show')->first();
        $actionLog = new ActionLog();
        $actionLog->action_log_type_id = $actionLogType->id;
        $actionLog->hostel_id = $booking->hostel_id;
        $actionLog->booking_id = $booking->id;
        $actionLog->user_id = Auth::user()->id ?? null;
        $actionLog->created_at = Carbon::now()->addSecond()->toDateTimeString();
        $actionLog->updated_at = Carbon::now()->addSecond()->toDateTimeString();
        $actionLog->save();

        $this->dispatch(new SyncAvailability($booking->hostel_id, $start_date, $end_date));

        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, null, $calendar_options);
        DB::commit();
        return response()->json($response);
    }

    public function unallocate($bookingid) {
        $hostel_id = Auth::user()->currenthostel->id;

        DB::beginTransaction();
        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', $hostel_id)->first();
        if (!$booking->unallocate(true)) {
            return response("Failed", "500")->header('Content-Type', 'text-plain');
        }

        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, null, $calendar_options);
        DB::commit();
        return response()->json($response);
    }

    public function createNewBookingAndStay() {

        $guest = new Guest();
        $guest->firstname = "NEW";
        $guest->lastname = "NEW";
        $guest->hostel_id = Auth::user()->currenthostel->id;
        $guest->save();

        $booking = new Booking();
        $booking->status = "Allocated";
        $booking->hostel_id = Auth::user()->currenthostel->id;
        $booking->guest_id = $guest->id;
        $booking->save();

        $bedNumber = Input::get('bednumber');
        $roomId = Input::get('roomid');
        $date = Input::get('date');

        $beds = Input::get('beds');
        $days = Input::get('days');
        if (!$booking->addStayToExistingTemp($date, $roomId, $bedNumber, true, $beds, $days)) {
            return response("Failed to add Stay", "500")->header('Content-Type', 'text-plain');
        }

        return response($booking->id, "200")->header('Content-Type', 'text-plain');
    }

    public function createNewBookingAndBlock() {
        DB::beginTransaction();
        $guest = new Guest();
        $guest->firstname = "BLOCKED";
        $guest->lastname = "BLOCKED";
        $guest->hostel_id = Auth::user()->currenthostel->id;
        $guest->save();

        $booking = new Booking();
        $booking->status = "Allocated";
        $booking->hostel_id = Auth::user()->currenthostel->id;
        $booking->guest_id = $guest->id;
        $booking->save();

        $bedNumber = Input::get('bednumber');
        $roomId = Input::get('roomid');
        $date = Input::get('date');
        if (!$booking->addStayToExistingTemp($date, $roomId, $bedNumber, true)) {
            return response("Failed to add Stay", "500")->header('Content-Type', 'text-plain');
        }
        $booking->block();
        DB::commit();
        return response($booking->id, "200")->header('Content-Type', 'text-plain');
    }

    public function addStayToExistingTemp($bookingid) {
        $hostel_id = Auth::user()->currenthostel->id;

        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', $hostel_id)->first();

        $bedNumber = Input::get('bednumber');
        $roomId = Input::get('roomid');
        
        $beds = Input::get('beds');
        $days = Input::get('days');
        
        if($beds && $days)
        {
            $new = true;
        }
        else
        {
            $beds = 1;
            $days = 1;
            $new = false;            
        }
        
        if (Input::get('date')) {
            $date = Input::get('date');
        } else {
            $date = Auth::user()->currenthostel->today_at_hostel;
        }

        DB::beginTransaction();
        if (!$booking->addStayToExistingTemp($date, $roomId, $bedNumber, $new, $beds, $days)) {
            return response("Failed to add Stay", "500")->header('Content-Type', 'text-plain');
        }
        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, $booking->id, $calendar_options, true);
        DB::commit();
        return response()->json($response);
    }

    public function moveStay($bookingid, Request $request) {
        $hostel_id = Auth::user()->currenthostel->id;

        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', $hostel_id)->first();

        $stay_id = $request->input('stayid');
        $date = $request->input('date');
        $room_id = $request->input('roomid');
        $bed_number = $request->input('bednumber');
        $open_booking_id = $request->input('openbookingid');

        DB::beginTransaction();
        $stay = $booking->stays()->where('stay.id', '=', $stay_id)->first();
        if ((!$stay) || (!$stay->moveTo($date, $room_id, $bed_number)) || ($booking->id == $open_booking_id)) {
            $calendar_options = json_decode($request->input('calendar'), true);
            $response = new PackagedResponseService($hostel_id, $booking->id == $open_booking_id ? $booking->id : null, $calendar_options, true);
        } else {
            $response = "Success";
        }
        DB::commit();

        return response()->json($response);
    }

    public function show($bookingId) {

        DB::beginTransaction();
        $hostel_id = Auth::user()->currenthostel->id;
        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, $bookingId, $calendar_options);
        DB::commit();

        return response()->json($response);
    }

    public function edit($bookingid) {

        $hostel_id = Auth::user()->currenthostel->id;

        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', $hostel_id)->first();
        if (!$booking) {
            return response("Booking not found. This page will now reload in 5 seconds. Please try again afterwards.", "599")->header('Content-Type', 'text-plain');
        }

        DB::beginTransaction();

        $stayid = Input::get('stayid');

        $stay = Stay::where('id', '=', $stayid)->where('booking_id', '=', $booking->id)->with('room')->first();

        if (!$stay) {
            return response("Temporary Error: Failed to update booking. The page will now reload in 5 seconds. Please try again afterwards.", "599")->header('Content-Type', 'text-plain');
        }

        $deleteStay = Input::get('delete');
        if (!empty($deleteStay)) {
            $stay->delete();
        }

        $newRoom = Input::get('room');

        $enddate = Input::get('enddate');
        if (!empty($enddate)) {
            $enddate = DateHelper::subDays(DateHelper::convertDateToYearMonthDay($enddate),1);
        }

        $startdate = Input::get('startdate');
        if (!empty($startdate)) {
            $startdate = DateHelper::convertDateToYearMonthDay($startdate);
            if ($startdate > $stay->end_date) {
                $enddate = DateHelper::addDays(DateHelper::convertDateToYearMonthDay($startdate), 1);
            }
        }

        $newNumberOfGuests = Input::get('guests');
        $newNumberOfNights = Input::get('number-of-nights');
        if (!empty($newNumberOfNights)) {
            $enddate = DateHelper::addDays($stay->start_date, $newNumberOfNights - 1);
        }

        if (!empty($stay->room_id) || !empty($newRoom)) {
            $checker = new CapacityChecker($hostel_id, $startdate ?? $stay->start_date, $enddate ?? $stay->end_date, $newNumberOfGuests ?? $stay->number_of_guests);
            $checker->setRoomId($newRoom ?? $stay->room_id);
            $checker->setIgnoreStay($stay->id);
            $checker->setBookingId($booking->id);
            if (!$checker->check()) {
                return response("There is not enough capacity for your selection.", "500")->header('Content-Type', 'text-plain');
            }

            //We must be using the add line function on the sidebar.
            if ($newRoom) {
                $stay->room_id = $newRoom;
                $stay->load('room');
                $rate = Rate::where('room_type_id', '=', $stay->room->room_type_id)->where('date', '=', $stay->start_date)->first();
                $stay->price_per_night = $rate->price;
                $stay->save();
            }
        }

        if ((!empty($newRoom) || !empty($startdate) || !empty($enddate) || !empty($newNumberOfGuests)) && (!empty($stay->room_id))) {

            $tempBooking = $booking->replicate();
            $tempBooking->save();

            $dates = DateHelper::getArrayOfDatesByString($startdate ?? $stay->start_date, $enddate ?? $stay->end_date);

            $room_type_id = $stay->room->room_type_id;

            if (!empty($newRoom)) {
                $room = Room::whereIn('id', Auth::user()->currenthostel->rooms->pluck('id')->all())->where('id', '=', $newRoom)->first();
                $room_type_id = $room->room_type_id;
                if ($room->isPrivateRoom()) {
                    $newNumberOfGuests = $room->roomtype->number_of_guests;
                }
            }

            $inserts = array();
            foreach ($dates as $index => $date) {
                array_push($inserts,array(
                    'date' => $date,
                    'price_per_night' => $stay->price_per_night,
                    'room_type_id' => $room_type_id,
                    'number_of_guests' => $newNumberOfGuests ?? $stay->number_of_guests,
                    'booking_id' => $tempBooking->id));
            }
            BoardUnallocated::insert($inserts);

            $stay->delete();

            $allocator = new SmartAllocator($hostel_id);
            $allocator->addBooking($tempBooking->id);
            $allocator->setAllowedRoomIds([$newRoom ?? $stay->room_id]);
            $allocator->setStayIdsToIgnore($booking->stays->pluck('id')->all());
            $allocator->setStayIdsToInclude(array_diff($booking->stays->pluck('id')->all(),[$stayid]));
            $allocator->processAndSave();

            DB::table('stay')->where('booking_id', '=', $tempBooking->id)->update(['booking_id' => $booking->id]);
            DB::table('board_temp')->where('booking_id', '=', $tempBooking->id)->update(['booking_id' => $booking->id]);
            $tempBooking->delete();

        }

        //If we are changing an unallocated booking then just change the stay.
        if ((!empty($newRoom) || !empty($startdate) || !empty($enddate) || !empty($newNumberOfGuests)) && (empty($stay->room_id))) {
            $stay->room_id = $newRoom ?? $stay->room_id ?? null;
            $stay->start_date = $startdate ?? $stay->start_date;
            $stay->end_date = $enddate ?? $stay->end_date;
            $stay->number_of_guests = $newNumberOfGuests ?? $stay->number_of_guests;
            $stay->save();
        }

        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, $booking->id, $calendar_options, true);
        DB::commit();
        return response()->json($response);
    }

    public function destroy($bookingid) {
        $booking = Booking::where('id', '=', $bookingid)->where('hostel_id', '=', Auth::user()->currenthostel->id)->first();
        $start_date = $booking->nonTempStays()->min('start_date');
        $end_date = $booking->nonTempStays()->max('end_date');
        $booking->cancel(Input::get('reason'));
        $this->dispatch(new SyncAvailability($booking->hostel_id, $start_date, $end_date));
    }
}