<?php namespace App\Http\Controllers;

use App\HostelPro\Models\ReportHousekeeping;
use View;
use Carbon;
use Html;
use Input;
use Redirect;
use Auth;
use DB;
use PDF;
use Gate;
use Yajra\Datatables\Facades\Datatables;

use Illuminate\Http\Request;

use App\HostelPro\Models\Board;
use App\HostelPro\Models\DateHelper;
use App\HostelPro\Models\ReportDashboard;
use App\HostelPro\Models\ReportRevenueView;
use App\HostelPro\Models\ReportPaymentView;
use App\HostelPro\Models\ReportPNL;
use App\HostelPro\Models\Room;
use App\HostelPro\Models\RoomType;
use App\HostelPro\Models\Stay;
use App\HostelPro\Models\MoneyHelper;

use App\HostelPro\Models\DayViewReport;

use App\HostelPro\Models\Booking;

class ReportsController extends Controller {

    public function houseKeeping(Request $request) {

        $date = $request->input('date') ?? Carbon::now(Auth::user()->currenthostel->timezone->timezone)->format('d-m-Y');

        $report = new ReportHousekeeping(Auth::user()->currenthostel->id, DateHelper::convertDateToYearMonthDay($date));

        $dayreport = new DayViewReport(Auth::user()->currenthostel->id, DateHelper::convertDateToYearMonthDay($date));

        //dd($dayreport->totals['changing_rooms']);

        return View::make('pages.reports.housekeeping')->with('report', $report)->with('dayreport', $dayreport)->with('date', $date);
        return View::make('pdf.housekeeping')->with('report', $report);

        //$html = View::make('pdf.housekeeping')->with('rooms', $rooms);
        //return PDF::loadHTML($html)->setPaper('a4')->setOrientation('landscape')->setOption('margin-bottom', 0)->download('housekeeping.pdf');
    }


    public function revenue() {

        $this->authorize('advanced_reports');

        $start = Input::get('startdate');
        $end = Input::get('enddate');
        if (!empty($start) && !empty($end)) {
            $start = DateHelper::convertDateToYearMonthDay($start);
            $end = DateHelper::convertDateToYearMonthDay($end);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-t');
        }
        $hostel_id = Auth::user()->currenthostel->id;

        $report = new ReportRevenueView($hostel_id, $start, $end);

        $start = DateHelper::convertStringDateToCarbon($start)->format('d-m-Y');
        $end = DateHelper::convertStringDateToCarbon($end)->format('d-m-Y');

        return View::make('pages.reports.revenue')->with('report', $report)->with('startdate', $start)->with('enddate', $end);
    }

    public function dashboard() {

        $this->authorize('advanced_reports');

        $start = Input::get('startdate');
        $end = Input::get('enddate');
        if (!empty($start) && !empty($end)) {
            $start = DateHelper::convertDateToYearMonthDay($start);
            $end = DateHelper::convertDateToYearMonthDay($end);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $hostel_id = Auth::user()->currenthostel->id;

        $report = new ReportDashboard($hostel_id, $start, $end);

        $start = DateHelper::convertStringDateToCarbon($start)->format('d-m-Y');
        $end = DateHelper::convertStringDateToCarbon($end)->format('d-m-Y');

        //dd($report);

        return View::make('pages.reports.dashboard')->with('report', $report)->with('startdate', $start)->with('enddate', $end);
    }

    public function pnl(Request $request) {

        $this->authorize('advanced_reports');

        $hostel_id = Auth::user()->currenthostel->id;

        $report = new ReportPNL($hostel_id, $request->input('year') ?? date('Y'));

        $created_at = Auth::user()->currenthostel->created_at;
        $number_of_years = $created_at->diffInYears(Carbon::now());
        $created_year = (int) $created_at->format('Y');

        $years = array($request->input('year') ?? date('Y'));
        for ($i = 0; $i <= $number_of_years; $i++) {
            array_push($years, $created_year + $i);
        }
        $years = array_unique($years);

        return View::make('pages.reports.pnl')->with('report', $report)->with('years', $years);
    }
    
    public function m2m(Request $request) {
        $this->authorize('advanced_reports');

        $now = new \DateTime('now');

        $firstDayOfMonth = clone $now;
        $firstDayOfMonth->modify('first day of this month');

        $lastMonth = clone $now;
        $lastMonth->modify('-1 month');

        $firstDayOfLastMonth = clone $lastMonth;
        $firstDayOfLastMonth->modify('first day of this month');

        $lastYear = clone $now;
        $lastYear->modify('-1 year');

        $firstDayOfLastYearMonth = clone $lastYear;
        $firstDayOfLastYearMonth->modify('first day of this month');

        $hostel_id = Auth::user()->currenthostel->id;

        $reportThisMonth = new ReportPNL($hostel_id, $now->format('Y'), $firstDayOfMonth->format('Y-m-d'),
            $now->format('Y-m-d'));

        $reportLastMonth = new ReportPNL($hostel_id, $lastMonth->format('Y'), $firstDayOfLastMonth->format('Y-m-d'),
            $lastMonth->format('Y-m-d'));

        $reportLastYear = new ReportPNL($hostel_id, $lastYear->format('Y'), $firstDayOfLastYearMonth->format('Y-m-d'),
            $lastYear->format('Y-m-d'));


        $reportPrevMonth = $reportThisMonth->formM2MReportMonthly($reportThisMonth, $reportLastMonth);
        $reportPrevYear = $reportThisMonth->formM2MReportYearly($reportThisMonth, $reportLastYear);

        $monthData = array(
            'current' => date('m.Y', strtotime('now')),
            'prev' => date('m.Y', strtotime('last month')),
        );

        $yearData = array(
            'current' => date('m.Y', strtotime('now')),
            'prev' => date('m.Y', strtotime('last year')),
        );

        return View::make('pages.reports.m2m')
            ->with('reportPrevMonth', $reportPrevMonth)
            ->with('reportPrevYear', $reportPrevYear)
            ->with('monthData', $monthData)->with('yearData', $yearData);
    }

    public function paymentView() {

        $this->authorize('advanced_reports');

        $hostel_id = Auth::user()->currenthostel->id;

        $start = Input::get('startdate');

        $end = Input::get('enddate');

        if (empty($start) && empty($end)) {
            //Need to make these carbon so that it is timezone-dependant.
            $start = date('01-m-Y 00:00');
            $end = date('t-m-Y 23:59');
        }

        $report = new ReportPaymentView($hostel_id, DateHelper::convertDateToYearMonthDayMinutes($start), DateHelper::convertDateToYearMonthDayMinutes($end));

        return View::make('pages.reports.paymentview')->with('report', $report)->with('startdate', $start)->with('enddate', $end);
    }

    public function bookingList() {

        $this->authorize('advanced_reports');

        $start = (new Carbon('first day of this month'))->format('d-m-Y');
        $end = (new Carbon('last day of this month'))->format('d-m-Y');
        $channels = Booking::select('booking.myallocator_channel_id', 'myallocator_channel.name')
            ->where('hostel_id', '=', 13)
            ->join('myallocator_channel', 'myallocator_channel.id', '=', 'booking.myallocator_channel_id')
            ->groupBy('myallocator_channel_id')
            ->pluck('name', 'myallocator_channel_id')
            ->all();

        $statuses = array('Cancelled' => 'Cancelled', 'CheckedIn' => 'Checked In', 'CheckedOut' => 'Checked Out', 'NoShow' => 'No Show', 'Unpaid' => 'Unpaid');

        return View::make('pages.reports.bookinglist')->with('start', $start)->with('end', $end)->with('channels', $channels)->with('statuses', $statuses);
    }

    public function bookingListData(Request $request) {

        $this->authorize('advanced_reports');

        if ($request->has('startdate')) {
            $start = DateHelper::convertDateToYearMonthDay($request->input('startdate'));
        } else {
            $start = (new Carbon('first day of this month'))->toDateString();
        }

        if ($request->has('enddate')) {
            $end = DateHelper::convertDateToYearMonthDay($request->input('enddate'));
        } else {
            $end = (new Carbon('last day of this month'))->toDateString();
        }

        $bookings = Booking::selectRaw('booking.id,
        booking.label_type_id,
        min(stay.start_date) as date,
        booking.status,
        guest.lastname,
        guest.firstname,
        guest.email,
        guest.address_line_1,
        guestcountry.country_name AS guestcountry,
        guest.invoice_number,
        guest.passport_number,
        passportcountry.country_name AS passportcountry,
        guest.place_of_birth,
        guest.date_of_birth,
        guest.passport_issue_date,
        guest.passport_expiry_date,
        round(sum(stay.number_of_guests)) as number_of_guests,
        (SELECT DATEDIFF(max(stay.end_date), min(stay.start_date)) + 1 FROM stay where booking.id=stay.booking_id) AS nights,
        (SELECT sum(payment.total) FROM payment WHERE booking.id=payment.booking_id) AS payment_actual,
        COALESCE(commission.converted_amount,commission.original_amount) as commission_amount,
        commission.original_amount as commission_original_amount,
        currency.currency_code as commission_currency,
        booking.myallocator_id as myallocator_channel_id,
        myallocator_channel.name as myallocator_channel_name,
        booking.created_at,
        booking.updated_at')
            ->whereIn('status', ['Allocated', 'Cancelled', 'NoShow'])
            ->where('booking.hostel_id', '=', Auth::user()->currenthostel->id)
            ->where(function ($query) {
                $query->where('booking.label_type_id', '!=', '18')
                    ->orWhereNull('booking.label_type_id');
            })
            ->where('stay.temp', '=', false)
            ->having('date', '>=', $start)
            ->having('date', '<=', $end)
            ->join('stay', 'stay.booking_id', '=', 'booking.id')
            ->leftJoin('guest', 'booking.guest_id', '=', 'guest.id')
            ->leftJoin('commission', 'commission.booking_id', '=', 'booking.id')
            ->leftJoin('currency', 'currency.id', '=', 'commission.original_currency_id')
            ->leftJoin('myallocator_channel', 'myallocator_channel.id', '=', 'booking.myallocator_channel_id')
            ->leftJoin('country as guestcountry', 'guestcountry.id', '=', 'guest.country_id')
            ->leftJoin('country as passportcountry', 'passportcountry.id', '=', 'guest.passport_country_id')
            ->groupBy('booking.id')
            ->groupBy('commission.converted_amount')
            ->groupBy('commission.original_amount')
            ->groupBy('currency.currency_code');

        if ($request->has('channel')) {
            $bookings = $bookings->whereIn('myallocator_channel_id', $request->input('channel'));
        }

        if ($request->has('status')) {
            $bookings = $bookings->where(function ($query) use ($request) {
                if (in_array("Cancelled", $request->input('status'))) {
                    $query->orWhere('booking.status', '=', 'Cancelled');
                }
                if (in_array("CheckedIn", $request->input('status'))) {
                    $query->orWhereIn('booking.label_type_id', [9, 12])
                        ->where('booking.status', '=', 'Allocated');
                }
                if (in_array("CheckedOut", $request->input('status'))) {
                    $query->orWhereIn('booking.label_type_id', [15])
                        ->where('booking.status', '=', 'Allocated');
                }
                if (in_array("NoShow", $request->input('status'))) {
                    $query->orWhere('booking.status', '=', 'NoShow');
                }
                if (in_array("Unpaid", $request->input('status'))) {
                    $query->orWhere('booking.status', '=', 'Allocated')
                        ->where(function ($query) {
                            $query->whereIn('booking.label_type_id', [3, 6, 21])
                                ->orWhereNull('booking.label_type_id');
                        });
                }
            });
        }

        $datatables = Datatables::of($bookings);

        $datatables->editColumn('id', function ($booking) {
            return '<a class="loadBooking" data-bookingid="' . $booking->id . '">' . $booking->id . '</a>';
        });

        $datatables->editColumn('status', function ($booking) {
            if ($booking->status == 'Cancelled') {
                return '<span class="label bg-danger">Cancelled</span>';
            }

            if ($booking->status == 'NoShow') {
                return '<span class="label bg-danger">No Show</span>';
            }

            if ($booking->label_type_id == 15) {
                return '<span class="label booking-checked-out">Checked Out</span>';
            }

            if (($booking->label_type_id == 9) || ($booking->label_type_id == 12)) {
                return '<span class="label booking-paid">Checked In</span>';
            }

            return '<span class="label booking-unpaid">Unpaid</span>';
        });

        $datatables->editColumn('payment_actual', function ($booking) {
            if ($booking->payment_actual) {
                return MoneyHelper::convertCentsToDollars($booking->payment_actual);
            }
            return null;
        });

        $datatables->editColumn('commission_amount', function ($booking) {
            return MoneyHelper::convertCentsToDollars($booking->commission_original_amount) . " " . $booking->commission_currency;
        });

        $datatables->editColumn('created_at', function ($booking) {
            return $booking->created_at->diffForHumans();
        });

        $datatables->editColumn('myallocator_channel_id', function ($booking) {
            return $booking->myallocator_channel_name;
        });

        $datatables->editColumn('updated_at', function ($booking) {
            return $booking->updated_at->diffForHumans();
        });

        return $datatables->make(true);
    }


}