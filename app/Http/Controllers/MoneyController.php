<?php namespace App\Http\Controllers;

use Auth;
use View;
use Input;
use Redirect;
use App\HostelPro\Models\Money;

class MoneyController extends Controller {

    public function create() {
        $this->authorize('hostel_settings');
        $money = Money::where('hostel_id', '=', Auth::user()->currenthostel->id)->orderBy('amount', 'ASC')->get();
        return View::make('pages.money')->with('money', $money);
    }

    public function store() {
        $this->authorize('hostel_settings');
        $newMoney = new Money();
        $newMoney->hostel_id = Auth::user()->currenthostel->id;
        $newMoney->amount = Input::get('amount');
        $newMoney->save();
        $money = new \Illuminate\Database\Eloquent\Collection;
        $money->add($newMoney);
        return View::make('partials.money')->with('money', $money);

    }

    public function destroy($moneyId) {
        $this->authorize('hostel_settings');
        Money::where('id', '=', $moneyId)->where('hostel_id', '=', Auth::user()->currenthostel->id)->delete();
        $money = Money::where('hostel_id', '=', Auth::user()->currenthostel->id)->orderBy('amount', 'ASC')->get();
        return View::make('partials.money')->with('money', $money);
    }

}