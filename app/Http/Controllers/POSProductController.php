<?php namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use Input;
use Redirect;
use App\HostelPro\Models\POSProduct;
use App\HostelPro\Models\MoneyHelper;
use App\Http\Requests\POSProductRequest;

class POSProductController extends Controller {

    public function index() {
        $products = Auth::user()->currenthostel->posProducts;
        return View::make('partials.pos.settingsproducts')->with('products', $products);
    }

    public function store(POSProductRequest $request) {

        $this->authorize('hostel_settings');

        $product = new POSProduct();
        $product->name = Input::get('name');
        $product->category_id = Input::get('category_id');
        $product->price = MoneyHelper::convertToCents(Input::get('price'));
        $product->is_fixed_price = Input::get('is_fixed_price');
        $product->deposit = Input::get('deposit');
        $product->save();

        return "Success";
    }

    public function edit($id) {
        $this->authorize('hostel_settings');

        $product = Auth::user()->currenthostel->posProducts()->where('pos_products.id', '=', $id)->firstOrFail();

        return View::make('partials.pos.settingsproductedit')->with('product', $product);
    }

    public function update($id) {
        $this->authorize('hostel_settings');

        $allowed_category_ids = Auth::user()->currenthostel->posCategories->pluck('id')->all();

        $product = Auth::user()->currenthostel->posProducts()->where('pos_products.id', '=', $id)->firstOrFail();
        $product->name = Input::get('name');
        if (in_array(Input::get('category_id'),$allowed_category_ids)) {
            $product->category_id = Input::get('category_id');
        }
        $product->price = MoneyHelper::convertToCents(Input::get('price'));
        $product->is_fixed_price = Input::get('is_fixed_price');
        $product->deposit = Input::get('deposit');
        $product->save();

        return View::make('partials.pos.settingsproduct')->with('product', $product);
    }

    public function destroy($productId) {
        $this->authorize('hostel_settings');

        POSProduct::join('pos_categories', 'pos_products.category_id', '=', 'pos_categories.id')
            ->where('pos_products.id', '=', $productId)
            ->where('pos_categories.hostel_id', '=', Auth::user()->currenthostel->id)
            ->delete();
        $products = Auth::user()->currenthostel->posProducts;
        return View::make('partials.pos.settingsproducts')->with('products', $products);
    }

    public function sort() {
        $this->authorize('hostel_settings');

        $allowed_product_ids = Auth::user()->currenthostel->posProducts->pluck('id')->all();
        $product_ids = array_intersect(Input::get('product'), $allowed_product_ids);
        $product_ids_with_commas = implode(',', $product_ids);

        DB::beginTransaction();
        DB::statement('set @position = 0');
        DB::unprepared('UPDATE `pos_products` SET sort=@position:=@position+1 where `id` in (' . $product_ids_with_commas . ') order by FIELD(id, ' . $product_ids_with_commas . ')');
        DB::commit();
    }

}