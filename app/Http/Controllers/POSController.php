<?php namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use Input;
use Redirect;
use App\HostelPro\Models\Booking;
use App\HostelPro\Models\Hostel;
use App\HostelPro\Models\Payment;
use App\HostelPro\Models\POSCategory;
use App\HostelPro\Models\POSProduct;
use App\HostelPro\Models\AdditionalItem;
//use App\Http\Requests\POSDeleteItemRequest;

class POSController extends Controller {

    public function create() {
        $categories = Auth::user()->currenthostel->posCategories;
        $products = Auth::user()->currenthostel->posProducts;
        return View::make('pages.pos')->with('categories', $categories)->with('products', $products);
    }

    public function getProductsByCategoryId($categoryId) {
        if (!POSCategory::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('id', '=', $categoryId)->exists()) {
            return "Category not found.";
        }
        $category = POSCategory::find($categoryId);
        $products = POSProduct::where('category_id', '=', $categoryId)->orderBy('sort')->get();
        return View::make('partials.pos.products')->with('products', $products)->with('category', $category);
    }

    public function deleteItem($id) {
        $item = AdditionalItem::where('id', '=', $id)->where('hostel_id', '=', Auth::user()->currenthostel->id)->first();
        //sleep(5);

        if (!$item) {
            return response("The item you are trying to delete could not be found.", "422")->header('Content-Type', 'text-plain');
        }

        if (Auth::user()->roles->first()->name == "receptionist") {

            if ($item->user_id != Auth::user()->id) {
                return response("You cannot delete this item because you are not the person who added this item.", "422")->header('Content-Type', 'text-plain');
            }

            if ($item->created_at->diffInMinutes() > 10) {
                return response("You cannot delete this item because it is over 10 minutes old.", "422")->header('Content-Type', 'text-plain');
            }

        }

        $item->delete();

        return "Successfully deleted item.";
    }

    public function deletePayment($id) {
        $payment = Payment::where('id', '=', $id)->where('hostel_id', '=', Auth::user()->currenthostel->id)->first();

        if (!$payment) {
            return response("The item you are trying to delete could not be found.", "422")->header('Content-Type', 'text-plain');
        }

        if (Auth::user()->roles->first()->name == "receptionist") {

            if ($payment->user_id != Auth::user()->id) {
                return response("You cannot delete this item because you are not the person who added this item.", "422")->header('Content-Type', 'text-plain');
            }

            if ($payment->created_at->diffInMinutes() > 10) {
                return response("You cannot delete this item because it is over 10 minutes old.", "422")->header('Content-Type', 'text-plain');
            }

        }

        $payment->delete();

        return "Successfully deleted item.";
    }

}