<?php namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use Redirect;
use Input;
use App\HostelPro\Models\Guest;
use App\HostelPro\Services\PackagedResponseService;

class GuestController extends Controller {

    public function update($id) {
        $hostel_id = Auth::user()->currenthostel->id;

        DB::beginTransaction();
        $guest = Guest::where('id', '=', $id)->where('hostel_id', '=', $hostel_id)->first();
        $guest->update(Input::only('firstname', 'lastname', 'email', 'phone', 'country_id', 'city_id',
            'passport_number', 'passport_country_id', 'date_of_birth', 'place_of_birth', 'passport_issue_date', 'passport_expiry_date', 'invoice_number'));
        $guest->save();

        $calendar_options = json_decode(Input::get('calendar'), true);
        $response = new PackagedResponseService($hostel_id, Input::get('bookingid'), $calendar_options);
        DB::commit();
        return response()->json($response);
    }


}