<?php namespace App\Http\Controllers;

use App\HostelPro\Jobs\SyncAvailability;
use Auth;
use Carbon\Carbon;
use DB;
use Input;
use Redirect;
use View;

use App\HostelPro\Models\MyAllocatorSyncRooms;
use App\HostelPro\Models\MyAllocatorToken;

use App\HostelPro\Models\Allocation;

use App\HostelPro\Models\MyAllocatorLog;
use App\HostelPro\Models\MyAllocatorProcessor;
use App\HostelPro\Models\SmartAllocator;

use App\HostelPro\Models\Booking;

use App\HostelPro\Models\RoomType;
use App\HostelPro\Models\Guest;
use App\HostelPro\Models\MoneyHelper;
use App\HostelPro\Models\DateHelper;

use MyAllocatorApi\AssociatePropertyToPMS;
use MyAllocatorApi\AssociateUserToPMS;
use MyAllocatorApi\PropertyList;
use MyAllocatorApi\BookingList;

class MyAllocatorController extends Controller {

    public function create() {
        $this->authorize('hostel_settings');

        return View::make('pages.myallocator');
    }

    public function store() {
        $this->authorize('hostel_settings');

        $userId = Input::get('UserId');
        $userPassword = Input::get('UserPassword');
        $propertyId = Input::get('PropertyId');

        $api = new PropertyList($userId, $userPassword);
        $rsp = $api->callApi();

        if (isset($rsp['response']['body']['Errors'])) {
            return Redirect::route('settings.myallocator.create')->with('flash_message', 'Error: ' . $rsp['response']['body']['Errors'][0]['ErrorMsg'])->with('alert-class', 'alert-danger');
        }

        $properties = $rsp['response']['body']['Properties'];

        $propertyIdList = array_column($properties, 'id');

        if (count($propertyIdList) == 1) {
            $propertyId = $propertyIdList[0];
        }

        //If user has more than one property in MyAllocator, show them a list of properties and let them pick which one to link with
        if (!isset($propertyId)) {
            return View::make('pages.myallocator')->with('properties', $properties)->with('UserId', $userId)->with('UserPassword', $userPassword);
        }

        // If the property ID is an invalid property id (ie user might've changed it)
        if (!in_array($propertyId, $propertyIdList)) {
            return Redirect::route('settings.myallocator.create')->with('flash_message', 'Error: The property ID you gave is not valid')->with('alert-class', 'alert-danger');
        }

        $hostel_id = Auth::user()->currenthostel->id;

        $api = new AssociateUserToPMS($userId, $userPassword);
        $rsp = $api->callApi();
        $user_token = $rsp['response']['body']['Auth/UserToken'];

        $myAllocatorTokenObject = new MyAllocatorToken();
        $myAllocatorTokenObject->SaveTokenDetails($hostel_id, $user_token, $propertyId);

        $api = new AssociatePropertyToPMS($hostel_id);
        $rsp = $api->callApi();
        $token = $rsp['response']['body']['Auth/PropertyToken'];

        $myAllocatorTokenObject->property_token = $token;
        $myAllocatorTokenObject->save();

        Auth::user()->currenthostel->roomtypes()->forceDelete();

        $syncRooms = new MyAllocatorSyncRooms($hostel_id);
        if (!$syncRooms->sync()) {
            return Redirect::route('settings.myallocator.create')->with('flash_message', 'Successfully linked, but could not synchronize rooms between HostelPro and MyAllocator')->with('alert-class', 'alert-info');
        }

        $start = date("Y-m-d");
        $end = DateHelper::addDays($start, 30);

        DB::beginTransaction();
        $log_ids = [];
        for ($i = 1; $i < 18; $i++) {
            $api = new BookingList($hostel_id);
            $params = array(
                'ArrivalStartDate' => $start,
                'ArrivalEndDate' => $end,
            );
            $rsp = $api->callApiWithParams($params);

            $bookings = json_decode($rsp['response']['body_raw'], true);

            $bookings2 = $bookings['Bookings'];
            foreach ($bookings2 as $booking) {
                $log = new MyAllocatorLog;
                $log->hostel_id = Auth::user()->currenthostel->id;
                $log->data = serialize($booking);
                $log->save();
                array_push($log_ids, $log->id);
            }
            $start = DateHelper::addDays($end, 1);
            $end = DateHelper::addDays($start, 30);
            $i++;
        }
        DB::commit();

        DB::beginTransaction();
        $processor = new MyAllocatorProcessor();
        $processor->addMyAllocatorLogId($log_ids);
        $processor->processAndSave();
        DB::commit();

        DB::beginTransaction();
        $bookings = Booking::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('status', '=', 'Unallocated')->get();
        if ($bookings->count() > 0) {
            $allocator = new SmartAllocator(Auth::user()->currenthostel->id);

            $allocator->addBookingArray($bookings->pluck('id')->all());

            $booking_ids_to_allocate = $allocator->processAndSave();
            Allocation::changeManyToAllocated($booking_ids_to_allocate);
        }
        DB::commit();

        dispatch(new SyncAvailability($hostel_id, Carbon::now()->toDateString(), Carbon::now()->addYear()->toDateString()));

        return Redirect::route('settings.myallocator.create')->with('flash_message', 'Successfully linked with MyAllocator')->with('alert-class', 'alert-success');
    }

    public function resync() {
        $hostel_id = Auth::user()->currenthostel->id;

        $syncRooms = new MyAllocatorSyncRooms($hostel_id);
        $syncRooms->sync();

        dispatch(new SyncAvailability($hostel_id, Carbon::now()->toDateString(), Carbon::now()->addYear()->toDateString()));

        return Redirect::route('settings.RoomsOverview')->with('flash_message', 'Successfully resynchronized rooms between HostelPro and MyAllocator')->with('alert-class', 'alert-success');
    }

}