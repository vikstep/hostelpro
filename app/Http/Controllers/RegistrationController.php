<?php namespace App\Http\Controllers;

use App\HostelPro\Models\ExpenseCategory;
use App\HostelPro\Models\Money;
use App\HostelPro\Models\PaymentType;
use View;
use Html;
use Input;
use Redirect;
use Auth;
use DB;
use App\Http\Requests\UserRegistrationRequest;
use App\HostelPro\Models\User;
use App\HostelPro\Models\Hostel;
use App\HostelPro\Models\Role;
use App\HostelPro\Models\RoomType;
use App\HostelPro\Models\Room;
use App\HostelPro\Models\Timezone;
use App\HostelPro\Models\Country;

class RegistrationController extends Controller {

	/*
	protected $registrationForm;

	function __construct(Registration $registrationForm) {
		$this->registrationForm = $registrationForm;
	}*/

	/**
	 * Show the form for creating a new resource.
	 * GET /registration/create
	 *
	 * @return Response
	 */
	public function create()
	{

		//$detect_country = Array();
//		$timezone_options = array();
//		if (isset($_SERVER["HTTP_CF_IPCOUNTRY"])) {
//			$detect_country = DB::table('country')->where('country_code', '=', $_SERVER["HTTP_CF_IPCOUNTRY"])->orderBy('country_name', 'asc')->pluck('country_name', 'id')->all();
//			$timezone_options = Timezone::getTimezonesbyCountryCode($_SERVER["HTTP_CF_IPCOUNTRY"]);
//		} else {
//			$detect_country = array('0' => 'Select Country');
//			//$timezone_options = $this->getTimezonesbyCountryCode('AR');
//		}

		$country_options = array('0' => 'Select Country') + DB::table('country')->orderBy('country_name', 'asc')->pluck('country_name', 'id')->all();

		return View::make('pages.register', array('country_options' => $country_options));
	}

	public function getTimezonesbyCountryID() {
		$countryid = Input::get('countryid');
		$countrycode = DB::table('country')->select('country_code')->find($countryid)->country_code;
		$timezones = $this->getTimezonesbyCountryCode($countrycode);
		//dd($timezones);
		foreach($timezones as $id => $timezone) {
			echo '<option value="' . $id . '">Timezone: ' . $timezone . '</option>';
		}
		//return $timezones;
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /registration
	 *
	 * @return Response
	 */
	public function store(UserRegistrationRequest $request)
	{
//		if (isset($_SERVER["HTTP_CF_IPCOUNTRY"])) {
//			//$country = DB::table('country')->select('id', 'default_currency_id')->where('country_code', '=', $_SERVER["HTTP_CF_IPCOUNTRY"])->first();
//			$country = Country::where('country_code', '=', $_SERVER["HTTP_CF_IPCOUNTRY"])->first();
//			$timezone = Timezone::getTimezonesbyCountryCode($_SERVER["HTTP_CF_IPCOUNTRY"]);
//			$timezone = array_keys($timezone)[0];
//		} else {
//			//No CF IP set, so default to Hungary & budapest timezone
//			$country = Country::where('country_code', '=', 'HU')->first();
//			$timezone = '295';
//		}

		$country = Country::find(Input::get('country'));
		$timezone = Timezone::where('country_code', '=', $country->country_code)->first();

		/* Create Hostel */
		$hostel = Hostel::create([
			'name' => Input::get('hostelname'),
			'country_id' => $country->id,
			'timezone_id' => $timezone->id,
			'currency_id' => $country->default_currency_id,
			'setup_steps_remaining' => 4]);

		if (!Auth::check()) {
			/* Create User */
			$user = User::create(Input::only('firstname', 'lastname', 'email'));

			$user->password = bcrypt(Input::get('password'));
			/* Set the current_hostel attribute so we know what hostel the user is currently looking at (useful later for multi-hostel users) */
			$user->current_hostel = $hostel->id;
			$user->save();

			/* Log the user in */
			Auth::login($user);

			/* Add the user we just created as an admin to the Hostel we just created */
			Auth::user()->assignRole('admin', $hostel->id);
		} else {
			Auth::user()->current_hostel = $hostel->id;
			Auth::user()->assignRole('admin', $hostel->id);
			Auth::user()->save();
		}

		/* Create an example room type and an example room */
		$roomtype = new RoomType(array('name' => '8 Bed Mixed', 'gender' => 'Mixed', 'type' => 'Dorm', 'number_of_guests' => '8'));
		$hostel->roomtypes()->save($roomtype);
		$room = new Room(array('name' => 'Upstairs Room'));
		$roomtype->rooms()->save($room);

		/* Generate another example room type and example room */
		$roomtype = new RoomType(array('name' => '6 Bed Female', 'gender' => 'Female', 'type' => 'Dorm', 'number_of_guests' => '6'));
		$hostel->roomtypes()->save($roomtype);
		$room = new Room(array('name' => 'Downstairs Room'));
		$roomtype->rooms()->save($room);

		$paymentType = new PaymentType(array('name' => 'Cash Payment', 'kept_in_til' => true));
		$hostel->paymentTypes()->save($paymentType);

		$paymentType = new PaymentType(array('name' => 'Bank Transfer', 'kept_in_til' => false));
		$hostel->paymentTypes()->save($paymentType);

		$paymentType = new PaymentType(array('name' => 'Credit Card', 'kept_in_til' => false));
		$hostel->paymentTypes()->save($paymentType);

		$request->session()->flash('show-intro', 'First time registering!');

		$currency_code = $hostel->currency->currency_code;
		if (array_key_exists($currency_code, $this->denominations)) {
			if (array_key_exists($currency_code, $this->denominations)) {
				$denominations = $this->denominations[$currency_code];
				foreach ($denominations as $denomination) {
					$money = new Money();
					$money->hostel_id = $hostel->id;
					$money->amount = $denomination;
					$money->save();
				}
			}
		}

		$expense_categories = ['Rent', 'Electricity', 'Water', 'Misc.', 'Staff Pay'];

		foreach ($expense_categories as $category) {
			$expense_category = new ExpenseCategory();
			$expense_category->name = $category;
			$expense_category->hostel_id = $hostel->id;
			$expense_category->save();
		}

		return Redirect::route('dashboard');

//		return Redirect::route('dashboard')->with('flash_message', 'Welcome! To get you started we have automatically generated some example rooms.<br><br>
//			We have set your country as: <strong>' . $hostel->country->country_name . '</strong><br>
//			We have set your timezone as: <strong>' . $hostel->timezone->timezone . '</strong><br>
//			We have set your currency as: <strong>' . $hostel->currency->currency_code . '</strong> (' . $hostel->currency->currency_name . ')<br><br>
//			 All of these options and settings can be deleted or changed from the settings menu.');
	}

	protected $denominations = [
		'USD' => [0.01, 0.05, 0.1, 0.25, 0.5, 1, 5, 10, 20, 50, 100],
		'EUR' => [0.01,0.02,0.05,0.1,0.2,0.5,1,2,5,10,20,50,100,200,500],
		'JPY' => [1,5,10,50,100,500,1000,2000,5000,10000],
		'GBP' => [0.01,0.02,0.05,0.1,0.2,0.5,1,2,5,20,100],
		'AUD' => [0.05,0.1,0.2,0.5,1,2,5,10,20,50,100],
		'CAD' => [0.05,0.1,0.25,0.5,1,2,5,10,20,50,100],
		'CHF' => [0.05,0.1,0.2,0.5,1,2,5,10,20,50,100,200,1000],
		'CNY' => [0.1,0.5,1,5,10,20,50,100],
		'SEK' => [1,2,5,10,20,50,100,200,500],
		'NZD' => [0.1,0.2,0.5,1,2,5,10,20,50,100],
		'MXN' => [0.1,0.2,0.5,1,2,5,10,20,50,100,200,500,1000],
		'SGD' => [0.01,0.05,0.1,0.2,0.5,1,2,5,10,50,100,1000],
		'HKD' => [0.1,0.2,0.5,1,2,5,10,20,50,100,150,500,1000],
		'NOK' => [1,5,10,20,50,100,200,500,1000],
		'KRW' => [10,50,100,500,1000,5000,10000,50000],
		'TRY' => [0.05,0.1,0.25,0.5,1,5,10,20,50,100,200],
		'RUB' => [0.01,0.05,0.1,0.25,0.5,1,2,5,10,50,100,500,1000,5000],
		'INR' => [0.5,1,2,5,10,20,50,100,500],
		'BRL' => [0.01,0.05,0.1,0.25,0.5,1,2,5,10,20,50,100],
		'ZAR' => [0.1,0.2,0.5,1,2,5,10,20,50,100,200],
		'DKK' => [0.5,1,2,5,10,20,50,100,200,500,1000],
		'BGN' => [0.01,0.02,0.05,0.1,0.2,0.5,1,2,5,10,20,50,100],
		'HRK' => [0.05,0.1,0.2,0.5,1,2,5,10,20,50,100,200,500],
		'CZK' => [1,2,5,10,20,50,100,200,500,1000,2000],
		'HUF' => [5,10,20,50,100,200,500,1000,2000,5000,10000,20000],
		'PLZ' => [0.01,0.02,0.05,0.1,0.2,0.5,1,2,5,10,20,50,100,200,500],
		'RON' => [0.05,0.1,0.5,1,5,10,50,100],
		'BAM' => [0.05,0.1,0.2,0.5,1,2,5,10,20,50,100,200],
		'RSD' => [1,2,5,10,20,50,100,200,500,1000,2000,5000],
		'ALL' => [5,10,20,50,100,200,500,1000,2000,5000],
		'MKD' => [1,2,5,10,50,100,200,500,1000,2000,5000],
		'MDL' => [0.01,0.05,0.1,0.25,0.5,1,5,10,20,50,100,200,500,1000],
		'UAH' => [0.01,0.02,0.05,0.1,0.25,0.5,1,2,5,10,20,50,100,200,500],
		'BYN' => [0.01,0.02,0.05,0.1,0.2,0.5,1,2,5,10,20,50,100],
		'ARS' => [0.05,0.1,0.25,0.5,1,2,5,10,20,50,100,200,500],
		'BOB' => [0.1,0.2,0.5,1,2,5,10,20,50,100,200],
		'CLP' => [1,5,10,50,100,500,1000,2000,5000,10000,20000],
		'COP' => [50,100,200,500,1000,2000,5000,10000,20000,50000,100000],
		'GYD' => [1,5,10,20,50,100,500,1000,5000],
		'PYG' => [50,100,500,1000,2000,5000,10000,20000,50000,100000],
		'PEN' => [0.5,0.1,0.2,0.5,1,2,5,10,20,50,100],
		'SRD' => [0.01,0.05,0.1,0.25,1,2.5,5,10,20,50,100],
		'UYU' => [1,2,5,10,20,50,100,200,500,1000,2000],
		'VEF' => [1,10,50,100,500,1000,2000,5000,10000,20000]
	];

}