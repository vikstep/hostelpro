<?php namespace App\Http\Controllers;

use Auth;
use View;
use Input;
use Redirect;
use App\HostelPro\Models\ExpenseCategory;

class ExpenseCategoryController extends Controller {

    public function create() {
        $this->authorize('hostel_settings');
        $expenseCategories = ExpenseCategory::where('hostel_id', '=', Auth::user()->currenthostel->id)->orderBy('name', 'ASC')->get();
        return View::make('pages.expensecategories')->with('expenseCategories', $expenseCategories);
    }

    public function store() {
        $this->authorize('hostel_settings');
        $newCategory = new ExpenseCategory();
        $newCategory->name = Input::get('categoryname');
        $newCategory->hostel_id = Auth::user()->currenthostel->id;
        $newCategory->save();

        $expenseCategories = new \Illuminate\Database\Eloquent\Collection;
        $expenseCategories->add($newCategory);
        return View::make('partials.expensecategory')->with('expenseCategories', $expenseCategories);
    }

    public function destroy($expenseCategoryId) {
        $this->authorize('hostel_settings');
        ExpenseCategory::where('id', '=', $expenseCategoryId)->where('hostel_id', '=', Auth::user()->currenthostel->id)->delete();
        $expenseCategories = ExpenseCategory::where('hostel_id', '=', Auth::user()->currenthostel->id)->orderBy('name', 'ASC')->get();
        return View::make('partials.expensecategory')->with('expenseCategories', $expenseCategories);
    }

}