<?php namespace App\Http\Controllers;

use Auth;
use View;
use Input;
use Redirect;

class SessionsController extends Controller {

	public function create() {
		if (Auth::check()) {
			return Redirect::route('calendar');
		}
		return View::make('pages.login');
	}

	public function store() {
		$input = Input::only('email', 'password');
		if (Auth::attempt($input)) {
			return Redirect::route('calendar');
		}
		return Redirect::back()->withInput()->with('flash_message', 'Incorrect email or password')->with('alert-class', 'alert-danger');
	}

	public function destroy() {
		Auth::logout();
		return Redirect::route('login')->with('flash_message', 'You have successfully logged out');
	}

	public function changeActiveHostel($hostel_id) {

		if (!Auth::check()) {
			return Redirect::route('login');
		}

		if (in_array($hostel_id, array_keys(Auth::user()->hostelArrayList()))) {
			Auth::user()->current_hostel = $hostel_id;
			Auth::user()->save();
		}
		return Redirect::route('calendar');
	}

}