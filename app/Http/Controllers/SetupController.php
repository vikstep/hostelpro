<?php namespace App\Http\Controllers;

use Auth;
use View;
use Redirect;

class SetupController extends Controller {

    public function currentStep() {
        $this->authorize('hostel_settings');

        $hostel = Auth::user()->currenthostel;

        if ($hostel->setup_steps_remaining == 4) {
            return Redirect::route('settings.myallocator.create');
        }

        if ($hostel->setup_steps_remaining == 3) {
            return Redirect::route('settings.RoomsOverview');
        }

        if ($hostel->setup_steps_remaining == 2) {
            return Redirect::route('settings.rates.index');
        }

        return Redirect::route('users.index');
    }

    public function nextStep() {
        $this->authorize('hostel_settings');

        $hostel = Auth::user()->currenthostel;

        $hostel->decrement('setup_steps_remaining');

        if ($hostel->setup_steps_remaining == 0) {
            return Redirect::route('calendar')
                ->with('flash_message', '<h4>Setup Complete!</h4><p>You have successfully setup your HostelProfessional account. Contact us any time using the chat window in the bottom right corner.</p>')
                ->with('alert-class', 'alert-success');
        }

        return Redirect::route('setup.currentstep');
    }

    public function previousStep() {
        $this->authorize('hostel_settings');

        $hostel = Auth::user()->currenthostel;

        $hostel->increment('setup_steps_remaining');

        return Redirect::route('setup.currentstep');
    }

}