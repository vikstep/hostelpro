<?php namespace App\Http\Controllers;

use App\HostelPro\Jobs\ScrapePricesJob;
use App\HostelPro\Models\CompetitorURL;
use App\HostelPro\Services\PriceScraperService;
use Auth;
use Illuminate\Http\Request;
use View;
use Redirect;

class CompetitorRatesController extends Controller {

    public function index()
    {
        $this->authorize('rates');


        return View::make('pages.competitorratessettings');
    }

    public function store(Request $request) {
        $this->authorize('rates');

        if (Auth::user()->currenthostel->competitorURLs()->count() >= 5) {
            return Redirect::route('settings.competitor.index')
                ->with('flash_message', 'You can only add five competitors to track.')
                ->with('alert-class', 'alert-danger');
        }

        if (!preg_match('/(http(s)?:\/\/).+/i',$request->input('url'))) {
            $url = parse_url('http://' . $request->input('url'));
        } else {
            $url = parse_url($request->input('url'));
        }

        if (preg_match('/\w?(booking.com)/i',$url['host'])) {
            $channel = 'boo';
        } else if (preg_match('/\w?(hostelworld.com)/i',$url['host'])) {
            $channel = 'hw2';
        } else {
            return Redirect::route('settings.competitor.index')
                ->with('flash_message', 'The URL you entered appears to be invalid. If you feel this is an error please contact support for further assistance.<br><br>Please note at the moment we only support Booking.com and HostelWorld URLs')
                ->with('alert-class', 'alert-danger');
        }

        $formatted_url = $url['scheme'] . "://" . $url['host'] . $url['path'];

        if (Auth::user()->currenthostel->competitorURLs()->where('url', '=', $formatted_url)->count() > 0) {
            return Redirect::route('settings.competitor.index')
                ->with('flash_message', 'You have already added the given URL.')
                ->with('alert-class', 'alert-danger');
        }

        $competitor_url = new CompetitorURL();
        $competitor_url->hostel_id = Auth::user()->currenthostel->id;
        $competitor_url->myallocator_channel_id = $channel;
        $competitor_url->url = $formatted_url;
        $competitor_url->save();

        //$test = new PriceScraperService(Auth::user()->currenthostel->id);
        dispatch(new ScrapePricesJob($competitor_url->hostel_id));

        return Redirect::route('settings.competitor.index')
            ->with('flash_message', 'Successfully added! Please allow up to 15 minutes for the initial pricing to be retrieved.')
            ->with('alert-class', 'alert-success');
    }

    public function destroy($id) {
        $this->authorize('rates');

        Auth::user()->currenthostel->competitorURLs()->where('id', '=', $id)->delete();

        return Redirect::route('settings.competitor.index');
    }

}