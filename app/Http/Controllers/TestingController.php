<?php namespace App\Http\Controllers;

use App\HostelPro\Models\Allocation;
use App\HostelPro\Models\Board;
use App\HostelPro\Models\Booking;
use App\HostelPro\Models\DateHelper;
use App\HostelPro\Models\MyAllocatorLog;
use App\HostelPro\Models\MyAllocatorProcessor;
use App\HostelPro\Models\SmartAllocator;
use App\HostelPro\Models\Stay;
use Auth;
use Carbon\Carbon;
use MyAllocatorApi\BookingList;
use View;
use Redirect;
use DB;

class TestingController extends Controller
{

    public function testBookings()
    {
        $upcoming_stay_ids = Board::where('date', '>=', Carbon::now()->subWeek()->toDateString())
            ->groupBy('stay_id')
            ->get();

        //dd($upcoming_stay_ids->pluck('stay_id')->all());

        $stays = Stay::whereIn('id', $upcoming_stay_ids->pluck('stay_id')->all())->get();

        foreach ($stays as $stay) {
            $expected = $stay->number_of_guests * DateHelper::countNumberOfDays($stay->start_date, $stay->end_date);
            $count = Board::where('stay_id', '=', $stay->id)->count();
            if ($expected != $count) {
                //dd($stay);
                echo "Expected: " . $expected . " , got: " . $count . " for stay ID: " . $stay->id . "<br><br>";
            }
        }

        return "DONE";
        //dd($stays);
    }

    public function testPrivateBookingsImport()
    {
        $hostel_id = 45;
        $start = date("Y-m-d");
        $end = DateHelper::addDays($start, 30);

        /*
        DB::beginTransaction();
        $log_ids = [];
        for ($i = 1; $i < 18; $i++) {
            $api = new BookingList($hostel_id);
            $params = array(
                'ArrivalStartDate' => $start,
                'ArrivalEndDate' => $end,
            );
            $rsp = $api->callApiWithParams($params);

            $bookings = json_decode($rsp['response']['body_raw'], true);

            $bookings2 = $bookings['Bookings'];
            foreach ($bookings2 as $booking) {
                $log = new MyAllocatorLog;
                $log->hostel_id = $hostel_id;
                $log->data = serialize($booking);
                $log->save();
                array_push($log_ids, $log->id);
            }
            $start = DateHelper::addDays($end, 1);
            $end = DateHelper::addDays($start, 30);
            $i++;
        }
        DB::commit();*/

        $log_ids = [
            0 => 36643,
  1 => 36644,
  2 => 36645,
  3 => 36646,
  4 => 36647,
  5 => 36648,
  6 => 36649,
  7 => 36650,
  8 => 36651,
  9 => 36652,
  10 => 36653,
  11 => 36654,
  12 => 36655,
  13 => 36656,
  14 => 36657,
  15 => 36658,
  16 => 36659,
  17 => 36660,
  18 => 36661,
  19 => 36662,
  20 => 36663,
  21 => 36664,
  22 => 36665,
  23 => 36666,
  24 => 36667,
  25 => 36668,
  26 => 36669,
  27 => 36670,
  28 => 36671,
  29 => 36672,
  30 => 36673,
  31 => 36674,
  32 => 36675,
  33 => 36676,
  34 => 36677,
  35 => 36678,
  36 => 36679,
  37 => 36680,
  38 => 36681,
  39 => 36682,
  40 => 36683,
  41 => 36684,
  42 => 36685,
  43 => 36686,
  44 => 36687,
  45 => 36688,
  46 => 36689,
  47 => 36690,
  48 => 36691,
  49 => 36692,
  50 => 36693,
  51 => 36694,
  52 => 36695,
  53 => 36696,
  54 => 36697
        ];

        //dd($log_ids);

        DB::beginTransaction();
        $processor = new MyAllocatorProcessor();
        $processor->addMyAllocatorLogId($log_ids);
        $processor->processAndSave();
        DB::commit();

        DB::beginTransaction();
        $bookings = Booking::where('hostel_id', '=', $hostel_id)->where('status', '=', 'Unallocated')->get();

        if ($bookings->count() > 0) {
            $allocator = new SmartAllocator($hostel_id);

            $allocator->addBookingArray($bookings->pluck('id')->all());

            $booking_ids_to_allocate = $allocator->processAndSave();
            Allocation::changeManyToAllocated($booking_ids_to_allocate);
        }
        DB::commit();


        return "DONE";
    }

    public function testPrivateBookingsImport2()
    {

        $bookings2 = Booking::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('created_at', '<', '2016-10-10')->where('status', '=', 'Unallocated')->delete();

        $log_ids = ['35644', '35635', '35659', '35650', '35641'];

        MyAllocatorLog::whereIn('id', $log_ids)->update(['hostel_id' => 13]);

        DB::beginTransaction();
        $processor = new MyAllocatorProcessor();
        $processor->addMyAllocatorLogId($log_ids);
        $processor->enableOverride();
        $processor->processAndSave();
        DB::commit();


        DB::beginTransaction();
        $bookings = Booking::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('status', '=', 'Unallocated')->get();
        if ($bookings->count() > 0) {
            $allocator = new SmartAllocator(Auth::user()->currenthostel->id);

            $allocator->addBookingArray($bookings->pluck('id')->all());

            $booking_ids_to_allocate = $allocator->processAndSave();
            Allocation::changeManyToAllocated($booking_ids_to_allocate);
        }
        DB::commit();

        return "DONE";

    }


}