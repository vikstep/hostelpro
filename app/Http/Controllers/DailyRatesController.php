<?php 
namespace App\Http\Controllers;

use Auth;
use App\HostelPro\Models\Rate;
use App\HostelPro\Models\Board;
use App\HostelPro\Models\RoomType;
use App\HostelPro\Models\DailyRate;
use App\HostelPro\Models\MoneyHelper;
use Illuminate\Http\Request;


class DailyRatesController extends Controller {

    public function index(){
    }

    public function store(Request $request){        
        $rates = $request->get('rates');

        $response = array();

        $hostelId = Auth::user()->currenthostel->id;
        $roomTypesIds = RoomType::where('hostel_id', '=', $hostelId)->pluck('id')->all();

        $startDate = new \DateTime('now');
        $endDate = new \DateTime('now');

        $count = count($rates);

        $roomTypesIdsForUpdate = array();

        if($count > 0)
        {
            foreach ($rates as $rate)
            {
                $id = $rate[0];
                $date = $rate[1];
                $price = MoneyHelper::convertToCents($rate[2]);

                //check do we update current hostel data
                if(in_array($id, $roomTypesIds))
                {
                    $rateFirst = Rate::where('room_type_id', $id)
                        ->where('date', $date);

                    if(!$rateFirst->first())
                    {
                        $rateFirst = new Rate();
                        $rateFirst->room_type_id = $id;
                        $rateFirst->date = $date;
                        $rateFirst->price = $price;
                        $rateFirst->save();
                    }
                    else
                    {
                        $rateFirst
                            ->update(
                                [
                                    'price' => $price,
                                ]
                            );
                    }

                    $dailyRate = DailyRate::where('room_type_id', '=', $id)
                        ->where('date', '=', $date)
                        ->first();

                    if(!$dailyRate)
                    {
                        $dailyRate = new DailyRate();
                        $dailyRate->room_type_id = $id;
                        $dailyRate->date = $date;
                    }

                    $dailyRate->price = $price;

                    $dailyRate->save();

                    $currentDateTime = new \DateTime($date);

                    if($currentDateTime < $startDate)
                    {
                        $startDate = $currentDateTime;
                    }

                    if($currentDateTime > $endDate)
                    {
                        $endDate = $currentDateTime;
                    }

                    $roomTypesIdsForUpdate[] = $id;
                }
            }
            try
            {
                DailyRate::updateRates($hostelId, $startDate, $endDate, $roomTypesIdsForUpdate);
            }
            catch(\Exception $e)
            {
                $response['message'] = $e->getMessage();
                return response()->json($response);
            }
        }
        else
        {
            $count = 0;
        }

        $response['message'] = $count . ' items updated Successfully';

        return response()->json($response);
    }

    public function get(Request $request)
    {
        $startDate = $request->get('startDate');

        $roomtypes = Auth::user()->currenthostel->roomtypes;

        $items = array();
        $roomTypes = array();

        foreach ($roomtypes as $roomtype)
        {
            $date = new \DateTime($startDate);

            $items[$roomtype->id] = array();
            $roomTypes[$roomtype->name] = $roomtype->id;

            $countRooms = count($roomtype->rooms);
            $numberOfGuests = $roomtype->number_of_guests;
            $max_possible_bed_nights = $countRooms * $numberOfGuests;

            $room_ids = $roomtype->rooms->pluck('id')->all();

            //calculate for next 7 days
            for($i = 0; $i < 7; $i++)
            {
                $formattedDate = $date->format('Y-m-d');

                $rate =
                    DailyRate::where('date', $formattedDate)->whereIn('room_type_id', array($roomtype->id))->first();

                $total_bed_nights =
                    Board::whereIn('date', array($formattedDate))->whereIn('room_id', $room_ids)->count();

                $price = $rate ? MoneyHelper::convertCentsToDollars($rate->price) : 0;
                $percent = round(($total_bed_nights / $max_possible_bed_nights)*100,2);
                $color = Rate::getOccupancyColor($percent);

                $items[$roomtype->id][$i] = array(
                    'percent' => $percent,
                    'available' => $max_possible_bed_nights - $total_bed_nights,
                    'rate' => $price,
                    'color' => $color,
                    'date' => $formattedDate
                );

                $date->modify('+1 day');
            }
        }


        $response['items'] = $items;

        return response()->json($response);
    }

}