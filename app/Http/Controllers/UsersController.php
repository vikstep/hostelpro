<?php namespace App\Http\Controllers;

use App\Providers\PasswordResetServiceProvider;
use Auth;
use Illuminate\Auth\Passwords\DatabaseTokenRepository;
use Illuminate\Auth\Passwords\PasswordBroker;
use View;
use Input;
use Redirect;
use DB;
use Carbon\Carbon;

use App\HostelPro\Models\Role;
use App\HostelPro\Models\User;
use App\HostelPro\Emails\NewUserAddedToHostelEmail;

class UsersController extends Controller {

    public function index() {
        $this->authorize('user_management');

        $users = Auth::user()->currenthostel->users;

        if (Auth::user()->roles->first()->name == "admin") {
            $roles = Role::pluck('name', 'id')->all();
        } else {
            $roles = Role::where('id', '>', Auth::user()->roles->first()->id)->pluck('name', 'id')->all();
        }

        return View::make('pages.users')->with('users', $users)->with('roles', $roles);
    }

    public function show($user_id) {
        $this->authorize('user_management');
        $user = User::find($user_id);

        return View::make('pages.userpermissions')->with('user', $user);
    }

    public function store() {
        $this->authorize('user_management');

        //If user is manager they can only add receptionist
        if (Auth::user()->roles->first()->name == "admin") {
            $role_id = Input::get('role_id');
        } else {
            $role_id = 11;
        }
        $role = Role::find($role_id);

        //If the given email already exists, add them to this account and send them an email
        $existing_user = User::where('email', '=', Input::get('email'))->withTrashed()->first();
        if ($existing_user) {
            if ($existing_user->trashed()) {
                $existing_user->restore();
            }
            $existing_user->assignRole($role->name, Auth::user()->currenthostel->id);
            $existing_user->current_hostel = Auth::user()->currenthostel->id;
            $existing_user->save();
            /* NOTE: Should send an email to them here telling them they've been added */
        } else {
            //If the given email doesn't exist, add them, send them a welcome email to set their password.
            $user = new User();
            $user->firstname = Input::get('firstname');
            $user->lastname = Input::get('lastname');
            $user->email = trim(Input::get('email'));
            $user->password = bcrypt('YB9F43NfbFkgFMbX');
            $user->current_hostel = Auth::user()->currenthostel->id;
            $user->save();

            $user->assignRole($role->name, Auth::user()->currenthostel->id);

            $token = app('auth.password.broker')->createToken($user);
            DB::table('password_resets')->insert(['email' => $user->email, 'token' => $token, 'created_at' => Carbon::now()]);

            $email = new NewUserAddedToHostelEmail();
            $email->withData(['hostelname' => Auth::user()->currenthostel->name, 'url' => url('password/reset/' . $token . '?email=' . $user->email)])
                ->sendTo($user);
        }

        return redirect()->route('users.index');
    }

    public function destroy($id) {
        $this->authorize('user_management');

        $user = Auth::user()->currenthostel->users()->find($id);

        if (!$user) {
            return Redirect::route('users.index')->with('flash_message', 'User not found.')->with('alert-class', 'alert-danger');
        }

        if (!Auth::user()->canDeleteUser($user)) {
            return Redirect::route('users.index')->with('flash_message', 'You do not have permission to delete this user.')->with('alert-class', 'alert-danger');
        }

        $user->roles(Auth::user()->currenthostel->id)->sync([]);

        if ($user->hostels->count() == 0) {
            $user->current_hostel = null;
            $user->save();
            $user->delete();
        } else {
            $user->current_hostel = $user->hostels->first()->pivot->hostel_id;
            $user->save();
        }

        return Redirect::route('users.index')->with('flash_message', 'Successfully deleted user.')->with('alert-class', 'alert-success');
    }

}