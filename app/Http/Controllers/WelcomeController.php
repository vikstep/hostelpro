<?php namespace App\Http\Controllers;

use App\HostelPro\Models\User;
use App\Notifications\LandingContactFormNotification;
use Illuminate\Http\Request;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('landing.landing');
	}

	public function sendContactForm(Request $request) {
		$admin = User::find(3);
		$admin->notify(new LandingContactFormNotification($request));
	}

}
