<?php namespace App\Http\Controllers;

use App\HostelPro\Models\RoomType;
use App\HostelPro\Models\SeasonRate;
use App\HostelPro\Models\MoneyHelper;
use Auth;
use Illuminate\Http\Request;
use View;
use Redirect;
use App\HostelPro\Models\DiscountRate;

class DiscountRatesController extends Controller {

    public function save(Request $request) {
        $response = array();

        $discountRateId = $request->get('discountRateId');

        $seasonId = $request->get('seasonId');
        $seasonRate = SeasonRate::find($seasonId);

        $roomTypeId = $request->get('rooms');
        $roomType = RoomType::find($roomTypeId);

        $currenthostel = Auth::user()->currenthostel;

        //check is data for current hostel
        if($roomType->hostel_id == $currenthostel->id &&
            $seasonRate->hostel_id == $currenthostel->id)
        {
            $earlybird_discount = $request->get('earlyBirdPercent') ? (float)$request->get('earlyBirdPercent') : 0.0 ;
            $earlybird_days = $request->get('earlyBirdDays') ? (int)$request->get('earlyBirdDays') : 0;
            $lastminute_discount = $request->get('lastMinutePercent') ? (float)$request->get('lastMinutePercent') : 0.0;
            $lastminute_days = $request->get('lastMinuteHours') ? (int)$request->get('lastMinuteHours') : 0;
            $progressive_low_price = $request->get('progressiveLowPrice') ?
                MoneyHelper::convertToCents($request->get('progressiveLowPrice')) : 0.0;
            $progressive_high_price = $request->get('progressiveHighPrice') ?
                MoneyHelper::convertToCents($request->get('progressiveHighPrice')) : 0.0;

            //update
            if($discountRateId)
            {
                DiscountRate::where('id', $discountRateId)
                    ->where('roomtype_id', $roomTypeId)
                    ->where('season_rates', $seasonId)
                    ->update(
                        [
                            'earlybird_discount' => $earlybird_discount,
                            'earlybird_days' => $earlybird_days,
                            'lastminute_discount' => $lastminute_discount,
                            'lastminute_days' => $lastminute_days,
                            'progressive_low_price' => $progressive_low_price,
                            'progressive_high_price' => $progressive_high_price,
                        ]
                    );

                $response['message'] = 'Updated Successfully';
            }
            //insert
            else
            {
                $discountRate = new DiscountRate;
                $discountRate->roomtype_id = $roomTypeId;
                $discountRate->season_rates = $seasonId;
                $discountRate->earlybird_discount = $earlybird_discount;
                $discountRate->earlybird_days = $earlybird_days;
                $discountRate->lastminute_discount = $lastminute_discount;
                $discountRate->lastminute_days = $lastminute_days;
                $discountRate->progressive_low_price = $progressive_low_price;
                $discountRate->progressive_high_price = $progressive_high_price;

                $discountRate->save();

                $response['message'] = 'Created Successfully';
                $response['id'] = $discountRate->id;
            }

            try
            {
                DiscountRate::updateDiscountRates($seasonId, $roomTypeId);
            }
            catch (\Exception $e)
            {
                $response['message'] .= '. '.$e->getMessage();
            }
        }

        return response()->json($response);
    }

    public function find(Request $request) {
        $response = array();

        $roomTypeId = $request->get('rooms');
        $seasonId = $request->get('seasonId');

        $discountRate = DiscountRate::where('roomtype_id', $roomTypeId)
            ->where('season_rates', $seasonId)->first();

        if($discountRate)
        {
            if($discountRate->progressive_low_price)
            {
                $discountRate->progressive_low_price =
                    MoneyHelper::convertCentsToDollars($discountRate->progressive_low_price);
            }

            if($discountRate->progressive_high_price)
            {
                $discountRate->progressive_high_price =
                    MoneyHelper::convertCentsToDollars($discountRate->progressive_high_price);
            }

            $response['discountRate'] = $discountRate->toJson();
        }

        return response()->json($response);
    }

}