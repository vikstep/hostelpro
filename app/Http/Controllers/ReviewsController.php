<?php namespace App\Http\Controllers;

use App\HostelPro\Jobs\ScrapeReviewsJob;
use App\HostelPro\Services\ReviewScraperService;
use App\HostelPro\Models\ReviewURL;
use Auth;
use Carbon;
use Redirect;
use View;

use Illuminate\Http\Request;

class ReviewsController extends Controller {

    public function index() {

        //$scraper = new ReviewScraperService(Auth::user()->currenthostel->id);

        $reviews = Auth::user()->currenthostel->reviews()->paginate(50);

        return View::make('pages.reports.reviews')->with('reviews', $reviews);
    }

    public function create() {
        return View::make('pages.reviews');
    }

    public function store(Request $request) {

        if (!preg_match('/(http(s)?:\/\/).+/i',$request->input('url'))) {
            $url = parse_url('http://' . $request->input('url'));
        } else {
            $url = parse_url($request->input('url'));
        }

        if (preg_match('/\w?(booking.com)/i',$url['host'])) {
            $channel = 'boo';
        } else if (preg_match('/\w?(hostelworld.com)/i',$url['host'])) {
            $channel = 'hw2';
        } else if (preg_match('/\w?(hostelbookers.com)/i',$url['host'])) {
            $channel = 'hb2';
        } else {
            return Redirect::route('reports.reviews.index')
                ->with('flash_message', 'The URL you entered appears to be invalid. If you feel this is an error please contact support for further assistance.<br><br>Please note at the moment we only support Booking.com, HostelWorld and HostelBookers URLs')
                ->with('alert-class', 'alert-danger');
        }

        ReviewURL::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('myallocator_channel_id', '=', $channel)->delete();

        $review_url = new ReviewURL();
        $review_url->hostel_id = Auth::user()->currenthostel->id;
        $review_url->myallocator_channel_id = $channel;
        $review_url->url = $url['scheme'] . "://" . $url['host'] . $url['path'];
        $review_url->save();

        dispatch(new ScrapeReviewsJob($review_url->hostel_id));

        return Redirect::route('reports.reviews.create')
            ->with('flash_message', 'Successfully added! Please allow up to 15 minutes for all reviews to be retrieved.<br><br>HostelPro will from now on automatically check for new reviews each day.')
            ->with('alert-class', 'alert-success');
    }

}