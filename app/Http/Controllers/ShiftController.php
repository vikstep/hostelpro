<?php namespace App\Http\Controllers;

use Auth;
use View;
use Input;
use Redirect;
use App\HostelPro\Models\Money;
use App\HostelPro\Models\ShiftReport;
use App\HostelPro\Models\ShiftReportData;
use DB;

use App\HostelPro\Models\MoneyHelper;

class ShiftController extends Controller {

    public function create() {
        $money = Money::where('hostel_id', '=', Auth::user()->currenthostel->id)->orderBy('amount', 'ASC')->get();
        $report = ShiftReport::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('user_id', '=', Auth::user()->id)->where('end_time', '=', '0000-00-00 00:00:00')->first();
        return View::make('pages.shiftreportcreate')->with('money', $money)->with('report', $report);
    }

    public function index() {
        $this->authorize('advanced_reports');
        $reports = ShiftReport::where('hostel_id', '=', Auth::user()->currenthostel->id)
            ->with('user')
            ->orderBy(DB::raw('(end_time = \'0000-00-00 00:00:00\')'), 'DESC')
            ->orderBy('end_time', 'DESC')
            ->get();
        return View::make('pages.shiftreportindex')->with('reports', $reports);
    }

    public function show($shiftreportid) {
        $money = Money::where('hostel_id', '=', Auth::user()->currenthostel->id)->orderBy('amount', 'ASC')->get();
        $report = ShiftReport::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('id', '=', $shiftreportid)->first();

        #dd($report->deposits);

        return View::make('pages.shiftreportcreate')->with('report', $report)->with('money', $money);
    }

    public function store() {
        $shiftreportid = Input::get('shiftreportid');

        if (empty($shiftreportid)) {
            $unfinished_report = ShiftReport::where('hostel_id', '=', Auth::user()->currenthostel->id)
                ->where('user_id', '=', Auth::user()->id)
                ->whereNull('end_time')
                ->first();

            if ($unfinished_report) {
                return Redirect::route('reports.shift.create')->with('flash_message', 'You already have an open shift report. Please try again now.')->with('alert-class', 'alert-danger');
            }

            $report = new ShiftReport();
            $report->hostel_id = Auth::user()->currenthostel->id;
            $report->user_id = Auth::user()->id;
            $report->start_time = date("Y-m-d H:i:s");
            $report->save();
        } else {
            $report = ShiftReport::where('hostel_id', '=', Auth::user()->currenthostel->id)
                ->where('user_id', '=', Auth::user()->id)
                ->where('id', '=', $shiftreportid)
                ->whereNull('end_time')
                ->first();
            if (!$report) {
                return Redirect::route('reports.shift.create')->with('flash_message', 'The shift report has already been closed. Please try again now.')->with('alert-class', 'alert-danger');
            }
            $report->end_time = date("Y-m-d H:i:s");
            $report->save();
        }

        $items = Input::get('items');

        foreach ($items as $denomination => $quantity) {
            $data = new ShiftReportData();
            $data->shift_report_id = $report->id;
            $data->amount_of_money = $denomination;
            $data->quantity = $quantity;
            if (empty($shiftreportid)) {
                $data->is_start_shift = true;
            } else {
                $data->is_start_shift = false;
            }
            $data->save();
        }

        return Redirect::route('reports.shift.create')->with('flash_message', 'Successfully saved shift report.')->with('alert-class', 'alert-success');

    }

}