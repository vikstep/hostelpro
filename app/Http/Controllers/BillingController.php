<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use View;
use Redirect;
use Paymill;

class BillingController extends Controller
{

    public function create()
    {
        $this->authorize('hostel_settings');

        $offer = Auth::user()->currenthostel->getPaymillOffer();

        return View::make('pages.billing')->with('offer', $offer);
    }

    public function store(Request $request)
    {
        $this->authorize('hostel_settings');

        $paymill_private_api_key = '7df85ae5f403b76e849c70b9b13d693a';
        $paymill_request = new Paymill\Request($paymill_private_api_key);

        $payment = new \Paymill\Models\Request\Payment();
        $payment->setToken($request->input('paymill_token'));

        try {
            $paymill_request->create($payment);
            $response = $paymill_request->getLastResponse();
            //dd($response);

            $data = $response['body']['data'];

            $offer = Auth::user()->currenthostel->getPaymillOffer();
            $paymill_offer_id = $offer->paymill_id;

            $subscription = new Paymill\Models\Request\Subscription();
            $subscription->setClient($data['client']['id'])
                ->setOffer($paymill_offer_id)
                ->setName($offer->min_beds .'-' . $offer->max_beds . ' Beds (' . Auth::user()->currenthostel->name . ')')
                ->setPayment($data['client']['payment'][0]);

            $response = $paymill_request->create($subscription);

            dd($response);

        } catch (\Paymill\Services\PaymillException $e) {
            //Do something with the error informations below
            $e->getResponseCode();
            $e->getStatusCode();
            $e->getErrorMessage();
            $e->getRawError();
        }

    }

}