<?php namespace App\Http\Controllers;

use Auth;
use View;
use Redirect;
use Input;
use App\HostelPro\Models\Booking;

class NoteController extends Controller {

    public function index($bookingId) {
        $booking = Booking::where('id', '=', $bookingId)->where('hostel_id', '=', Auth::user()->currenthostel->id)->first();

        return View::make('partials.bookingnote')->with('booking', $booking);
    }

    public function store($bookingId) {
        $booking = Booking::where('id', '=', $bookingId)->where('hostel_id', '=', Auth::user()->currenthostel->id)->first();
        $text = Input::get('text');
        $booking->addNote($text);
        return View::make('partials.bookingnote')->with('booking', $booking);
    }


}