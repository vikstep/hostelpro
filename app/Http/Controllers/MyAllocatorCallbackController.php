<?php namespace App\Http\Controllers;

use Auth;
use DB;
use Input;
use Redirect;
use Request;
use View;
use MyAllocatorApi\LoopBookingCreate;
use MyAllocatorApi\MyAllocatorAuth;
use MyAllocatorApi\VendorSet;

use App\HostelPro\Models\Allocation;
use App\HostelPro\Models\Booking;
use App\HostelPro\Models\DateHelper;
use App\HostelPro\Models\Guest;
use App\HostelPro\Models\MoneyHelper;
use App\HostelPro\Models\MyAllocatorFormattedLog;
use App\HostelPro\Models\MyAllocatorLog;
use App\HostelPro\Models\MyAllocatorToken;
use App\HostelPro\Models\RoomType;
use App\HostelPro\Models\Hostel;
use App\HostelPro\Models\SmartAllocator;
use App\HostelPro\Models\MyAllocatorProcessor;

class MyAllocatorCallbackController extends Controller {

    public function setCallbackURL() {
        $url = "https://www.hostelprofessional.com/myallocator_callback";
        $auth = new MyAllocatorAuth();
        $api = new VendorSet();
        $api->setConfig('dataFormat', 'array');
        $api->setAuth($auth);
        $params = array(
            'Callback/URL' => $url,
            'Callback/Password' => 'tEyNR6cMF9gRGDZ7',
            'Callback/NotifyBooking' => true
        );
        try {
            $rsp = $api->callApiWithParams($params);
        } catch (\Exception $e) {
            //$rsp = 'Oops: '.$e->getMessage();
            return Redirect::route('settings.myallocator.create')->with('flash_message', 'Communication Error: ' . $e->getMessage())->with('alert-class', 'alert-danger');
        }
        dd($rsp);
    }

    public function setAvailability() {

        $roomid = RoomType::where('hostel_id', '=', Auth::user()->currenthostel->id)->first();
        $roomid = $roomid['myallocator_id'];
        $token = MyAllocatorToken::where('hostel_id', '=', Auth::user()->currenthostel->id)->first();

        $auth = new MyAllocatorAuth();
        $auth->userToken = $token['user_token'];
        $auth->propertyId = '5431';
        $api = new LoopBookingCreate();
        $api->setConfig('dataFormat', 'array');
        $api->setAuth($auth);

        $params = array(
            'Booking' => array(
                'StartDate' => "2015-08-25",
                'EndDate' => "2015-08-28",
            )
        );
        /*
        $api = new ARIUpdate();
        $api->setConfig('dataFormat', 'array');
        $api->setAuth($auth);
        $allocations = array(
            'RoomId' => $token['myallocator_id'],
            'StartDate' => '2015-03-22',
            'EndDate' => '2015-12-12',
            'Price' => '1750'
        );
        $params = array(
            'Channels' => array('all' => 'all'),
            'Allocations' => $allocations
        );*/

        try {
            $rsp = $api->callApiWithParams($params);
        } catch (\Exception $e) {
            //$rsp = 'Oops: '.$e->getMessage();
            return Redirect::route('settings.myallocator.create')->with('flash_message', 'Room Update Communication Error: ' . $e->getMessage())->with('alert-class', 'alert-danger');
        }

        dd($rsp);


        dd($token);
    }

    public function store() {

        $banned_property_ids = ['14488', 14488, '2835', 2835];

        DB::beginTransaction();
        $password = Input::get('password');
        if (strcasecmp($password, "tEyNR6cMF9gRGDZ7") != 0) {
            $errorArray = array("error" => array("code" => 10, "msg" => "Incorrect Password given."), "success" => false);
            return response()->json($errorArray);
        }

        $booking = json_decode(Input::get('booking'), true);
        $myallocator_id = $booking['MyallocatorId'];

        if (in_array($booking['PropertyId'], $banned_property_ids)) {
            //Some properties attempt to associate themselves to us incorrectly. In that case don't do anything but send a 200
            //response so that myallocator don't re-send the booking.
            return response()->json(['success' => true]);
        }

        $hostel_id = MyAllocatorToken::where('myallocator_id', '=', $booking['PropertyId'])->first();
        $hostel_id = $hostel_id['hostel_id'];

        $log = new MyAllocatorLog;
        $log->hostel_id = $hostel_id;
        $log->data = serialize($booking);
        $log->save();

        $processor = new MyAllocatorProcessor();
        $processor->addMyAllocatorLogId($log->id);
        $processor->processAndSave();

        DB::commit();

        $booking = Booking::where('myallocator_id', '=', $myallocator_id)->where('status', '=', 'Unallocated')->first();
        if ($booking) {
            DB::beginTransaction();
            $allocator = new SmartAllocator($hostel_id);
            $allocator->addBooking($booking->id);
            $booking_ids_to_allocate = $allocator->processAndSave();
            Allocation::changeManyToAllocated($booking_ids_to_allocate);
            DB::commit();
        }

        return response()->json(['success' => true]);
    }


}