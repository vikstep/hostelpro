<?php namespace App\Http\Controllers;

use Auth;
use DB;
use Exception;
use Input;
use Redirect;
use View;

use App\HostelPro\Models\AdditionalItem;
use App\HostelPro\Models\Allocation;
use App\HostelPro\Models\Hostel;
use App\HostelPro\Models\Booking;
use App\HostelPro\Models\Calendar;
use App\HostelPro\Models\DateHelper;
use App\HostelPro\Models\DayViewReport;
use App\HostelPro\Models\Guest;
use App\HostelPro\Models\LabelType;
use App\HostelPro\Models\MoneyHelper;
use App\HostelPro\Models\Payment;
use App\HostelPro\Models\POSProduct;
use App\HostelPro\Models\Stay;

class AjaxController extends Controller {

    public function calendar() {
        $start = Input::get('date');
        $start = DateHelper::convertDateToYearMonthDay($start);
        $viewmode = Input::get('viewmode');

        if ($viewmode == "day") {
            $report = new DayViewReport(Auth::user()->currenthostel->id, $start);
            return View::make('partials.dayview')->with('report', $report)->with('date', $start);
        }

        #$time_start = microtime(true);

        $days = Input::get('days');

        $tempBooking = Input::get('bookingid');
        $calendar = new Calendar(Auth::user()->currenthostel->id, $start, $days, $tempBooking);
        #echo 'Total execution time in seconds: ' . (microtime(true) - $time_start);
        if ($viewmode == "occupancy") {
            return View::make('partials.calendar')->with('calendar', $calendar);
        }

        //dd($calendar);

        $calendar->getAvailability();

        return View::make('partials.availabilitycalendar')->with('calendar', $calendar);
    }

    public function searchBookings() {
        $search = '+' . str_replace(' ', '* +', trim(Input::get('term'))) . '*';

        $guestsForward = Booking::select('booking.id', DB::raw('concat(COALESCE(stay.start_date,board_unallocated.date), "  ", lastname, ", ", firstname) AS text'))
            ->join('guest', 'booking.guest_id', '=', 'guest.id')
            ->leftJoin('stay', 'stay.booking_id', '=', 'booking.id')
            ->leftJoin('board_unallocated', 'board_unallocated.booking_id', '=', 'booking.id')
            ->where(function ($query) {
                $query->where('stay.start_date', '>=', DateHelper::getToday(Auth::user()->currenthostel->timezone->timezone))
                    ->orWhere('board_unallocated.date', '>=', DateHelper::getToday(Auth::user()->currenthostel->timezone->timezone));
            })
            ->where('booking.hostel_id', '=', Auth::user()->currenthostel->id)
            ->whereNotIn('booking.status', ['Cancelled','NoShow'])
            ->whereRaw("MATCH(firstname,lastname) AGAINST(? IN BOOLEAN MODE)", array($search))
            ->groupBy('booking.id')
            ->orderBy('stay.start_date', 'ASC')
            ->limit(6)
            ->get();

        /*
         * ->where(function ($query) {
                $query->whereNotNull('stay.id')
                    ->orWhereNotNull('board_unallocated.date');
            })
         */

        //At moment this gets all previous, should probably limit to just 2 weeks etc.

        $guestsBackward = Booking::select('booking.id', DB::raw('concat(COALESCE(stay.start_date,board_unallocated.date), "  ", lastname, ", ", firstname) AS text'))
            ->join('guest', 'booking.guest_id', '=', 'guest.id')
            ->leftJoin('stay', 'stay.booking_id', '=', 'booking.id')
            ->leftJoin('board_unallocated', 'board_unallocated.booking_id', '=', 'booking.id')
            ->where(function ($query) {
                $query->where('stay.start_date', '<', DateHelper::getToday(Auth::user()->currenthostel->timezone->timezone))
                    ->orWhere('board_unallocated.date', '<', DateHelper::getToday(Auth::user()->currenthostel->timezone->timezone));
            })
            ->where('booking.hostel_id', '=', Auth::user()->currenthostel->id)
            ->whereNotIn('booking.status', ['Cancelled','NoShow'])
            ->whereRaw("MATCH(firstname,lastname) AGAINST(? IN BOOLEAN MODE)", array($search))
            ->groupBy('booking.id')
            ->orderBy('stay.start_date', 'DESC')
            ->limit(6 - $guestsForward->count())
            ->get();

        return $guestsForward->merge($guestsBackward);
    }

    public function savePayment() {
        $paymentAmount = MoneyHelper::convertToCents(Input::get('payment'));
        $items = Input::get('items');
        $prices = Input::get('prices');

        if ((empty($items)) && (empty($paymentAmount))) {
            return "No payment data found.";
        }

        $booking = Booking::where('hostel_id', '=', Auth::user()->currenthostel->id)->where('id', '=', Input::get('bookingid'))->first();

        DB::beginTransaction();
        if ($prices) {

            foreach ($prices as $i => $price) {
                $prices[$i] = MoneyHelper::convertToCents($price);
            }

            foreach ($items as $item => $qty) {
                $newItem = new AdditionalItem();
                $product = POSProduct::find($item);
                if ($product) {
                    $newItem->name = $product->name;
                } else {
                    $newItem->name = "Accommodation";
                }
                $newItem->price = $prices[$item];
                $newItem->units = $qty;
                $newItem->total = $newItem->price * $newItem->units;
                if (($product) && ($product->deposit == true) && ($newItem->name != 'Booking Deposit')) {
                    $newItem->deposit = true;
                }
                $newItem->hostel_id = Auth::user()->currenthostel->id;
                $newItem->user_id = Auth::user()->id;
                if (!empty($booking)) {
                    $newItem->booking_id = $booking->id;
                }
                $newItem->currency_id = Auth::user()->currenthostel->currency->id;
                $newItem->save();

            }

        }

        if ($paymentAmount != 0) {

            if ($paymentAmount > $booking->totalAmountDue()) {
                $paymentAmount = $booking->totalAmountDue();
            }

            $payment = new Payment();
            $payment->name = Input::get('paymenttype');
            $payment->total = $paymentAmount;
            $payment->hostel_id = Auth::user()->currenthostel->id;
            $payment->user_id = Auth::user()->id;
            if (!empty($booking)) {
                $payment->booking_id = $booking->id;
            }
            $payment->currency_id = Auth::user()->currenthostel->currency->id;
            $payment->save();
        }

        DB::commit();

        return Redirect::route('dashboard')->with('flash_message', 'Payment Saved.')->with('alert-class', 'alert-success');

    }

    public function getNotifications() {
        return View::make('includes.notifications');
    }

    public function availabilityView() {
        dd("here");
    }

    public function index() {
        return Redirect::route('settings.rooms.index');
        //return View::make('pages.settings');
    }

    public function uptimeCheck() {
        try {
            $temp = DB::connection()->getDatabaseName();
            if ($temp) {
                $returnData = array(
                    'status' => 'ok',
                    'message' => 'Successfully connected to the DB.'
                );
                return response()->json($returnData, 200);
            }

        } catch (Exception $e) {
            $returnData = array(
                'status' => 'error',
                'message' => 'Could not connect to the DB. Reason: ' . $e->getMessage()
            );
            return response()->json($returnData, 500);
        }
    }
}