<?php namespace App\Http\Controllers;

use Auth;
use View;
use Redirect;

class SettingsController extends Controller {

    public function POSOverview() {
        $this->authorize('hostel_settings');
        $categories = Auth::user()->currenthostel->posCategories;
        $products = Auth::user()->currenthostel->posProducts;
        return View::make('pages.possettings')->with('categories', $categories)->with('products', $products);
    }

    public function index() {
        $this->authorize('hostel_settings');
        return Redirect::route('settings.RoomsOverview');
        //return View::make('pages.settings');
    }

    public function roomsOverview() {
        $this->authorize('hostel_settings');
        $roomtypes = Auth::user()->currenthostel->roomtypes;
        $rooms = Auth::user()->currenthostel->rooms;
        return View::make('pages.rooms')->with('roomtypes', $roomtypes)->with('rooms', $rooms);
    }

}