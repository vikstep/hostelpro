<?php namespace App\HostelPro\Services;

use App\HostelPro\Models\CompetitorPricing;
use App\HostelPro\Models\CompetitorURL;
use App\HostelPro\Models\Prices\BooPriceScraper;
use App\HostelPro\Models\Prices\HW2PriceScraper;
use Carbon\Carbon;

class PriceScraperService
{

    private $hostel_id;

    public function __construct(String $hostel_id)
    {
        $this->hostel_id = $hostel_id;

        $competitor_urls = CompetitorURL::where('hostel_id', '=', $hostel_id)->get();
        foreach ($competitor_urls as $competitor_url) {
            if ($competitor_url->last_crawled < Carbon::now()->toDateString()) {
                $this->crawl($competitor_url);
            }
        }

        return true;
    }

    private function crawl(CompetitorURL $competitor_url) {

        $results = [];

        if ($competitor_url->myallocator_channel_id == 'boo') {
            $scraper = new BooPriceScraper($competitor_url->url);

            if (!$competitor_url->name) {
                $competitor_url->name = $scraper->getNameOfHostel();
                $competitor_url->save();
            }

            $scraper->crawl();
            $results = $scraper->getNewPrices();
        }


        if ($competitor_url->myallocator_channel_id == 'hw2') {
            $scraper = new HW2PriceScraper($competitor_url->url);

            if (!$competitor_url->name) {
                $competitor_url->name = $scraper->getNameOfHostel();
                $competitor_url->save();
            }

            $scraper->crawl();
            $results = $scraper->getNewPrices();
        }

        if ($results) {
            foreach ($results as $index => $result) {
                $results[$index]['competitor_url_id'] = $competitor_url->id;
                $results[$index]['created_at'] = Carbon::now()->toDateTimeString();
                $results[$index]['updated_at'] = Carbon::now()->toDateTimeString();
            }

            CompetitorPricing::insert($results);
        }

        $competitor_url->last_crawled = Carbon::now()->toDateString();
        $competitor_url->save();
    }

}