<?php namespace App\HostelPro\Services;

use App\HostelPro\Models\Calendar;
use App\HostelPro\Models\DateHelper;
use App\HostelPro\Models\DayViewReport;
use View;

class CalendarResponseService
{

    private $hostel_id;
    private $start;
    private $days;
    private $viewmode;
    private $booking_id;

    public function __construct(String $hostel_id, String $date, String $days, String $viewmode, String $booking_id = null)
    {
        $this->hostel_id = $hostel_id;
        $this->start = DateHelper::convertDateToYearMonthDay($date);
        $this->days = $days;
        $this->viewmode = $viewmode;
        $this->booking_id = $booking_id;
    }

    public function render()
    {

        if ($this->viewmode == "day") {
            $report = new DayViewReport($this->hostel_id, $this->start);
            return View::make('partials.dayview')->with('report', $report)->with('date', $this->start)->render();
        }

        $calendar = new Calendar($this->hostel_id, $this->start, $this->days, $this->booking_id);

        if ($this->viewmode == "occupancy") {
            return View::make('partials.calendar')->with('calendar', $calendar)->render();
        }

        $calendar->getAvailability();

        return View::make('partials.availabilitycalendar')->with('calendar', $calendar)->render();
    }

}