<?php namespace App\HostelPro\Services;

use App\HostelPro\Models\Booking;
use App\HostelPro\Models\DayViewReport;
use App\HostelPro\Models\Email;
use App\HostelPro\Models\Hostel;
use App\HostelPro\Models\Stay;
use Carbon\Carbon;

class DailyEmailChecker
{

    public function checkAllHostels() {
        $emails = Email::all();

        foreach ($emails as $email) {

            if ($email->pre_email_days > 0) {
                $date = Carbon::now()->addDays($email->pre_email_days)->toDateString();
                $report = new DayViewReport($email->hostel_id, $date);

                foreach ($report->arriving as $arriving) {
                    $service = new SendEmailService($email, $arriving->id);
                    $service->email();
                }
            }

            if ($email->post_email_days > 0) {
                $date = Carbon::now()->addDays($email->post_email_days)->toDateString();
                $report = new DayViewReport($email->hostel_id, $date);

                foreach ($report->departing as $departing) {
                    $service = new SendEmailService($email, $departing->id, false);
                    $service->email();
                }
            }

        }
    }

}