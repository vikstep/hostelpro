<?php namespace App\HostelPro\Services;

use App\HostelPro\Models\Allocation;
use App\HostelPro\Models\Booking;
use App\HostelPro\Models\SmartAllocator;
use App\HostelPro\Models\Stay;
use View;

class BookingDetailsResponseService
{

    private $hostel_id;
    private $booking_id;

    public function __construct(String $hostel_id, String $booking_id)
    {
        $this->hostel_id = $hostel_id;
        $this->booking_id = $booking_id;
    }

    public function render() {
        $booking = Booking::where('id', '=', $this->booking_id)->where('hostel_id', '=', $this->hostel_id)->first();

        if (!$booking) {
            return "Booking Not Found";
        }

        if ($booking->status == 'Unallocated') {
            Stay::where('booking_id', '=', $this->booking_id)->delete();
            $allocator = new SmartAllocator($this->hostel_id);
            $allocator->addBooking($this->booking_id);
            $allocator->processAndSave();
        } else if ($booking->nonTempStays->count() > 0) {
            //Copy all the allocated stays to temp.
            Allocation::copyAllocatedStaysToTemp($this->booking_id);
        } //Otherwise we don't have to do anything as we probably just made this booking (and all it's stays are unallocated)


        $booking = Booking::where('hostel_id', '=', $this->hostel_id)
            ->where('booking.id', '=', $this->booking_id)
            ->with(['stays' => function ($q) {
                $q->where('stay.temp', '=', true);
            }])
            ->with('guest', 'notes', 'notes.user', 'myallocator_formatted_log', 'myallocator_formatted_log.roomtype', 'channel', 'hostel', 'hostel.roomtypes', 'hostel.roomtypes.rooms')
            ->first();

        return View::make('partials.bookingdetails')->with('booking', $booking)->render();
    }

}