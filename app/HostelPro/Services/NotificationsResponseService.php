<?php namespace App\HostelPro\Services;

use View;

class NotificationsResponseService
{

    public function render()
    {
        return View::make('includes.notifications')->render();
    }

}