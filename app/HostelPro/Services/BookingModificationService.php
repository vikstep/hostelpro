<?php namespace App\HostelPro\Services;

use App\HostelPro\Jobs\SyncAvailability;
use App\HostelPro\Models\ActionLog;
use App\HostelPro\Models\ActionLogType;
use App\HostelPro\Models\AdditionalItem;
use App\HostelPro\Models\Allocation;
use App\HostelPro\Models\Booking;
use App\HostelPro\Models\Calendar;
use App\HostelPro\Models\DateHelper;
use App\HostelPro\Models\Guest;
use App\HostelPro\Models\LabelType;
use App\HostelPro\Models\MoneyHelper;
use App\HostelPro\Models\Payment;
use App\HostelPro\Models\POSProduct;
use App\HostelPro\Models\SmartAllocator;
use App\HostelPro\Models\Stay;
use Carbon\Carbon;
use View;
use Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;

class BookingModificationService
{

    use DispatchesJobs;

    private $hostel_id;
    private $booking_id;
    private $success = true;

    public function __construct($hostel_id, $booking_id, $guestdata, $paymentdata, $bookingdata, $label)
    {
        $this->hostel_id = $hostel_id;
        $this->booking_id = $booking_id;

        if (!empty($guestdata)) {
            $this->processGuestData($guestdata);
        }

        if (!empty($paymentdata)) {
            $this->processPaymentData($paymentdata);
        }

        $this->processBookingData($bookingdata);

        if (!empty($label)) {
            $this->processLabel($label);
        }

        return $this->success;

    }

    public function getStatus()
    {
        return $this->success;
    }

    private function processPaymentData($paymentdata)
    {

        parse_str($paymentdata, $paymentdata);

        $paymentAmount = MoneyHelper::convertToCents($paymentdata['payment']);
        $items = $paymentdata['items'] ?? null;
        $prices = $paymentdata['prices'] ?? null;

        if ((empty($items)) && (empty($paymentAmount))) {
            return "No payment data found.";
        }

        $booking = Booking::where('hostel_id', '=', $this->hostel_id)->where('id', '=', $this->booking_id)->first();

        if ($prices) {

            foreach ($prices as $i => $price) {
                $prices[$i] = MoneyHelper::convertToCents($price);
            }

            foreach ($items as $item => $qty) {
                $newItem = new AdditionalItem();
                $product = POSProduct::find($item);
                if ($product) {
                    $newItem->name = $product->name;
                } else {
                    $newItem->name = "Accommodation";
                }
                $newItem->price = $prices[$item];
                $newItem->units = $qty;
                $newItem->total = $newItem->price * $newItem->units;
                if (($product) && ($product->deposit == true) && ($newItem->name != 'Booking Deposit')) {
                    $newItem->deposit = true;
                }
                $newItem->hostel_id = $this->hostel_id;
                $newItem->user_id = Auth::user()->id;
                if (!empty($booking)) {
                    $newItem->booking_id = $booking->id;
                }
                $newItem->currency_id = Auth::user()->currenthostel->currency->id;
                $newItem->save();

            }

        }

        if ($paymentAmount != 0) {

            if ($paymentAmount > $booking->totalAmountDue()) {
                $paymentAmount = $booking->totalAmountDue();
            }

            $payment = new Payment();
            $payment->name = $paymentdata['paymenttype'];
            $payment->total = $paymentAmount;
            $payment->hostel_id = Auth::user()->currenthostel->id;
            $payment->user_id = Auth::user()->id;
            if (!empty($booking)) {
                $payment->booking_id = $booking->id;
            }
            $payment->currency_id = Auth::user()->currenthostel->currency->id;
            $payment->save();
        }
        return true;
    }

    private function processGuestData($guestdata)
    {

        parse_str($guestdata, $guestdata);

        $id = $guestdata['guestid'];

        if (empty($id)) {
            //create a new guest and return
        }

        //Otherwise, the guest already exists.
        $guest = Guest::where('id', '=', $id)->where('hostel_id', '=', $this->hostel_id)->first();
        $guest->fill($guestdata);
        $guest->save();
    }

    private function processLabel($label)
    {

        $booking = Booking::where('hostel_id', '=', $this->hostel_id)->where('id', '=', $this->booking_id)->first();

        if ((!$booking) || (empty($label))) {
            return "Failed";
        }

        if ($label == "checkin") {
            if ($booking->totalAmountOwing() > 0) {
                $labelType = LabelType::where('type', '=', 'Checked in - Unpaid')->first();
            } else {
                $labelType = LabelType::where('type', '=', 'Checked in - Paid')->first();
            }

            $actionLogType = ActionLogType::where('name', '=', 'Checked In')->first();
            $actionLog = new ActionLog();
            $actionLog->action_log_type_id = $actionLogType->id;
            $actionLog->hostel_id = $booking->hostel_id;
            $actionLog->booking_id = $booking->id;
            $actionLog->user_id = Auth::user()->id ?? null;
            $actionLog->created_at = Carbon::now()->addSecond()->toDateTimeString();
            $actionLog->updated_at = Carbon::now()->addSecond()->toDateTimeString();
            $actionLog->save();

        }

        if ($label == "checkout") {
            $labelType = LabelType::where('type', '=', 'Checked Out')->first();

            $actionLogType = ActionLogType::where('name', '=', 'Checked Out')->first();
            $actionLog = new ActionLog();
            $actionLog->action_log_type_id = $actionLogType->id;
            $actionLog->hostel_id = $booking->hostel_id;
            $actionLog->booking_id = $booking->id;
            $actionLog->user_id = Auth::user()->id ?? null;
            $actionLog->created_at = Carbon::now()->addSecond()->toDateTimeString();
            $actionLog->updated_at = Carbon::now()->addSecond()->toDateTimeString();
            $actionLog->save();
        }

        $booking->label_type_id = $labelType->id;
        $booking->save();
        return true;
    }

    private function processBookingData($bookingdata)
    {

        $booking = Booking::where('id', '=', $this->booking_id)->where('hostel_id', '=', $this->hostel_id)->first();

        parse_str($bookingdata, $bookingdata);

        $price_per_night = $bookingdata['price_per_night'];
        $stay_ids = array_keys($price_per_night);

        $min_start_date = $booking->stays()->min('start_date');
        $max_end_date = $booking->stays()->max('end_date');
        $number_of_days = DateHelper::countNumberOfDays($min_start_date, $max_end_date);
        $calendar1 = new Calendar($booking->hostel_id, $min_start_date, $number_of_days);
        $calendar1->getAvailability();

        $success = Allocation::changeToAllocated($booking->id, $stay_ids, true);
        if (!$success) {
            $this->success = false;
            return false;
        }

        $stays = Stay::where('booking_id', '=', $booking->id)->whereNotNull('room_id')->get();

        foreach ($stays as $stay) {
            $stay->price_per_night = MoneyHelper::convertToCents($price_per_night[$stay->id]);
            $stay->save();
        }

        $calendar2 = new Calendar($booking->hostel_id, $min_start_date, $number_of_days);
        $calendar2->getAvailability();

        $resync_needed = $this->resyncIsNeeded($calendar1->availability, $calendar2->availability);
        //if ($resync_needed) {
            $this->dispatch(new SyncAvailability($booking->hostel_id, $min_start_date, $max_end_date));
        //}

        if ($booking->actionLogs()->count() == 0) {
            $actionLogType = ActionLogType::where('name', '=', 'Booking Created')->first();
            $actionLog = new ActionLog();
            $actionLog->action_log_type_id = $actionLogType->id;
            $actionLog->hostel_id = $booking->hostel_id;
            $actionLog->booking_id = $booking->id;
            $actionLog->user_id = ($booking->myallocator_formatted_log()->count() > 0) ? null : Auth::user()->id;
            $actionLog->created_at = $booking->created_at;
            $actionLog->updated_at = $booking->created_at;
            $actionLog->save();
        } else if ($resync_needed) {
            $actionLogType = ActionLogType::where('name', '=', 'Booking Modified')->first();
            $actionLog = new ActionLog();
            $actionLog->action_log_type_id = $actionLogType->id;
            $actionLog->hostel_id = $booking->hostel_id;
            $actionLog->booking_id = $booking->id;
            $actionLog->user_id = Auth::user()->id ?? null;
            $actionLog->created_at = Carbon::now()->addSecond()->toDateTimeString();
            $actionLog->updated_at = Carbon::now()->addSecond()->toDateTimeString();
            $actionLog->save();
        }

        return true;
    }

    private function resyncIsNeeded($calendar1, $calendar2)
    {

        foreach ($calendar1 as $index => $room) {
            foreach ($room['availability'] as $date => $beds_free) {
                if ($beds_free != $calendar2[$index]['availability'][$date]) {
                    //dd($beds_free . "calendar2 " . $index . " " . $date . " " . $calendar2[$index]['availability'][$date]);
                    return true;
                }
            }
        }

        return false;
    }

}