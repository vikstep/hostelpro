<?php namespace App\HostelPro\Services;

class PackagedResponseService
{

    public $cashregisteraccommodation;
    public $bookingdetails;
    public $calendar;
    public $notifications;

    public function __construct(string $hostel_id, string $booking_id = null, array $calendar_options = null, $cash_register_accommodation_only = null) {

        if (($cash_register_accommodation_only) && ($booking_id)) {
            $this->cashregisteraccommodation = (new CashRegisterAccommodationResponseService($hostel_id, $booking_id))->render();
        } else if ($booking_id) {
            $this->bookingdetails = (new BookingDetailsResponseService($hostel_id, $booking_id))->render();
        }

        if (!empty($calendar_options)) {

            $this->calendar = (new CalendarResponseService(
                $hostel_id,
                $calendar_options['date'],
                $calendar_options['days'],
                $calendar_options['viewmode'],
                $booking_id ?? $calendar_options['openbooking'] ?? null)
            )->render();
        }

        $this->notifications = (new NotificationsResponseService())->render();

        return $this;
    }

}