<?php namespace App\HostelPro\Services;

use App\HostelPro\Models\Booking;
use App\HostelPro\Models\Email;

class SendEmailService
{
    private $email;
    private $booking;
    private $pre;

    public function __construct(Email $email, $booking_id, $pre = true) {
        $this->email = $email;
        $this->booking = Booking::find($booking_id);
        $this->pre = $pre;
    }

    public function email() {

        if ($this->pre) {
            $body = $this->email->pre_email_text;

            if ($this->booking->pre_email_sent) {
                return;
            }

            $this->booking->pre_email_sent = true;
        } else {
            $body = $this->email->post_email_text;

            if ($this->booking->post_email_sent) {
                return;
            }

            $this->booking->post_email_sent = true;
        }

        //Save now so we only attempt to send each email once.
        $this->booking->save();

        $to_email = $this->booking->guest->email;
        if (!$to_email) {
            return;
        }

        $body = str_replace(['[GuestFirstName]', '[GuestLastName]'], [$this->booking->guest->firstname, $this->booking->guest->lastname], $body);

        $transport = \Swift_SmtpTransport::newInstance($this->email->host, $this->email->port, $this->email->encryption);
        $transport->setUsername($this->email->username);
        $transport->setPassword($this->email->password);

        $logger = new \Swift_Plugins_Loggers_ArrayLogger();

        $mailer = \Swift_Mailer::newInstance($transport);
        $mailer->getTransport()->start();
        $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

        $message = new \Swift_Message('A message from ' . $this->booking->hostel->name);
        $message->setFrom([$this->email->username => $this->booking->hostel->name]);
        $message->setTo($to_email);
        $message->setBody($body, 'text/html');
        $mailer->send($message);
    }
}