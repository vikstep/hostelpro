<?php namespace App\HostelPro\Services;

use App\HostelPro\Models\Review;
use App\HostelPro\Models\ReviewURL;
use App\HostelPro\Models\Reviews\BooReviewScraper;
use App\HostelPro\Models\Reviews\HB2ReviewScraper;
use App\HostelPro\Models\Reviews\HW2ReviewScraper;

use Carbon\Carbon;

class ReviewScraperService
{

    private $hostel_id;

    public function __construct(String $hostel_id)
    {
        $this->hostel_id = $hostel_id;

        $reviewURLs = ReviewURL::where('hostel_id', '=', $hostel_id)->get();
        foreach ($reviewURLs as $reviewURL) {
            $this->crawl($reviewURL);
        }

        return true;
    }

    private function crawl(ReviewURL $reviewURL) {

        $results = [];
        $last_review_date = $reviewURL->reviews()->max('date');
        $last_reviews = $reviewURL->reviews()->where('date', '=', $last_review_date)->get();

        if ($reviewURL->myallocator_channel_id == 'boo') {
            $scraper = new BooReviewScraper($reviewURL->url, $last_reviews);
            $scraper->crawl();
            $results = $scraper->getNewReviews();
        }

        if ($reviewURL->myallocator_channel_id == 'hb2') {
            $scraper = new HB2ReviewScraper($reviewURL->url, $last_reviews);
            $scraper->crawl();
            $results = $scraper->getNewReviews();
        }

        if ($reviewURL->myallocator_channel_id == 'hw2') {
            $scraper = new HW2ReviewScraper($reviewURL->url, $last_reviews);
            $scraper->crawl();
            $results = $scraper->getNewReviews();
        }

        if ($results) {
            foreach ($results as $index => $result) {
                $results[$index]['review_url_id'] = $reviewURL->id;
                $results[$index]['created_at'] = Carbon::now()->toDateString();
                $results[$index]['updated_at'] = Carbon::now()->toDateString();
            }

            Review::insert($results);
        }

    }

}