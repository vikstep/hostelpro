<?php namespace App\HostelPro\Services;

use App\HostelPro\Models\Allocation;
use App\HostelPro\Models\Booking;
use App\HostelPro\Models\SmartAllocator;
use View;

class CashRegisterAccommodationResponseService
{

    private $hostel_id;
    private $booking_id;

    public function __construct(String $hostel_id, String $booking_id)
    {
        $this->hostel_id = $hostel_id;
        $this->booking_id = $booking_id;
    }

    public function render() {
        $booking = Booking::where('hostel_id', '=', $this->hostel_id)->where('booking.id', '=', $this->booking_id)->with(['stays' => function ($q) {
            $q->where('stay.temp', '=', true);
        }])->first();

        return View::make('partials.cashregisteraccommodation')->with('booking', $booking)->render();
    }

}