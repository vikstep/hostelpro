<?php namespace App\HostelPro\Models\Prices;

use App\HostelPro\Models\DateHelper;
use Carbon;
use Goutte\Client;

class BooPriceScraper extends PriceScraper {

    protected $current_date = null;
    protected $new_prices = [];

    public function __construct($url) {
        $this->url = $url;
    }

    public function crawl() {

        $client = new Client();

        //Note: Booking.com returns a 301 if you don't set both the user-agent and accept-language.
        //The user-agent must also be on an approved list,
        //So sending the guzzle user agent or anything random will *not* work

        $client->setHeader('user-agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36');
        $client->setHeader('accept-language', 'en-US,en;q=0.8');

        $start_date = Carbon::now();
        $end_date = Carbon::now()->addDays(31);

        $dates = DateHelper::getArrayOfDates($start_date, $end_date);

        foreach ($dates as $date) {
            $this->current_date = $date;
            $url = $this->url . '?checkin=' . $date . '&checkout=' . Carbon::createFromFormat('Y-m-d', $date)->addDay()->toDateString() . '&selected_currency=EUR&no_rooms=1&group_adults=1';
            $crawler = $client->request('GET', $url);
            $this->processPage($crawler);
        }
    }

    public function getNewPrices() {
        return $this->new_prices;
    }

    public function getNameOfHostel() {

        $client = new Client();
        $client->setHeader('user-agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36');
        $client->setHeader('accept-language', 'en-US,en;q=0.8');

        $crawler = $client->request('GET', $this->url);
        $raw_name = trim($crawler->filter('#hp_hotel_name')->text());

        return $raw_name;
    }

    private function processPage($crawler) {
        $crawler->filter('#room_availability_container > tr[data-occupancy]')->each(function ($node) {
            $this->processNode($node);
        });
    }

    private function processNode($node) {

        $price = null;
        $room_description = null;
        $bed_description = null;

        if ($node->filter('strong[data-price-without-addons]')->count() > 0) {
            $price = $node->filter('strong[data-price-without-addons]')->first()->text();
            $price = (int) (substr(trim($price),5) * 100);
        }

        if ($node->filter('td[data-room-name]')->count() > 0) {
            $room_description = trim($node->filter('td[data-room-name]')->first()->attr('data-room-name'));
        }

        /** Below doesn't work because the room description is in the previous TR. would need to get sibling then do it */
        /*
        if ($node->filter('li.rt-bed-type')->count() > 0) {
            $bed_description = trim($node->filter('li.rt-bed-type')->first()->text());
        }*/

        if ((!$price) || (!$room_description)) {
            return;
        }

        $data = [
            'date' => $this->current_date,
            'price' => $price,
            'currency_id' => 45,
            'room_description' => $room_description,
            'bed_description' => $bed_description
        ];

        array_push($this->new_prices, $data);
    }
}