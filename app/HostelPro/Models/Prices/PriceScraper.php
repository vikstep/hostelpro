<?php namespace App\HostelPro\Models\Prices;

abstract class PriceScraper {

    protected $url;
    protected $new_prices = [];

    abstract protected function __construct($url);

    abstract protected function crawl();

    abstract protected function getNewPrices();
}