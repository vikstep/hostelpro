<?php namespace App\HostelPro\Models\Prices;

use App\HostelPro\Models\Currency;
use App\HostelPro\Models\DateHelper;
use Carbon;
use Goutte\Client;

class HW2PriceScraper extends PriceScraper {

    protected $current_date = null;
    protected $new_prices = [];
    protected $currency_id;

    public function __construct($url) {
        $this->url = $url;
    }

    public function crawl() {

        $client = new Client();

        $start_date = Carbon::now();
        $end_date = Carbon::now()->addDays(31);

        $dates = DateHelper::getArrayOfDates($start_date, $end_date);

        //HW2 returns the results in the hostel's native currency then converts it using javascript. So we need to figure out the hostel's currency.
        $crawler = $client->request('GET', $this->url);
        $crawler->filter('script')->each(function ($node) {
            $position = strpos($node->html(), "propertyCurrency");
            if ($position) {
                $currency_code = substr($node->html(), $position + 19, 3);
                $this->currency_id = Currency::where('currency_code', '=', $currency_code)->first()->id;
            }
        });

        $pathFragments = explode('/', $this->url);
        $propNum = end($pathFragments);

        foreach ($dates as $date) {
            $this->current_date = $date;
            $url = 'http://www.hostelworld.com/microsite/get-availability?propNum=' . $propNum . '&dateFrom=' . $date . '&dateTo=' . Carbon::createFromFormat('Y-m-d', $date)->addDay()->toDateString() . '&number_of_guests=1';
            $crawler = $client->getClient()->request('GET', $url);
            $raw_data = $crawler->getBody()->getContents();
            $this->processPage(json_decode($raw_data));
        }
    }

    public function getNewPrices() {
        return $this->new_prices;
    }

    public function getNameOfHostel() {

        $client = new Client();
        $crawler = $client->request('GET', $this->url);
        $raw_name = trim($crawler->filter('h1[data-name]')->text());

        return $raw_name;
    }

    private function processPage($data) {
        if (property_exists($data, 'rooms')) {
            foreach ($data->rooms->dorms as $room) {
                $this->processRoom($room);
            }

            foreach ($data->rooms->privates as $room) {
                $this->processRoom($room);
            }
        }
    }

    private function processRoom($room) {

        if (is_array($room->averagePrice)) {
            $price = (int)($room->averagePrice[0] * 100);
        } else {
            $price = (int)($room->averagePrice * 100);
        }
        $room_description = $room->numofbeds . ' ' . $room->roomtypename;
        $bed_description = $room->numofbeds;

        $data = [
            'date' => $this->current_date,
            'price' => $price,
            'currency_id' => $this->currency_id,
            'room_description' => $room_description,
            'bed_description' => $bed_description
        ];

        array_push($this->new_prices, $data);
    }
}