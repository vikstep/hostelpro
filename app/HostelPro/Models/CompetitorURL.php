<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class CompetitorURL extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'competitor_url';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = true;

    public function pricings()
    {
        return $this->hasMany(CompetitorPricing::class, 'competitor_url_id', 'id');
    }

    public function hostel()
    {
        return $this->belongsTo(Hostel::class);
    }

    public function channel()
    {
        return $this->belongsTo(MyAllocatorChannel::class, 'myallocator_channel_id', 'id');
    }

}