<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class POSProduct extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pos_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = false;

    public function category() {
        return $this->belongsTo('App\HostelPro\Models\POSCategory', 'category_id', 'id');
    }

    public function getFormattedPriceAttribute()
    {
        return MoneyHelper::convertCentsToDollars($this->price);
    }

}