<?php namespace MyAllocatorApi;

use MyAllocator\phpsdk\src\Api\RoomCreate as MyAllocatorRoomCreate;

class RoomCreate extends MyAllocatorRoomCreate {

    public function __construct($hostelId) {
        parent::__construct();

        $auth = new MyAllocatorAuth();
        $auth->createByHostelId($hostelId);

        $this->setAuth($auth);
        $this->setConfig('dataFormat', 'array');
    }

}