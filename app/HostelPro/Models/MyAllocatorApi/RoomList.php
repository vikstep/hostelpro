<?php namespace MyAllocatorApi;

use MyAllocator\phpsdk\src\Api\RoomList as MyAllocatorRoomList;

class RoomList extends MyAllocatorRoomList {

    public function __construct($hostelId) {
        parent::__construct();

        $auth = new MyAllocatorAuth();
        $auth->createByHostelId($hostelId);

        $this->setAuth($auth);
        $this->setConfig('dataFormat', 'array');
    }


}