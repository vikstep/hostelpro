<?php namespace MyAllocatorApi;

use MyAllocator\phpsdk\src\Api\PropertyList as MyAllocatorPropertyList;

class PropertyList extends MyAllocatorPropertyList {

    public function __construct($userId, $userPassword) {
        parent::__construct();

        $auth = new MyAllocatorAuth();
        $auth->createByUserIdAndPassword($userId, $userPassword);

        $this->setAuth($auth);
        $this->setConfig('dataFormat', 'array');
    }

}