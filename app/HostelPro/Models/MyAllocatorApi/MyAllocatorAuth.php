<?php namespace MyAllocatorApi;

use App\HostelPro\Models\MyAllocatorToken;
use MyAllocator\phpsdk\src\Object\Auth as MyAllocatorAuthOriginal;

class MyAllocatorAuth extends MyAllocatorAuthOriginal {

    public function __construct() {
        $this->vendorId = getenv('MYALLOCATOR_VENDORID');
        $this->vendorPassword = getenv('MYALLOCATOR_VENDORPASSWORD');
    }

    public function createByHostelId($hostelId) {
        $myAllocatorToken = MyAllocatorToken::where('hostel_id', '=', $hostelId)->first();

        $this->propertyId = $myAllocatorToken->myallocator_id;
        $this->userToken = $myAllocatorToken->user_token;

        /*
        if ($myAllocatorToken->property_token) {
            $this->propertyToken = $myAllocatorToken->property_token;
        } else {
            $this->propertyId = $myAllocatorToken->myallocator_id;
            $this->userToken = $myAllocatorToken->user_token;
        }*/

    }

    public function createByUserIdAndPassword($userId, $userPassword, $PMSUserId = null) {
        $this->userId = $userId;
        $this->userPassword = $userPassword;
        if ($PMSUserId) {
            $this->PMSUserId = $PMSUserId;
        }
    }

}