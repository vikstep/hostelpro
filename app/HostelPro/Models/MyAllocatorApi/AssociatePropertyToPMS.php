<?php namespace MyAllocatorApi;

use MyAllocator\phpsdk\src\Api\AssociatePropertyToPMS as MyAllocatorAssociatePropertyToPMS;

class AssociatePropertyToPMS extends MyAllocatorAssociatePropertyToPMS {

    public function __construct($hostelId) {
        parent::__construct();

        $auth = new MyAllocatorAuth();
        $auth->createByHostelId($hostelId);

        $this->setAuth($auth);
        $this->setConfig('dataFormat', 'array');
    }

}