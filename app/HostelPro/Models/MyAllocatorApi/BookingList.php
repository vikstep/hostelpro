<?php namespace MyAllocatorApi;

use MyAllocator\phpsdk\src\Api\BookingList as MyAllocatorBookingList;

class BookingList extends MyAllocatorBookingList {

    public function __construct($hostelId) {
        parent::__construct();

        $auth = new MyAllocatorAuth();
        $auth->createByHostelId($hostelId);

        $this->setAuth($auth);
        $this->setConfig('dataFormat', 'array');
    }

}