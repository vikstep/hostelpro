<?php namespace MyAllocatorApi;

use MyAllocator\phpsdk\src\Api\RoomUpdate as MyAllocatorRoomUpdate;

class RoomUpdate extends MyAllocatorRoomUpdate {

    public function __construct($hostelId) {
        parent::__construct();

        $auth = new MyAllocatorAuth();
        $auth->createByHostelId($hostelId);

        $this->setAuth($auth);
        $this->setConfig('dataFormat', 'array');
    }

}