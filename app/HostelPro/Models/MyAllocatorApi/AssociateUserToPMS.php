<?php namespace MyAllocatorApi;

use MyAllocator\phpsdk\src\Api\AssociateUserToPMS as MyAllocatorAssociateUserToPMS;

class AssociateUserToPMS extends MyAllocatorAssociateUserToPMS {

    public function __construct($userId, $userPassword) {
        parent::__construct();

        $auth = new MyAllocatorAuth();
        $auth->createByUserIdAndPassword($userId, $userPassword);

        $this->setAuth($auth);
        $this->setConfig('dataFormat', 'array');
    }

}