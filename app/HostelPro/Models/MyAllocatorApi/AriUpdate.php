<?php namespace MyAllocatorApi;

use MyAllocator\phpsdk\src\Api\ARIUpdate as MyAllocatorAriUpdate;
#use MyAllocatorApiOriginal\AriUpdate as MyAllocatorAriUpdate;

class AriUpdate extends MyAllocatorAriUpdate {

    public function __construct($hostelId) {
        parent::__construct();

        $auth = new MyAllocatorAuth();
        $auth->createByHostelId($hostelId);

        $this->setAuth($auth);
        $this->setConfig('dataFormat', 'array');
    }

}