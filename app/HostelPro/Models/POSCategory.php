<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class POSCategory extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pos_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = false;

}