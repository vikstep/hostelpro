<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Carbon;

class Booking extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'booking';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public function actionLogs()
    {
        return $this->hasMany(ActionLog::class, 'booking_id', 'id');
    }

    public function stays()
    {
        return $this->hasMany('App\HostelPro\Models\Stay', 'booking_id', 'id');
    }

    public function channel()
    {
        return $this->belongsTo('App\HostelPro\Models\MyAllocatorChannel', 'myallocator_channel_id', 'id');
    }

    public function getCommissionPretty()
    {
        $commission = $this->commission->first();
        $prettyString = $commission->original_amount . " " . $commission->original_currency->currency_code;
        if (!$commission->converted_amount) {
            return $prettyString;
        }
        return $prettyString . " (" . $commission->converted_amount . " " . $commission->converted_currency->currency_code . ")";
    }

    public function hasCommission()
    {
        if ($this->commission()->where('is_deposit', '=', false)->count() == 0) {
            return false;
        }
        return true;
    }

    public function commission()
    {
        return $this->hasMany('App\HostelPro\Models\Commission', 'booking_id', 'id');
    }

    public function notes()
    {
        return $this->hasMany('App\HostelPro\Models\BookingNote', 'booking_id', 'id')->orderBy('created_at', 'DESC');
    }

    public function nonTempStays()
    {
        return $this->hasMany('App\HostelPro\Models\Stay', 'booking_id', 'id')->nonTemp();
    }

    public function tempStays()
    {
        return $this->hasMany('App\HostelPro\Models\Stay', 'booking_id', 'id')->temp();
    }

    public function payments()
    {
        return $this->hasMany('App\HostelPro\Models\Payment', 'booking_id', 'id');
    }

    public function items()
    {
        return $this->hasMany('App\HostelPro\Models\AdditionalItem', 'booking_id', 'id');
    }

    public function transactions()
    {
        $transactions = $this->payments->merge($this->items)->merge($this->actionLogs);

        return $transactions->sortBy(function ($transaction) {
            return $transaction->created_at . get_class($transaction);
        });

    }

    public function myallocator_formatted_log()
    {
        return $this->hasMany('App\HostelPro\Models\MyAllocatorFormattedLog', 'booking_id', 'id');
    }

    public function guest()
    {
        return $this->belongsTo('App\HostelPro\Models\Guest', 'guest_id', 'id');
    }

    public function unallocated($query)
    {
        return $query->where('status', '=', 'Unallocated');
    }

    public function label()
    {
        return $this->hasOne('App\HostelPro\Models\LabelType', 'id', 'label_type_id');
    }

    public function hostel()
    {
        return $this->belongsTo('App\HostelPro\Models\Hostel', 'hostel_id', 'id');
    }


    public function getLabelFormattedAttribute()
    {
        if ($this->label()->count() == 1) {
            return $this->label->type;
        } else {
            return "Booking - Unpaid";
        }
    }

    public function getLabelCSSAttribute()
    {
        if (!empty($this->label)) {
            if ($this->label->type == "Booking - Paid") {
                return "booking-paid";
            } else if ($this->label->type == "Checked in - Paid") {
                return "booking-checked-in-paid";
            } else if ($this->label->type == "Booking - Unpaid") {
                return "booking-unpaid";
            } else if ($this->label->type == "Checked in - Unpaid") {
                return "booking-checked-in-unpaid";
            } else if ($this->label->type == "Multi-Room Booking") {
                return "booking-multi-room";
            } else if ($this->label->type == "Checked Out") {
                return "booking-checked-out";
            } else if ($this->label->type == "Blocked Room") {
                return "booking-blocked-room";
            }
        }

        return "booking-unpaid";
    }

    public function addNote($text)
    {
        $bookingNote = new BookingNote();
        $bookingNote->text = $text;
        $bookingNote->user_id = Auth::user()->id;
        $bookingNote->booking_id = $this->id;
        $bookingNote->save();
        return true;
    }

    public function board_unallocated()
    {
        return $this->hasMany('App\HostelPro\Models\BoardUnallocated', 'booking_id', 'id');
    }

    public function block()
    {
        foreach ($this->stays as $stay) {
            $stay->price_per_night = '0';
            $stay->save();
        }
        $label_type_id = LabelType::where('type', '=', 'Blocked Room')->pluck('id')->all()[0];
        $this->label_type_id = $label_type_id;
        $this->save();
        return true;
    }

    public function unblock()
    {
        foreach ($this->stays as $stay) {
            $stay->price_per_night = Rate::getAveragePricePerNight($stay->room->room_type_id, $stay->start_date, $stay->end_date);
            $stay->save();
        }
        $this->label_type_id = null;
        $this->save();
        return true;
    }

    public function isPossibleNoShow()
    {
        return ($this->stays->min('start_date') < $this->hostel->today_at_hostel) && (in_array($this->label_type_id, [3, 6, 21, null]));
    }

    public function noshow()
    {
        $this->status = 'NoShow';
        $this->save();
        Board::whereIn('stay_id', $this->stays->pluck('id')->all())->delete();
        return true;
    }

    /* Change a booking from allocated to unallocated */
    public function unallocate($show_notification = false)
    {

        $stays = $this->stays->pluck('id');

        $boards = Board::whereIn('stay_id', $stays)
            ->select('stay_id', 'board.date', DB::raw('room_types.id AS room_type_id'), DB::raw('count(date) AS total_number_of_guests'))
            ->join('room', 'room.id', '=', 'board.room_id')
            ->join('room_types', 'room.room_type_id', '=', 'room_types.id')
            ->with('stay')
            ->groupBy('board.date')
            ->groupBy('room_types.id')
            ->get();

        foreach ($boards as $board) {
            BoardUnallocated::insert(array('date' => $board['date'], 'price_per_night' => $board->stay->price_per_night, 'room_type_id' => $board['room_type_id'], 'number_of_guests' => $board['total_number_of_guests'], 'booking_id' => $this->id));
        }

        $this->stays()->delete();
        $this->status = 'Unallocated';
        $this->show_notification = $show_notification;
        $this->save();
        return true;
    }

    public static function unallocateMany(Array $booking_ids)
    {

        $boards = Board::whereIn('stay.booking_id', $booking_ids)
            ->select('stay.booking_id', 'stay.price_per_night', 'stay.temp', 'board.stay_id', 'board.date', DB::raw('room_types.id AS room_type_id'), DB::raw('count(date) AS total_number_of_guests'))
            ->join('room', 'room.id', '=', 'board.room_id')
            ->join('room_types', 'room.room_type_id', '=', 'room_types.id')
            ->join('stay', 'board.stay_id', '=', 'stay.id')
            ->groupBy('board.date')
            ->groupBy('room_types.id')
            ->groupBy('stay.booking_id')
            ->get();

        if ($boards->count() == 0) {
            return true;
        }

        $data = array();

        foreach ($boards as $board) {
            array_push($data, array(
                'date' => $board['date'],
                'price_per_night' => $board['price_per_night'],
                'room_type_id' => $board['room_type_id'],
                'number_of_guests' => $board['total_number_of_guests'],
                'booking_id' => $board['booking_id']));
        }

        BoardUnallocated::insert($data);
        Stay::whereIn('booking_id', $booking_ids)->delete();

        //Use the query builder instead of Eloquent so that we don't update the timestamps.
        DB::table('booking')->whereIn('id', $boards->pluck('booking_id')->all())->update(['status' => 'Unallocated']);

        //Check each one to see if they are or aren't split

        return true;
    }

    public function addStayToExistingTemp($date, $roomId, $bedNumber, $new = false, $number_of_guests = 1, $days = 1)
    {

        if (!$roomId) {
            $stay = new Stay();
            $stay->booking_id = $this->id;
            $stay->start_date = $date;
            $stay->end_date = $date;
            $stay->number_of_guests = 1;
            $stay->price_per_night = 0;
            $stay->temp = true;
            $stay->save();
            return true;
        }

        $end_date = DateHelper::addDays($date, $days -1);

        $room = Auth::user()->currenthostel->rooms()->where('room.id', '=', $roomId)->first();
        if ($room->isPrivateRoom()) {
            $bedNumber = 1;
            $number_of_guests = $room->roomtype->number_of_guests;
        }

        $count = BoardTemp::where('booking_id', '=', $this->id)->where('hostel_id', '=', Auth::user()->currenthostel->id)->count();

        //Account for overbooking situations where we couldn't allocate entirely into a room.
        if ($this->nonTempStays->count() == 0) {
            $new = true;
        }

        if (($count == 0) && ($new == false)) {
            return false;
        }

        //Check if another temp booking is already in spot, if so delete it
        $existing = BoardTemp::where('room_id', '=', $roomId)
            ->whereBetween('bed_number', [$bedNumber, $bedNumber + $number_of_guests-1])
            ->whereBetween('date', [$date, $end_date])
            ->where('hostel_id', '=', Auth::user()->currenthostel->id)
            ->first();

        if ($existing) {
            if ($existing->booking_id == $this->id) {
                //This booking already occupies this spot so we can consider it already added/successful.
                return true;
            } else {
                BoardTemp::clearTempStays($roomId, $date, $end_date, $bedNumber, $bedNumber + $number_of_guests-1);
                //Stay::where('booking_id', '=', $existing->booking_id)->where('temp', '=', true)->delete();
            }
        }


        if ($this->label_type_id != 18) {
            $rate = Rate::where('room_type_id', '=', $room->room_type_id)->where('date', '=', $date)->first();
        } else {
            $rate = null;
        }

        $stay = new Stay();
        $stay->booking_id = $this->id;
        $stay->room_id = $roomId;
        $stay->start_date = $date;
        $stay->end_date = $end_date;
        $stay->number_of_guests = $number_of_guests;
        $stay->price_per_night = $rate ? $rate->price : 0;
        $stay->temp = true;
        $stay->save();

        $inserts = array();
        for ($day = 1; $day <= $days; $day++) {
            for ($i = $bedNumber; $i <= $bedNumber - 1 + $number_of_guests; $i++) {
                array_push($inserts, array('stay_id' => $stay->id, 'date' => $date, 'room_id' => $roomId, 'bed_number' => $i, 'booking_id' => $this->id, 'hostel_id' => Auth::user()->currenthostel->id));
            }
            $date = DateHelper::addDays($date, 1);
        }
        BoardTemp::insert($inserts);

        return true;
    }

    public function setLabel($label_type_id, $save = true)
    {
        if ($this->label_type_id == $label_type_id) {
            return;
        }
        $this->label_type_id = $label_type_id;
        if ($save) {
            $this->save();
        }
    }

    public function cancel($note_text = null)
    {
        $this->status = 'Cancelled';
        $this->save();

        /** Investigate the implications of the below line further, ie with daily allocatoin */
        BoardUnallocated::where('booking_id', '=', $this->id)->delete();

        $stays = Stay::where('booking_id', '=', $this->id)->pluck('id');
        Board::whereIn('stay_id', $stays)->delete();

        if ($note_text) {
            $this->addNote($note_text);
        }

        $actionLogType = ActionLogType::where('name', '=', 'Booking Cancelled')->first();
        $actionLog = new ActionLog();
        $actionLog->action_log_type_id = $actionLogType->id;
        $actionLog->hostel_id = $this->hostel_id;
        $actionLog->booking_id = $this->id;
        $actionLog->user_id = Auth::user()->id ?? null;
        $actionLog->created_at = Carbon::now()->addSecond()->toDateTimeString();
        $actionLog->updated_at = Carbon::now()->addSecond()->toDateTimeString();
        $actionLog->save();
    }

    public function getDepositAmount()
    {
        return $this->items()->where('name', '=', 'Booking Deposit')->sum('total');
    }

    public function getAccommodationItemsAmount()
    {
        return $this->items->where('name', '=', 'Accommodation')->sum('total');
    }

    public function addUnallocatedStay($startdate, $enddate, $room_type_id, $number_of_guests, $price_per_night)
    {
        $dates = DateHelper::getArrayOfDatesByString($startdate, $enddate);
        foreach ($dates as $date) {
            BoardUnallocated::insert(array('date' => $date, 'price_per_night' => $price_per_night, 'room_type_id' => $room_type_id, 'number_of_guests' => $number_of_guests, 'booking_id' => $this->id));
        }
    }

    public function groupHasBeenSplit()
    {
        $number_of_guests = 0;

        foreach($this->nonTempStays as $stay) {
            $number_of_guests += $stay->number_of_guests;
        }

        $roomIds = array();

        foreach ($this->nonTempStays as $r)
        {
            $roomIds[$r->room_id] = $r->room_id;
        }
        
        if(count($roomIds) < 2)
        {
            return false;
        }

        $roomTypeIds = Room::whereIn('id', $roomIds)->groupBy('room_type_id')->pluck('room_type_id')->all();
        
        if(count($roomTypeIds) > 1)
        {
            return false;
        }

        $roomTypesMaxGuestNumber = RoomType::whereIn('id', $roomTypeIds)->pluck('number_of_guests')->max();

        return $roomTypesMaxGuestNumber >= $number_of_guests;
    }

    public function totalAmountDue()
    {
        $additional_items = $this->items()->where('name', '!=', 'Booking Deposit')->sum('total');

        $cashPaid = $this->payments()->sum('total');

        return $additional_items - $cashPaid;
    }

    public function totalAmountOwing($tempstays = false)
    {

        $accommodation = 0;

        if ($tempstays == false) {
            foreach ($this->nonTempStays as $stay) {
                $accommodation += $stay->number_of_guests * $stay->price_per_night * DateHelper::countNumberOfDays($stay->start_date, $stay->end_date);
            }
        } else {
            foreach ($this->tempStays as $stay) {
                $accommodation += $stay->number_of_guests * $stay->price_per_night * DateHelper::countNumberOfDays($stay->start_date, $stay->end_date);
            }
        }

        $additional_items = $this->items->sum('total');

        $cashPaid = $this->payments->sum('total');

        $accommodationItems = $this->getAccommodationItemsAmount();

        //return $additional_items - $cashPaid - $this->getDepositAmount();
        return $accommodation + $additional_items - $cashPaid - $accommodationItems;
    }

    public function totalAmountOwingFormatted()
    {
        return MoneyHelper::convertCentsToDollars($this->totalAmountOwing());
    }


    public function save(array $options = array())
    {
        #$this->generateInvoiceNumber();

        if ($this->groupHasBeenSplit()) {
            if (empty($this->label_type_id) || ($this->label_type_id == 6)) {
                $this->setLabel(21, false);
            }

            if (($this->label_type_id == 9) || ($this->label_type_id == 12)) {
                if ($this->totalAmountOwing() > 0) {
                    $this->setLabel(12, false);
                } else {
                    $this->setLabel(9, false);
                }
            }

        } else if (($this->label_type_id == 9) || ($this->label_type_id == 12)) {

            if ($this->totalAmountOwing() > 0) {
                $this->setLabel(12, false);
            } else {
                $this->setLabel(9, false);
            }
        } else if ($this->label_type_id == 21) {
            $this->setLabel(6, false);
        }

        parent::save();
    }


}