<?php namespace App\HostelPro\Models;

use DB;

class ReportDashboard
{

    private $hostel_id;
    private $start;
    private $end;
    private $dates;
    private $bookings;

    public $channels = array();
    public $booking_sources = array();
    public $accommodation_revenue = array();

    public $lead_times = array();

    public $room_types = array();

    public $occupancy = array();

    public $channel_totals = array();

    public $channel_percentages = array();

    public $lead_time_average = array();

    public $bookings_per_day = array();

    public $grand_totals = ['Allocated' => 0, 'Cancelled' => 0, 'NoShow' => 0];

    public function __construct($hostel_id, $start, $end)
    {
        $this->hostel_id = $hostel_id;
        $this->start = $start;
        $this->end = $end;
        $this->dates = DateHelper::getArrayOfDatesByString($this->start, $this->end);

        $this->bookings = $this->getBookings();
        $this->channels = $this->getDistinctChannels($this->bookings);

        $this->room_types = Hostel::find($hostel_id)->roomtypes()->orderBy('number_of_guests')->get()->keyBy('id');

        $this->occupancy = $this->initialize($this->room_types->pluck('name', 'id')->all());

        $this->booking_sources = $this->initialize($this->channels, 0);
        $this->accommodation_revenue = $this->initialize($this->channels, 0);
        $this->commission = $this->initialize($this->channels, 0);
        $this->channel_totals = $this->initialize($this->channels,['Allocated' => 0,
                'Cancelled' => 0,
                'NoShow' => 0,
                'TotalBedNights' => 0,
                'TotalStay' => 0,
                'AvgStay' => 0]);

        $this->channel_percentages = $this->initialize($this->channels,['Allocated' => 0, 'Cancelled' => 0, 'NoShow' => 0]);

        $this->lead_times = $this->initialize($this->channels,['Mon' => [],
            'Tue' => [],
            'Wed' => [],
            'Thu' => [],
            'Fri' => [],
            'Sat' => [],
            'Sun' => []]);

        $this->lead_time_average = $this->initialize($this->channels,['Same Day' => 0, '1-7 Days' => 0, '8-14 Days' => 0, '15-30 Days' => 0, '31-60 Days' => 0,'61+ Days' => 0]);

        $this->bookings_per_day = $this->initialize($this->channels,['Mon' => 0,
            'Tue' => 0,
            'Wed' => 0,
            'Thu' => 0,
            'Fri' => 0,
            'Sat' => 0,
            'Sun' => 0]);

        $this->processBookings();
    }

    public function processBookings()
    {
        foreach ($this->bookings as $booking) {
            $this->channel_totals[$booking->myallocator_channel_name][$booking->status]++;

            $this->channel_totals[$booking->myallocator_channel_name]['TotalStay'] += $booking->nights;

            $this->grand_totals[$booking->status]++;
            foreach($booking->nonTempStays as $stay) {
                $bed_nights_this_stay = $stay->number_of_guests * DateHelper::countNumberOfDays($stay->start_date, $stay->end_date);
                $this->channel_totals[$booking->myallocator_channel_name]['TotalBedNights'] += $bed_nights_this_stay;
                #$this->occupancy_data[$stay->room->room_type_id] += $bed_nights_this_stay;
            }

            //Only add the commission if we've taken a payment for it
            if ($booking->payments->sum('total') > 0) {
                $this->commission[$booking->myallocator_channel_name] += $booking->commission_amount;
            }

            foreach ($booking->payments as $payment) {
                $this->accommodation_revenue[$booking->myallocator_channel_name] += $payment->total;
            }

            $lead_time_in_days = DateHelper::countNumberOfDays($booking->created_at->toDateString(), $booking->date) - 1;
            array_push($this->lead_times[$booking->myallocator_channel_name][$booking->created_at->format('D')], $lead_time_in_days);
            $this->bookings_per_day[$booking->myallocator_channel_name][$booking->created_at->format('D')]++;

            $this->addToAverageLeadTime($booking->myallocator_channel_name, $lead_time_in_days);
        }

        foreach($this->lead_times as $channel => $days) {

            //$flattened_array = call_user_func_array('array_merge', $days);

            foreach ($days as $day => $lead_time_array) {
                $this->lead_times[$channel][$day] = $this->calculateMedian($lead_time_array);
            }
        }

        foreach ($this->lead_time_average as $channel => $lead_time_totals) {
            foreach ($lead_time_totals as $period => $total) {
                $this->lead_time_average[$channel][$period] = $this->getAverageLeadTime($channel, $total);
            }
        }

        foreach ($this->channel_totals as $channel => &$total) {
            if (($total['TotalBedNights'] == 0) || ($total['Allocated'] == 0)) {
                $total['AvgStay'] = 0;
            } else {
                $total['AvgStay'] = round($total['TotalBedNights'] / $total['Allocated'],1);
            }
            $this->booking_sources[$channel] = $total['Allocated'];

            $total_bookings = $total['Allocated'] + $total['Cancelled'] + $total['NoShow'];
            $this->channel_percentages[$channel]['Allocated'] = round(($total['Allocated'] / ($total_bookings)) * 100,1);
            $this->channel_percentages[$channel]['Cancelled'] = round(($total['Cancelled'] / ($total_bookings)) * 100,1);
            $this->channel_percentages[$channel]['NoShow'] = round(($total['NoShow'] / ($total_bookings)) * 100,1);
        }

        foreach($this->room_types as $room_type) {
            $number_of_beds_per_room = $room_type->number_of_guests;
            $number_of_rooms = $room_type->rooms->count();
            $number_of_days = count($this->dates);

            $max_possible_bed_nights = $number_of_beds_per_room * $number_of_rooms * $number_of_days;

            $room_ids = $room_type->rooms->pluck('id')->all();

            $total_bed_nights = Board::whereIn('date', $this->dates)->whereIn('room_id', $room_ids)->count();

            $this->occupancy[$room_type->name] = round(($total_bed_nights / $max_possible_bed_nights)*100,2);
        }

    }

    public function addToAverageLeadTime($channel, $lead_time_in_days) {
        if ($lead_time_in_days == 0) {
            $this->lead_time_average[$channel]['Same Day']++;
        }

        if (1 <= $lead_time_in_days && $lead_time_in_days <= 7) {
            $this->lead_time_average[$channel]['1-7 Days']++;
        }

        if (8 <= $lead_time_in_days && $lead_time_in_days <= 14) {
            $this->lead_time_average[$channel]['8-14 Days']++;
        }

        if (15 <= $lead_time_in_days && $lead_time_in_days <= 30) {
            $this->lead_time_average[$channel]['15-30 Days']++;
        }

        if (31 <= $lead_time_in_days && $lead_time_in_days <= 60) {
            $this->lead_time_average[$channel]['31-60 Days']++;
        }

        if (61 <= $lead_time_in_days) {
            $this->lead_time_average[$channel]['61+ Days']++;
        }
    }

    public function getAverageLeadTime($channel, $total) {
        $total_bookings_made_in_channel = $this->channel_totals[$channel]['Allocated']
            + $this->channel_totals[$channel]['Cancelled']
            + $this->channel_totals[$channel]['NoShow'];

        $percentage = ($total / $total_bookings_made_in_channel) * 100;

        return round($percentage, 2);
    }

    public function initialize($keys, $initial_value = null)
    {
        $array_to_return = array();
        foreach ($keys as $key) {
            $array_to_return[$key] = $initial_value;
        }
        return $array_to_return;
    }

    public function getBookings()
    {
        return Booking::selectRaw('booking.id,
        min(stay.start_date) as date,
        booking.status,
        booking.label_type_id,
        round(sum(stay.number_of_guests)) as number_of_guests,
        ABS((DATEDIFF(min(end_date), max(start_date)) + 1)) as nights,
        sum(payment.total) as payment_actual,
        COALESCE(commission.converted_amount,commission.original_amount) as commission_amount,
        COALESCE(NULLIF(myallocator_channel.name, \'\'), \'Other\') as myallocator_channel_name,
        booking.created_at,
        booking.updated_at')
            ->whereIn('status', ['Allocated', 'Cancelled', 'NoShow'])
            ->where('booking.hostel_id', '=', $this->hostel_id)
            ->where(function ($query) {
                $query->whereNull('booking.label_type_id')
                    ->orWhere('booking.label_type_id', '!=', '18');
            })
            ->where('stay.temp', '=', false)
            ->where('booking.created_at', '>=', $this->start)
            ->where('booking.created_at', '<', DateHelper::addDays($this->end,1))
            ->with('nonTempStays')
            ->with('payments')
            ->join('stay', 'stay.booking_id', '=', 'booking.id')
            ->leftJoin('payment', 'payment.booking_id', '=', 'booking.id')
            ->leftJoin('commission', 'commission.booking_id', '=', 'booking.id')
            ->leftJoin('currency', 'currency.id', '=', 'commission.original_currency_id')
            ->leftJoin('myallocator_channel', 'myallocator_channel.id', '=', 'booking.myallocator_channel_id')
            ->groupBy('booking.id')
            ->get();
    }

    public function getDistinctChannels($bookings)
    {
        return array_values(array_unique($bookings->pluck('myallocator_channel_name')->all()));
    }

    public function calculateMedian($array) {
        if (count($array) == 0) {
            return 0;
        }
        rsort($array);
        $middle = (int)round(count($array) / 2);
        return $array[$middle-1];
    }

}