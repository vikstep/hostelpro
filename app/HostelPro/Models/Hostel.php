<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Hostel extends Model
{

    use Notifiable;

    protected $table = 'hostel';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'country_id', 'city', 'timezone_id', 'currency_id', 'setup_steps_remaining'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];


    /* Remember Verify this works, it probably doesn't as-is */
    /*
    public function users() {
        return $this->hasMany('App\HostelPro\Models\User', 'hostel_id', 'id');
    }*/

    public function getTodayAtHostelAttribute() {
        $timezone = $this->timezone->timezone;
        return DateHelper::getToday($timezone);
    }

    public function getYesterdayAtHostelAttribute() {
        $timezone = $this->timezone->timezone;
        return DateHelper::getYesterday($timezone);
    }

    public function timezone()
    {
        return $this->belongsTo('App\HostelPro\Models\Timezone', 'timezone_id', 'id');
    }

    public function myallocatorToken()
    {
        return $this->hasOne('App\HostelPro\Models\MyAllocatorToken', 'hostel_id', 'id');
    }

    public function email()
    {
        return $this->hasOne('App\HostelPro\Models\Email', 'hostel_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo('App\HostelPro\Models\Country', 'country_id', 'id');
    }

    public function currency()
    {
        return $this->belongsTo('App\HostelPro\Models\Currency', 'currency_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany('App\HostelPro\Models\User', 'role_user')->withTrashed();
    }

    public function roomtypes()
    {
        return $this->hasMany('App\HostelPro\Models\RoomType', 'hostel_id', 'id');
    }

    public function paymentTypes()
    {
        return $this->hasMany('App\HostelPro\Models\PaymentType', 'hostel_id', 'id')->orderBy('sort');
    }

    public function rooms()
    {
        return $this->hasManyThrough('App\HostelPro\Models\Room', 'App\HostelPro\Models\RoomType', 'hostel_id', 'room_type_id')->orderBy('sort');
    }

    public function posCategories()
    {
        return $this->hasMany('App\HostelPro\Models\POSCategory', 'hostel_id', 'id')->orderBy('sort');
    }

    public function posProducts()
    {
        return $this->hasManyThrough('App\HostelPro\Models\POSProduct', 'App\HostelPro\Models\POSCategory', 'hostel_id', 'category_id')
            ->orderBy('pos_categories.sort')
            ->orderBy('pos_products.sort');
    }

    public function bookings()
    {
        return $this->hasMany('App\HostelPro\Models\Booking', 'hostel_id', 'id');
    }

    public function unallocatedBookings()
    {
        return $this->hasMany('App\HostelPro\Models\Booking', 'hostel_id', 'id')->where('status', '=', 'Unallocated');
    }

    public function countUnallocatedBookings()
    {
        return $this->hasMany('App\HostelPro\Models\Booking', 'hostel_id', 'id')->where('status', '=', 'Unallocated')->count();
    }

    public function seasonRates()
    {
        return $this->hasMany('App\HostelPro\Models\SeasonRate', 'hostel_id', 'id')->orderBy('start_date');
    }

    public function reviewURLs()
    {
        return $this->hasMany(ReviewURL::class);
    }

    public function reviews()
    {
        return $this->hasManyThrough(Review::class, ReviewURL::class, null, 'review_url_id', null);
    }

    public function competitorURLs()
    {
        return $this->hasMany(CompetitorURL::class);
    }

    public function reviewURLFor($channel)
    {
        return $this->hasMany(ReviewURL::class)
            //below line is probably unnecessary, check and delete
            ->where('hostel_id', '=', \Auth::user()->currenthostel->id)
            ->where('myallocator_channel_id', '=', $channel)
            ->first();
    }

    public function totalNumberOfBeds() {
        $total = 0;
        foreach ($this->rooms as $room) {
            $total += $room->roomtype->number_of_guests;
        }
        return $total;
    }

    public function getPaymillOffer() {
        $total_beds = $this->totalNumberOfBeds();
        return PaymillOffer::where('min_beds', '<=', $total_beds)
            ->where('max_beds', '>=', $total_beds)
            ->first();
    }

}