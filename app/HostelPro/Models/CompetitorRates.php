<?php namespace App\HostelPro\Models;

use DB;
use Carbon;

class CompetitorRates {

    public $competitor_urls;
    public $room_types;
    public $calendar = [];
    public $rates = [];

    private $hostel_id;
    private $dates;
    private $pricings;
    private $lookup_table = [];

    public function __construct($hostel_id, $start, $end) {
        $this->hostel_id = $hostel_id;
        $this->currency_code = Hostel::find($hostel_id)->currency->currency_code;
        $this->dates = DateHelper::getArrayOfDatesByString($start, $end);

        $this->competitor_urls = CompetitorURL::where('hostel_id', '=', $hostel_id)->get();

        if ($this->competitor_urls->count() == 0) {
            return;
        }

        $this->room_types = RoomType::where('hostel_id', '=', $hostel_id)->get();
        if ($this->room_types->count() == 0) {
            return;
        }

        foreach ($this->room_types as $room_type) {
            foreach ($this->competitor_urls as $competitor_url) {
                foreach ($this->dates as $date) {
                    $this->rates[$room_type->id][$competitor_url->id][$date] = null;
                }
            }
        }

        $this->pricings = CompetitorPricing::whereIn('competitor_url_id', $this->competitor_urls->pluck('id')->all())
            ->where('created_at', '>=', Carbon::now()->subDays(1)->toDateTimeString())
            ->groupBy('competitor_url_id', 'room_description', 'date')
            ->orderBy('created_at', 'DESC')
            ->get();

        $this->generateLookupTable();

        foreach ($this->pricings as $pricing) {
            if ($this->lookup_table[$pricing->competitor_url_id][$pricing->room_description]) {
                $room_type_id = $this->lookup_table[$pricing->competitor_url_id][$pricing->room_description];
                if ($pricing->currency->currency_code != $this->currency_code) {
                    $price = round($this->convertCurrency($pricing->currency->currency_code, $this->currency_code, $pricing->price / 100),2);
                } else {
                    $price = $pricing->price / 100;
                }

                $this->rates[$room_type_id][$pricing->competitor_url_id][$pricing->date->toDateString()] = $price;
            }
        }

        $this->generateCalendar();

    }

    private function generateLookupTable() {
        foreach ($this->competitor_urls as $competitor_url) {
            $room_descriptions = $this->pricings->where('competitor_url_id', $competitor_url->id)->unique('room_description');
            foreach ($room_descriptions as $room_description) {
                if ($room_description->url->myallocator_channel_id == 'boo') {
                    $this->lookup_table[$competitor_url->id][$room_description->room_description] = $this->matchBookingDotCom($room_description);
                }
                if ($room_description->url->myallocator_channel_id == 'hw2') {
                    $this->lookup_table[$competitor_url->id][$room_description->room_description] = $this->matchHW2($room_description);
                }
            }
        }
    }

    private function matchBookingDotCom(CompetitorPricing $pricing) {

        foreach ($this->room_types as $room_type) {
            if ((strpos($pricing->room_description, $room_type->number_of_guests . '-Bed') !== false) &&
                (strpos($pricing->room_description, 'Bed in ') !== false) &&
                (strpos($pricing->room_description, $room_type->gender) !== false)) {
                return $room_type->id;
            }
        }

        return null;
    }

    private function matchHW2(CompetitorPricing $pricing) {
        foreach ($this->room_types as $room_type) {
            $number_of_guests = intval(trim(substr($pricing->room_description,0,2)));
            if (($number_of_guests == $room_type->number_of_guests) &&
                (strpos($pricing->room_description, $room_type->gender) !== false)) {
                return $room_type->id;
            }
        }

        return null;
    }

    private function convertCurrency($from, $to, $amount) {

        $rate = \Cache::remember('rate-' . $from . '/' . $to, 3600, function() use ($from, $to) {
            $httpAdapter = new \Ivory\HttpAdapter\FileGetContentsHttpAdapter();
            $chainProvider = new \Swap\Provider\ChainProvider([
                new \Swap\Provider\YahooFinanceProvider($httpAdapter),
                new \Swap\Provider\GoogleFinanceProvider($httpAdapter)
            ]);
            $swap = new \Swap\Swap($chainProvider);
            return $swap->quote($from . '/' . $to)->getValue();
        });

        return $amount * $rate;
    }

    private function generateCalendar() {

        $start = Carbon::now()->startOfWeek();
        $end = Carbon::now()->addDays(31);
        $week = 0;
        $i = 0;
        while ($start->lt($end) > 0) {
            $this->calendar[$week][$start->toDateString()] = $start->copy();
            $start->addDay();
            $i++;
            if ($i % 7 == 0) {
                $week++;
            }
        }
    }

}