<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Auth;

class BookingNote extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'booking_notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = true;

    public function user() {
        return $this->belongsTo('App\HostelPro\Models\User', 'user_id', 'id')->withTrashed();
    }

    public function getCreatedAtAttribute($date) {
        $newtime = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        $newtime->timezone(Auth::user()->currenthostel->timezone->timezone);
        return $newtime;
    }

    public function getTextAttribute($value) {
        return nl2br($value);
    }

}