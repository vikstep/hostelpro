<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ShiftReport extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shift_report';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    protected $dates = ['start_time', 'end_time'];

    public $timestamps = false;

    public function hostel()
    {
        return $this->belongsTo('App\HostelPro\Models\Hostel', 'hostel_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\HostelPro\Models\User', 'user_id', 'id')->withTrashed();
    }

    public function notes() {
        $relation = $this->hasMany('App\HostelPro\Models\BookingNote', 'user_id', 'user_id')
            ->where('booking_notes.created_at', '>=', $this->attributes['start_time']);

        if ($this->isCompleted()) {
            $relation = $relation->where('booking_notes.created_at', '<=', $this->attributes['end_time']);
        }

        return $relation;
    }

    public function expenses()
    {
        $relation = $this->hasMany('App\HostelPro\Models\Expense', 'user_id', 'user_id')
            ->where('taken_from_cash_register', '=', true)
            ->where('expense.created_at', '>=', $this->attributes['start_time']);

        if ($this->isCompleted()) {
            $relation = $relation->where('expense.created_at', '<=', $this->attributes['end_time']);
        }

        return $relation;
    }

    public function deposits() {
        $relation = $this->hasMany('App\HostelPro\Models\AdditionalItem', 'user_id', 'user_id')
            ->where('additional_item.created_at', '>=', $this->attributes['start_time'])
            ->where('deposit', '=', true);

        if ($this->isCompleted()) {
            $relation = $relation->where('additional_item.created_at', '<=', $this->attributes['end_time']);
        }

        return $relation;
    }

    public function depositTotals() {
        $totals = array();
        foreach ($this->deposits as $transaction) {

            if ((isset($transaction->units)) && (!isset($totals[$transaction->name]))) {
                $totals[$transaction->name] = 0;
            }

            if (isset($transaction->units)) {
                $totals[$transaction->name] += $transaction->total;
            }
        }

        return $totals;
    }

    public function transactions() {
        $relation = $this->hasMany('App\HostelPro\Models\AdditionalItem', 'user_id', 'user_id')
            ->where('additional_item.created_at', '>=', $this->attributes['start_time']);

        if ($this->isCompleted()) {
            $relation = $relation->where('additional_item.created_at', '<=', $this->attributes['end_time']);
        }

        $transactions = $relation->get()->merge($this->payments)->merge($this->logs);

        return $transactions->sortBy(function ($transaction) {
            return $transaction->created_at . get_class($transaction);
        });
    }

    public function transactionTotals() {
        $totals = array();
        foreach ($this->transactions() as $transaction) {

            if ((isset($transaction->units)) && (!isset($totals[$transaction->name]))) {
                $totals[$transaction->name] = 0;
            }

            if (isset($transaction->units)) {
                $totals[$transaction->name] += $transaction->total;
            }
        }

        return $totals;
    }

    public function transactionTotalsWithoutDeposits() {
        $totals = array();
        $transactions = $this->transactions()->reject(function ($transaction) {
            return $transaction->deposit;
        });
        foreach ($transactions as $transaction) {

            if ((isset($transaction->units)) && (!isset($totals[$transaction->name]))) {
                $totals[$transaction->name] = 0;
            }

            if (isset($transaction->units)) {
                $totals[$transaction->name] += $transaction->total;
            }
        }

        return $totals;
    }

    public function paymentTotals() {
        $totals = array();
        foreach ($this->payments as $payment) {

            if (!isset($totals[$payment->name])) {
                $totals[$payment->name] = 0;
            }

            $totals[$payment->name] += $payment->total;
        }

        return $totals;
    }

    public function payments() {
        $relation = $this->hasMany('App\HostelPro\Models\Payment', 'user_id', 'user_id')
            ->where('payment.created_at', '>=', $this->attributes['start_time']);

        if ($this->isCompleted()) {
            $relation = $relation->where('payment.created_at', '<=', $this->attributes['end_time']);
        }

        return $relation;
    }

    public function logs() {
        $relation = $this->hasMany('App\HostelPro\Models\ActionLog', 'user_id', 'user_id')
            ->where('action_log.created_at', '>=', $this->attributes['start_time'])
            ->with('type');

        if ($this->isCompleted()) {
            $relation = $relation->where('action_log.created_at', '<=', $this->attributes['end_time']);
        }

        return $relation;
    }

    public function items()
    {
        return $this->hasMany('App\HostelPro\Models\ShiftReportData', 'shift_report_id', 'id');
    }

    public function itemsStartOfShift()
    {
        return $this->hasMany('App\HostelPro\Models\ShiftReportData', 'shift_report_id', 'id')->where('is_start_shift', '=', true);
    }

    public function itemsEndOfShift()
    {
        return $this->hasMany('App\HostelPro\Models\ShiftReportData', 'shift_report_id', 'id')->where('is_start_shift', '=', false);
    }

    public function isCompleted()
    {
        if ($this->attributes['end_time'] == "0000-00-00 00:00:00") {
            return false;
        }
        return true;
    }

    public function getStartTimeAttribute($value)
    {
        $timezone = $this->hostel->timezone->timezone;
        return DateHelper::convertUTCDateTime($value, $timezone);
    }

    public function getEndTimeAttribute($value)
    {
        $timezone = $this->hostel->timezone->timezone;
        return DateHelper::convertUTCDateTime($value, $timezone);
    }

    public function totalDifference() {
        if (!$this->isCompleted()) {
            return "";
        }
        $start = $this->hasMany('App\HostelPro\Models\ShiftReportData', 'shift_report_id', 'id')->where('is_start_shift', '=', true)->sum(DB::raw('quantity * amount_of_money'));
        $end = $this->hasMany('App\HostelPro\Models\ShiftReportData', 'shift_report_id', 'id')->where('is_start_shift', '=', false)->sum(DB::raw('quantity * amount_of_money'));
        return MoneyHelper::convertToCents($end - $start);
    }

    public function startTotal()
    {
        $total = $this->hasMany('App\HostelPro\Models\ShiftReportData', 'shift_report_id', 'id')->where('is_start_shift', '=', true)->sum(DB::raw('quantity * amount_of_money'));
        return MoneyHelper::convertToEnglishFormat($total);
    }

    public function startTotalCents()
    {
        return MoneyHelper::convertToCents($this->hasMany('App\HostelPro\Models\ShiftReportData', 'shift_report_id', 'id')->where('is_start_shift', '=', true)->sum(DB::raw('quantity * amount_of_money')));
    }

    public function endTotal()
    {
        if (!$this->isCompleted()) {
            return "";
        }
        $total = $this->hasMany('App\HostelPro\Models\ShiftReportData', 'shift_report_id', 'id')->where('is_start_shift', '=', false)->sum(DB::raw('quantity * amount_of_money'));
        return MoneyHelper::convertToEnglishFormat($total);
    }


}