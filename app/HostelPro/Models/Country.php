<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'country';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];


    /* Remember Verify this works, it probably doesn't as-is */
    public function hostels() {
        return $this->hasMany('App\HostelPro\Models\Hostel', 'country_id', 'id');
    }

}