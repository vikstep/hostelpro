<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class CompletedAllocation extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'completed_allocation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = true;

    public function hostel() {
        return $this->belongsTo('App\HostelPro\Models\Hostel', 'hostel_id', 'id');
    }

}