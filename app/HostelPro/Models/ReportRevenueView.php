<?php namespace App\HostelPro\Models;

use DB;
use Auth;

class ReportRevenueView {

    public $totals = array();
    public $bookingdata = array();
    public $grandtotals = array();

    private $hostel_id;
    private $dates;
    private $bookings;

    public function __construct($hostel_id, $start, $end) {
        $this->hostel_id = $hostel_id;
        $this->dates = DateHelper::getArrayOfDatesByString($start, $end);
        $this->initializeTotalsAndGrandTotals();
        $this->getBookings();
        $this->loopThroughBookingsAndCalculate();
        $this->calculateTotals();
        $this->calculateGrandTotals();
        $this->changeZeroesToDashes();
    }

    private function initializeTotalsAndGrandTotals() {
        //Initialize everything to zero
        foreach ($this->dates as $date) {
            $this->totals[$date]['accommodation-expected'] = 0;
            $this->totals[$date]['accommodation-actual'] = 0;
            $this->totals[$date]['deposits-taken'] = 0;
            $this->totals[$date]['deposits-returned'] = 0;
            $this->totals[$date]['other'] = 0;
            $this->totals[$date]['date'] = $date;
            $this->bookingdata[$date] = array();
        }

        $this->grandtotals['accommodation-expected'] = 0;
        $this->grandtotals['accommodation-actual'] = 0;
        $this->grandtotals['deposits-taken'] = 0;
        $this->grandtotals['deposits-returned'] = 0;
        $this->grandtotals['other'] = 0;
    }


    private function getBookings() {
        //Get a list of all bookings and get the minimum stay start date (so you know which date to associate it to)
        $booking_ids = Board::select('stay.booking_id')
            ->join('stay', 'stay_id', '=', 'stay.id')
            ->where('hostel_id', '=', $this->hostel_id)
            ->whereIn('date', $this->dates)
            ->groupBy('booking_id')
            ->pluck('booking_id')
            ->all();

        $this->bookings = Booking::select('booking.id',
            'stay.number_of_guests',
            'stay.start_date',
            'stay.end_date',
            'guest.lastname',
            'price_per_night',
            DB::raw('min(stay.start_date) as firstdate'))
            ->join('stay', 'booking.id', '=', 'stay.booking_id')
            ->join('guest', 'guest.id', '=', 'booking.guest_id')
            #->where('status', '=', 'Allocated')
            ->where('stay.temp', '!=', '1')
            ->whereIn('booking.id', $booking_ids)
            ->whereIn('stay.start_date', $this->dates)
            ->with('payments', 'items', 'nonTempStays')
            ->groupBy('booking.id')
            ->get();
        //REMEMBER TO LIMIT TO ONLY ALLOCATED BOOKINGS ie status='ALLOCATED';
    }

    private function loopThroughBookingsAndCalculate() {

        foreach ($this->bookings as $booking) {
            $date = $booking->firstdate;
            $this->bookingdata[$date][$booking->id] = array();
            $this->bookingdata[$date][$booking->id]['bookingid'] = $booking->id;
            $this->bookingdata[$date][$booking->id]['lastname'] = $booking->lastname;
            $this->bookingdata[$date][$booking->id]['accommodation-expected'] = 0;
            $this->bookingdata[$date][$booking->id]['accommodation-actual'] = 0;
            $this->bookingdata[$date][$booking->id]['deposits-taken'] = 0;
            $this->bookingdata[$date][$booking->id]['deposits-returned'] = 0;
            $this->bookingdata[$date][$booking->id]['other'] = 0;

            //Calculate the total expected accommodation (including the deposit)
            foreach ($booking->nonTempStays as $stay) {
                $number_of_nights = DateHelper::getDaysBetween($stay->start_date, $stay->end_date) + 1;
                $total = $number_of_nights * $stay->number_of_guests * $stay->price_per_night;
                $this->bookingdata[$date][$booking->id]['accommodation-expected'] += $total;
            }

            /* Remove the deposit that they may have already paid.
               Deposit is stored as a negative, so we add it to remove it from the expected amount.
               We run this sum on the collection so that we avoid extra db queries.*/
            $this->bookingdata[$date][$booking->id]['accommodation-expected'] += $booking->items->where('name', 'Booking Deposit')->sum('total');

            $this->bookingdata[$date][$booking->id]['accommodation-actual'] = $booking->payments->sum('total');

            $this->bookingdata[$date][$booking->id]['deposits-taken'] = $booking->items->filter(function ($item) {
                return $item->deposit == true && $item->total > 0;
            })->sum('total');

            $this->bookingdata[$date][$booking->id]['deposits-returned'] = abs($booking->items->filter(function ($item) {
                return $item->deposit == true && $item->total < 0;
            })->sum('total'));

            $this->bookingdata[$date][$booking->id]['other'] = $booking->items->filter(function ($item) {
                return $item->deposit == false && $item->name != 'Booking Deposit' && $item->name != 'Accommodation';
            })->sum('total');

            $differenceBetweenDeposits = ($this->bookingdata[$date][$booking->id]['deposits-taken'] - $this->bookingdata[$date][$booking->id]['deposits-returned']);
            if ($differenceBetweenDeposits > 0) {
                $this->bookingdata[$date][$booking->id]['accommodation-actual'] -= $differenceBetweenDeposits;
            }
            $this->bookingdata[$date][$booking->id]['accommodation-actual'] -= $this->bookingdata[$date][$booking->id]['other'];

        }
    }

    private function calculateTotals() {
        foreach ($this->bookingdata as $key => $date) {
            foreach ($date as $booking) {
                $this->totals[$key]['accommodation-actual'] += $booking['accommodation-actual'];
                $this->totals[$key]['accommodation-expected'] += $booking['accommodation-expected'];
                $this->totals[$key]['deposits-taken'] += $booking['deposits-taken'];
                $this->totals[$key]['deposits-returned'] += $booking['deposits-returned'];
                $this->totals[$key]['other'] += $booking['other'];

            }
        }
    }

    private function calculateGrandTotals() {
        foreach ($this->totals as $date) {
            $this->grandtotals['accommodation-actual'] += $date['accommodation-actual'];
            $this->grandtotals['accommodation-expected'] += $date['accommodation-expected'];
            $this->grandtotals['deposits-taken'] += $date['deposits-taken'];
            $this->grandtotals['deposits-returned'] += $date['deposits-returned'];
            $this->grandtotals['other'] += $date['other'];
        }
    }

    private function changeZeroesToDashes() {
        array_walk_recursive($this->totals, function(&$item, &$key) {
            if ($item == 0) {
                $item = "-";
            } else if ($key != "date") {
                $item = MoneyHelper::convertCentsToDollars($item);
            }
        });

        array_walk_recursive($this->bookingdata, function(&$item, &$key) {
            if (($item == 0) && ($key != "lastname")) {
                $item = "-";
            } else if (($key != "lastname") && ($key != "bookingid")) {
                $item = MoneyHelper::convertCentsToDollars($item);
            }
        });

        array_walk_recursive($this->grandtotals, function(&$item) {
                $item = MoneyHelper::convertCentsToDollars($item);
        });

    }
    
}