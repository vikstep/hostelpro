<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomType extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'room_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'type', 'gender', 'number_of_guests'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['hostel_id'];

    protected $dates = ['deleted_at'];

    public $timestamps = false;

    public function rooms() {
        return $this->hasMany('App\HostelPro\Models\Room', 'room_type_id', 'id')->orderBy('sort', 'ASC');
    }

    public function getNonEmptyRoomTypes() {
        return $this->hasMany('App\HostelPro\Models\Room', 'room_type_id', 'id')->where();
    }

}