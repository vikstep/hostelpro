<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\HostelPro\Jobs\SyncRatesJob;

class SeasonRate extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'season_rates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    public function hostel() {
        return $this->belongsTo(Hostel::class);
    }

    public function getFormattedStartDate() {
        return DateHelper::convertYearMonthDayToDate($this->start_date);
    }

    public function getFormattedEndDate() {
        return DateHelper::convertYearMonthDayToDate($this->end_date);
    }

    public function updateDates($startdate, $enddate) {
        $this->start_date = DateHelper::convertDateToYearMonthDay($startdate);
        $this->end_date = DateHelper::convertDateToYearMonthDay($enddate);
        $this->save();
    }

    public function updateRates($prices, $nights) {
        DB::beginTransaction();

        $dates = DateHelper::getArrayOfDatesByString($this->start_date, $this->end_date);
        $room_type_ids = RoomType::where('hostel_id', '=', $this->hostel_id)->pluck('id')->all();

        $dataToSave = array();

        foreach ($room_type_ids as $room_type_id) {
            foreach ($dates as $date) {
                $day_of_week = DateHelper::getDayOfWeek($date);
                array_push($dataToSave, array(
                    'room_type_id' => $room_type_id,
                    'date' => $date,
                    'price' => MoneyHelper::convertToCents($prices[$room_type_id][$day_of_week]),
                    'min_stay' => $nights[$room_type_id][$day_of_week])
                );
            }
        }

        Rate::whereIn('date', $dates)->whereIn('room_type_id', $room_type_ids)->delete();
        Rate::insert($dataToSave);

        $dataToSave = array();

        foreach ($prices as $room_type_id => $weeklyPrice) {
            $weeklyPriceInCents = array();
            
            foreach ($weeklyPrice as $day => $price){
                $weeklyPriceInCents[$day] = MoneyHelper::convertToCents($price);    
            }
            
            array_push($dataToSave, array(
                    'room_type_id' => $room_type_id,
                    'season_id' => $this->id,
                    'rates' => serialize($weeklyPriceInCents),
                    'min_stay' => serialize($nights[$room_type_id])
                )
            );
        }

        DailySeasonRate::where('season_id', $this->id)->whereIn('room_type_id', $room_type_ids)->delete();
        DailySeasonRate::insert($dataToSave);
        DB::commit();

        if (MyAllocatorToken::where('hostel_id', '=', $this->hostel_id)->count() > 0) {
            dispatch(new SyncRatesJob($this->hostel_id, $this->start_date, $this->end_date));
        }
    }

}