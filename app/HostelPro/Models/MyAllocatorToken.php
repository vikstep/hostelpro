<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class MyAllocatorToken extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'myallocator_token';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['user_token'];

    protected $primaryKey = 'hostel_id';

    public $timestamps = true;

    public $incrementing = false;

    public function hostel() {
        return $this->belongsTo('App\HostelPro\Models\Hostel', 'hostel_id', 'id');
    }

    public function saveTokenDetails($hostel_id, $token, $property_id) {
        MyAllocatorToken::where('hostel_id', '=', $hostel_id)->delete();
        MyAllocatorToken::where('myallocator_id', '=', $property_id)->delete();
        $this->attributes['hostel_id'] = $hostel_id;
        $this->attributes['user_token'] = $token;
        $this->attributes['myallocator_id'] = $property_id;
        $this->save();
    }

}