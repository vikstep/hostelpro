<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class BoardUnallocated extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'board_unallocated';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public function booking() {
        return $this->belongsTo('App\HostelPro\Models\Booking', 'booking_id', 'id');
    }

}