<?php namespace App\HostelPro\Models;

use DB;
use Auth;

class DayViewReport {

    public $hostel_id;
    public $date;
    public $formattedDate;
    public $yesterday;

    public $departing = array();
    public $changing_rooms = array();
    public $arriving = array();
    public $remaining = array();
    public $blocked = array();

    public $totals = array();

    public function __construct($hostel_id, $date) {

        $this->hostel_id = $hostel_id;
        $this->date = $date;
        $this->formattedDate = DateHelper::convertStringDateToCarbon($date)->format('l jS \\of F Y');
        $this->yesterday = DateHelper::subDays($this->date, 1);

        $this->generateReport();
    }

    private function generateReport() {

        $bookings_today = $this->getAllBookingsForDate($this->date);
        $bookings_yesterday = $this->getAllBookingsForDate($this->yesterday);

        $this->departing = $bookings_yesterday->diff($bookings_today)->reject(function ($booking) {
            return $booking->status == 'Blocked Room';
        });
        $this->arriving = $bookings_today->diff($bookings_yesterday)->reject(function ($booking) {
            return $booking->status == 'Blocked Room';
        });
        $this->remaining = $bookings_today->intersect($bookings_yesterday);
        $this->remaining = $this->remaining->filter(function ($item) {
            return $item->status != 'Blocked Room';
        });
        $this->blocked = $bookings_today->where('status', 'Blocked Room');

        $this->changing_rooms = $this->remaining->filter(function ($item) use ($bookings_yesterday) {

            $id = $item->id;
            $room_id = $item->room_id;
            return $bookings_yesterday->contains(function ($booking) use ($id, $room_id) {

                if (($booking->id == $id) && ($booking->status != "Multi-Room Booking") && ($booking->room_id != $room_id)) {
                    return true;
                };
                return false;
            });

        });

        $totals = array();
        $totals['departing'] = $this->departing->sum('number_of_guests');
        $totals['changing_rooms'] = $this->changing_rooms->sum('number_of_guests');
        $totals['arriving'] = $this->arriving->sum('number_of_guests');
        $totals['remaining'] = $this->remaining->sum('number_of_guests');
        $totals['blocked'] = $this->blocked->sum('number_of_guests');
        $this->totals = $totals;

    }

    private function getAllBookingsForDate($date) {
        $allocated_bookings = Booking::select(
            'booking.id',
            DB::raw('COALESCE(label_types.type,\'Booking - Unpaid\') AS status'),
            'guest.firstname',
            'guest.lastname',
            'board.date',
            'board.room_id',
            DB::raw('room.name AS room_name'),
            DB::raw('GROUP_CONCAT(bed_number order by bed_number ASC) AS bed'),
            DB::raw('NULL as room_type_id'),
            DB::raw('COUNT(board.date) as number_of_guests'))
            ->where('booking.hostel_id', '=', $this->hostel_id)
            ->join('stay', 'stay.booking_id', '=', 'booking.id')
            ->join('board', 'board.stay_id', '=', 'stay.id')
            ->join('guest', 'guest.id', '=', 'booking.guest_id')
            ->join('room', 'board.room_id', '=', 'room.id')
            ->leftJoin('label_types', 'booking.label_type_id', '=', 'label_types.id')
            ->where('board.date', '=', $date)
            ->with('nonTempStays', 'payments', 'items')
            ->groupBy('board.room_id', 'booking.id')
            ->orderBy('board.room_id', 'bed');

        $all_bookings = Booking::select(
            'booking.id',
            DB::raw('COALESCE(label_types.type,\'Booking - Unpaid\') AS status'),
            'guest.firstname',
            'guest.lastname',
            'board_unallocated.date',
            DB::raw('NULL as room_id'),
            DB::raw('CONCAT(\'TBD - \',room_types.name) AS room_name'),
            DB::raw('NULL AS bed'),
            'board_unallocated.room_type_id',
            'board_unallocated.number_of_guests as number_of_guests')
            ->where('booking.hostel_id', '=', $this->hostel_id)
            ->join('board_unallocated', 'board_unallocated.booking_id', '=', 'booking.id')
            ->join('guest', 'guest.id', '=', 'booking.guest_id')
            ->join('room_types', 'room_types.id', '=', 'board_unallocated.room_type_id')
            ->leftJoin('label_types', 'booking.label_type_id', '=', 'label_types.id')
            ->where('board_unallocated.date', '=', $date)
            ->with('nonTempStays', 'payments', 'items')
            ->groupBy('board_unallocated.room_type_id', 'booking.id')
            ->orderBy('board_unallocated.room_type_id')
            ->union($allocated_bookings)
            ->get();

        $room_order = array_flip(Hostel::find($this->hostel_id)->rooms->pluck('id')->all());
        
        $all_bookings = $all_bookings->sortBy(function ($booking) use ($room_order) {
            return $room_order[$booking->room_id] ?? 1;
        });

        return $all_bookings;
    }


}