<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class MyAllocatorLog extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'myallocator_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = true;

}