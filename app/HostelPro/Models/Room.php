<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'room';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'room_type_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    protected $dates = ['deleted_at'];

    public $timestamps = false;

    public function roomtype() {
        return $this->belongsTo('App\HostelPro\Models\RoomType', 'room_type_id', 'id')->withTrashed();
    }

    public function isPrivateRoom() {
        return $this->roomtype->type == "Private" ?? false;
    }

}