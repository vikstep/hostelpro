<?php namespace App\HostelPro\Models;

use DB;
use MyAllocatorApi\RoomList;
use MyAllocatorApi\RoomCreate;
use MyAllocatorApi\RoomUpdate;

class MyAllocatorSyncRooms {

    private $myAllocatorRoomTypes;
    private $rooms;
    private $hostelId;

    public function __construct($hostelId) {
        $this->hostelId = $hostelId;
        $api = new RoomList($hostelId);
        $rsp = $api->callApi();

        if (!empty($rsp['response']['body']['RoomTypes'])) {
            $this->myAllocatorRoomTypes = $rsp['response']['body']['RoomTypes'];
        }

        $room_ids = RoomType::where('hostel_id', '=', $hostelId)->pluck('id')->all();
        $this->rooms = Room::join('room_types', 'room.room_type_id', '=', 'room_types.id')
            ->select('number_of_guests', 'room_type_id', 'type', 'gender', DB::raw('count(*) as number_of_rooms'))
            ->whereIn('room_type_id', $room_ids)
            ->groupBy('room_type_id')
            ->get();
    }

    public function sync() {

        if (empty($this->myAllocatorRoomTypes)) {
            return false;
        }

        foreach ($this->myAllocatorRoomTypes as $myAllocatorRoomType) {
            $this->syncExistingMyAllocatorRoomToHostelPro($myAllocatorRoomType);
        }

        /*
        foreach ($this->rooms as $index => $room) {
            $this->syncExistingHostelProRoomToMyAllocator($room, $index);
        }*/

        $successfulSync = true;

        foreach ($this->rooms as $room) {
            if (empty($room->roomtype->myallocator_id)) {
                $successfulSync = false;
            }
        }

        return $successfulSync;
    }

    private function syncExistingHostelProRoomToMyAllocator($room, $index) {
        $gender = self::formatGenderToMyAllocator($room['gender']);
        $privateRooms = self::formatRoomTypeToMyAllocator($room['type']);
        $myAllocatorId = $this->createRoomOnMyAllocator($this->hostelId, $room['number_of_rooms'], $room['number_of_guests'], $gender, $room->roomtype->name, $privateRooms);
        if ($myAllocatorId) {
            $room->myallocator_id = $myAllocatorId;
            $room->save();
        }
    }

    private function syncExistingMyAllocatorRoomToHostelPro($myAllocatorRoomType) {
        $number_of_rooms = $myAllocatorRoomType['Units'];
        $gender = self::formatGenderToHostelPro($myAllocatorRoomType['Gender']);
        $occupancy = $myAllocatorRoomType['Occupancy'];
        $type = self::formatRoomTypeToHostelPro($myAllocatorRoomType['PrivateRoom']);
        $exists = false;
        foreach ($this->rooms as $key => $room) {
            if (($room['number_of_guests'] == $occupancy) && ($room['type'] == $type) && ($room['gender'] == $gender)) {
                $exists = true;
                if ($number_of_rooms > $room['number_of_rooms']) {
                    //If there are more of this type on myallocator, update in HostelPro
                    $this->createNewRooms($room['room_type_id'], ($number_of_rooms - $room['number_of_rooms']));
                } else if ($room['number_of_rooms'] > $number_of_rooms) {
                    //If there are more of this type in HostelPro, update on MyAllocator
                    $this->updateRoomOnMyAllocator($this->hostelId, $myAllocatorRoomType['RoomId'], $room['number_of_rooms'], $occupancy, $myAllocatorRoomType['Gender'], $myAllocatorRoomType['PrivateRoom']);
                }
                $room->roomtype->myallocator_id = $myAllocatorRoomType['RoomId'];
                $room->push();
                //We've done everything we need to with this room, so remove from Array
                unset($this->rooms[$key]);

                break;
            }
        }
        if ($exists == false) {
            //Room type doesn't exist, so create the room type in HostelPro then create the appropriate number of rooms in HostelPro
            $this->createNewRoomTypeAndRooms($occupancy, $gender, $type, $myAllocatorRoomType['RoomId'], $number_of_rooms);
        }
    }

    private function createRoomOnMyAllocator($hostelId, $units, $occupancy, $gender, $label, $privateRooms) {
        $api = new RoomCreate($hostelId);
        $roomToSend = array(
            'Units' => $units,
            'Occupancy' => (int)$occupancy,
            'Gender' => $gender,
            'Label' => $label,
            'PrivateRoom' => $privateRooms
        );
        $params = array(
            'Room' => $roomToSend,
            'Rooms' => $roomToSend,
        );
        $rsp = $api->callApiWithParams($params);
        if (isset($rsp['body']['Rooms'][0]['RoomId'])) {
            return $rsp['body']['Rooms'][0]['RoomId'];
        }
        //Maybe add some more error checking here later
        return null;
    }

    private function updateRoomOnMyAllocator($hostelId, $roomId, $units, $occupancy, $gender, $privateRooms) {
        $api = new RoomUpdate($hostelId);
        $roomToSend = array(
            'RoomId' => $roomId,
            'Units' => $units,
            'Occupancy' => (int)$occupancy,
            'Gender' => $gender,
            'PrivateRoom' => $privateRooms
        );
        $params = array(
            'Room' => $roomToSend,
            'Rooms' => $roomToSend,
        );
        $rsp = $api->callApiWithParams($params);
        //Maybe add some error checking here later
    }

    private function createNewRoomTypeAndRooms($occupancy, $gender, $type, $myallocator_id, $number_of_rooms) {
        $newRoomType = new RoomType();
        $newRoomType->name = $occupancy . " bed " . $gender;
        $newRoomType->type = $type;
        $newRoomType->gender = $gender;
        $newRoomType->number_of_guests = $occupancy;
        $newRoomType->hostel_id = $this->hostelId;
        $newRoomType->myallocator_id = $myallocator_id;
        $newRoomType->save();
        $this->createNewRooms($newRoomType->id, $number_of_rooms);
    }

    private function createNewRooms($room_type_id, $number_of_rooms_to_create) {
        for ($i = 1; $i <= $number_of_rooms_to_create; $i++) {
            $newRoom = new Room();
            $newRoom->name = 'MyAllocator Room ' . $i . ' (Imported)';
            $newRoom->room_type_id = $room_type_id;
            $newRoom->save();
        }
    }

    public static function formatGenderToMyAllocator($gender) {
        if ($gender == 'Mixed') {
            return ('MI');
        }
        if ($gender == 'Male') {
            return ('MA');
        }
        if ($gender == 'Female') {
            return ('FE');
        }
        return 'Unknown_Input_Format';
    }

    public static function formatGenderToHostelPro($gender) {
        if ($gender == 'MI') {
            return ('Mixed');
        }
        if ($gender == 'MA') {
            return ('Male');
        }
        if ($gender == 'FE') {
            return ('Female');
        }
        return 'Unknown_Input_Format';
    }

    public static function formatRoomTypeToMyAllocator($privateRoom) {
        if ($privateRoom == "Private") {
            return ("true");
        }
        if ($privateRoom == "Dorm") {
            return ("false");
        }
        return 'Unknown_Input_Format';
    }

    public static function formatRoomTypeToHostelPro($privateRoom) {
        if ($privateRoom == "true") {
            return ('Private');
        }
        if ($privateRoom == "false") {
            return ('Dorm');
        }
        return 'Unknown_Input_Format';
    }

}