<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class AdditionalItem extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'additional_item';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = true;

    public function user() {
        return $this->hasOne('App\HostelPro\Models\User', 'id', 'user_id')->withTrashed();
    }

    public function getCreatedAtAttribute($date) {
        $newtime = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        $newtime->timezone(Auth::user()->currenthostel->timezone->timezone);
        return $newtime;
    }

}