<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class Stay extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stay';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['booking_id', 'room_id', 'start_date', 'end_date', 'number_of_guests', 'price_per_night'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['booking_id', 'room_id'];

    public function booking() {
        return $this->belongsTo('App\HostelPro\Models\Booking', 'booking_id', 'id');
    }

    public function scopeNonTemp($query) {
        return $query->where('temp', '=', '0');
    }

    public function scopeTemp($query) {
        return $query->where('temp', '=', '1');
    }

    public function room() {
        return $this->hasOne('App\HostelPro\Models\Room', 'id', 'room_id')->withTrashed();
    }

    public function getNumberOfNightsAttribute() {
        return DateHelper::countNumberOfDays($this->start_date, $this->end_date);
    }

    public function getPrettyStartDateAttribute() {
        return DateHelper::convertYearMonthDayToDate($this->start_date);
    }

    public function getPrettyEndDateAttribute() {
        return DateHelper::convertYearMonthDayToDate(DateHelper::addDays($this->end_date,1));
    }

    public function moveTo($date, $room_id, $bed_number) {

        $number_of_nights = DateHelper::countNumberOfDays($this->start_date, $this->end_date);
        $dates = DateHelper::getArrayOfDatesByString($date, DateHelper::addDays($date, $number_of_nights - 1));
        $new_start_date = $dates[0];
        $new_end_date = $dates[count($dates)-1];
        $beds = range($bed_number, $bed_number + $this->number_of_guests - 1);

        if (!$this->temp) {
            $count = Board::where('room_id', '=', $room_id)->whereIn('date', $dates)->whereIn('bed_number', $beds)->where('stay_id', '!=', $this->id)->count();
            if ($count > 0) {
                return false;
            }
        } else {
            BoardTemp::clearTempStays($room_id, $new_start_date, $new_end_date, $bed_number, $bed_number + $this->number_of_guests - 1);
        }

        $inserts = array();

        foreach ($dates as $date) {
            foreach ($beds as $bed) {
                array_push($inserts, [
                    'date' => $date,
                    'room_id' => $room_id,
                    'bed_number' => $bed,
                    'stay_id' => $this->id,
                    'hostel_id' => $this->booking->hostel_id
                ]);
                if ($this->temp) {
                    $inserts[count($inserts)-1]['booking_id'] = $this->booking_id;
                }
            }
        }

        if (!$this->temp) {
            Board::where('stay_id', '=', $this->id)->delete();
            Board::insert($inserts);
        } else {
            BoardTemp::where('stay_id', '=', $this->id)->delete();
            BoardTemp::insert($inserts);
        }

        $this->start_date = $dates[0];
        $this->end_date = $dates[count($dates)-1];
        $this->room_id = $room_id;
        $this->save();
        return true;
    }

}