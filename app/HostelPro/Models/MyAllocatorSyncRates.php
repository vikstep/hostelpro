<?php namespace App\HostelPro\Models;

use DB;
use MyAllocatorApi\AriUpdate;

class MyAllocatorSyncRates {

    public function __construct($hostelId, $startdate, $enddate, $room_type_ids = array()) {
        $api = new AriUpdate($hostelId);

        if(empty($room_type_ids))
        {
            $room_type_ids = RoomType::where('hostel_id', '=', $hostelId)->pluck('id')->all();
        }  
        
        $dates = DateHelper::getArrayOfDatesByString($startdate, $enddate);

        $rates = Rate::whereIn('date', $dates)->whereIn('room_type_id', $room_type_ids)->where('price', '>', '0')->get();
        $allocationsToSend = array();
        foreach ($rates as $rate) {
            if (isset($rate->roomtype->myallocator_id)) {
                $data = array('RoomId' => $rate->roomtype->myallocator_id,
                    'StartDate' => $rate->date,
                    'EndDate' => $rate->date,
                    'MinStay' => $rate->min_stay,
                    'Price' => $rate->formattedPrice());
                array_push($allocationsToSend, $data);
            }
        }

        #dd($allocationsToSend);

        $params = array(
            'Channels' => ['all'],
            'Allocations' => $allocationsToSend
        );
        $rsp = $api->callApiWithParams($params);
        #dd($rsp);
    }

}