<?php namespace App\HostelPro\Models;

use DB;
use Auth;
use Carbon;

class ReportPNL {

    private $hostel_id;

    public $months;

    public $revenue = array();

    public $expenses = array();
    public $expenseTotals = array();

    public $yearToDate = array();

    public $grossprofit = array();

    public function __construct($hostel_id, $year, $start = null, $end = null) {
        $this->hostel_id = $hostel_id;
        
        if(!$start)
        {
            $start = $this->getStartDate($year);
        }
        
        if(!$end)
        {
            $end = $this->getEndDate($year);
        }        
        
        $this->months = $this->getMonthsBetweenDates($start, $end);

        $revenue = $this->calculateRevenue($start, $end);
        $expenses = $this->calculateExpenses($start, $end);

        $this->yearToDate['revenue'] = array_sum($revenue);
        $this->yearToDate['expenses'] = array_sum($this->expenseTotals);
        $this->yearToDate['gross'] = $this->yearToDate['revenue'] - $this->yearToDate['expenses'];

        foreach ($this->months as $month) {
            $this->grossprofit[$month] = $revenue[$month] - $this->expenseTotals[$month];
        }

        $this->revenueRaw = $this->formatRevenue2($revenue);
        $this->grossprofitRaw = $this->formatRevenue2($this->grossprofit);
        $this->expensetotalsRaw = $this->formatRevenue2($this->expenseTotals);

        $this->revenue = $this->formatRevenue($revenue);
        $this->expenses = $this->formatRevenue($expenses);
        $this->expenseTotals = $this->formatRevenue($this->expenseTotals);
        $this->yearToDate = $this->formatRevenue($this->yearToDate);
        $this->grossprofit = $this->formatRevenue($this->grossprofit);
    }

    private function getStartDate($year) {
        $start = Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->currenthostel->created_at)->modify('first day of this month')->toDateString();
        $startOfYear = $year . "-01-01";
        if ($start < $startOfYear) {
            return $startOfYear;
        };
        return $start;
    }

    private function getEndDate($year) {
        if ($year == date('Y')) {
            //set to last day of this month
            return date('Y-m-t');
        } else {
            return $year . "-12-31";
        }
    }

    private function getMonthsBetweenDates($start, $end) {
        $months = array();
        $start = Carbon::createFromFormat('Y-m-d', $start);
        $end = Carbon::createFromFormat('Y-m-d', $end);

        while($start->diffInSeconds($end, false) > 0) {
            array_push($months, $start->format('M'));
            $start->modify('first day of next month');
        }

        return $months;
    }

    private function calculateRevenue($start, $end) {
        $revenue = array();

        foreach ($this->months as $month) {
            $revenue[$month] = 0;
        }

        $payments = $this->getPayments($start, $end);

        foreach ($payments as $payment) {
            $revenue[$payment->month] = $payment->monthTotal;
        }

        $deposits = $this->getDeposits($start, $end);
        foreach ($deposits as $deposit) {
            $revenue[$deposit->month] -= $deposit->monthTotal;
        }

        return $revenue;
    }

    private function getPayments($start, $end) {
        return Payment::select(DB::raw('DATE_FORMAT(CAST(payment.created_at AS DATE),\'%b\') as month'), DB::raw('SUM(payment.total) as monthTotal'))
            ->where('hostel_id', '=', $this->hostel_id)
            ->whereBetween('payment.created_at', [$start . ' 00:00:00', $end . ' 23:59:59'])
            ->groupBy(DB::raw('MONTH(payment.created_at)'))
            ->get();
    }

    private function getDeposits($start, $end) {
        return AdditionalItem::select(DB::raw('DATE_FORMAT(CAST(additional_item.created_at AS DATE),\'%b\') as month'), DB::raw('SUM(additional_item.total) as monthTotal'))
            ->where('hostel_id', '=', $this->hostel_id)
            ->where('deposit', '=', true)
            ->whereBetween('additional_item.created_at', [$start . ' 00:00:00', $end . ' 23:59:59'])
            ->groupBy(DB::raw('MONTH(additional_item.created_at)'))
            ->get();
    }

    private function calculateExpenses($start, $end) {
        $categories = $this->getExpenseCategories($start, $end);

        $expenseArray = array();
        foreach ($categories as $category) {
            foreach ($this->months as $month) {
                $expenseArray[$category][$month] = 0;

            }
        }

        foreach ($this->months as $month) {
            $this->expenseTotals[$month] = 0;
        }

        $expenses = $this->getExpenses($start, $end);

        foreach ($expenses as $expense) {
            $expenseArray[$expense->category][$expense->month] = $expense->monthTotal;
            $this->expenseTotals[$expense->month] += $expense->monthTotal;
        }

        return $expenseArray;
    }

    private function getExpenses($start, $end) {
        return Expense::select('category', DB::raw('DATE_FORMAT(date,\'%b\') as month'), DB::raw('SUM(price) as monthTotal'))
            ->where('hostel_id', '=', $this->hostel_id)
            ->whereBetween('date', [$start, $end])
            ->groupBy('category')
            ->groupBy(DB::raw('MONTH(date)'))
            ->get();
    }

    private function getExpenseCategories($start, $end) {
        return Expense::select('category')
            ->where('hostel_id', '=', $this->hostel_id)
            ->whereBetween('date', [$start, $end])
            ->distinct()
            ->orderBy('category', 'ASC')
            ->pluck('category')
            ->all();
    }

    private function formatRevenue($revenue) {
        array_walk_recursive($revenue, function (&$item, &$key) {
            if ($item == 0) {
                $item = "-";
            } else if ($key != "date") {
                $item = MoneyHelper::convertToEnglishFormatWithoutDecimals(MoneyHelper::convertCentsToDollars($item));
            }
        });
        return $revenue;
    }

    private function formatRevenue2($revenue) {
        array_walk_recursive($revenue, function (&$item, &$key) {
            if ($item == 0) {
                $item = "0";
            } else if ($key != "date") {
                $item = round(MoneyHelper::convertCentsToDollars($item));
            }
        });
        return $revenue;
    }

    public function formM2MReportMonthly(ReportPNL $report, ReportPNL $reportLastYear){
        $data = array();

        $monthKeyCurrent = last($report->months);


        //if we working with first month of the year, we need to get prev month using previous year report
        if(count($report->months) == 1)
        {
            $monthKeyPrev = last($reportLastYear->months);
            $reportForPrevMonth = $reportLastYear;
        }
        else
        {
            $monthKeyPrev = $report->months[count($report->months)-2];
            $reportForPrevMonth = $report;
        }

        //no report for previous month
        if(empty($reportForPrevMonth->months) || !isset($reportForPrevMonth->revenue[$monthKeyPrev]))
        {
            $reportForPrevMonth->revenue[$monthKeyPrev] =
                $reportForPrevMonth->revenueRaw[$monthKeyPrev] =
                    $reportForPrevMonth->expenseTotals[$monthKeyPrev] =
                        $reportForPrevMonth->expensetotalsRaw[$monthKeyPrev] =
                            $reportForPrevMonth->grossprofitRaw[$monthKeyPrev] = 0;

        }

        $revenue = array(
            'prev' => $reportForPrevMonth->revenue[$monthKeyPrev],
            'current' => $report->revenue[$monthKeyCurrent],
            'change' => $report->revenueRaw[$monthKeyCurrent] - $reportForPrevMonth->revenueRaw[$monthKeyPrev],
        );

        $expense = array(
            'prev' => $reportForPrevMonth->expenseTotals[$monthKeyPrev],
            'current' => $report->expenseTotals[$monthKeyCurrent],
            'change' => $report->expensetotalsRaw[$monthKeyCurrent] - $reportForPrevMonth->expensetotalsRaw[$monthKeyPrev],
        );

        $expenseCategories = array();
        foreach ($report->expenses as $key => $values)
        {
            if(isset($reportForPrevMonth->expenses[$key]))
            {
                $lastMonthValue = $reportForPrevMonth->expenses[$key][$monthKeyPrev];
            }
            else
            {
                $lastMonthValue = '-';
            }

            //need to convert
            $change = (float) str_replace(',', '', $values[$monthKeyCurrent]) - 
                (float) str_replace(',', '', $lastMonthValue);
            
            $expenseCategories[$key] = array(
                'prev' => $lastMonthValue,
                'current' =>  $values[$monthKeyCurrent],
                'change' => $change,
            );
        }

        foreach ($reportForPrevMonth->expenses as $key => $values)
        {
            if(isset($expenseCategories[$key]))
            {
                continue;
            }

            if(isset($report->expenses[$key]))
            {
                $value = $report->expenses[$key][$monthKeyPrev];
            }
            else
            {
                $value = '-';
            }

            //need to convert
            $change = (float) str_replace(',', '', $value) -
                (float) str_replace(',', '', $values[$monthKeyPrev]);

            $expenseCategories[$key] = array(
                'prev' => $values[$monthKeyPrev],
                'current' =>  $value,
                'change' => $change,
            );
        }
        
        //data for charts
        $raw = array(
            'revenueCurrent' => $report->revenueRaw[$monthKeyCurrent],
            'revenuePrev' => $reportForPrevMonth->revenueRaw[$monthKeyPrev],
            'expensesCurrent' => $report->expensetotalsRaw[$monthKeyCurrent],
            'expensesPrev' => $reportForPrevMonth->expensetotalsRaw[$monthKeyPrev],
            'grossprofitCurrent' => $report->grossprofitRaw[$monthKeyCurrent],
            'grossprofitPrev' => $reportForPrevMonth->grossprofitRaw[$monthKeyPrev],
        );

        $expense['categories'] = $expenseCategories;
        $data['expense'] = $expense;
        $data['revenue'] = $revenue;
        $data['raw'] = $raw;

        return $data;
    }
    
    public function formM2MReportYearly(ReportPNL $reportCurrent, ReportPNL $reportLastYear){
        $data = array();

        $monthKey = last($reportCurrent->months);

        //no report for previous year
        if(empty($reportLastYear->months) || !isset($reportLastYear->revenue[$monthKey]))
        {
            $reportLastYear->revenue[$monthKey] =
                $reportLastYear->revenueRaw[$monthKey] =
                    $reportLastYear->expenseTotals[$monthKey] =
                        $reportLastYear->expensetotalsRaw[$monthKey] =
                            $reportLastYear->grossprofitRaw[$monthKey] = 0;

        }

        $revenue = array(
            'prev' => $reportLastYear->revenue[$monthKey],
            'current' => $reportCurrent->revenue[$monthKey],
            'change' => $reportCurrent->revenueRaw[$monthKey] - $reportLastYear->revenueRaw[$monthKey],
        );

        $expense = array(
            'prev' => $reportLastYear->expenseTotals[$monthKey],
            'current' => $reportCurrent->expenseTotals[$monthKey],
            'change' => $reportCurrent->expensetotalsRaw[$monthKey] - $reportLastYear->expensetotalsRaw[$monthKey],
        );

        $expenseCategories = array();
        foreach ($reportCurrent->expenses as $key => $values)
        {
            if(isset($reportLastYear->expenses[$key]))
            {
                $lastYearValue = $reportLastYear->expenses[$key][$monthKey];
            }
            else
            {
                $lastYearValue = '-';
            }
            
            //need to convert
            $change = (float) str_replace(',', '', $values[$monthKey]) -
                (float) str_replace(',', '', $lastYearValue);

            $expenseCategories[$key] = array(
                'prev' => $lastYearValue,
                'current' =>  $values[$monthKey],
                'change' => $change,
            );
        }

        foreach ($reportLastYear->expenses as $key => $values)
        {
            if(isset($expenseCategories[$key]))
            {
                continue;
            }

            if(isset($reportCurrent->expenses[$key]))
            {
                $value = $reportCurrent->expenses[$key][$monthKey];
            }
            else
            {
                $value = '-';
            }

            //need to convert
            $change = (float) str_replace(',', '', $value) -
                (float) str_replace(',', '', $values[$monthKey]);

            $expenseCategories[$key] = array(
                'prev' => $values[$monthKey],
                'current' =>  $value,
                'change' => $change,
            );
        }

        //data for charts
        $raw = array(
            'revenueCurrent' => $reportCurrent->revenueRaw[$monthKey],
            'revenuePrev' => $reportLastYear->revenueRaw[$monthKey],
            'expensesCurrent' => $reportCurrent->expensetotalsRaw[$monthKey],
            'expensesPrev' => $reportLastYear->expensetotalsRaw[$monthKey],
            'grossprofitCurrent' => $reportCurrent->grossprofitRaw[$monthKey],
            'grossprofitPrev' => $reportLastYear->grossprofitRaw[$monthKey],
        );

        $expense['categories'] = $expenseCategories;
        $data['expense'] = $expense;
        $data['revenue'] = $revenue;
        $data['raw'] = $raw;

        return $data;
    }

}