<?php namespace App\HostelPro\Models;

//use Illuminate\Database\Eloquent\Model;
//use App\HostelPro\Models\RoomType;

class Guest extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'guest';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname', 'lastname', 'email', 'phone', 'country_id',
        'city_id', 'address_line_1', 'address_line_2', 'zip_code', 'gender', 'guest_notes',
        'passport_number', 'passport_country_id', 'date_of_birth', 'place_of_birth', 'passport_issue_date', 'passport_expiry_date', 'invoice_number'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['hostel_id'];

    protected $nullable = ['email', 'phone', 'country_id', 'city_id', 'address_line_1', 'address_line_2', 'zip_code', 'gender', 'guest_notes',
        'passport_number', 'passport_country_id', 'date_of_birth', 'place_of_birth', 'passport_issue_date', 'passport_expiry_date'];

    public function city() {
        return $this->hasOne('App\HostelPro\Models\City', 'geonameid', 'city_id');
    }

    public function getCityNameAttribute() {
        if (!$this->attributes['city_id']) {
            return null;
        }
        return $this->city->name;
    }

    public function setDateOfBirthAttribute($value) {
        if (empty($value)) {
            $this->attributes['date_of_birth'] = null;
        } else {
            $this->attributes['date_of_birth'] = DateHelper::convertDateToYearMonthDay($value);
        }
    }

    public function setPassportIssueDateAttribute($value) {
        if (empty($value)) {
            $this->attributes['passport_issue_date'] = null;
        } else {
            $this->attributes['passport_issue_date'] = DateHelper::convertDateToYearMonthDay($value);
        }
    }

    public function setPassportExpiryDateAttribute($value) {
        if (empty($value)) {
            $this->attributes['passport_expiry_date'] = null;
        } else {
            $this->attributes['passport_expiry_date'] = DateHelper::convertDateToYearMonthDay($value);
        }
    }

    public function getDateOfBirthAttribute($value) {
        return $this->formatDate($value);
    }

    public function getPassportIssueDateAttribute($value) {
        return $this->formatDate($value);
    }

    public function getPassportExpiryDateAttribute($value) {
        return $this->formatDate($value);
    }

    private function formatDate($value) {
        if (empty($value)) {
            return null;
        }
        return DateHelper::convertYearMonthDayToDate($value);
    }

}