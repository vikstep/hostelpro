<?php namespace App\HostelPro\Models;

use DB;

class AllocateBooking {

    private $priority_rooms = array();

    //used to be smartAssignBookingToRooms
    public function __construct($bookingid) {
        $booking = Booking::find($bookingid);

        BoardTemp::where('hostel_id', '=', $booking->hostel_id)->delete();
        Stay::where('booking_id', '=', $booking->id)->delete();

        $unallocated = BoardUnallocated::where('booking_id', '=', $booking->id)->orderBy('room_type_id')->orderBy('date')->get();
        $unallocated_boards = Array();

        /* Begin Temp Fix */
        $overbooking = false;
        foreach ($unallocated as $date) {
            if (Allocation::isOverbooking($date->room_type_id, $date->number_of_guests, $date->date)) {
                $overbooking = true;
            }
        }
        if ($overbooking) {
            $previous = null;
            $previousStay = null;

            foreach ($unallocated as $day) {
                if ($previous != DateHelper::subDays($day->date,1)) {
                    $newStay = new Stay();
                    $newStay->booking_id = $booking->id;
                    $newStay->room_id = null;
                    $newStay->start_date = $day->date;
                    $newStay->end_date = $day->date;
                    $newStay->number_of_guests = $day->number_of_guests;
                    $newStay->price_per_night = $day->price_per_night;
                    $newStay->temp = true;
                    $newStay->save();

                    $previous = $day->date;
                    $previousStay = $newStay;
                } else {
                    $previousStay->end_date = $day->date;
                    $previousStay->save();
                    $previous = $day->date;
                }
            }

            return $booking;
        }
        /* End Temp Fix */

        foreach ($unallocated as $date) {
            $new = true;

            if (end($unallocated_boards) != false) {
                //Pop off the last stay
                $previous = array_pop($unallocated_boards);
                $expected_date = DateHelper::addDays($previous['end_date'], 1);
                if (($date['date'] == $expected_date) && ($previous['room_type_id'] == $date['room_type_id']) && ($previous['number_of_guests'] == $date['number_of_guests']) && ($previous['price_per_night'] == $date['price_per_night'])) {
                    //Everything matches and it is a consecutive day, so increase the end date
                    $previous['end_date'] = $date['date'];
                    $new = false;
                }
                //Push back onto array
                array_push($unallocated_boards, $previous);
            }

            //Create a new stay and add to the array
            if ($new == true) {
                $data = array('start_date' => $date['date'], 'end_date' => $date['date'], 'room_type_id' => $date['room_type_id'], 'number_of_guests' => $date['number_of_guests'], 'price_per_night' => $date['price_per_night'], 'temp' => '1');
                array_push($unallocated_boards, $data);
            }
        }

        foreach ($unallocated_boards as $unallocated_board) {
            $this->smartAssignRoomTypeToRooms($bookingid, $unallocated_board['room_type_id'], $unallocated_board['start_date'], $unallocated_board['end_date'], $unallocated_board['number_of_guests'], $unallocated_board['price_per_night']);
        }

        return $booking;
    }

    private function smartAssignRoomTypeToRooms($bookingid, $room_type_id, $start_date, $end_date, $number_guests_to_assign, $price_per_night) {
        $booking = Booking::find($bookingid);
        $room_ids = Room::where('room_type_id', '=', $room_type_id)->pluck('id')->all();
        $max_capacity_per_room = RoomType::find($room_type_id)->number_of_guests;
        $dates = DateHelper::getArrayOfDatesByString($start_date, $end_date);

        $boards = Board::select('date', 'room_id', 'bed_number')
            ->whereIn('room_id', $room_ids)
            ->whereIn('date', $dates)
            ->orderBy('room_id', 'DESC')
            ->orderBy('bed_number', 'DESC')
            ->get();

        $finalarray = Array();
        foreach ($dates as $date) {
            foreach ($room_ids as $room_id) {
                for ($bed = 1; $bed <= $max_capacity_per_room; $bed++) {
                    $finalarray[$date][$room_id][$bed] = null;
                }
            }
        }

        foreach ($boards as $board) {
            $finalarray[$board->date][$board->room_id][$board->bed_number] = 0;
        }



        foreach ($dates as $date) {
            foreach ($room_ids as $room_id) {
                $current = 0;
                for ($bed = 1; $bed <= $max_capacity_per_room; $bed++) {
                    if (is_null($finalarray[$date][$room_id][$bed])) {
                        $current++;
                        $finalarray[$date][$room_id][$bed] = $current;
                    } else {
                        $current = 0;
                    }
                }
            }
        }

        dd($finalarray);

        $guests_to_assign = Array();
        foreach ($dates as $date) {
            $guests_to_assign[$date] = $number_guests_to_assign;
        }

        foreach ($dates as $date) {
            $max = $guests_to_assign[$date];

            //Check if we are trying to fit more people than possible into a room, eg 10 people into a 8bed dorm.
            if ($max > $max_capacity_per_room) {
                $max = $max_capacity_per_room;
            }

            //While we still have one or more guests to assign, assign the largest
            while ($guests_to_assign[$date] >= 1) {
                if ($max > $guests_to_assign[$date]) {
                    $max = $guests_to_assign[$date];
                }

                $largest = $this->getLargestPossiblePlaces($finalarray, $date, $max);

                if (!empty($largest)) {
                    $largest = $largest[0];
                    #echo "FOUND: " . print_r($largest);

                    array_push($this->priority_rooms, $largest['room']);
                    #echo "<br><br>HERE2 : " . var_dump($priority_rooms);
                    $this->createStayAndAssignToBoard($booking, $largest, $max, $price_per_night);
                    $finalarray = $this->removeAllocatedFromArray($finalarray, $largest, $max);
                    $guests_to_assign = $this->removeGuestsFromArray($guests_to_assign, $largest, $max);
                    //echo "<p>Guests to assign: " . print_r($guests_to_assign) . "</p>";

                } else {
                    $max--;
                }

            }

            //Have assigned all guests for this particular date, so remove it from the array
            unset($finalarray[$date]);
            unset($guests_to_assign[$date]);
        }


    }

    private function createStayAndAssignToBoard(Booking $booking, $details, $number_of_guests, $price_per_night) {
        $stay = new Stay;
        $stay->start_date = $details['date'];
        $stay->end_date = $details['enddate'];
        $stay->room_id = $details['room'];
        $stay->number_of_guests = $number_of_guests;
        $stay->price_per_night = $price_per_night;
        $stay->booking()->associate($booking);
        $stay->save();

        $dates = DateHelper::getArrayOfDatesByString($details['date'], $details['enddate']);

        $data = array();
        foreach ($dates as $date) {
            $bed = ($details['bed'] - $number_of_guests) + 1;
            for ($count = 1; $count <= $number_of_guests; $count++) {
                array_push($data, array('date' => $date, 'room_id' => $details['room'], 'bed_number' => $bed, 'stay_id' => $stay->id, 'booking_id' => $booking->id, 'hostel_id' => $booking->hostel_id));
                $bed++;
            }
        }

        BoardTemp::insert($data);

    }

    private function removeGuestsFromArray(Array $guests, Array $largest, $number_of_guests) {
        $dates = DateHelper::getArrayOfDatesByString($largest['date'], $largest['enddate']);
        foreach ($dates as $index => $date) {
            $guests[$date] = $guests[$date] - $number_of_guests;

        }
        return $guests;
    }

    private function removeAllocatedFromArray(Array $board, Array $largest, $number_of_guests) {
        $dates = DateHelper::getArrayOfDatesByString($largest['date'], $largest['enddate']);
        //dd($largest);
        foreach ($dates as $date) {
            for ($bed = ($largest['bed'] - $number_of_guests) + 1; $bed <= $largest['bed']; $bed++) {
                $board[$date][$largest['room']][$bed] = 0;
            }
        }
        //dd($board);
        return $board;
    }

    private function getLargestPossiblePlaces(Array $board, $date, $number_of_guests) {
        $possible_places = Array();

        foreach ($board[$date] as $room_id => $room) {
            foreach ($room as $bed => $index) {
                if ($board[$date][$room_id][$bed] >= $number_of_guests) {
                    //$result = array('date' => $date, 'room' => $room_id, 'bed' => ($bed-$number_of_guests)+1);
                    $result = array('date' => $date, 'room' => $room_id, 'bed' => $bed, 'enddate' => $date);
                    array_push($possible_places, $result);
                }
            }
        }
        return $this->sortPlacesFromLargeToSmall($board, $possible_places, $number_of_guests);
    }

    private function sortPlacesFromLargeToSmall(Array $board, $possible_places, $number_of_guests) {

        //For each possible place, calculate the max number of days we can spread across
        foreach ($possible_places as $index => $possible_place) {
            //start at zero because we always check the first day first (ie we will always get at least 1)
            $possible_places[$index]['days'] = 0;
            foreach ($board as $date => $value) {
                if ($board[$date][$possible_place['room']][$possible_place['bed']] >= $number_of_guests) {
                    $possible_places[$index]['days']++;
                    $possible_places[$index]['enddate'] = $date;
                } else {
                    break;
                }
            }
        }

        //Sort from largest to smallest
        usort($possible_places, function ($a, $b) {
            if ($b['days'] == $a['days']) {
                if ($a['room'] == $b['room']) {
                    return $a['bed'] - $b['bed'];
                }
                if (in_array($b['room'],$this->priority_rooms)) {
                    return 1;
                }
                if (in_array($a['room'],$this->priority_rooms)) {
                    return 0;
                }
                return $a['bed'] - $b['bed'];
            }
            return $b['days'] - $a['days'];
        });

        #dd($possible_places);

        //Return the reordered array
        return $possible_places;
    }

}