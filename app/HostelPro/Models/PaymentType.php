<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model {

    protected $table = 'payment_types';

    protected $fillable = ['name', 'kept_in_till'];

    protected $hidden = [''];

    public $timestamps = false;

    public function hostel() {
        return $this->belongsTo('App\HostelPro\Models\Hostel', 'hostel_id', 'id');
    }
}