<?php namespace App\HostelPro\Models;

use DB;
use Exception;

class SmartAllocator {

    private $hostel_id;

    private $priority_rooms = array();
    private $board_inserts = array();
    private $booking_ids = array();
    private $board_array = array();
    private $board_unallocateds = array();
    //private $stay_inserts = array();

    private $booking_metadata = array();

    private $room_type_collection;
    private $rooms_collection;

    private $allowed_room_ids;

    private $stay_ids_to_ignore = array();
    private $stay_ids_to_include = array();

//    private $disable_temp_board_clear = false;

    public function __construct($hostel_id) {
        $this->hostel_id = $hostel_id;
    }

    public function addBooking($booking_id) {
        if (in_array($booking_id, $this->booking_ids)) {
            return false;
        }
        array_push($this->booking_ids, $booking_id);
        return true;
    }

    public function addBookingArray(Array $booking_ids) {
        foreach ($booking_ids as $booking_id) {
            $this->addBooking($booking_id);
        }
    }

    public function processAndSave() {
        if (count($this->booking_ids) == 0) {
            return true;
        }
        $this->setupRoomAndRoomTypeCollections();
        $this->setBoardUnallocatedArray();
        $this->clearBoardTempAndStayTables();
        $this->setupBoardArray();

        foreach ($this->booking_ids as $i => $booking_id) {
            $this->processIndividualBooking($booking_id);
        }

        /*
        foreach ($this->board_inserts as $board_insert) {
            try {
                BoardTemp::insert($board_insert);
            } catch (Exception $e) {
                dd($board_insert);
            }
        }*/

        BoardTemp::insert($this->board_inserts);

        return $this->booking_ids;
    }

    public function setAllowedRoomIds(Array $ids) {
        $this->allowed_room_ids = $ids;
    }

    public function setStayIdsToIgnore(Array $ids) {
        $this->stay_ids_to_ignore = $ids;
    }

    public function setStayIdsToInclude(Array $ids) {
        $this->stay_ids_to_include = $ids;
    }

//    public function disableTempBoardClear() {
//        $this->disable_temp_board_clear = true;
//    }

    private function setupRoomAndRoomTypeCollections() {
        $this->room_type_collection = RoomType::where('hostel_id', '=', $this->hostel_id)->get()->keyBy('id');
        if (count($this->allowed_room_ids) > 0) {
            $this->rooms_collection = Room::whereIn('id', $this->allowed_room_ids)->with('roomtype')->get()->keyBy('id');
        } else {
            $this->rooms_collection = Room::whereIn('room_type_id', $this->room_type_collection->pluck('id')->all())->with('roomtype')->get()->keyBy('id');
        }
    }

    private function setupBoardArray() {
        //dd(BoardUnallocated::whereIn('booking_id', $this->booking_ids)->min('date'));
        $min_date = BoardUnallocated::whereIn('booking_id', $this->booking_ids)->min('date');
        $min_date = DateHelper::subDays($min_date, 1);

        $max_date = BoardUnallocated::whereIn('booking_id', $this->booking_ids)->max('date');
        $max_date = DateHelper::addDays($max_date, 1);

        $room_ids = $this->rooms_collection->pluck('id')->all();

        $boards = Board::select('date', 'room_id', 'bed_number')
            ->whereIn('room_id', $room_ids)
            ->whereNotIn('stay_id', $this->stay_ids_to_ignore)
            ->whereBetween('date', [$min_date, $max_date])
            ->orderBy('room_id', 'DESC')
            ->orderBy('bed_number', 'DESC')
            ->get();

        $boards_temp = BoardTemp::select('date', 'room_id', 'bed_number')
            ->whereIn('room_id', $room_ids)
            ->whereIn('stay_id', $this->stay_ids_to_include)
            ->whereBetween('date', [$min_date, $max_date])
            ->orderBy('room_id', 'DESC')
            ->orderBy('bed_number', 'DESC')
            ->get();

        $dates = DateHelper::getArrayOfDatesByString($min_date, $max_date);

        $finalarray = Array();
        foreach ($dates as $date) {
            foreach ($room_ids as $room_id) {
                for ($bed = 1; $bed <= $this->getMaxNumberOfGuestsFromRoomId($room_id); $bed++) {
                    $finalarray[$date][$room_id][$bed] = null;
                }
            }
        }

        foreach ($boards as $board) {
            $finalarray[$board->date][$board->room_id][$board->bed_number] = 0;
        }

        foreach($boards_temp as $board_temp) {
            $finalarray[$board_temp->date][$board_temp->room_id][$board_temp->bed_number] = 0;
        }

        foreach ($dates as $date) {
            foreach ($room_ids as $room_id) {
                $current = 0;
                for ($bed = 1; $bed <= $this->getMaxNumberOfGuestsFromRoomId($room_id); $bed++) {
                    if (is_null($finalarray[$date][$room_id][$bed])) {
                        $current++;
                        $finalarray[$date][$room_id][$bed] = $current;
                    } else {
                        $current = 0;
                    }
                }
            }
        }

        $this->board_array = $finalarray;
    }

    private function getMaxNumberOfGuestsFromRoomId($room_id) {
        return $this->rooms_collection->get($room_id)->roomtype->number_of_guests;
    }

    private function processIndividualBooking($booking_id) {
        $unallocateds = $this->getUnallocatedsForBooking($booking_id);

        if (count($unallocateds) == 0) {
            return false;
        }

        $this->setBookingMetaData($booking_id, $unallocateds);

        if ($this->isOverbooking($unallocateds)) {
            return $this->processAndSaveOverbooking($booking_id, $unallocateds);
        }

        $unallocated_boards = $this->getFormattedAndGroupedUnallocatedBoards($unallocateds);

        foreach ($unallocated_boards as $unallocated_board) {
            $this->smartAssignRoomTypeToRooms($booking_id, $unallocated_board['room_type_id'], $unallocated_board['start_date'], $unallocated_board['end_date'], $unallocated_board['number_of_guests'], $unallocated_board['price_per_night']);
        }

        return true;
    }

    private function setBookingMetaData($booking_id, $unallocateds) {
        $this->booking_metadata[$booking_id]['maxdate'] = array();

        foreach ($unallocateds as $unallocated) {
            $date = $unallocated->date;
            if (!isset($this->booking_metadata[$booking_id]['startdate'])) {
                $this->booking_metadata[$booking_id]['startdate'] = $date;
            }
            $current_maximum_date = $this->booking_metadata[$booking_id]['maxdate'];
            if ((!$current_maximum_date) || (strtotime($date) > strtotime($current_maximum_date))) {
                $this->booking_metadata[$booking_id]['maxdate'] = $date;
            }
        }
    }

    private function getFormattedAndGroupedUnallocatedBoards($unallocateds) {
        $unallocated_boards = Array();

        foreach ($unallocateds as $date) {
            $new = true;

            if (end($unallocated_boards) != false) {
                //Pop off the last stay
                $previous = array_pop($unallocated_boards);
                $expected_date = DateHelper::addDays($previous['end_date'], 1);
                if (($date['date'] == $expected_date) && ($previous['room_type_id'] == $date['room_type_id']) && ($previous['number_of_guests'] == $date['number_of_guests']) && ($previous['price_per_night'] == $date['price_per_night'])) {
                    //Everything matches and it is a consecutive day, so increase the end date
                    $previous['end_date'] = $date['date'];
                    $new = false;
                }
                //Push back onto array
                array_push($unallocated_boards, $previous);
            }

            //Create a new stay and add to the array
            if ($new == true) {
                $data = array('start_date' => $date['date'], 'end_date' => $date['date'], 'room_type_id' => $date['room_type_id'], 'number_of_guests' => $date['number_of_guests'], 'price_per_night' => $date['price_per_night'], 'temp' => '1');
                array_push($unallocated_boards, $data);
            }
        }

        return $unallocated_boards;
    }

    private function clearBoardTempAndStayTables() {

        $dates = array();
        foreach ($this->board_unallocateds as $booking) {
            foreach ($booking as $unallocated) {
                array_push($dates, $unallocated->date);
            }
        }
        $min = min($dates);
        $max = max($dates);

        $booking_ids = BoardTemp::whereIn('room_id', $this->rooms_collection->pluck('id')->all())
            ->where('hostel_id', '=', $this->hostel_id)
            ->whereBetween('date', [$min, $max])
            ->pluck('booking_id')
            ->all();

        BoardTemp::whereIn('booking_id', $booking_ids)->whereNotIn('stay_id', $this->stay_ids_to_include)->delete();

        //if ($this->disable_temp_board_clear != true) {
        /*
            $test = BoardTemp::where('hostel_id', '=', $this->hostel_id)
                ->whereBetween('date', [$min, $max])
                ->whereIn('room_id', $this->rooms_collection->pluck('id')->all())
                ->whereNotIn('stay_id', $this->stay_ids_to_include)
                ->delete();
        */
        //dd("HERE");
        //}
        //Stay::whereIn('booking_id', $this->booking_ids)->delete();
    }

    private function getUnallocatedsForBooking($booking_id) {
        if (!isset($this->board_unallocateds[$booking_id])) {
            return null;
        }
        return $this->board_unallocateds[$booking_id];
    }

    private function setBoardUnallocatedArray() {
        $unallocateds = BoardUnallocated::whereIn('booking_id', $this->booking_ids)->orderBy('room_type_id')->orderBy('date')->get();
        foreach ($unallocateds as $unallocated) {
            if (!isset($this->board_unallocateds[$unallocated->booking_id])) {
                $this->board_unallocateds[$unallocated->booking_id] = array();
            }
            array_push($this->board_unallocateds[$unallocated->booking_id], $unallocated);
        }
    }

    private function getRoomsFromRoomType($room_type_id) {
        return $this->rooms_collection->filter(function ($room) use ($room_type_id) {
            if ($room->room_type_id == $room_type_id) {
                return $room;
            }
        });
    }

    private function getSubSetOfBoardArray($room_type_id, $start_date, $end_date) {
        $room_ids = $this->getRoomsFromRoomType($room_type_id)->pluck('id')->all();

        $array = array_where($this->board_array, function ($value, $key) use ($start_date, $end_date) {
            if (($key >= $start_date) && ($key <= $end_date)) {
                return 1;
            }
            return 0;
        });

        foreach ($array as $date => $room) {
            foreach ($room as $room_id => $bed) {
                if (!in_array($room_id, $room_ids)) {
                    unset($array[$date][$room_id]);
                }
            }
        }

        return $array;
    }

    private function smartAssignRoomTypeToRooms($booking_id, $room_type_id, $start_date, $end_date, $number_guests_to_assign, $price_per_night) {
        $max_capacity_per_room = $this->room_type_collection->get($room_type_id)->number_of_guests;
        $dates = DateHelper::getArrayOfDatesByString($start_date, $end_date);
        $current_array = $this->getSubSetOfBoardArray($room_type_id, $start_date, $end_date);
        #$current_array = $this->getSubSetOfBoardArray($room_type_id, $start_date, $this->booking_metadata[$booking_id]['maxdate']);

        $guests_to_assign = Array();
        foreach ($dates as $date) {
            $guests_to_assign[$date] = $number_guests_to_assign;
        }

        foreach ($dates as $date) {
            $max = $guests_to_assign[$date];

            //Check if we are trying to fit more people than possible into a room, eg 10 people into a 8bed dorm.
            if ($max > $max_capacity_per_room) {
                $max = $max_capacity_per_room;
            }

            //While we still have one or more guests to assign, assign the largest
            while ($guests_to_assign[$date] >= 1) {
                if ($max > $guests_to_assign[$date]) {
                    $max = $guests_to_assign[$date];
                }

                $largest = $this->getLargestPossiblePlaces($current_array, $date, $max, $booking_id);

                if (!empty($largest)) {
                    $largest = $largest[0];

                    $this->addRoomToPriorityRoomsArray($booking_id, $largest['room']);
                    $this->createStayAndAssignToBoard($booking_id, $largest, $max, $price_per_night);
                    $current_array = $this->removeAllocatedFromArray($current_array, $largest, $max);

                    $this->removeBedsFromBoard($largest, $max);
                    $guests_to_assign = $this->removeGuestsFromArray($guests_to_assign, $largest, $max);
                } else {
                    $max--;
                }

            }

            //Have assigned all guests for this particular date, so remove it from the array
            unset($current_array[$date]);
            unset($guests_to_assign[$date]);
        }

    }

    private function addRoomToPriorityRoomsArray($booking_id, $room_id) {
        if (!isset($this->priority_rooms[$booking_id])) {
            $this->priority_rooms[$booking_id] = array();
        }
        array_push($this->priority_rooms[$booking_id], $room_id);
    }

    private function createStayAndAssignToBoard($booking_id, $details, $number_of_guests, $price_per_night) {

        /*
        array_push($this->stay_inserts, array(
            'start_date' => $details['date'],
            'end_date' => $details['enddate'],
            'room_id' => $details['room'],
            'number_of_guests' => $number_of_guests,
            'price_per_night' => $price_per_night,
            'booking_id' => $booking_id
        ));*/

        $stay = new Stay;
        $stay->start_date = $details['date'];
        $stay->end_date = $details['enddate'];
        $stay->room_id = $details['room'];
        $stay->number_of_guests = $number_of_guests;
        $stay->price_per_night = $price_per_night;
        $stay->booking_id = $booking_id;
        $stay->temp = true;
        $stay->save();

        $dates = DateHelper::getArrayOfDatesByString($details['date'], $details['enddate']);

        foreach ($dates as $date) {
            $bed = ($details['bed'] - $number_of_guests) + 1;
            for ($count = 1; $count <= $number_of_guests; $count++) {
                array_push($this->board_inserts, array(
                        'date' => $date,
                        'room_id' => $details['room'],
                        'bed_number' => $bed,
                        'stay_id' => $stay->id,
                        'booking_id' => $booking_id,
                        'hostel_id' => $this->hostel_id)
                );
                $bed++;
            }
        }
    }

    private function removeGuestsFromArray(Array $guests, Array $largest, $number_of_guests) {
        $dates = DateHelper::getArrayOfDatesByString($largest['date'], $largest['enddate']);
        foreach ($dates as $index => $date) {
            $guests[$date] = $guests[$date] - $number_of_guests;

        }
        return $guests;
    }

    private function removeAllocatedFromArray(Array $board, Array $largest, $number_of_guests) {

        $dates = DateHelper::getArrayOfDatesByString($largest['date'], $largest['enddate']);
        /*
        foreach ($dates as $date) {
            for ($bed = ($largest['bed'] - $number_of_guests) + 1; $bed <= $largest['bed']; $bed++) {
                $board[$date][$largest['room']][$bed] = 0;
            }
        }*/


        foreach ($dates as $date) {
            $counter = 1;
            for ($bed = ($largest['bed'] - $number_of_guests) + 1; $bed <= $this->getMaxNumberOfGuestsFromRoomId($largest['room']); $bed++) {
                if ($bed <= $largest['bed']) {
                    //This is one of the beds we just allocated, so set it to zero
                    $board[$date][$largest['room']][$bed] = 0;
                } else if ($board[$date][$largest['room']][$bed] != 0) {
                    $board[$date][$largest['room']][$bed] = $counter;
                    $counter++;
                } else {
                    //We reached a divider, so we no longer need to adjust the bed numbers - break out of loop.
                    break;
                }
            }
        }

        return $board;
    }

    private function removeBedsFromBoard($details, $number_of_guests) {
        $dates = DateHelper::getArrayOfDatesByString($details['date'], $details['enddate']);

        foreach ($dates as $date) {
            $counter = 1;
            for ($bed = ($details['bed'] - $number_of_guests) + 1; $bed <= $this->getMaxNumberOfGuestsFromRoomId($details['room']); $bed++) {
                if ($bed <= $details['bed']) {
                    //This is one of the beds we just allocated, so set it to zero
                    $this->board_array[$date][$details['room']][$bed] = 0;
                } else if ($this->board_array[$date][$details['room']][$bed] != 0) {
                    $this->board_array[$date][$details['room']][$bed] = $counter;
                    $counter++;
                } else {
                    //We reached a divider, so we no longer need to adjust the bed numbers - break out of loop.
                    break;
                }
            }
        }

    }

    private function getLargestPossiblePlaces(Array $board, $date, $number_of_guests, $booking_id) {
        $possible_places = Array();

        foreach ($board[$date] as $room_id => $room) {
            foreach ($room as $bed => $index) {
                if ($board[$date][$room_id][$bed] >= $number_of_guests) {
                    //$result = array('date' => $date, 'room' => $room_id, 'bed' => ($bed-$number_of_guests)+1);
                    $result = array('date' => $date, 'room' => $room_id, 'bed' => $bed, 'enddate' => $date);
                    array_push($possible_places, $result);
                }
            }
        }

        return $this->sortPlacesFromLargeToSmall($board, $possible_places, $number_of_guests, $booking_id);
    }

    private function sortPlacesFromLargeToSmall(Array $board, $possible_places, $number_of_guests, $booking_id) {

        #dd($possible_places);
        $booking_maximum_date = $this->booking_metadata[$booking_id]['maxdate'];

        #dd($this->board_array);

        //For each possible place, calculate the max number of days we can spread across
        foreach ($possible_places as $index => $possible_place) {
            //start at zero because we always check the first day first (ie we will always get at least 1)
            $possible_places[$index]['days'] = 0;
            foreach ($board as $date => $value) {
                if ($board[$date][$possible_place['room']][$possible_place['bed']] >= $number_of_guests) {
                    $possible_places[$index]['days']++;
                    $possible_places[$index]['enddate'] = $date;
                } else {
                    break;
                }
            }
        }

        foreach ($possible_places as $index => $possible_place) {
            $possible_places[$index]['maxdays'] = 0;
            $start_date = $possible_place['date'];
            $days = DateHelper::getDaysBetween($start_date, $booking_maximum_date);
            $dates = DateHelper::getArrayOfDatesByString($start_date, $booking_maximum_date);
            for ($day = 0; $day <= $days; $day++) {
                if ($this->board_array[$dates[$day]][$possible_place['room']][$possible_place['bed']] >= $number_of_guests) {
                    $possible_places[$index]['maxdays']++;
                } else {
                    break;
                }
            }
        }

        //Sort from largest to smallest
        usort($possible_places, function ($a, $b) use ($booking_id) {
            if ($b['maxdays'] == $a['maxdays']) {

                if ($a['room'] != $b['room']) {
                    if ($this->isAPriorityRoom($booking_id, $b['room'])) {
                        return 1;
                    }

                    if ($this->isAPriorityRoom($booking_id, $a['room'])) {
                        return -1;
                    }
                }

                $beds_taken_in_room_a = $this->calculateNumberBedsTakenInRoom($a['room'], $a['date'], $a['enddate']);
                $beds_taken_in_room_b = $this->calculateNumberBedsTakenInRoom($b['room'], $b['date'], $b['enddate']);


                if ($beds_taken_in_room_a > $beds_taken_in_room_b) {
                    return -1;
                }

                if ($beds_taken_in_room_b > $beds_taken_in_room_a) {
                    return 1;
                }

                if ($a['bed'] == $b['bed']) {
                    return $a['room'] - $b['room'];
                }

                return $a['bed'] - $b['bed'];
            }
            return $b['maxdays'] - $a['maxdays'];
        });

        //Return the reordered array
        return $possible_places;
    }

    private function isAPriorityRoom($booking_id, $room_id) {
        if (!isset($this->priority_rooms[$booking_id])) {
            return false;
        }

        if (in_array($room_id, $this->priority_rooms[$booking_id])) {
            return true;
        }

        return false;
    }

    private function calculateNumberBedsTakenInRoom($room_id, $start_date, $end_date) {
        $days = DateHelper::getArrayOfDatesByString($start_date, $end_date);
        $max_bed_number = $this->getMaxNumberOfGuestsFromRoomId($room_id);

        $number_of_beds_taken = 0;
        foreach ($days as $day) {
            for ($bed = 1; $bed < $max_bed_number; $bed++) {
                if ($this->board_array[$day][$room_id][$bed] == 0) {
                    $number_of_beds_taken++;
                }
            }
        }

        return $number_of_beds_taken;
    }

    private function isOverbooking($unallocateds) {

        //First we need to make a deep copy of the array, otherwise all the models are copied by reference, and if we
        //change them below then we run into strange behaviour later on.
        $temp_unallocateds = array();
        foreach ($unallocateds as $k => $v) {
            $temp_unallocateds[$k] = clone $v;
        }
        $unallocateds = $temp_unallocateds;

        //The below loop is used to merge all unallocateds that are on the same day but at different price points.
        //Occasionally booking channels will send bookings with slight price differences eg 3x5.48euro + 1x5.49euros
        //This means we would only check 3 instead of 4, so that is why we need to merge.

        foreach ($unallocateds as $index => $unallocated1) {
            foreach($unallocateds as $index2 => $unallocated2) {
                if ($index2 > $index) {
                    if (($unallocated1->date == $unallocated2->date) && ($unallocated1->room_type_id == $unallocated2->room_type_id)) {
                        $unallocateds[$index]->number_of_guests += $unallocated2->number_of_guests;
                        unset($unallocateds[$index2]);
                    }
                }
            }
        }

        foreach ($unallocateds as $date) {
            //Create an array that contains all the room ids
            $room_ids = $this->getRoomsFromRoomType($date->room_type_id)->pluck('id')->all();

            //Work out the maximum possible capacity per day (number of rooms * number_of_guests)
            $max_capacity_per_day = count($room_ids) * $this->getMaxNumberOfGuestsFromRoomId($room_ids[0]);

            //If trying to book more beds than max possible capacity
            if ($date->number_of_guests > $max_capacity_per_day) {
                return true;
            }

            $beds_taken = 0;
            foreach ($room_ids as $room_id) {
                foreach ($this->board_array[$date->date][$room_id] as $bed) {
                    if ($bed == 0) {
                        $beds_taken++;
                    }
                }
            }

            if ($date->number_of_guests > $max_capacity_per_day - $beds_taken) {
                return true;
            }

        }
        return false;
    }

    private function removeBooking($booking_id) {
        $this->booking_ids = array_diff($this->booking_ids, [$booking_id]);
    }

    private function processAndSaveOverbooking($booking_id, $unallocateds) {
        $previous = null;
        $previousStay = new Stay();

        Stay::where('booking_id', '=', $booking_id)->delete();

        foreach ($unallocateds as $day) {
            if (($previous != DateHelper::subDays($day->date, 1)) || ($previousStay->price_per_night != $day->price_per_night)) {
                $newStay = new Stay();
                $newStay->booking_id = $booking_id;
                $newStay->room_id = null;
                $newStay->start_date = $day->date;
                $newStay->end_date = $day->date;
                $newStay->number_of_guests = $day->number_of_guests;
                $newStay->price_per_night = $day->price_per_night;
                $newStay->temp = true;
                $newStay->save();

                $previous = $day->date;
                $previousStay = $newStay;
            } else {
                $previousStay->end_date = $day->date;
                $previousStay->save();
                $previous = $day->date;
            }
        }

        $booking = Booking::find($booking_id);
        $booking->show_notification = true;
        $booking->save();

        $this->removeBooking($booking_id);

        return false;
    }

}