<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'city';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    /* Remember Verify this works, it probably doesn't as-is */
    public function hostels() {
        return $this->belongsTo('App\HostelPro\Models\Country', 'country_code', 'country_code');
    }
}