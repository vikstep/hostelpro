<?php namespace App\HostelPro\Models;

use DB;
use Carbon;

class Calendar {
    public $hostel_id;
    public $start;
    public $end;
    public $days;
    public $tempBooking_id;

    public $table_month_headings;
    public $table_headings;
    public $rooms;
    public $boardarray;
    public $dates;
    public $availability;

    private $room_type_ids;
    private $room_type_lookup = array();
    private $room_type_collection;

    private $today_at_hostel;

    public function __construct($hostel_id, $start, $days, $tempBooking_id = null) {

        $this->hostel_id = $hostel_id;
        $this->start = Carbon::createFromFormat('Y-m-d', $start)->subDay()->toDateString();
        $this->end = DateHelper::addDays($this->start, $days);
        //$this->end = DateHelper::convertStringDateToCarbon($start)->addDays($days - 1)->toDateString();

        $this->days = $days + 1;
        $this->tempBooking_id = $tempBooking_id;

        $this->today_at_hostel = Hostel::find($this->hostel_id)->today_at_hostel;

        #$time_start = microtime(true);
        $this->generateMonthHeadingData();
        $this->generateDaysAndWeeksArray();
        $this->generateCalendarData();
        //$this->generateUnallocatedCalendarData();
        #echo 'Total execution time in seconds: ' . (microtime(true) - $time_start);

    }

    private function generateCalendarData() {

        $this->room_type_collection = RoomType::where('hostel_id', '=', $this->hostel_id)->has('rooms')->get()->keyBy('id');
        $this->room_type_ids = $this->room_type_collection->pluck('id')->all();

        #$room_type_ids = RoomType::select('id')->where('hostel_id', '=', $this->hostel_id)->pluck('id')->all();
        #$this->room_type_ids = $room_type_ids;

        $this->rooms = Room::whereIn('room_type_id', $this->room_type_ids)->with('roomtype')->orderBy('sort')->get();

        foreach ($this->rooms as $room) {
            if (!isset($this->room_type_lookup[$room->room_type_id])) {
                $this->room_type_lookup[$room->room_type_id] = array();
            }
            array_push($this->room_type_lookup[$room->room_type_id], $room->id);
        }


        $room_ids = $this->rooms->pluck('id')->all();
        $this->dates = DateHelper::getArrayOfDatesByString($this->start, DateHelper::addDays($this->start, $this->days - 1));
        $stay_ids_to_ignore = Array();
        if (!is_null($this->tempBooking_id)) {
            $stay_ids_to_ignore = Stay::where('booking_id', '=', $this->tempBooking_id)->where('temp', '=', 'TRUE')->pluck('id')->all();
        }

        $boardarray = Array();
        //Create blank value for every single date
        foreach ($this->rooms as $room) {
            for ($bed_number = 1; $bed_number <= $room->roomtype->number_of_guests; $bed_number++) {
                for ($day = 0; $day < $this->days; $day++) {
                    $boardarray[$room->id][$bed_number][$this->dates[$day]] = "";
                }
            }
        }

        $boardstays = Board::whereIn('room_id', $room_ids)
            ->whereBetween('date', [$this->start, $this->end])
            ->with([
                'stay' => function ($query) {
                $query->select('id', 'number_of_guests', 'booking_id', 'start_date', 'end_date');
            }, 'stay.booking' => function ($query) {
                $query->select('id', 'guest_id', 'label_type_id', 'allocated_by_system');
            }, 'stay.booking.guest' => function ($query) {
                $query->select('id', 'firstname', 'lastname');
                },
                'stay.booking.label'
            ])
            ->whereNotIn('stay_id', $stay_ids_to_ignore)
            ->orderBy('room_id')
            ->orderBy('date')
            ->orderBy('bed_number')
            ->groupBy('stay_id')
            ->get();

        if (!is_null($this->tempBooking_id)) {
            $tempBooking = BoardTemp::whereIn('room_id', $room_ids)
                ->whereIn('date', $this->dates)
                ->where('booking_id', '=', $this->tempBooking_id)
                ->with('stay.booking', 'stay.booking.guest', 'stay.booking.label', 'stay.room')
                ->orderBy('room_id')
                ->orderBy('date')
                ->orderBy('bed_number')
                ->groupBy('stay_id')
                ->get();
            foreach ($tempBooking as $stay) {
                $boardstays->add($stay);
            }
        }

        foreach ($boardstays as $stay) {

            //Account for overflow situations (eg stay is longer than given time period or one side exceeds the min/max)
            $days = DateHelper::getDaysBetween(max($stay->stay->start_date, $this->start), min($stay->stay->end_date, $this->end));
            $number_of_guests = $stay->stay->number_of_guests;

            $draggable = true;
            if (($stay->stay->start_date < $this->start) || ($stay->stay->end_date > $this->end)) {
                $draggable = false;
            }

            $boardarray[$stay->room_id][$stay->bed_number][$stay->date] = array('stay_id' => $stay->stay_id,
                'colspan' => $days + 1,
                'rowspan' => $number_of_guests,
                'firstname' => $stay->stay->booking->guest->firstname,
                'lastname' => $stay->stay->booking->guest->lastname,
                'stayid' => $stay->stay->id,
                'bookingid' => $stay->stay->booking->id,
                'possiblenoshow' => $stay->stay->start_date < $this->today_at_hostel ? 'true' : 'false',
                'draggable' => $draggable,
                'locked' => $stay->stay->booking->allocated_by_system ? 'false' : 'true',
                'label' => 'calendar-' . $stay->stay->booking->LabelCSS);

            if ($days + 1 > 1) {
                $dates_to_delete = DateHelper::getArrayOfDatesByString($stay->date, DateHelper::addDays($stay->date, $days));

                foreach ($dates_to_delete as $key => $date_to_delete) {
                    for ($i = 0; $i < $number_of_guests; $i++) {
                        if (($key != 0) || ($i != 0)) {
                            unset($boardarray[$stay->room_id][$stay->bed_number + $i][$date_to_delete]);
                        }

                    }
                }
            } else if ($number_of_guests > 1) {
                for ($i = 1; $i < $number_of_guests; $i++) {
                    unset($boardarray[$stay->room_id][$stay->bed_number + $i][$stay->date]);
                }
            }
        }

        //dd($boardarray[238]);

        $this->boardarray = $boardarray;
    }

    private function generateUnallocatedCalendarData() {
        $unallocateds = BoardUnallocated::select('booking_id', 'room_type_id', 'date', DB::raw('SUM(number_of_guests) AS daytotal'))
            ->where('booking.status', '!=', 'Cancelled')
            ->join('booking', 'booking.id', '=', 'board_unallocated.booking_id')
            ->whereIn('room_type_id', $this->room_type_ids)
            ->whereBetween('date', [$this->start, $this->end]);

        if (isset($this->tempBooking_id)) {
            $unallocateds = $unallocateds->where('booking_id', '!=', $this->tempBooking_id);
        }

        $unallocateds = $unallocateds->groupBy('room_type_id')->groupBy('date')->get();

        foreach($unallocateds as $unallocated) {
            $this->addUnallocatedStaysToBoard($unallocated->room_type_id, $unallocated->date, $unallocated->daytotal);
        }
    }

    private function addUnallocatedStaysToBoard($room_type_id, $date, $number_of_guests_to_add) {

        $total_number_of_beds_per_room = $this->room_type_collection[$room_type_id]->number_of_guests;
        $total_number_of_rooms = count($this->room_type_lookup[$room_type_id]);

        $room_counter = 0;
        $bed_counter = 1;

        while (($number_of_guests_to_add >= 1) && ($room_counter < $total_number_of_rooms)) {
            $room_id = $this->room_type_lookup[$room_type_id][$room_counter];
            if (array_key_exists($date,$this->boardarray[$room_id][$bed_counter]) && (empty($this->boardarray[$room_id][$bed_counter][$date]))) {
                #echo("<br>FREE: " . $room_id . " BED: " . $bed_counter);
                $this->boardarray[$room_id][$bed_counter][$date] = array('unallocated' => true);
                $number_of_guests_to_add--;
            }

            if ($bed_counter == $total_number_of_beds_per_room) {
                $bed_counter = 1;
                $room_counter++;
            } else {
                $bed_counter++;
            }
        }
        #dd($this->boardarray);

    }

    private function generateDaysAndWeeksArray() {
        $start = DateHelper::convertStringDateToCarbon($this->start);
        $calendar = array();
        foreach ($this->table_month_headings as $month) {
            $temp = Array();
            for ($x = 1; $x <= $month['days']; $x++) {
                array_push($temp, array($start->day, substr($start->format('l'), 0, 3)));
                $start->addDay();
            }
            array_push($calendar, $temp);
        }

        $this->table_headings = $calendar;
    }

    private function generateMonthHeadingData() {
        $days = $this->days;
        $start = DateHelper::convertStringDateToCarbon($this->start);
        $table_month_headings = Array();
        while ($days >= 1) {
            $month = $start->format('F');
            $year = $start->format('Y');
            $daysUntilEndOfMonth = (($start->copy()->endOfMonth()->day) - ($start->copy()->day));
            $daysUntilEndOfMonth += 1;
            if ($daysUntilEndOfMonth > $days) {
                $daysUntilEndOfMonth = $days;
            }

            $days = $days - $daysUntilEndOfMonth;

            array_push($table_month_headings, array('month' => $month, 'year' => $year,'days' => $daysUntilEndOfMonth));
            $start->addDays($daysUntilEndOfMonth);
        }
        $this->table_month_headings = $table_month_headings;
    }

    public function getAvailability() {

        $availability = Array();

        $roomtypes = $this->room_type_collection;
        foreach ($roomtypes as $roomtype) {
            //Get all room IDs for this type of room
            $room_ids = Room::where('room_type_id', '=', $roomtype->id)->pluck('id');

            //Work out the maximum possible capacity per day (number of rooms * number_of_guests)
            $max_capacity_per_day = count($room_ids) * RoomType::find($roomtype->id)->number_of_guests;

            if ($roomtype->type == "Private") {
                $max_capacity_per_day = $max_capacity_per_day / $roomtype->number_of_guests;
            }

            $finalarray = Array();

            foreach ($this->dates as $date) {
                $finalarray[$date] = $max_capacity_per_day;
            }

            $boardstays = Board::select('date', DB::raw('count(date) as total'))
                ->whereIn('room_id', $room_ids)
                ->whereIn('date', $this->dates)
                ->orderBy('date')
                ->groupBy('date')
                ->get()
                ->toArray();

            foreach($boardstays as $boardstay) {
                if ($roomtype->type == "Private") {
                    $finalarray[$boardstay['date']] -= ($boardstay['total'] / $roomtype->number_of_guests);
                } else {
                    $finalarray[$boardstay['date']] -= $boardstay['total'];
                }
            }

            $unallocatedboards = BoardUnallocated::select('date', DB::raw('sum(number_of_guests) as total'))
                ->where('room_type_id', $roomtype->id)
                ->whereIn('date', $this->dates)
                ->groupBy('date')
                ->orderBy('date')
                ->get();

            foreach($unallocatedboards as $boardstay) {
                if ($roomtype->type == "Private") {
                    $finalarray[$boardstay['date']] -= ($boardstay['total'] / $roomtype->number_of_guests);
                } else {
                    $finalarray[$boardstay['date']] -= $boardstay['total'];
                }
            }

            $seasonRates = Rate::whereIn('date', $this->dates)->where('room_type_id', '=', $roomtype->id)->get();
            $rates = array();
            foreach ($seasonRates as $seasonRate) {
                $rates[$seasonRate->date] = array('price' => MoneyHelper::convertCentsToDollars($seasonRate->price), 'min-stay' => $seasonRate->min_stay);
            }

            array_push($availability, array('id' => $roomtype->id, 'name' => $roomtype->name, 'max_per_day' => $max_capacity_per_day, 'availability' => $finalarray , 'rates' => $rates));
        }

        //dd($availability);

        $this->availability = $availability;

    }

}