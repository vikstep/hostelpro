<?php namespace App\HostelPro\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CompetitorPricing extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'competitor_pricing';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = ['date', 'created_at', 'updated_at'];

    protected $with = ['url', 'currency'];

    public $timestamps = true;

    public function url()
    {
        return $this->belongsTo(CompetitorURL::class, 'competitor_url_id');
    }

    public function currency()
    {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    protected static function boot() {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('date', 'desc');
        });
    }

}