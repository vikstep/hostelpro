<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class MoneyHelper extends Model {

    public static function convertToCents($string) {
        $dollars = str_replace('$', '', $string);
        return number_format((float)$dollars*100., 0, '.', '');
    }

    public static function convertToEnglishFormat($number) {
        return number_format($number, 2, '.', ',');
    }

    public static function convertToEnglishFormatWithoutDecimals($number) {
        return number_format($number, 0, '.', ',');
    }

    public static function convertCentsToDollars($string) {
        //Note to self: need to check later that this won't cause floating point issues in long-term or with larger numbers.
        // This casts string to int, then casts to floating point, then uses number_format
        #return number_format($string/100, 2, '.', '');

        //This is probably better I guess?
        $amount = substr($string, 0, -2) . "." . substr($string, -2, 2);

        if ($amount == .0) {
            return 0.00;
        }

        return (float) $amount;
    }

}