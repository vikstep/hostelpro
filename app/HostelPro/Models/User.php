<?php namespace App\HostelPro\Models;

use App\HostelPro\Emails\PasswordResetEmail;
use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Notifiable, SoftDeletes, Authenticatable, Authorizable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	#protected $fillable = ['name', 'email', 'password'];
	protected $fillable = ['firstname', 'lastname', 'email', 'password', 'terms_and_conditions'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token', 'current_hostel'];

	//protected $appends = ['current_hostel'];

	public function routeNotificationForSlack() {
		return 'https://hooks.slack.com/services/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';
	}

//	public function setPasswordAttribute($password) {
//		$this->attributes['password'] = Hash::make($password);
//	}

	/*
	public function hostels() {
		return $this->belongsToMany('App\HostelPro\Models\Hostel', 'hostel_user');
	}*/

	public function currenthostel() {
		return $this->belongsTo('App\HostelPro\Models\Hostel', 'current_hostel', 'id');
	}

	public function fullName() {
		return $this->attributes['firstname'] . " " . $this->attributes['lastname'];
	}

	public function roleAtCurrentHostel() {
		return $this->roles($this->attributes['current_hostel'])->first()->name ?? null;
	}

	public function hostelArrayList() {
		$hostels = [];
		foreach ($this->hostels as $hostel) {
			$hostel_id = $hostel->pivot->hostel_id;
			$hostel = Hostel::find($hostel_id);
			$hostels[$hostel_id] = $hostel->name;
		}
		return $hostels;
	}

	public function hostels() {
		return $this->belongsToMany(Role::class)->withPivot('hostel_id');
	}

	public function roles($hostel_id = null) {

		if (!$hostel_id) {
			$hostel_id = $this->current_hostel;
		}

		return $this->belongsToMany(Role::class)->wherePivot('hostel_id', '=', $hostel_id);

		/*
		if (empty(Auth::user()->currenthostel)) {
			return $this->belongsToMany(Role::class)->wherePivot('hostel_id', '=', $this->current_hostel);
		}

		return $this->belongsToMany(Role::class)->wherePivot('hostel_id', '=', Auth::user()->currenthostel->id);*/
	}

	public function hasRole($role) {
		if (is_string($role)) {
			return $this->roles->contains('name', $role);
		}

		return !! $role->intersect($this->roles)->count();
	}

	public function assignRole($role, $hostel_id) {
		//$hostel_id = $this->attributes['current_hostel'];

		$this->roles($hostel_id)->sync(
			[Role::where('name', '=', $role)->firstOrFail()->id => ['hostel_id' => $hostel_id]]
		);
	}

	public function hasPermission($permission)
	{
		if (is_string($permission)) {
			$permission = Permission::where('name', '=', $permission)->first();
			if (!$permission) {
				return false;
			}
		}
		return $this->hasRole($permission->roles);
	}

	public function canDelete($model) {
		if ($this->roles->first()->name == "receptionist") {

			if ($model->user_id != $this->id) {
				return false;
			}

			if ($model->created_at->diffInMinutes() > 10) {
				return false;
			}

		}

		return true;
	}

	public function canDeleteUser(User $user) {

		if ($this->roles->first()->name == "receptionist") {
			return false;
		}

		if (($this->roles->first()->name == "manager") && ($user->roles($this->current_hostel)->first()->name != "receptionist")) {
			return false;
		}

		return true;
	}

	public function sendPasswordResetNotification($token)
	{
		(new PasswordResetEmail())->withData(['url' => url('password/reset/' . $token . '?email=' . $this->email)])->sendTo($this);
		//$this->notify(new ResetPasswordNotification($token));
	}

}
