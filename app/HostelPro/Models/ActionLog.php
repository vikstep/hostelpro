<?php namespace App\HostelPro\Models;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ActionLog extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'action_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = true;

    public function type()
    {
        return $this->belongsTo(ActionLogType::class, 'action_log_type_id', 'id');
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function hostel()
    {
        return $this->belongsTo(Hostel::class);
    }

    public function user() {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function getCreatedAtAttribute($date) {
        $newtime = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        $newtime->timezone(Auth::user()->currenthostel->timezone->timezone);
        return $newtime;
    }

}