<?php namespace App\HostelPro\Models;

use DB;
use Auth;
use Carbon;

class ReportPaymentView
{

    private $hostel_id;

    public $data = array();
    public $totals = array();
    public $grandtotal = 0;

    public function __construct($hostel_id, $start, $end)
    {

        $this->hostel_id = $hostel_id;
        $dates = DateHelper::getArrayOfDatesByString(DateHelper::convertDateToYearMonthDay(substr($start, 0, 10)), DateHelper::convertDateToYearMonthDay(substr($end, 0, 10)));

        foreach ($dates as $date) {
            $this->data[$date] = collect();
            $this->totals[$date] = 0;
        }

        $startUTC = Carbon::createFromFormat('Y-m-d H:i:s', $start, Auth::user()->currenthostel->timezone->timezone);
        $startUTC->setTimezone('UTC');
        $endUTC = Carbon::createFromFormat('Y-m-d H:i:s', $end, Auth::user()->currenthostel->timezone->timezone);
        $endUTC->setTimezone('UTC');

        $this->getAndProcessPayments($hostel_id, $startUTC->toDateTimeString(), $endUTC->toDateTimeString());
        $this->getAndProcessItems($hostel_id, $startUTC->toDateTimeString(), $endUTC->toDateTimeString());
        $this->getAndProcessActionLog($hostel_id, $startUTC->toDateTimeString(), $endUTC->toDateTimeString());

        foreach ($dates as $date) {
            $this->data[$date] = $this->data[$date]->sortBy(function ($transaction) {
                return $transaction->created_at . get_class($transaction);
            });

            $total = $this->data[$date]->filter(function ($transaction) {
                return $transaction instanceof Payment;
            })->sum('total');

            $this->totals[$date] = MoneyHelper::convertCentsToDollars($total);
            $this->grandtotal += $total;
        }
    }

    private function getAndProcessPayments($hostel_id, $start, $end) {
        $payments = Payment::select('id', 'name', 'total', 'user_id', 'booking_id', 'created_at')
            ->where('hostel_id', '=', $hostel_id)
            ->whereBetween('created_at', array($start, $end))
            ->whereNotNull('user_id')
            ->with('user')
            ->get();

        foreach ($payments as $payment) {
            $date = $payment->created_at->toDateString();
            $this->data[$date] = $this->data[$date]->push($payment);
        }
    }

    private function getAndProcessItems($hostel_id, $start, $end) {
        $items = AdditionalItem::select('id', 'name', 'price', 'units', 'total', 'user_id', 'booking_id', 'created_at')
            ->where('hostel_id', '=', $hostel_id)
            ->whereBetween('created_at', array($start, $end))
            ->whereNotNull('user_id')
            ->with('user')
            ->get();

        foreach ($items as $item) {
            $date = $item->created_at->toDateString();
            $this->data[$date] = $this->data[$date]->push($item);
        }
    }

    private function getAndProcessActionLog($hostel_id, $start, $end) {
        $items = ActionLog::where('hostel_id', '=', $hostel_id)
            ->whereBetween('created_at', array($start, $end))
            ->whereNotNull('user_id')
            ->with('user', 'type')
            ->get();

        foreach ($items as $item) {
            $date = $item->created_at->toDateString();
            $this->data[$date] = $this->data[$date]->push($item);
        }
    }
}