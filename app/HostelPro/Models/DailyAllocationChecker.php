<?php namespace App\HostelPro\Models;

use App\HostelPro\Jobs\ScrapePricesJob;
use App\HostelPro\Jobs\ScrapeReviewsJob;
use DB;
use Exception;
use Illuminate\Foundation\Bus\DispatchesJobs;

use App\HostelPro\Jobs\SyncAvailability;

class DailyAllocationChecker {

    use DispatchesJobs;

    public function checkAllHostels () {
        $hostels = Hostel::all();
        foreach ($hostels as $hostel) {
            $this->checkHostel($hostel->id);
        }
    }

    public function checkHostel($hostel_id) {
        $timezone = Hostel::find($hostel_id)->timezone->timezone;
        $current_date_at_hostel = DateHelper::getToday($timezone);
        if (CompletedAllocation::where('hostel_id', '=', $hostel_id)->where('date', '=', $current_date_at_hostel)->count() >= 1) {
            return false;
        }

        DB::beginTransaction();
        try {
            $was_successful = Allocation::processDailyAllocations($hostel_id, $current_date_at_hostel);
        } catch (Exception $e) {
            DB::rollBack();
            $was_successful= false;
        }
        DB::commit();

        $completed = new CompletedAllocation();
        $completed->hostel_id = $hostel_id;
        $completed->date = $current_date_at_hostel;
        $completed->was_successful = $was_successful;
        $completed->save();

        $token = MyAllocatorToken::where('hostel_id', '=', $hostel_id)->first();
        if ($token) {
            $this->syncAvailability($hostel_id);
        }
        $this->dispatch(new ScrapeReviewsJob($hostel_id));
        $this->dispatch(new ScrapePricesJob($hostel_id));
    }

    private function syncAvailability($hostel_id) {
        $timezone = Hostel::find($hostel_id)->timezone->timezone;
        $min_start_date = DateHelper::getToday($timezone);
        $max_end_date = DateHelper::addDays($min_start_date, 180);
        $this->dispatch(new SyncAvailability($hostel_id, $min_start_date, $max_end_date));
    }

}