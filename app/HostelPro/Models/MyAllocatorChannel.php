<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class MyAllocatorChannel extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'myallocator_channel';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public $incrementing = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function bookings()
    {
        return $this->hasMany('App\HostelPro\Models\Booking', 'myallocator_channel_id', 'id');
    }

}