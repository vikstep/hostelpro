<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Email extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['host', 'port', 'encryption', 'username', 'password', 'pre_email_days', 'pre_email_text', 'post_email_days', 'post_email_text'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = true;

    public function hostel() {
        return $this->belongsTo('App\HostelPro\Models\Hostel', 'hostel_id', 'id');
    }

}