<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    public function roomtype() {
        return $this->belongsTo('App\HostelPro\Models\RoomType', 'room_type_id', 'id');
    }

    public function formattedPrice() {
        return MoneyHelper::convertCentsToDollars($this->price);
    }

    public static function getAveragePricePerNight($room_type_id, $start_date, $end_date) {
        $dates = DateHelper::getArrayOfDatesByString($start_date, $end_date);

        $average = Rate::where('room_type_id', '=', $room_type_id)->whereIn('date', $dates)->avg('price');

        return $average ?: '0';
    }

    public static function getOccupancyColor($percent)
    {
        if($percent < 50)
        {
            return 'green';
        }
        
        if($percent < 76)
        {
            return 'yellow';
        }
        
        return 'red';
    }

}