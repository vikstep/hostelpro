<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'currency';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public function hostels() {
        return $this->hasMany('App\HostelPro\Models\Hostel', 'hostel_id', 'id');
    }
}