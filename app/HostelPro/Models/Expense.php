<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'expense';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = true;

    public function hostel() {
        return $this->belongsTo('App\HostelPro\Models\Hostel', 'hostel_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\HostelPro\Models\User', 'user_id', 'id')->withTrashed();
    }

    public function getPrettyPriceAttribute() {
        return MoneyHelper::convertCentsToDollars($this->attributes['price']);
    }

}