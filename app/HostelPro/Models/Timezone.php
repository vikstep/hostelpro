<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Timezone extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'timezones';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];


    /* Remember Verify this works, it probably doesn't as-is */
    public function hostels() {
        return $this->hasMany('App\HostelPro\Models\Hostel', 'timezone_id', 'id');
    }

    public static function getTimezonesbyCountryCode($countrycode) {
        //$countrycode = DB::table('country')->select('country_code')->find($countryid)->country_code;
        $timezones = DB::table('timezones')->where('country_code', '=', $countrycode)->orderBy('id', 'asc')->pluck('timezone', 'id');
        return $timezones;
    }
}