<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'commission';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    protected $casts = [
        'is_deposit' => 'boolean',
    ];

    public function original_currency() {
        return $this->hasOne('App\HostelPro\Models\Currency', 'id', 'original_currency_id');
    }

    public function converted_currency() {
        return $this->hasOne('App\HostelPro\Models\Currency', 'id', 'converted_currency_id');
    }

    public function setOriginalAmountAttribute($value)
    {
        $this->attributes['original_amount'] = MoneyHelper::convertToCents($value);
    }

    public function getOriginalAmountAttribute($value)
    {
        return MoneyHelper::convertCentsToDollars($value);
    }

    public function setConvertedAmountAttribute($value)
    {
        $this->attributes['converted_amount'] = MoneyHelper::convertToCents($value);
    }

    public function getConvertedAmountAttribute($value)
    {
        return MoneyHelper::convertCentsToDollars($value);
    }

}