<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use App\HostelPro\Models\Rate;
use App\HostelPro\Models\Board;
use App\HostelPro\Models\Room;
use App\HostelPro\Models\MyAllocatorToken;
use Illuminate\Support\Facades\DB;
use App\HostelPro\Jobs\SyncRatesJob;

use App\HostelPro\Models\DateHelper;
use Illuminate\Support\Facades\Log;

class DiscountRate extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'discount_rates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    public function hostel() {
        return $this->belongsTo(Hostel::class);
    }

    public static function updateDiscountRates($season = 0, $roomtype = 0)
    {
        Log::info("Start: " . date("h:i:sa") . "\n");

        $daysInAdvance = 90;

        $now = new \DateTime('now');

        $lastDay = clone $now;
        $lastDay->modify('+'. $daysInAdvance .' day');

        $nowFormatted = $now->format('Y-m-d');
        $lastDayFormatted = $lastDay->format('Y-m-d');

        $queryForSeasonAndRoomType = '';

        if($season > 0 && $roomtype > 0)
        {
            $queryForSeasonAndRoomType = ' AND sr.id = '.$season. ' AND rt.id = ' .$roomtype;
        }

        $discounts = DB::select('SELECT 
          rt.id AS roomtypeid, h.id AS hostelid, h.name AS hostelname, rt.name AS roomname, rt.number_of_guests,
          sr.`id`seasonrateid, sr.`name` AS seasonname, sr.`start_date`, sr.`end_date`, 
          dwr.rates AS dailyweekrates, dwr.min_stay AS nights,
            dr.`earlybird_discount`,  dr.`earlybird_days`,
          dr.`lastminute_discount`,  dr.`lastminute_days`,  dr.`progressive_low_price`,  dr.`progressive_high_price`  
  
          FROM  `room_types` rt
          INNER JOIN `hostel` h ON rt.hostel_id = h.id
          INNER JOIN `season_rates` sr ON h.id = sr.hostel_id
          INNER JOIN `day_of_week_rate` dwr ON (dwr.season_id = sr.id AND rt.id = dwr.room_type_id)         
          
          LEFT  JOIN `discount_rates` dr ON dr.roomtype_id = rt.id

          WHERE sr.end_date >= ? AND sr.`start_date` <= ?'. $queryForSeasonAndRoomType .';',
            [$nowFormatted, $lastDayFormatted]);

        $hostelsToUpdate = array();


        foreach($discounts as $discount)
        {
            Log::info("Working with Hostel: ". $discount->hostelname .". Season: " . $discount->seasonname .
                "($discount->start_date - $discount->end_date). RoomType Name: " . $discount->roomname . " ".date("h:i:sa") . "\n");


            if(!array_key_exists($discount->hostelid, $hostelsToUpdate))
            {
                $hostelsToUpdate[$discount->hostelid] = $discount->hostelname;
            }

            $seasonEndDateTime = \DateTime::createFromFormat('Y-m-d', $discount->end_date);

            //is end date 90 days or season end?
            $lastDayFormatted = $lastDay < $seasonEndDateTime ? $lastDayFormatted : $seasonEndDateTime->format('Y-m-d');

            //fetch days list for current season
            $days = DateHelper::getArrayOfDatesByString($nowFormatted, $lastDayFormatted);
            //will fill this and update both DB and push to MyAllocator
            $dataToSave = array();

            $minNights = unserialize($discount->nights);
            $regularPrices = unserialize($discount->dailyweekrates);

            //fetch daily rates for this roomType and season
            $dailyRates = DailyRate::where('room_type_id', $discount->roomtypeid)->where('price', '>', '0')

                ->whereIn('date', $days)->where('date', '<=', $discount->end_date)->get()->keyBy('date')->toArray();

            //lets init data with regular prices and min. nights
            foreach ($days as $day) {

                $dayOfWeek = DateHelper::getDayOfWeek($day);

                $dataToSave[$day] = array(
                    'room_type_id' => $discount->roomtypeid,
                    'date' => $day,
                    'price' => $regularPrices[$dayOfWeek],
                    'min_stay' => $minNights[$dayOfWeek],
                );

                //do we have daily rate set for this day?
                if (isset($dailyRates[$day])) {
                    Log::info("Applying Daily Rate for $day: " . $dailyRates[$day]['price'] . " "
                        . date("h:i:sa") . "\n");

                    $dataToSave[$day]['price'] = $dailyRates[$day]['price'];
                }

            }



            //if we have Progressive Pricing just set it ignoring other discount - only checking for daily rates
            if($discount->progressive_low_price > 0 && $discount->progressive_high_price > 0)
            {
                Log::info("Applying Progressive Pricing:  $discount->progressive_low_price - $discount->progressive_high_price"
                    . " " . date("h:i:sa") . "\n");

                //loop all days
                foreach ($days as $day)
                {
                    //do we have daily rate set for this day?
                    if(isset($dailyRates[$day]))
                    {
                        Log::info("Applyied Daily Rate for $day: " . $dailyRates[$day]['price'] ." "
                            . date("h:i:sa") . "\n");
                    }
                    else
                    {
                        //calculate occupancy
                        $roomIds = Room::where('room_type_id', $discount->roomtypeid)->whereNull('deleted_at')
                            ->get()->keyBy('id')->toArray();

                        $max_possible_bed_nights = count($roomIds) * $discount->number_of_guests;

                        $total_bed_nights =
                            Board::where('date', $day)->whereIn('room_id', array_keys($roomIds))->count();

                        $occupancy = round(($total_bed_nights / $max_possible_bed_nights)*100,2);

                        $progressivePrice =  $discount->progressive_low_price +
                            $occupancy * ($discount->progressive_high_price - $discount->progressive_low_price);

                        $dataToSave[$day]['price'] = $progressivePrice;
                    }
                }
            }
            else
            {
                //do we use EarlyBird Discount?
                if($discount->earlybird_discount > 0 && $discount->earlybird_days > 0)
                {
                    Log::info("Applying Earlybird Discount:  $discount->earlybird_discount% for dates after 
                        $discount->earlybird_days days. "   . date("h:i:sa") . "\n");

                    $firstDay = clone $now;
                    $firstDay->modify('+'.$discount->earlybird_days.' day');

                    $daysForDiscount = DateHelper::getArrayOfDatesByString($firstDay->format('Y-m-d'), $lastDayFormatted);

                    if(empty($daysForDiscount))
                    {
                        Log::info("No applicable dates found\n");
                    }
                    else
                    {
                        Log::info("Applicable dates from: ".$daysForDiscount[0] . "; till: " . last($daysForDiscount) ."\n");

                        foreach ($daysForDiscount as $dayForDiscount)
                        {
                            //do we have daily rate set for this day?
                            if(isset($dailyRates[$dayForDiscount]))
                            {
                                Log::info("Applied Daily Rate for $dayForDiscount: " . $dailyRates[$dayForDiscount]['price'] ." "
                                    . date("h:i:sa") . "\n");
                            }
                            else
                            {
                                $oldPrice = $dataToSave[$dayForDiscount]['price'];
                                $newPrice = ((100-$discount->earlybird_discount)*$oldPrice)/100;

                                $dataToSave[$dayForDiscount]['price'] = $newPrice;
                            }
                        }
                    }
                }

                //do we use Last Minute Discount?
                if($discount->lastminute_discount > 0 && $discount->lastminute_days > 0)
                {
                    Log::info("Last Minute Discount:  $discount->lastminute_discount% for next 
                        $discount->lastminute_days days. "   . date("h:i:sa") . "\n");

                    $firstDay = clone $now;
                    $firstDay->modify('+'.$discount->lastminute_days.' day');

                    $daysForDiscount = DateHelper::getArrayOfDatesByString($nowFormatted, $firstDay->format('Y-m-d'));

                    if(empty($daysForDiscount))
                    {
                        Log::info("No applicable dates found\n");
                    }
                    else
                    {
                        Log::info("Applicable dates from: ".$daysForDiscount[0] . "; till: " . last($daysForDiscount) ."\n");

                        foreach ($daysForDiscount as $dayForDiscount)
                        {
                            //do we have daily rate set for this day?
                            if(isset($dailyRates[$dayForDiscount]))
                            {
                                Log::info("Applied Daily Rate for $dayForDiscount: " . $dailyRates[$dayForDiscount]['price'] ." "
                                    . date("h:i:sa") . "\n");
                            }
                            else
                            {
                                $oldPrice = $dataToSave[$dayForDiscount]['price'];
                                $newPrice = ((100-$discount->lastminute_discount)*$oldPrice)/100;

                                $dataToSave[$dayForDiscount]['price'] = $newPrice;
                            }
                        }
                    }
                }
            }

            DB::beginTransaction();
            //delete rates for this roomtype and these days
            Rate::whereIn('date', $days)->where('room_type_id', $discount->roomtypeid)->delete();
            Rate::insert($dataToSave);
            DB::commit();

        }

        foreach ($hostelsToUpdate as $hostel => $name)
        {
            if (MyAllocatorToken::where('hostel_id', '=', $hostel)->count() > 0) {
                Log::info("Updating hostel ". $name . "\n");
                dispatch(new SyncRatesJob($hostel, $nowFormatted, $lastDayFormatted));
            }
        }

        Log::info("Updated hostels ". count($hostelsToUpdate) .". Completed on: " . date("h:i:sa") . "\n");
    }

}