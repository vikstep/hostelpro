<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Board extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'board';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = false;

    /*
    public function scopeCompositeKey($query, $date, $room_id, $bed_number) {
        return $query->where('date', '=', $date)->where('room_id', '=', $room_id)->where('bed_number', '=', $bed_number);
    }*/

    public function stay() {
        return $this->hasOne('App\HostelPro\Models\Stay', 'id', 'stay_id');
    }

    public function room() {
        return $this->hasOne('App\HostelPro\Models\Room', 'id', 'room_id');
    }

}