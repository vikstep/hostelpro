<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class BoardTemp extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'board_temp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = false;

    public function stay() {
        return $this->hasOne('App\HostelPro\Models\Stay', 'id', 'stay_id');
    }

    public static function clearTempStays($room_id, $start_date, $end_date, $start_bed_number, $end_bed_number) {
        $dates = DateHelper::getArrayOfDatesByString($start_date, $end_date);
        $bed_numbers = array();
        while ($start_bed_number <= $end_bed_number) {
            array_push($bed_numbers, $start_bed_number);
            $start_bed_number++;
        }

        $booking_ids = BoardTemp::where('room_id', '=', $room_id)
            ->where('hostel_id', '=', Auth::user()->currenthostel->id)
            ->whereIn('date', $dates)
            ->whereIn('bed_number', $bed_numbers)
            ->pluck('booking_id')
            ->all();

        BoardTemp::whereIn('booking_id', $booking_ids)->delete();
    }

}