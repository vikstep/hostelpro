<?php namespace App\HostelPro\Models\Reviews;

use Goutte\Client;
use Carbon\Carbon;

class BooReviewScraper extends ReviewScraper {

    public function __construct($url, $last_reviews = null) {
        $this->url = $url;
        $this->last_reviews = $last_reviews;
    }

    public function crawl() {

        $client = new Client();

        //Note: Booking.com returns a 301 if you don't set both the user-agent and accept-language.
        //The user-agent must also be on an approved list,
        //So sending the guzzle user agent or anything random will *not* work

        $client->setHeader('user-agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36');
        $client->setHeader('accept-language', 'en-US,en;q=0.8');

        $explode = explode('/', $this->url);
        $hostel_identifier = explode('.',(end($explode)))[0];

        $url = 'http://www.booking.com/reviewlist.html?pagename=' . $hostel_identifier . '&cc1=hu&rows=100';
        $crawler = $client->request('GET', $url);

        while ($this->processPage($crawler)) {
            $url = $crawler->filter('#review_next_page_link')->first()->link();
            $crawler = $client->click($url);
        }

    }

    public function getNewReviews() {
        return $this->new_reviews;
    }

    private function processPage($crawler) {
        //$count_before_processing = count($this->new_reviews);

        $crawler->filter('li.review_item')->each(function ($node) {
            $this->processNode($node);
        });

        //Commented code below so that we crawl all pages each time. Reason being that Booking.com sorts first by
        //reviews that have left text, then followed by reviews with no text. This means we can have new reviews
        //pop up pretty much on any page.
        /*
        if (count($this->new_reviews) <= $count_before_processing) {
            return false;
        }*/

        if ($crawler->filter('#review_next_page_link')->count() == 0) {
            return false;
        }

        //Return true if there is a "next page" link
        return true;
    }

    private function processNode($node) {
        $overall_score = (int) $node->filter('div.review_item_review_score')->first()->text() * 10;
        $rawDate = preg_replace("/[^A-Za-z0-9 ]/", '', $node->filter('.review_item_date')->first()->text());
        $date = Carbon::createFromFormat('F j Y',$rawDate)->toDateString();
        $liked = ($node->filter('.review_pos')->count() > 0) ? substr(trim($node->filter('.review_pos')->first()->text()),3) : null;
        $disliked = ($node->filter('.review_neg')->count() > 0) ? substr(trim($node->filter('.review_neg')->first()->text()),3) : null;

        $data = [
            'overall_score' => $overall_score,
            'date' => $date,
            'liked' => $liked,
            'disliked' => $disliked
        ];

        //if it's older than our newest review in the DB then we don't need it
        if (($this->last_reviews->count() > 0) && ($data['date'] < $this->last_reviews[0]->date->toDateString())) {
            return;
        }

        //If it's same date as last review date, check if this is a diff review
        if (($this->last_reviews->count() > 0) && ($data['date'] == $this->last_reviews[0]->date->toDateString())) {

            foreach($this->last_reviews as $last_review) {
                if (($last_review->overall_score == $data['overall_score'])
                    && ($last_review->liked == $data['liked'])
                    && ($last_review->disliked == $data['disliked'])) {
                    //Must be the same review, so don't add it
                    return;
                }
            }
        }

        //Passed all checks so this must be a new review.
        array_push($this->new_reviews, $data);
    }

    private function testing() {
        $crawler = $client->getClient()->request('GET', 'http://www.booking.com/reviewlist.html?pagename=david-hasselhostel-budapest&cc1=hu', [
            'http_errors' => false,
            'debug' => true,
            'headers' => [
                'Accept-Language' => 'en-US,en;q=0.8',
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'
            ]
        ]);
    }
}