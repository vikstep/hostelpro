<?php namespace App\HostelPro\Models\Reviews;

abstract class ReviewScraper {

    protected $url;
    protected $last_reviews;
    protected $new_reviews = [];

    abstract protected function __construct($url, $last_reviews);

    abstract protected function crawl();
}