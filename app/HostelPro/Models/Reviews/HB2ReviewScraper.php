<?php namespace App\HostelPro\Models\Reviews;

use Goutte\Client;
use Carbon\Carbon;

class HB2ReviewScraper extends ReviewScraper {

    public function __construct($url, $last_reviews = null) {
        $this->url = $url;
        $this->last_reviews = $last_reviews;
    }

    public function crawl() {

        $client = new Client();
        $crawler = $client->request('GET', $this->url);

        $link = $crawler->filter('a[data-tab="TABREVIEWS"]')->first()->link();
        $crawler = $client->click($link);

        while ($this->processPage($crawler)) {
            $url = $crawler->filter('.pagination a.next')->first()->link();
            $crawler = $client->click($url);
        }

    }

    public function getNewReviews() {
        return $this->new_reviews;
    }

    private function processPage($crawler) {
        $count_before_processing = count($this->new_reviews);

        $crawler->filter('.guides-review-wrapper')->each(function ($node) {
            $this->processNode($node);
        });


        if (count($this->new_reviews) <= $count_before_processing) {
            return false;
        }

        if ($crawler->filter('.pagination a.next')->count() == 0) {
            return false;
        }

        //Return true if there is a "next page" link
        return true;
    }

    private function processNode($node) {
        $overall_score = (int) $node->filter('span.rating-number')->first()->text();
        $rawDate = $node->filter('.guides-top-info-wrapper p')->first()->filter('span')->eq(1)->text();
        $date = Carbon::createFromFormat('d M Y',$rawDate)->toDateString();
        $liked = null;
        $disliked = null;

        $node->filter('.text-quotes p')->each(function ($node) use (&$liked, &$disliked) {
            $children = $node->children();
            if ($children->eq(0)->text() == "Liked:") {
                $liked = $children->eq(1)->text();
            } else if ($children->eq(0)->text() == "Disliked:") {
                $disliked = $children->eq(1)->text();
            }
        });

        $scores = $node->filter('div.top ul li span');
        $atmosphere_score = substr($scores->eq(0)->text(),0,-1);
        $cleanliness_score = substr($scores->eq(1)->text(),0,-1);
        $facilities_score = substr($scores->eq(2)->text(),0,-1);
        $location_score = substr($scores->eq(3)->text(),0,-1);
        $safety_score = substr($scores->eq(4)->text(),0,-1);
        $staff_score = substr($scores->eq(5)->text(),0,-1);
        $value_score = substr($scores->eq(6)->text(),0,-1);

        $data = [
            'overall_score' => $overall_score,
            'atmosphere_score' => $atmosphere_score,
            'cleanliness_score' => $cleanliness_score,
            'facilities_score' => $facilities_score,
            'location_score' => $location_score,
            'safety_score' => $safety_score,
            'staff_score' => $staff_score,
            'value_score' => $value_score,
            'date' => $date,
            'liked' => $liked,
            'disliked' => $disliked
        ];

        //if it's older than our newest review in the DB then we don't need it
        if (($this->last_reviews->count() > 0) && ($data['date'] < $this->last_reviews[0]->date->toDateString())) {
            return;
        }

        //If it's same date as last review date, check if this is a diff review
        if (($this->last_reviews->count() > 0) && ($data['date'] == $this->last_reviews[0]->date->toDateString())) {

            foreach($this->last_reviews as $last_review) {
                if (($last_review->overall_score == $data['overall_score'])
                    && ($last_review->liked == $data['liked'])
                    && ($last_review->disliked == $data['disliked'])) {
                    //Must be the same review, so don't add it
                    return;
                }
            }
        }

        //Passed all checks so this must be a new review.
        array_push($this->new_reviews, $data);
    }
}