<?php namespace App\HostelPro\Models\Reviews;

use Goutte\Client;
use Carbon\Carbon;

class HW2ReviewScraper extends ReviewScraper {

    public function __construct($url, $last_reviews = null) {
        $this->url = $url;
        $this->last_reviews = $last_reviews;
    }

    public function crawl() {

        $client = new Client();
        $crawler = $client->request('GET', $this->url);

        //Add on the following params to the review URL so we get all possible reviews
        $url = $crawler->filter('a.review_link')->first()->attr('href') . '?lang=all&translateTo=en&showOlderReviews=1';
        $crawler = $client->request('GET', $url);

        while ($this->processPage($crawler)) {
            $url = $crawler->filter('.pagination li.pagination-next a')->first()->link();
            $crawler = $client->click($url);
        }

    }

    public function getNewReviews() {
        return $this->new_reviews;
    }

    private function processPage($crawler) {
        $count_before_processing = count($this->new_reviews);

        $crawler->filter('.reviewdetails')->each(function ($node) {
            $this->processNode($node);
        });

        if (count($this->new_reviews) <= $count_before_processing) {
            return false;
        }

        if ($crawler->filter('.pagination li.pagination-next a')->count() == 0) {
            return false;
        }

        //Return true if there is a "next page" link
        return true;
    }

    private function processNode($node) {
        $overall_score = (int) ($node->filter('.textrating')->first()->text() * 10);
        $rawDate = trim($node->filter('.reviewdate')->first()->text());
        $date = Carbon::createFromFormat('jS M Y',$rawDate)->toDateString();
        $overall = trim($node->filter('.reviewtext')->first()->text());

        $scores = $node->filter('ul.ratingbreakdown li span');
        $atmosphere_score = (int) ($scores->eq(5)->text() * 10);
        $cleanliness_score = (int) ($scores->eq(6)->text() * 10);
        $facilities_score = (int) ($scores->eq(3)->text() * 10);
        $location_score = (int) ($scores->eq(2)->text() * 10);
        $safety_score = (int) ($scores->eq(1)->text() * 10);
        $staff_score = (int) ($scores->eq(4)->text() * 10);
        $value_score = (int) ($scores->eq(0)->text() * 10);

        $data = [
            'overall_score' => $overall_score,
            'atmosphere_score' => $atmosphere_score,
            'cleanliness_score' => $cleanliness_score,
            'facilities_score' => $facilities_score,
            'location_score' => $location_score,
            'safety_score' => $safety_score,
            'staff_score' => $staff_score,
            'value_score' => $value_score,
            'date' => $date,
            'overall' => $overall
        ];

        //if it's older than our newest review in the DB then we don't need it
        if (($this->last_reviews->count() > 0) && ($data['date'] < $this->last_reviews[0]->date->toDateString())) {
            return;
        }

        //If it's same date as last review date, check if this is a diff review
        if (($this->last_reviews->count() > 0) && ($data['date'] == $this->last_reviews[0]->date->toDateString())) {

            foreach($this->last_reviews as $last_review) {
                if (($last_review->overall_score == $data['overall_score'])
                    && ($last_review->overall == $data['overall'])) {
                    //Must be the same review, so don't add it
                    return;
                }
            }
        }

        //Passed all checks so this must be a new review.
        array_push($this->new_reviews, $data);
    }
}