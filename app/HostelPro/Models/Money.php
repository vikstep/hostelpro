<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class Money extends Model {

    protected $table = 'money';

    protected $fillable = [''];

    protected $hidden = [''];

    public $timestamps = false;

    public function hostel() {
        return $this->belongsTo('App\HostelPro\Models\Hostel', 'hostel_id', 'id');
    }

    public function setAmountAttribute($value) {
        $this->attributes['amount'] = MoneyHelper::convertToCents($value);
    }

    public function getAmountAttribute($value) {
        return MoneyHelper::convertCentsToDollars($value);
    }
}