<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Builder;
use DB;
use App\HostelPro\Jobs\SyncRatesJob;
use  App\HostelPro\Models\MyAllocatorToken;

class DailyRate extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_rate';

    protected $primaryKey = array('room_type_id', 'date');
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['price'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    public function roomtype() {
        return $this->belongsTo(RoomType::class);
    }

    public static function updateRates($hostelId, $startDate, $endDate, $roomTypeIds){
        if (MyAllocatorToken::where('hostel_id', '=', $hostelId)->count() > 0) {
            dispatch(new SyncRatesJob($hostelId, $startDate->format('Y-m-d'), $endDate->format('Y-m-d'), $roomTypeIds));
        }
    }


    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }


}