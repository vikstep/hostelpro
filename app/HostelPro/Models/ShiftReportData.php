<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class ShiftReportData extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shift_report_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = false;

    public function shift() {
        return $this->belongsTo('App\HostelPro\Models\ShiftReport', 'shift_report_id', 'id');
    }

}