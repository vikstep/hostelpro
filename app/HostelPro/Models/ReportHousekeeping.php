<?php namespace App\HostelPro\Models;

use DB;

class ReportHousekeeping
{

    public $hostel_id;
    public $date;
    public $formatted_date;

    public $rooms;
    public $data = [];

    public $today;
    public $yesterday;

    public function __construct($hostel_id, $date)
    {
        $this->hostel_id = $hostel_id;
        $this->date = $date;
        $this->formatted_date = DateHelper::convertStringDateToCarbon($date)->format('l jS \\of F Y');

        $this->rooms = Hostel::find($hostel_id)->rooms()->with('roomtype')->get();
        $this->today = $this->getBookings($date);
        $this->yesterday = $this->getBookings(DateHelper::subDays($date, 1));

        $this->initializeRoomsToZero();
        $this->calculate();
    }

    private function initializeRoomsToZero()
    {
        foreach ($this->rooms as $room) {
            $this->data[$room->id] = ['name' => $room->name, 'arr' => 0, 'dep' => 0, 'rem' => 0, 'niu' => 0, 'free' => $room->roomtype->number_of_guests];
        }
    }

    private function getBookings($date)
    {
        return Board::select('board.date', 'stay.booking_id', 'board.room_id', 'booking.label_type_id', DB::raw('count(board.date) AS number_of_guests'))
            ->join('stay', 'stay_id', '=', 'stay.id')
            ->join('booking', 'stay.booking_id', '=', 'booking.id')
            ->where('board.hostel_id', '=', $this->hostel_id)
            ->where('board.date', '=', $date)
            ->groupBy('board.room_id')
            ->groupBy('stay.booking_id')
            ->get();
    }

    private function calculate()
    {
        foreach ($this->today as $booking) {
            $this->calculateToday($booking);
        }

        foreach ($this->yesterday as $booking) {
            $this->calculateYesterday($booking);
        }
    }

    private function calculateYesterday($booking) {
        //We want to ignore any blocked bookings for yesterday (we only use blocked bookings in today)
        if ($booking->label_type_id == '18') {
            return;
        }

        $found = $this->findBooking('today', $booking->booking_id, $booking->room_id);

        //if we didn't find the booking then they're leaving this room.
        if ($found->count() == 0) {
            $this->data[$booking->room_id]['dep'] += $booking->number_of_guests;
        }

        //It's still in the room today
        if ($found->count() == 1) {

            $number_of_guests_tomorrow = $found->first()->number_of_guests;
            if ($booking->number_of_guests == $number_of_guests_tomorrow) {
                $this->data[$booking->room_id]['rem'] += $booking->number_of_guests;
                $this->data[$booking->room_id]['free'] -= $booking->number_of_guests;
            } else if ($booking->number_of_guests > $number_of_guests_tomorrow) {
                $this->data[$booking->room_id]['dep'] += $booking->number_of_guests - $number_of_guests_tomorrow;
                $this->data[$booking->room_id]['rem'] += $number_of_guests_tomorrow;
                $this->data[$booking->room_id]['free'] -= $number_of_guests_tomorrow;
            } else if ($booking->number_of_guests < $number_of_guests_tomorrow) {
                $this->data[$booking->room_id]['arr'] += $number_of_guests_tomorrow - $booking->number_of_guests;
                $this->data[$booking->room_id]['rem'] += $booking->number_of_guests;
                $this->data[$booking->room_id]['free'] -= $booking->number_of_guests;
            }

        }
    }

    private function calculateToday($booking) {
        if ($booking->label_type_id == '18') {
            $this->data[$booking->room_id]['niu'] += $booking->number_of_guests;
            $this->data[$booking->room_id]['free'] -= $booking->number_of_guests;
            return;
        }

        $found = $this->findBooking('yesterday', $booking->booking_id, $booking->room_id);

        //if we didn't find the booking then they're arriving into this room.
        if ($found->count() == 0) {
            $this->data[$booking->room_id]['arr'] += $booking->number_of_guests;
            $this->data[$booking->room_id]['free'] -= $booking->number_of_guests;
        }
    }

    private function findBooking($collection, $booking_id, $room_id)
    {

        return $this->{$collection}->filter(function ($booking) use ($booking_id, $room_id) {
            return $booking->booking_id == $booking_id && $booking->room_id == $room_id;
        });

    }

}