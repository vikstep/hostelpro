<?php namespace App\HostelPro\Models;

use Cache;
use Swap;

class MyAllocatorProcessor {

    private $myAllocatorLogIds = array();
    private $overrideExisting = false;

    public function addMyAllocatorLogId($myAllocatorLogId) {
        if (is_array($myAllocatorLogId)) {
            foreach($myAllocatorLogId as $id) {
                $this->addMyAllocatorLogId($id);
            }
        }

        array_push($this->myAllocatorLogIds, $myAllocatorLogId);
    }

    public function enableOverride() {
        $this->overrideExisting = true;
    }

    public function disableOverride() {
        $this->overrideExisting = false;
    }

    public function processAndSave() {
        foreach($this->myAllocatorLogIds as $myAllocatorLogId) {
            $this->processBooking($myAllocatorLogId);
        }

    }

    private function processBooking($myAllocatorLogId) {

        $myAllocatorBookingLog = MyAllocatorLog::find($myAllocatorLogId);
        if (empty($myAllocatorBookingLog->data)) {
            return true;
        }
        $myAllocatorBookingData = unserialize($myAllocatorBookingLog->data);
        $hostel_id = $myAllocatorBookingLog->hostel_id;
        $myallocator_id = $myAllocatorBookingData['MyallocatorId'];

        if ($myAllocatorBookingData['IsCancellation']) {
            $booking = Booking::where('myallocator_id', '=', $myallocator_id)->first();
            if ($booking) {
                $booking->cancel();
            }
            return true;
        }

        $count = Booking::where('myallocator_id', '=', $myallocator_id)->count();
        if (($count > 0) && ($this->overrideExisting == false) && ($myAllocatorBookingData['MyallocatorModificationTime'] == $myAllocatorBookingData['MyallocatorCreationTime'])) {
            return true;
        } else if ($count > 0) {
            Booking::where('myallocator_id', '=', $myallocator_id)->delete();
        }

        //Add a new guest. Change later to a foreach loop as we might have more than one guest.
        $guest = $this->addGuest($myAllocatorBookingData['Customers'][0], $hostel_id);

        $newBooking = new Booking;
        $newBooking->guest_id = $guest->id;
        $newBooking->hostel_id = $hostel_id;
        $newBooking->myallocator_id = $myallocator_id;
        $newBooking->myallocator_channel_id = (MyAllocatorChannel::where('id', '=', $myAllocatorBookingData['Channel'])->exists()) ? $myAllocatorBookingData['Channel'] : null;
        $newBooking->save();

        if ($guest->guest_notes) {
            $this->addBookingNote($newBooking->id, $guest->guest_notes);
        }

        $rooms = $this->mergeAllPossibleRooms($myAllocatorBookingData['Rooms']);

        foreach ($rooms as $room) {
            $formattedRoomData = $this->getFormattedRoomData($hostel_id, $room);

            $newBooking->addUnallocatedStay($formattedRoomData['startdate'],
                $formattedRoomData['enddate'],
                $formattedRoomData['room_type_id'],
                $formattedRoomData['number_of_guests'],
                $formattedRoomData['price_per_night']
            );

            $this->createFormattedLog($newBooking->id,
                $formattedRoomData['startdate'],
                $formattedRoomData['enddate'],
                $formattedRoomData['number_of_guests'],
                $formattedRoomData['myallocator_room_type_id'],
                $formattedRoomData['subtotal'],
                $formattedRoomData['currency']
            );

            if ($formattedRoomData['startdate'] == Hostel::find($hostel_id)->today_at_hostel) {
                //$newBooking->show_notification = true;
                //$newBooking->save();
            }

            /*
            if (!Allocation::hasAvailableCapacity($formattedRoomData['room_type_id'], $formattedRoomData['number_of_guests'], $formattedRoomData['startdate'], $formattedRoomData['enddate'])) {
                $newBooking->show_notification = true;
                $newBooking->save();
            }*/
        }

        $hostel_currency = Hostel::find($hostel_id)->currency;

        if (isset($myAllocatorBookingData['Commission'])) {
            if ($myAllocatorBookingData['CommissionCurrency'] != $hostel_currency->currency_code) {
                $commissionAmount = $this->convertCurrency($myAllocatorBookingData['CommissionCurrency'], $hostel_currency->currency_code, $myAllocatorBookingData['Commission']);
                $commission_currency = Currency::where('currency_code', '=', $myAllocatorBookingData['CommissionCurrency'])->first();
                $this->addCommission($newBooking->id, false, $myAllocatorBookingData['Commission'], $commission_currency->id, $commissionAmount, $hostel_currency->id);
            } else {
                $this->addCommission($newBooking->id, false, $myAllocatorBookingData['Commission'], $hostel_currency->id);
            }
        }

        if (isset($myAllocatorBookingData['Deposit'])) {
            if ($myAllocatorBookingData['DepositCurrency'] != $hostel_currency->currency_code) {
                $depositAmount = $this->convertCurrency($myAllocatorBookingData['DepositCurrency'], $hostel_currency->currency_code, $myAllocatorBookingData['Deposit']);
                $this->addDeposit($hostel_id, $newBooking->id, $depositAmount, $hostel_currency->id);

                $commission_currency = Currency::where('currency_code', '=', $myAllocatorBookingData['DepositCurrency'])->first();
                $this->addCommission($newBooking->id, true, $myAllocatorBookingData['Deposit'], $commission_currency->id, $depositAmount, $hostel_currency->id);
            } else {
                $this->addDeposit($hostel_id, $newBooking->id, $myAllocatorBookingData['Deposit'], $hostel_currency->id);
                $this->addCommission($newBooking->id, true, $myAllocatorBookingData['Deposit'], $hostel_currency->id);
            }
        }

        return true;
    }

    private function addCommission($booking_id, $is_deposit, $original_amount, $original_currency_id, $converted_amount = null, $converted_currency_id = null) {
        $newCommission = new Commission();
        $newCommission->booking_id = $booking_id;
        $newCommission->is_deposit = $is_deposit;
        $newCommission->original_amount = $original_amount;
        $newCommission->original_currency_id = $original_currency_id;
        if (isset($converted_amount) && isset($converted_currency_id)) {
            $newCommission->converted_amount = $converted_amount;
            $newCommission->converted_currency_id = $converted_currency_id;
        }
        $newCommission->save();
    }

    private function mergeAllPossibleRooms($rooms) {
        if (count($rooms) <= 1) {
            return $rooms;
        }

        for ($i = 0; $i < count($rooms)-1 ; $i++) {
            $price_per_night_room1 = $rooms[$i]['Price'] / $rooms[$i]['Units'] / (DateHelper::getDaysBetween($rooms[$i]['StartDate'],$rooms[$i]['EndDate'])+1);

            $price_per_night_room2 = $rooms[$i+1]['Price'] / $rooms[$i+1]['Units'] / (DateHelper::getDaysBetween($rooms[$i+1]['StartDate'],$rooms[$i+1]['EndDate'])+1);

            $price_per_night_room1 = round($price_per_night_room1, 2);
            $price_per_night_room2 = round($price_per_night_room2, 2);

            if ($price_per_night_room1 == $price_per_night_room2) {
                if (($rooms[$i]['StartDate'] == $rooms[$i+1]['StartDate']) && ($rooms[$i]['EndDate'] == $rooms[$i+1]['EndDate']))  {
                    $rooms[$i]['Units'] = $rooms[$i]['Units'] + $rooms[$i+1]['Units'];
                    $rooms[$i]['Price'] = $rooms[$i]['Price'] + $rooms[$i+1]['Price'];
                    unset($rooms[$i+1]);
                    $rooms = array_values($rooms);
                    if (array_key_exists($i, $rooms)) {
                        $i--;
                    }

                }
            }
        }

        return $rooms;
    }

    private function addGuest($customer, $hostel_id) {
        $guest = new Guest();
        $guest->firstname = $customer['CustomerFName'] ?? '';
        $guest->lastname = $customer['CustomerLName'] ?? null;
        $guest->email = $customer['CustomerEmail'] ?? null;
        $guest->phone = $customer['CustomerPhone'] ?? $customer['CustomerPhoneMobile'] ?? null;
        $guest->address_line_1 = $customer['CustomerAddress'] ?? null;
        if (isset($customer['CustomerCountry']) || isset($customer['CustomerNationality'])) {
            $guest->country_id = $this->convertMyAllocatorCountryToHostelProId($customer['CustomerCountry'] ?? $customer['CustomerNationality']);
            if ((isset($guest->country_id)) && (isset($customer['CustomerCity']))) {
                $guest->city_id = $this->convertMyAllocatorCityToHostelProId($guest->country_id, $customer['CustomerCity']);
            }
        }
        $guest->zip_code = $customer['CustomerPostCode'] ?? null;
        $guest->guest_notes = $customer['CustomerNote'] ?? null;
        if (isset($customer['CustomerGender']) || isset($customer['CustomerNationality'])) {
            $guest->gender = $this->convertMyAllocatorGenderToHostelProFormat($customer['CustomerGender'] ?? $customer['CustomerNationality']);
        }
        $guest->hostel_id = $hostel_id;
        $guest->save();
        return $guest;
    }

    private function convertMyAllocatorCountryToHostelProId($country_name) {
        if (($country_name == "USA") || ($country_name == "United States of America") || ($country_name == "America")) {
            return 184;
        }
        if (($country_name == "England")
            || ($country_name == "Great Britain")
            || ($country_name == "UK")
            || ($country_name == "Scotland")
            || ($country_name == "Wales")
            || ($country_name == "Northern Ireland")) {
            return 183;
        }
        if ($country_name == "South Korea") {
            return 90;
        }
        if ($country_name == "China") {
            return 36;
        }
        $country = Country::where('country_name', '=', $country_name)->orWhere('country_code', '=', $country_name)->first();
        if (!$country) {
            //echo "Unknown Country: " . $country_name . "<br>";
            return null;
        }
        return $country->id;
    }

    private function convertMyAllocatorCityToHostelProId($country_id, $city_name) {
        $country = Country::find($country_id);
        $city = City::where('name', '=', $city_name)->where('country_code', '=', $country->country_code)->first();
        if (!$city) {
            //echo "Unknown City: " . $city_name . " , country is: " . $country->name . "<br>";
            return null;
        }
        return $city->geonameid;
    }

    private function convertMyAllocatorGenderToHostelProFormat($gender) {

        /* NOTE: According to MyAllocator API Gender can also be Free text, so might need to add in some additional checks for that. */

        if (($gender == "FE") || ($gender == "Female") || ($gender == "FEMALE"))  {
            return 'Female';
        }

        if (($gender == "MA") || ($gender == "Male") || ($gender == "MALE"))  {
            return 'Male';
        }

        return 'Mixed';
    }

    private function getFormattedRoomData($hostel_id, $room) {

        $myallocator_room_type_id = $room['RoomTypeIds'][0];

        //Set to myallocator room type id or set to first one if it's an invalid ID.
        $room_type = RoomType::where('myallocator_id', '=', $myallocator_room_type_id)->first() ?? RoomType::where('hostel_id', '=', $hostel_id)->whereNotNull('myallocator_id')->first();
        $room_type_id = $room_type->id;

        $hostel_currency_code = Hostel::find($hostel_id)->currency->currency_code;

        if ($room_type->type == "Private") {
            $number_of_guests = $room['Units'] * $room_type->number_of_guests;
        } else {
            $number_of_guests = $room['Units'];
        }

        $startdate = $room['StartDate'];
        $enddate = $room['EndDate'];

        if ((count($room['RoomTypeIds']) > 1) && (!Allocation::hasAvailableCapacity($room_type_id, $number_of_guests, $startdate, $enddate))) {
            $myallocator_room_type_id = $room['RoomTypeIds'][1];
            $room_type_id = RoomType::where('myallocator_id', '=', $myallocator_room_type_id)->first()->id;
        }

        $number_of_nights = DateHelper::countNumberOfDays($startdate, $enddate);

        if (isset($room['Price'])) {

            if ($room_type->type == "Private") {
                $price_per_night = $room['Price'] / $room['Units'] / $number_of_nights / $room_type->number_of_guests;
            } else {
                $price_per_night = $room['Price'] / $number_of_guests / $number_of_nights;
            }

            if ((!empty($room['Currency'])) && ($room['Currency'] != $hostel_currency_code)) {
                $price_per_night = $this->convertCurrency($room['Currency'], $hostel_currency_code, $price_per_night);
            }

            $price_per_night = MoneyHelper::convertToCents($price_per_night);
        } else {
            $price_per_night = 0;
        }

        $formatted = array();
        $formatted['startdate'] = $startdate;
        $formatted['enddate'] = $enddate;
        $formatted['room_type_id'] = $room_type_id;
        $formatted['number_of_guests'] = $number_of_guests;
        $formatted['price_per_night'] = $price_per_night;
        $formatted['myallocator_room_type_id'] = $myallocator_room_type_id;
        $formatted['subtotal'] = MoneyHelper::convertToCents($room['Price'] ?? 0);
        if (isset($room['Currency'])) {
            $formatted['currency'] = Currency::where('currency_code', '=', $room['Currency'])->first()->id;
        } else {
            $formatted['currency'] = Hostel::find($hostel_id)->currency->id;
        }

        return $formatted;
    }

    private function createFormattedLog($booking_id, $start_date, $end_date, $number_of_guests, $myallocator_room_type_id, $subtotal, $currency_id) {
        $newLog = new MyAllocatorFormattedLog();
        $newLog->booking_id = $booking_id;
        $newLog->start_date = $start_date;
        $newLog->end_date = $end_date;
        $newLog->number_of_guests = $number_of_guests;
        $newLog->myallocator_room_type_id = $myallocator_room_type_id;
        $newLog->subtotal = $subtotal;
        $newLog->currency_id = $currency_id;
        $newLog->save();
    }

    private function addDeposit($hostel_id, $booking_id, $depositAmount, $currency_id) {
        $newItem = new AdditionalItem();
        $newItem->name = 'Booking Deposit';
        $newItem->price = 0 - MoneyHelper::convertToCents($depositAmount);
        $newItem->units = '1';
        $newItem->total = 0 - MoneyHelper::convertToCents($depositAmount);
        $newItem->hostel_id = $hostel_id;
        $newItem->booking_id = $booking_id;
        $newItem->currency_id = $currency_id;
        $newItem->save();
    }

    private function addBookingNote($booking_id, $text) {
        $bookingNote = new BookingNote();
        $bookingNote->booking_id = $booking_id;
        $bookingNote->text = $text;
        $bookingNote->save();
    }

    private function convertCurrency($from, $to, $amount) {

        $rate = Cache::remember('rate-' . $from . '/' . $to, 3600, function() use ($from, $to) {
            $swap = (new Swap\Builder())
                ->add('fixer')
                ->build();
            return $swap->latest($from . '/' . $to)->getValue();
        });

        return $amount * $rate;
    }


}