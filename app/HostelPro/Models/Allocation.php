<?php namespace App\HostelPro\Models;

use DB;
use Exception;

class Allocation {

    public static function hasAvailableCapacity($room_type_id, $number_of_guests, $startdate, $enddate) {

        //Create an array that contains all the room ids
        $room_ids = Room::where('room_type_id', '=', $room_type_id)->pluck('id')->all();

        //Work out the maximum possible capacity per day (number of rooms * number_of_guests)
        $max_capacity_per_day = count($room_ids) * RoomType::find($room_type_id)->number_of_guests;

        $dates = DateHelper::getArrayOfDatesByString($startdate, $enddate);
        //If end date is before start date
        if (count($dates) == 0) {
            return false;
        }

        //If trying to book more beds than max possible capacity
        if ($max_capacity_per_day < $number_of_guests) {
            return false;
        }

        $boards = Board::select('date', DB::raw('count(date) as total'))
            ->whereIn('room_id', $room_ids)
            ->whereIn('date', $dates)
            ->groupBy('date')
            ->orderBy('date')
            ->get();

        $boards = $boards->keyBy('date')->toArray();

        foreach ($dates as $date) {
            $number_of_guests_today = 0;

            if (array_key_exists($date, $boards)) {
                $number_of_guests_today += $boards[$date]['total'];
            }

            if (($max_capacity_per_day - $number_of_guests_today) < $number_of_guests) {
                //Not enough capacity for this day, so return false
                return false;
            }
        }

        //Passed everything, must be enough capacity
        return true;
    }

    public static function isOverbooking($room_type_id, $number_of_guests, $date) {

        //Create an array that contains all the room ids
        $room_ids = Room::where('room_type_id', '=', $room_type_id)->pluck('id')->all();

        //Work out the maximum possible capacity per day (number of rooms * number_of_guests)
        $max_capacity_per_day = count($room_ids) * RoomType::find($room_type_id)->number_of_guests;

        //If trying to book more beds than max possible capacity
        if ($number_of_guests > $max_capacity_per_day) {
            return true;
        }

        $boards = Board::select('date', DB::raw('count(date) as total'))
            ->whereIn('room_id', $room_ids)
            ->where('date', '=', $date)
            ->groupBy('date')
            ->orderBy('date')
            ->get();

        $boards = $boards->keyBy('date')->toArray();

        if (isset($boards[$date])) {
            if ($number_of_guests > $max_capacity_per_day - $boards[$date]['total']) {
                return true;
            }
        }

        //Passed everything; must not be an overbooking
        return false;
    }

    /* BELOW IS TO SWITCH EVERYTHING TO BOARD */

    public static function changeToAllocated($bookingid, $stay_ids, $allocated_by_system = TRUE) {
        $tempStays = Stay::where('booking_id', '=', $bookingid)->where('temp', '=', true)->pluck('id')->all();
        foreach ($tempStays as $tempStay) {
            if (!in_array($tempStay, $stay_ids)) {
                #echo "Failed 1";
                return false;
            }
        }

        //Perform some cleanup in case this was already allocated
        $newStays = BoardTemp::where('booking_id', '=', $bookingid)->pluck('stay_id')->all();
        if (count($newStays) == 0) {
            #echo "Failed 2";
            return false;
        }
        Stay::whereIn('id', $newStays)->update(array('temp' => false));
        $oldStays = Stay::where('booking_id', '=', $bookingid)->pluck('id')->all();
        $stays_to_delete = array_diff($oldStays, $newStays);
        Stay::whereIn('id', $stays_to_delete)->delete();
        //End cleanup

        $status = DB::statement('INSERT INTO board (date,room_id,bed_number,stay_id,hostel_id) SELECT date,room_id,bed_number,stay_id,hostel_id FROM board_temp WHERE booking_id=:booking_id', array('booking_id' => $bookingid));
        if (!$status) {
            #echo "Failed 3";
            return false;
        }
        $booking = Booking::find($bookingid);
        BoardTemp::where('booking_id', '=', $booking->id)->delete();
        BoardUnallocated::where('booking_id', '=', $booking->id)->delete();
        Stay::where('booking_id', '=', $booking->id)->whereNull('room_id')->delete();
        $booking->status = 'Allocated';
        $booking->allocated_by_system = $allocated_by_system;
        $booking->show_notification = false;
        $booking->save();
        return true;
    }

    public static function copyAllocatedStaysToTemp($bookingid) {
        $newStays = array();

        $booking = Booking::find($bookingid);

        //Perform some cleanup in case we are re-opening a bookingid
        $oldStays = Stay::where('booking_id', '=', $booking->id)->where('temp', '=', true)->pluck('id')->all();
        #$oldStays = BoardTemp::where('booking_id', '=', $booking->id)->pluck('stay_id');
        Stay::whereIn('id', $oldStays)->delete();

        $stays = Stay::where('booking_id', '=', $bookingid)->get();
        $staysArray = Stay::where('booking_id', '=', $bookingid)->pluck('id')->all();
        foreach ($stays as $stay) {
            $newStay = $stay->replicate();
            $newStay->temp = true;
            $newStay->save();
            $newStays[$stay->id] = $newStay->id;
        }

        $originalBoards = Board::whereIn('stay_id', $staysArray)->get();

        $data = array();
        foreach ($originalBoards as $originalBoard) {
            array_push($data, array('date' => $originalBoard->date, 'room_id' => $originalBoard->room_id, 'bed_number' => $originalBoard->bed_number, 'stay_id' => $newStays[$originalBoard->stay_id], 'booking_id' => $bookingid, 'hostel_id' => $booking->hostel_id));
        }
        try {
            BoardTemp::insert($data);
        } catch (Exception $e) {
            //Under certain situations we can have other temp bookings in the spot of where this booking is, if so clear it and then the insert will work.
            foreach ($stays as $stay) {
                BoardTemp::where('room_id', '=', $stay->room_id)->whereBetween('date', [$stay->start_date, $stay->end_date])->delete();
            }
            BoardTemp::insert($data);
        }

        return array_values($newStays);
    }

    /** New Code is below */

    public static function processDailyAllocations($hostel_id, $date) {

        //Get a list of all booking ids that we need to unallocate. Don't include checked in, checked out or locked bookings
        $allocateds = Board::selectRaw('booking.id as id, min(board.date) as date, count(date) as bednights')
            ->where('board.hostel_id', '=', $hostel_id)
            ->where('booking.allocated_by_system', '=', true)
            ->where(function($query) {
                $query->whereNull('label_type_id')
                    ->orWhereIn('label_type_id', ['3', '6', '21']);
            })
            ->leftJoin('stay', 'board.stay_id', '=', 'stay.id')
            ->leftJoin('booking', 'stay.booking_id', '=', 'booking.id')
            ->having('date', '>=', $date)
            ->groupBy('booking.id')
            ->orderBy('board.date', 'ASC')
            ->get();

        $booking_ids = $allocateds->pluck('id')->all();

        if (count($booking_ids) == 0) {
            return true;
        }

        Booking::unallocateMany($booking_ids);

        $allocator = new SmartAllocator($hostel_id);
        $allocator->addBookingArray($booking_ids);
        $allocator->processAndSave();
        self::changeManyToAllocated($booking_ids);

        return true;
    }

    public static function changeManyToAllocated(Array $booking_ids) {
        #dd($booking_ids);
        if (count($booking_ids) == 0) {
            return true;
        }

        $newStays = BoardTemp::whereIn('booking_id', $booking_ids)->distinct('stay_id')->pluck('stay_id')->all();
        $oldStays = Stay::whereIn('booking_id', $booking_ids)->where('temp', '=', false)->distinct('stay_id')->pluck('id')->all();

        $stays_to_delete = array_diff($oldStays, $newStays);

        Stay::whereIn('id', $stays_to_delete)->delete();

        $formatted_booking_ids = implode(',', $booking_ids);

        $status = DB::statement('INSERT INTO board (date,room_id,bed_number,stay_id,hostel_id) SELECT date,room_id,bed_number,stay_id,hostel_id FROM board_temp WHERE booking_id IN (' . $formatted_booking_ids . ')');

        //Probably don't need this line
        BoardTemp::whereIn('booking_id', $booking_ids)->delete();

        BoardUnallocated::whereIn('booking_id', $booking_ids)->delete();

        //Clean up overbookings
        Stay::whereIn('booking_id', $booking_ids)->whereNull('room_id')->delete();

        //Use the query builder instead of Eloquent so we don't update the timestamps
        DB::table('booking')->whereIn('id', $booking_ids)->update(['status' => 'Allocated']);

        Stay::whereIn('id', $newStays)->update(array('temp' => false));

        $bookings = Booking::whereIn('id', $booking_ids)->with('nonTempStays')->get();

        foreach ($bookings as $booking) {
            $split = $booking->groupHasBeenSplit();
            if (($split) && (($booking->label_type_id == 6) || ($booking->label_type_id == null))) {
                $booking->timestamps = false;
                $booking->setLabel(21);
            } else if (($split == false) && ($booking->label_type_id == 21)) {
                $booking->timestamps = false;
                $booking->setLabel(6);
            }
        }

        return true;
    }

    public static function smartAllocateLargeBookings($hostel_id) {

        DB::beginTransaction();

        $timezone = Hostel::find($hostel_id)->timezone->timezone;
        $start_date = DateHelper::getToday($timezone);
        $end_date = DateHelper::addDays($start_date, 365);

        $start_date = "2016-03-01";
        $end_date = "2016-12-31";

        $allocateds = Board::select('booking.id')
            ->where('booking.hostel_id', '=', $hostel_id)
            ->where('booking.status', '!=', 'Cancelled')
            ->where('booking.allocated_by_system', '=', TRUE)
            ->whereBetween('board.date', [$start_date, $end_date])
            ->where(function($query) {
                $query->whereNull('label_type_id')
                ->orWhereIn('label_type_id', ['3','6','21']);

                //Below line is only for testing on local/staging
                #->orWhereIn('label_type_id', ['3','6','9', '21', '12', '15']);
            })
            ->join('stay', 'board.stay_id', '=', 'stay.id')
            ->join('booking', 'stay.booking_id', '=', 'booking.id')
            ->groupBy('booking.id')
            ->havingRaw('min(date) > \'' . DateHelper::addDays($start_date, 1) . '\'')
            ->get();

        if($allocateds->count() > 0) {
            Booking::unallocateMany($allocateds->pluck('id')->all());
        }

        $upcomings = BoardUnallocated::select('booking.show_notification', 'board_unallocated.booking_id', 'board_unallocated.date', 'board_unallocated.room_type_id',
            DB::raw('max(board_unallocated.number_of_guests) as guests'),
            DB::raw('room_types.number_of_guests as max'),
            DB::raw('min(date) as start_date'),
            DB::raw('max(date) as end_date'),
            DB::raw('DATEDIFF(max(date), min(date)) as days'))
            ->join('room_types', 'board_unallocated.room_type_id', '=', 'room_types.id')
            ->join('booking', 'board_unallocated.booking_id', '=', 'booking.id')
            ->where('booking.hostel_id', '=', $hostel_id)
            ->where('booking.allocated_by_system', '=', TRUE)
            ->where('booking.status', '!=', 'Cancelled')
            ->where('booking.show_notification', '=', false)
            ->whereBetween('date', [$start_date, $end_date])
            //->groupBy('room_type_id')
            ->groupBy('booking_id')
            //->groupBy('date')
            ->orderBy('guests', 'DESC')
            ->orderBy('days', 'DESC')
            ->get();


        foreach($upcomings as $upcoming) {
            $allocator = new SmartAllocator($hostel_id);
            $allocator->addBooking($upcoming->booking_id);
            $booking_ids_to_allocate = $allocator->processAndSave();
            self::changeManyToAllocated($booking_ids_to_allocate);
        }

        /*
        $allocator = new SmartAllocator($hostel_id);
        foreach($upcomings as $upcoming) {
            if (($upcoming->guests / $upcoming->max >= 0.00)) {
                $allocator->addBooking($upcoming->booking_id);
            }
        }

        $booking_ids_to_allocate = $allocator->processAndSave();
        self::changeManyToAllocated($booking_ids_to_allocate);*/

        DB::commit();
    }


}