<?php namespace App\HostelPro\Models;

use MyAllocatorApi\AriUpdate;
use DB;

class MyAllocatorSyncAvailability {

    public function __construct($hostelId, $startdate, $enddate) {
        //We have the below line for Daemon mode
        DB::reconnect();
        
        $api = new AriUpdate($hostelId);

        //$room_types = RoomType::where('hostel_id', '=', $hostelId)->pluck('id', 'myallocator_id');
        $room_types = RoomType::where('hostel_id', '=', $hostelId)->get();
        $dates = DateHelper::getArrayOfDatesByString($startdate, $enddate);
        $allocationsToSend = array();

        $calendar = new Calendar($hostelId, $startdate, DateHelper::countNumberOfDays($startdate, $enddate));
        $calendar->getAvailability();

        //dd($room_types);

        foreach ($dates as $date) {
            $i = 0;
            foreach($room_types as $room_type) {
                if (isset($calendar->availability[$i]['availability'][$date]) && !empty($room_type->myallocator_id)) {
                    $data = array('RoomId' => $room_type->myallocator_id,
                        'StartDate' => $date,
                        'EndDate' => $date,
                        'Units' => ($calendar->availability[$i]['availability'][$date] > 0) ? $calendar->availability[$i]['availability'][$date] : 0);
                    array_push($allocationsToSend, $data);
                }
                $i++;
            }
        }

        $params = array(
            'Channels' => ['all'],
            'Allocations' => $allocationsToSend,
            'Options' => [
                'QueryForStatus' => true,
                'IgnoreInvalidRooms' => true
            ]
        );
        //dd($params);
        $rsp = $api->callApiWithParams($params);
        //dd($rsp);
    }

}