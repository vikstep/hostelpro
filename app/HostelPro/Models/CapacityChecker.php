<?php namespace App\HostelPro\Models;

use DB;

class CapacityChecker
{
    private $hostel_id;
    private $start_date;
    private $end_date;
    private $number_of_guests;

    private $room_type_id = null;
    private $room_ids = array();
    private $room_collection = array();

    private $booking = null;

    private $ignore_stay_ids = array();

    public function __construct($hostel_id, $start_date, $end_date, $number_of_guests) {
        $this->hostel_id = $hostel_id;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->number_of_guests = $number_of_guests;
    }

    public function setRoomTypeId($room_type_id) {
        $this->room_type_id = $room_type_id;
    }

    public function setRoomId($room_ids) {
        if (is_array($room_ids)) {
            foreach ($room_ids as $room_id) {
                $this->setRoomId($room_id);
            }
        } else {
            array_push($this->room_ids, $room_ids);
        }
    }

    public function setBookingId($booking_id) {

        $this->booking = Booking::where('hostel_id', '=', $this->hostel_id)
            ->where('id', '=', $booking_id)
            ->with('stays')
            ->first();

        //If we didn't find a booking then set to null so that we don't encounter an error later when we run check()
        if (!$this->booking) {
            $this->booking = null;
        }

    }

    public function setIgnoreStay($stay_ids) {
        if (is_array($stay_ids)) {
            foreach ($stay_ids as $stay_id) {
                $this->setIgnoreStay($stay_id);
            }
        } else {
            array_push($this->ignore_stay_ids, $stay_ids);
        }
    }

    private function getRoomCollection() {
        $this->room_collection = Room::selectRaw('room.id AS id, room_types.id AS room_type_id, number_of_guests')
            ->where('hostel_id', '=', $this->hostel_id)
            ->where('room_type_id', '=', $this->room_type_id)
            ->orWhereIn('room.id', $this->room_ids)
            ->leftJoin('room_types', 'room.room_type_id', '=', 'room_types.id')
            ->get();
    }

    public function check() {

        $this->getRoomCollection();
        if ($this->room_collection->count() == 0) {
            //No matching rooms found, therefore we don't have enough capacity.
            return false;
        }

        //Work out the maximum possible capacity per day
        $max_capacity_per_day = $this->room_collection->sum('number_of_guests');

        $dates = DateHelper::getArrayOfDatesByString($this->start_date, $this->end_date);
        if (count($dates) == 0) {
            //If end date is before start date
            return false;
        }

        //Initialize all the dates to 0
        $dates = array_fill_keys($dates, 0);

        if ($this->number_of_guests > $max_capacity_per_day) {
            //If trying to book more beds than max possible capacity
            return false;
        }

        $boards_collection = Board::select('date', DB::raw('count(date) as total'))
            ->whereIn('room_id', $this->room_collection->pluck('id')->all())
            ->whereIn('date', array_keys($dates))
            ->groupBy('date')
            ->orderBy('date');

        if ($this->booking) {
            $stay_ids_to_ignore = $this->booking->stays->pluck('id')->all();
            $boards_collection = $boards_collection->whereNotIn('stay_id', $stay_ids_to_ignore);
        }

        $boards_collection = $boards_collection->get();

        foreach ($boards_collection as $board) {
            $dates[$board->date] += $board->total;
        }

        if ($this->booking) {

            $boards_temp_collection = BoardTemp::select('date', DB::raw('count(date) as total'))
                ->whereIn('room_id', $this->room_collection->pluck('id')->all())
                ->where('booking_id', '=', $this->booking->id)
                ->whereNotIn('stay_id', $this->ignore_stay_ids)
                ->whereIn('date', array_keys($dates))
                ->groupBy('date')
                ->orderBy('date')
                ->get();

            foreach ($boards_temp_collection as $board) {
                $dates[$board->date] += $board->total;
            }
        }

        foreach ($dates as $date => $number_of_guests_today) {

            if ($this->number_of_guests > ($max_capacity_per_day - $number_of_guests_today)) {
                //Not enough capacity for this day, so return false
                return false;
            }
        }

        //Passed everything, must be enough capacity
        return true;
    }

}