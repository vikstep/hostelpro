<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class MyAllocatorFormattedLog extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'myallocator_formatted_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = true;

    public function roomtype() {
        return $this->belongsTo('App\HostelPro\Models\RoomType', 'myallocator_room_type_id', 'myallocator_id');
    }

}