<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class PaymillOffer extends Model {

    protected $table = 'paymill_offers';

    protected $fillable = [''];

    protected $hidden = [''];

    public $timestamps = true;
}