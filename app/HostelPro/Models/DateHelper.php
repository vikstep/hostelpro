<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use DatePeriod;
use DateTime;
use DateInterval;

class DateHelper extends Model {

    public static function countNumberOfDays($startdate, $enddate) {
        $startdate = DateHelper::convertStringDateToCarbon($startdate);
        $enddate = DateHelper::convertStringDateToCarbon($enddate);
        return ($enddate->diffInDays($startdate) + 1);
    }

    public static function convertStringDateToCarbon($date) {
        $carbonDate = Carbon::createFromFormat('Y-m-d', $date);
        return $carbonDate;
    }

    public static function convertStringDateTimeToCarbon($date) {
        $carbonDate = Carbon::createFromFormat('Y-m-d H:i', $date);
        return $carbonDate;
    }

    public static function convertDateToYearMonthDay($date) {
        return date("Y-m-d", strtotime($date));
    }

    public static function convertYearMonthDayToDate($date) {
        $carbonDate = Carbon::createFromFormat('Y-m-d', $date);
        return $carbonDate->format('d-m-Y');
    }

    public static function convertDateToYearMonthDayMinutes($date) {
        $temp = Carbon::createFromFormat('d-m-Y H:i', $date);
        return $temp->toDateTimeString();
    }

    public static function getToday($timezone) {
        return Carbon::now($timezone)->toDateString();
    }

    public static function getYesterday($timezone) {
        return Carbon::now($timezone)->subDay()->toDateString();
    }

    public static function getDayOfWeek($date) {
        return Carbon::createFromFormat('Y-m-d', $date)->format("D");
    }

    public static function addDays($start, $days) {
        $newdate = DateHelper::convertStringDateToCarbon($start);
        $newdate->addDays($days);
        return $newdate->toDateString();
    }

    public static function subDays($start, $days) {
        $newdate = DateHelper::convertStringDateToCarbon($start);
        $newdate->subDays($days);
        return $newdate->toDateString();
    }

    public static function getDaysBetween($startdate, $enddate) {
        $startdate = DateHelper::convertStringDateToCarbon($startdate);
        $enddate = DateHelper::convertStringDateToCarbon($enddate);
        return $startdate->diffInDays($enddate);
    }

    /*
    public static function getArrayOfDatesByString($startdate, $enddate) {
        //Convert start date from String to Carbon Object
        $startdate = DateHelper::convertStringDateToCarbon($startdate);

        //Convert end date from String to Carbon Object
        $enddate = DateHelper::convertStringDateToCarbon($enddate);

        //Pass the Carbon objects to the Carbon function and return the result
        return DateHelper::getArrayOfDates($startdate, $enddate);
    }*/

    public static function getArrayOfDatesByString($startdate, $enddate) {
        $date_array = array();
        $period = new DatePeriod(
            new DateTime($startdate),
            new DateInterval('P1D'),
            new DateTime($enddate)
        );
        foreach( $period as $date) {
            array_push($date_array,$date->format('Y-m-d'));
        }
        array_push($date_array, $enddate);
        return $date_array;
    }

    public static function getArrayOfDates(Carbon $startdate, Carbon $enddate) {
        //Create dates array
        $dates = Array();

        //Add all days between start date and end date to dates array
        while ($startdate->diffInDays($enddate, false) >= 0) {
            array_push($dates, $startdate->toDateString());
            $startdate->addDay();
        }
        return $dates;
    }

    public static function convertUTCDateTime($datetime, $timezone = 'UTC') {
        $carbonDate = Carbon::createFromFormat('Y-m-d H:i:s', $datetime);
        $carbonDate->tz($timezone);
        return $carbonDate->toDateTimeString();
    }

}