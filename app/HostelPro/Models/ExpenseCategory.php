<?php namespace App\HostelPro\Models;

use Illuminate\Database\Eloquent\Model;

class ExpenseCategory extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'expense_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = false;

    public function hostel() {
        return $this->belongsTo('App\HostelPro\Models\Hostel', 'hostel_id', 'id');
    }
}