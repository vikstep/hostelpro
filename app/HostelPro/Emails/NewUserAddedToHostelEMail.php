<?php
namespace App\HostelPro\Emails;

class NewUserAddedToHostelEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return '16b0dff7-1e85-443f-a7bc-e26816a89a4b';
    }

    /**
     * Get an array of variables for the CM email.
     *
     * @param  $user
     * @return array
     */
    public function variables($user, $hostelname, $url)
    {
        return [
            'firstname' => $user['firstname'],
            'hostelname' => $hostelname,
            'url' => $url
        ];
    }
}