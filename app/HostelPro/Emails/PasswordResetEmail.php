<?php
namespace App\HostelPro\Emails;

class PasswordResetEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return 'a99db57e-51e0-48e8-99c8-a6d796fd6cce';
    }

    /**
     * Get an array of variables for the CM email.
     *
     * @param  $user
     * @return array
     */
    public function variables($user, $url)
    {
        return [
            'firstname' => $user['firstname'],
            'url' => $url
        ];
    }
}