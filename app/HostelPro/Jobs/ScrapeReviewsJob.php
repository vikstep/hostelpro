<?php

namespace App\HostelPro\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\HostelPro\Services\ReviewScraperService;

class ScrapeReviewsJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $hostel_id;

    public function __construct($hostel_id)
    {
        $this->hostel_id = $hostel_id;
    }

    public function handle()
    {
        new ReviewScraperService($this->hostel_id);
    }
}