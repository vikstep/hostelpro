<?php

namespace App\HostelPro\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\HostelPro\Models\MyAllocatorSyncRates;

class SyncRatesJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $hostel_id;
    protected $start_date;
    protected $end_date;
    protected $room_type_ids;

    public function __construct($hostel_id, $start_date, $end_date, $room_type_ids = array())
    {
        $this->hostel_id = $hostel_id;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->room_type_ids = $room_type_ids;
    }

    public function handle()
    {
        new MyAllocatorSyncRates($this->hostel_id, $this->start_date, $this->end_date, $this->room_type_ids);
    }
}
