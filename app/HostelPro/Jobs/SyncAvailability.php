<?php

namespace App\HostelPro\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\HostelPro\Models\MyAllocatorSyncAvailability;

class SyncAvailability extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $hostel_id;
    protected $start_date;
    protected $end_date;

    public function __construct($hostel_id, $start_date, $end_date)
    {
        $this->hostel_id = $hostel_id;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    public function handle()
    {
        new MyAllocatorSyncAvailability($this->hostel_id, $this->start_date, $this->end_date);
    }
}