<?php

function errors_for($attribute, $errors) {
	return $errors->first($attribute, '<div class="alert alert-danger">:message</div>');
}

function setActive($route, $class = 'active')
{
    return (Route::currentRouteName() == $route) ? $class : '';
}

function displayTime($seconds = 0) {
	$hours = floor($seconds / 3600);
	$minutes = floor(($seconds / 60) % 60);
	$seconds = $seconds % 60;
	return sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);
}

function displayFormattedMoney($amount) {
	return \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($amount);
}