<?php namespace App\Exceptions;

#use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use GrahamCampbell\Exceptions\NewExceptionHandler as ExceptionHandler;

use Exception;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;

use MyAllocator\phpsdk\src\Exception\ApiConnectionException;
use MyAllocator\phpsdk\src\Exception\ApiAuthenticationException;

use Redirect;

use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e) {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e) {

        if ($e instanceof AuthenticationException) {
            return $this->unauthenticated($request, $e);
        }

        if (($e instanceof TokenMismatchException) && ($request->ajax())) {
            abort(401);
        }

        //Handle MyAllocator API Connection Exceptions
        if (($e instanceof ApiConnectionException)) {
            return Redirect::back()->with('flash_message', 'Communication Error: ' . $e->getMessage())->with('alert-class', 'alert-danger');
        }

        //Handle MyAllocator API Authentication Exception (ie when invalid Auth token provided)
        if (($e instanceof ApiAuthenticationException)) {
            return Redirect::back()->with('flash_message', 'Internal Error: Error Code 901: Invalid Auth Token')->with('alert-class', 'alert-danger');
        }

        //Handle ajax exceptions
        if (($e instanceof \PDOException) && ($request->ajax())) {
            $dbCode = trim($e->getCode());
            switch ($dbCode) {
                case 23000:
                    if (strpos($e->getMessage(), 'room_room_type_id_foreign') !== false) {
                        $errorMessage = "You must delete all rooms associated with this room type, before you can delete it.";
                    }
                    break;
                case 2002:
                    $errorMessage = 'Database is down';
                    break;
                /*default:
                    $errorMessage = 'Unknown database error';*/

            }
            if (isset($errorMessage)) {
                
                return response($errorMessage, "500")->header('Content-Type', 'text-plain');
            }
        }

//        if (config('app.debug') && !($e instanceof AuthenticationException)) {
//            return $this->renderExceptionWithWhoops($e);
//        }

        return parent::render($request, $e);

    }

    /**
     * Render an exception using Whoops.
     *
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    protected function renderExceptionWithWhoops(Exception $e) {
        $whoops = new \Whoops\Run;
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());

        return new \Illuminate\Http\Response(
            $whoops->handleException($e),
            $e->getStatusCode(),
            $e->getHeaders()
        );
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        return redirect()->guest('login');
    }

}
