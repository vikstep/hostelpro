<?php namespace App\Console;

use App\HostelPro\Services\DailyEmailChecker;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\HostelPro\Models\DailyAllocationChecker as DailyAllocationChecker;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\UpdateDiscountRates',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{

		$schedule->call(function () {
			$checker = new DailyAllocationChecker();
			$checker->checkAllHostels();
		})->everyFiveMinutes();

		$schedule->call(function () {
			$checker = new DailyEmailChecker();
			$checker->checkAllHostels();
		})->daily();

		$schedule->command('inspire')
				 ->hourly();

		$schedule->command('updateRates:updateRates')
				 ->daily();
	}

}
