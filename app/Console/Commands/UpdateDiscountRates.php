<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\HostelPro\Models\DiscountRate;
use Illuminate\Support\Facades\Log;

class UpdateDiscountRates extends Command
{

    protected $signature = 'updateRates:updateRates';

    protected $name = 'updateRates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Discount Rates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::useFiles(storage_path().'/laravel.log');
        DiscountRate::updateDiscountRates();
    }
}
