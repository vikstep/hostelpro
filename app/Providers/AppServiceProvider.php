<?php namespace App\Providers;

use Bugsnag\BugsnagLaravel\BugsnagServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use App;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Relation::morphMap([
			'user' => App\HostelPro\Models\User::class,
		]);
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
			);

		if($this->app->environment() !== 'production') {
			$this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
		}

		$this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
		$this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);

		/*
		$this->app->bind('App\HostelPro\Models\PasswordBroker', function($app) {
			$key = $app['config']['app.key'];
			$userToken = new \App\Model\NewUserToken;
			$tokens = new \App\Repository\NewTokenRepository($key,$userToken);
			$user = new \App\Model\NewUser;
			$view = $app['config']['auth.password.email'];
			return new \App\Model\PasswordBroker($tokens, $users, $app['mailer'], $view);
		});*/
	}

}
