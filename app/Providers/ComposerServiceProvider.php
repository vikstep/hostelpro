<?php namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('includes.alerts', 'App\Http\ViewComposers\AlertsComposer');
        View::composer('includes.notifications', 'App\Http\ViewComposers\NotificationsComposer');

        View::composer('partials.addexpense', 'App\Http\ViewComposers\AddExpenseComposer');

        View::composer('partials.form.country', 'App\Http\ViewComposers\CountryComposer');

        View::composer('partials.form.selectroom', 'App\Http\ViewComposers\SelectRoomComposer');
    }

    /**
     * Register
     *
     * @return void
     */
    public function register()
    {
        //
    }

}