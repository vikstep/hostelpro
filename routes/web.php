<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');
Route::post('home', ['as' => 'landingpage.contact', 'uses' => 'WelcomeController@sendContactForm']);

Route::group(array(), function() {
	#Authentication
	Route::get('/login', ['as' => 'login', 'uses' => 'SessionsController@create']);
	Route::get('/logout', ['as' => 'logout', 'uses' => 'SessionsController@destroy']);
	Route::resource('sessions', 'SessionsController', ['only' => ['create', 'store', 'destroy']]);
	Route::get('/changeActiveHostel/{id}', ['as' => 'changeactivehostel', 'uses' => 'SessionsController@changeActiveHostel']);

	#Registration
	Route::get('/register', ['as' => 'register', 'uses' => 'RegistrationController@create']);
	Route::post('/getTimezonesbyCountryID', ['as' => 'getTimezonesbyCountryID', 'uses' => 'RegistrationController@getTimezonesbyCountryID']);
	Route::resource('registration', 'RegistrationController', ['only' => ['create', 'store']]);

	#MyAllocatorCallback
	Route::post('/myallocator_callback', ['as' => 'myallocator.callback', 'uses' => 'MyAllocatorCallbackController@store']);

	#Uptime Page (This page is checked by pingdom and used to auto-switchover)
	Route::get('/uptimecheck', ['as' => 'uptimecheck', 'uses' => 'AjaxController@uptimeCheck']);

	#Testing Page
	Route::get('/testing', ['as' => 'testing', 'uses' => 'TestingController@index']);
	Route::post('/testing_add_guest', ['as' => 'testing.addGuest', 'uses' => 'TestingController@addGuest']);
	Route::post('/testing_add_booking', ['as' => 'testing.addBooking', 'uses' => 'TestingController@addBooking']);
	
});

Route::group(array('middleware' => 'auth'), function()
{
	#Calendar
	Route::get('/calendar', ['as' => 'calendar', 'uses' => 'CalendarController@create']);
	#legacy (can be removed soon):
	Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'CalendarController@create']);

	#Setup Steps
	Route::get('/setup', ['as' => 'setup.currentstep', 'uses' => 'SetupController@currentStep']);
	Route::get('/setup/next', ['as' => 'setup.nextstep', 'uses' => 'SetupController@nextStep']);
	Route::get('/setup/previous', ['as' => 'setup.previousstep', 'uses' => 'SetupController@previousStep']);

	#Calendar Test
	Route::get('/test', ['as' => 'testing.test', 'uses' => 'TestingController@testBookings']);
	Route::get('/test2', ['as' => 'testing.test2', 'uses' => 'TestingController@testPrivateBookingsImport']);

	#Ajax
	Route::post('/ajax/calendar', ['as' => 'ajax.calendar', 'uses' => 'AjaxController@calendar']);
	Route::get('/ajax/searchBookings', ['as' => 'ajax.searchBookings', 'uses' => 'AjaxController@searchBookings']);
	Route::post('/ajax/bookingDetails', ['as' => 'ajax.booking', 'uses' => 'AjaxController@bookingDetails']);
	Route::post('/ajax/getNotifications', ['as' => 'ajax.getnotifications', 'uses' => 'AjaxController@getNotifications']);

	Route::post('/savePayment', ['as' => 'savePayment', 'uses' => 'AjaxController@savePayment']);

	#Settings Page
	Route::get('/settings', ['as' => 'settings', 'uses' => 'SettingsController@index']);

	#MyAllocator callback setup
	Route::get('/set_myallocator_callback', ['as' => 'set.myallocator.callback', 'uses' => 'MyAllocatorCallbackController@setCallbackURL']);
	Route::get('/set_availability', ['as' => 'set.myallocator.availability', 'uses' => 'MyAllocatorCallbackController@setAvailability']);

	#Users Page
	Route::resource('users', 'UsersController', ['only' => ['index', 'show', 'store', 'destroy']]);

	#Guests Page
	Route::resource('guest', 'GuestController', ['only' => ['update']]);

	#Booking Page
	Route::resource('booking', 'BookingController', ['only' => ['show', 'update', 'edit', 'store', 'destroy']]);
	Route::post('/booking/{id}/updatelabel', ['as' => 'booking.updatelabel', 'uses' => 'BookingController@updateLabel']);
	Route::post('/booking/{id}/block', ['as' => 'booking.block', 'uses' => 'BookingController@block']);
	Route::post('/booking/{id}/unblock', ['as' => 'booking.unblock', 'uses' => 'BookingController@unblock']);
	Route::post('/booking/{id}/lock', ['as' => 'booking.lock', 'uses' => 'BookingController@lock']);
	Route::post('/booking/{id}/unlock', ['as' => 'booking.unlock', 'uses' => 'BookingController@unlock']);
	Route::post('/booking/{id}/noshow', ['as' => 'booking.noshow', 'uses' => 'BookingController@noshow']);
	Route::post('/booking/{id}/unallocate', ['as' => 'booking.unallocate', 'uses' => 'BookingController@unallocate']);
	Route::post('/booking/{id}/addStayToExistingTemp', ['as' => 'booking.addstaytoexistingtemp', 'uses' => 'BookingController@addStayToExistingTemp']);
	Route::post('/booking/{id}/movestay', ['as' => 'booking.movestay', 'uses' => 'BookingController@moveStay']);
	Route::post('/booking/createNewBookingAndBlock', ['as' => 'booking.createnewbookingandblock', 'uses' => 'BookingController@createNewBookingAndBlock']);
	Route::post('/booking/createNewBookingAndStay', ['as' => 'booking.createnewbookingandstay', 'uses' => 'BookingController@createNewBookingAndStay']);
	Route::resource('booking.note', 'NoteController', ['only' => ['index', 'store']]);

	#POS Page
	Route::resource('pos', 'POSController', ['only' => ['create']]);
	Route::get('/getProductsByCategoryId/{categoryId}', ['as' => 'pos.getProductsByCategoryId', 'uses' => 'POSController@getProductsByCategoryId']);
	Route::delete('/deleteItem/{id}', ['as' => 'pos.deleteitem', 'uses' => 'POSController@deleteItem']);
	Route::delete('/deletePayment/{id}', ['as' => 'pos.deletepayment', 'uses' => 'POSController@deletePayment']);

	#Invoice Page
	Route::get('/invoice/{id}/', ['as' => 'invoice.get', 'uses' => 'POSController@getInvoice']);

	#Expenses
	Route::resource('expense', 'ExpenseController', ['only' => ['index', 'store', 'destroy']]);

	#Reports
	Route::group(['prefix' => 'reports', 'as' => 'reports.'], function()
	{
		Route::get('/housekeeping', ['as' => 'housekeeping', 'uses' => 'ReportsController@houseKeeping']);
		Route::post('/housekeeping', ['as' => 'housekeepingdownload', 'uses' => 'ReportsController@houseKeeping']);
		Route::get('/paymentview', ['as' => 'paymentview', 'uses' => 'ReportsController@paymentView']);

		#DayView
		Route::get('/dayview', ['as' => 'dayview', 'uses' => 'ReportsController@dayView']);

		#Revenue
		Route::get('/revenue', ['as' => 'revenue', 'uses' => 'ReportsController@revenue']);

		#Dashboard
		Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'ReportsController@dashboard']);

		#Booking List
		Route::get('/bookinglist', ['as' => 'bookinglist', 'uses' => 'ReportsController@bookingList']);
		Route::get('/bookinglist/data', ['as' => 'bookinglistdata', 'uses' => 'ReportsController@bookingListData']);

		#PnL
		Route::get('/pnl', ['as' => 'profitandloss', 'uses' => 'ReportsController@pnl']);
		
		#M2m
		Route::get('/m2m', ['as' => 'month2month', 'uses' => 'ReportsController@m2m']);

		#Shift Reports
		Route::resource('shift', 'ShiftController', ['only' => ['index', 'create', 'store', 'show']]);

		#Shift Reports
		Route::resource('reviews', 'ReviewsController', ['only' => ['create', 'index', 'store', 'destroy']]);
	});

	#Settings
	Route::group(['prefix' => 'settings', 'as' => 'settings.'], function()
	{
		#Billing
		Route::resource('billing', 'BillingController', ['only' => ['create', 'store', 'destroy']]);

		#Room + Room Types Settings Page
		Route::get('/roomsOverview', ['as' => 'RoomsOverview', 'uses' => 'SettingsController@roomsOverview']);
		Route::resource('rooms', 'RoomsController', ['only' => ['edit', 'update', 'store', 'destroy']]);
		Route::post('/rooms/sort', ['as' => 'RoomsSort', 'uses' => 'RoomsController@sort']);
		Route::resource('roomtypes', 'RoomTypesController', ['only' => ['index', 'store', 'destroy']]);

		#POS Category + POS Product Settings Page
		Route::get('/POS', ['as' => 'posOverview', 'uses' => 'SettingsController@POSOverview']);
		Route::resource('POSCategory', 'POSCategoryController', ['only' => ['index', 'store', 'destroy', 'edit', 'update']]);
		Route::post('/POSCategory/sort', ['as' => 'POSCategory.sort', 'uses' => 'POSCategoryController@sort']);
		Route::resource('POSProduct', 'POSProductController', ['only' => ['index', 'store', 'destroy', 'edit', 'update']]);
		Route::post('/POSProduct/sort', ['as' => 'POSProduct.sort', 'uses' => 'POSProductController@sort']);

		#Money
		Route::resource('money', 'MoneyController', ['only' => ['create', 'store', 'destroy']]);

		#Money
		Route::get('/emailtest/{emailtype}', ['as' => 'email.test', 'uses' => 'EmailController@sendTestEmail']);
		Route::resource('email', 'EmailController', ['only' => ['create', 'store']]);

		#Payment Types
		Route::resource('paymenttypes', 'PaymentTypesController', ['only' => ['create', 'store', 'destroy']]);
		Route::post('/paymenttypes/sort', ['as' => 'paymenttypes.sort', 'uses' => 'PaymentTypesController@sort']);

		#Expenses
		Route::resource('expensecategory', 'ExpenseCategoryController', ['only' => ['create', 'store', 'destroy']]);

		#Rates
		Route::resource('rates', 'RatesController', ['only' => ['index', 'store', 'update', 'destroy']]);
		Route::get('/rates/competitor', ['as' => 'rates.competitor', 'uses' => 'RatesController@competitor']);
		Route::get('/rates/daily', ['as' => 'rates.daily', 'uses' => 'RatesController@daily']);
		Route::resource('competitor', 'CompetitorRatesController', ['only' => ['index', 'store', 'destroy']]);
		
		Route::post('/daily/store', ['as' => 'daily.store', 'uses' => 'DailyRatesController@store']);
		Route::post('/daily/get', ['as' => 'daily.get', 'uses' => 'DailyRatesController@get']);
		
		Route::get('/rates/discount', ['as' => 'rates.discount', 'uses' => 'RatesController@discount']);
		
		Route::post('/discount/find', ['as' => 'discount.find', 'uses' => 'DiscountRatesController@find']);
		Route::post('/discount/save', ['as' => 'discount.save', 'uses' => 'DiscountRatesController@save']);



		Route::get('/myallocator/resync', ['as' => 'myallocator.resync', 'uses' => 'MyAllocatorController@resync']);
		Route::resource('myallocator', 'MyAllocatorController', ['only' => ['create', 'store']]);
	});

});