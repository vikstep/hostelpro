/* Customized JS for the landing page goes here */
$(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - 50
                }, 200);
                $(this).blur();
                return false;
            }
        }
    });
});

$('body').scrollspy({
    target: '#navbar-scrolling',
    offset: 50
});

$('.carousel').carousel({
    interval: 5000
});

$('#demo-gif-image').click(function () {
   $(this).addClass('hidden');
    $('#demo-gif').removeClass('hidden');
});

$('#contactForm').submit(function (event) {
    event.preventDefault();

    $('#contactFormButton').html('Sending...');

    $.ajax({
        type: "POST",
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success: function( response ) {
            $('#contactFormButton').html('Message Sent!');
        }
    });

});