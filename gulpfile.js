var elixir = require('laravel-elixir');

var paths = {
    'bootstrap': './resources/bower/bootstrap-sass-official/assets/'
};

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {

    /*mix.sass(['main.scss'], 'resources/assets/css/main.css')*/
    mix.styles([
        'app.min.css',
        'jquery-ui.css',
        'jquery-ui-timepicker-addon.min.css',
        'font-awesome.min.css',
        'select2.min.css',
        'nprogress.css',
        'c3.min.css',
        'datatables.min.css',
        'datatables.fixedheader.min.css',
        'datatables.buttons.min.css',
        'sweetalert2.min.css',
        'jquery.scrolling-tabs.min.css',
        'introjs.min.css',
        'summernote.css',
        'custom2.css'
    ], 'public/assets/css/all.css');

    mix.scripts([
        'jquery.min.js',
        'jquery-ui.min.js',
        'jquery-ui-timepicker-addon.min.js',
        'bootstrap.min.js',
        'select2.min.js',
        'nprogress.js',
        'jquery.noty.packaged.min.js',
        'stickytableheaders.min.js',
        //'Chart.min.js',
        'd3.min.js',
        'c3.min.js',
        'datatables.min.js',
        'datatables.fixedheader.min.js',
        'datatables.buttons.min.js',
        'datatables.buttons.colvis.min.js',
        'datatables.buttons.html5.min.js',
        'datatables.buttons.print.min.js',
        'datatables.buttons.bootstrap.min.js',
        'pdfmake.min.js',
        'vfs_fonts.js',
        'ajaxq.js',
        'sweetalert2.min.js',
        'moment.js',
        'draganddrop.js',
        'jquery.scrolling-tabs.min.js',
        'intro.min.js',
        'summernote.min.js'
    ], 'public/assets/js');

    mix.version([
        'public/assets/css/all.css',
        'public/assets/js/all.js'
    ]);
});