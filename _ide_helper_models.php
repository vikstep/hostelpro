<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\BaseModel
 *
 */
	class BaseModel {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Board
 *
 * @property string $date
 * @property integer $room_id
 * @property boolean $bed_number
 * @property integer $stay_id
 * @property integer $hostel_id
 * @property-read \App\HostelPro\Models\Stay $stay
 * @property-read \App\HostelPro\Models\Room $room
 */
	class Board {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\BoardTemp
 *
 * @property string $date
 * @property integer $room_id
 * @property boolean $bed_number
 * @property integer $stay_id
 * @property integer $booking_id
 * @property integer $hostel_id
 * @property-read \App\HostelPro\Models\Stay $stay
 */
	class BoardTemp {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\BoardUnallocated
 *
 * @property string $date
 * @property integer $room_type_id
 * @property boolean $number_of_guests
 * @property integer $booking_id
 * @property integer $price_per_night
 * @property-read \App\HostelPro\Models\Booking $booking
 */
	class BoardUnallocated {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Booking
 *
 * @property integer $id
 * @property string $status
 * @property integer $hostel_id
 * @property integer $guest_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $myallocator_id
 * @property integer $label_type_id
 * @property boolean $allocated_by_system
 * @property boolean $show_notification
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Stay[] $stays
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\BookingNote[] $notes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Stay[] $nonTempStays
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Stay[] $tempStays
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Payment[] $payments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Payment[] $paymentsExcludingCash
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Payment[] $cashPayments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\MyAllocatorFormattedLog[] $myallocator_formatted_log
 * @property-read \App\HostelPro\Models\Guest $guest
 * @property-read \App\HostelPro\Models\LabelType $label
 * @property-read mixed $label_formatted
 * @property-read mixed $label_c_s_s
 * @property-read \App\HostelPro\Models\Hostel $hostel
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\BoardUnallocated[] $board_unallocated
 */
	class Booking {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\BookingNote
 *
 * @property integer $id
 * @property integer $booking_id
 * @property integer $user_id
 * @property string $text
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\HostelPro\Models\User $user
 */
	class BookingNote {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\City
 *
 * @property integer $geonameid
 * @property string $name
 * @property string $original_name
 * @property float $latitude
 * @property float $longitude
 * @property string $feature_code
 * @property string $country_code
 * @property integer $population
 * @property string $timezone
 * @property string $modification_date
 * @property-read \App\HostelPro\Models\Country $hostels
 */
	class City {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\CompletedAllocation
 *
 * @property integer $id
 * @property integer $hostel_id
 * @property boolean $was_successful
 * @property string $date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\HostelPro\Models\Hostel $hostel
 */
	class CompletedAllocation {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Country
 *
 * @property integer $id
 * @property string $country_name
 * @property string $country_code
 * @property integer $default_currency_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Hostel[] $hostels
 */
	class Country {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Currency
 *
 * @property integer $id
 * @property string $currency_code
 * @property string $currency_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Hostel[] $hostels
 */
	class Currency {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\DateHelper
 *
 */
	class DateHelper {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Expense
 *
 * @property integer $id
 * @property integer $hostel_id
 * @property integer $user_id
 * @property string $date
 * @property string $category
 * @property string $name
 * @property integer $price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\HostelPro\Models\Hostel $hostel
 * @property-read \App\HostelPro\Models\User $user
 * @property-read mixed $pretty_price
 */
	class Expense {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\ExpenseCategory
 *
 * @property integer $id
 * @property string $name
 * @property integer $hostel_id
 * @property-read \App\HostelPro\Models\Hostel $hostel
 */
	class ExpenseCategory {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Guest
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $phone
 * @property integer $hostel_id
 * @property integer $country_id
 * @property integer $city_id
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $zip_code
 * @property string $gender
 * @property string $guest_notes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
	class Guest {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Hostel
 *
 * @property integer $id
 * @property string $name
 * @property integer $country_id
 * @property integer $city
 * @property integer $timezone_id
 * @property integer $currency_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $pos_deposit_category
 * @property-read \App\HostelPro\Models\Timezone $timezone
 * @property-read \App\HostelPro\Models\Country $country
 * @property-read \App\HostelPro\Models\Currency $currency
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\RoomType[] $roomtypes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\POSCategory[] $posCategories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Booking[] $bookings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Booking[] $unallocatedBookings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\SeasonRate[] $seasonRates
 */
	class Hostel {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Label
 *
 * @property integer $id
 * @property integer $hostel_id
 * @property integer $label_type_id
 * @property string $hex_color
 * @property-read \App\HostelPro\Models\LabelType $type
 */
	class Label {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\LabelType
 *
 * @property integer $id
 * @property string $type
 * @property-read \App\HostelPro\Models\Label $label
 */
	class LabelType {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Money
 *
 * @property integer $id
 * @property integer $hostel_id
 * @property integer $amount
 * @property-read \App\HostelPro\Models\Hostel $hostel
 */
	class Money {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\MoneyHelper
 *
 */
	class MoneyHelper {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\MyAllocatorFormattedLog
 *
 * @property integer $booking_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $number_of_guests
 * @property integer $myallocator_room_type_id
 * @property integer $subtotal
 * @property integer $currency_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\HostelPro\Models\RoomType $roomtype
 */
	class MyAllocatorFormattedLog {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\MyAllocatorLog
 *
 * @property integer $id
 * @property integer $hostel_id
 * @property mixed $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
	class MyAllocatorLog {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\MyAllocatorToken
 *
 * @property integer $hostel_id
 * @property string $user_token
 * @property integer $myallocator_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\HostelPro\Models\Hostel $hostel
 */
	class MyAllocatorToken {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Payment
 *
 * @property integer $id
 * @property integer $total
 * @property integer $hostel_id
 * @property integer $user_id
 * @property integer $booking_id
 * @property integer $currency_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\PaymentData[] $items
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\PaymentData[] $cashOnlyItems
 * @property-read \App\HostelPro\Models\User $user
 */
	class Payment {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\PaymentData
 *
 * @property integer $id
 * @property integer $payment_id
 * @property string $name
 * @property integer $price
 * @property integer $units
 * @property integer $subtotal
 * @property boolean $deposit
 * @method static \Illuminate\Database\Query\Builder|\App\HostelPro\Models\PaymentData cashOnly()
 */
	class PaymentData {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Permission
 *
 * @property integer $id
 * @property string $name
 * @property string $label
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Role[] $roles
 */
	class Permission {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\POSCategory
 *
 * @property integer $id
 * @property string $name
 * @property integer $hostel_id
 */
	class POSCategory {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\POSProduct
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property integer $price
 * @property-read \App\HostelPro\Models\POSCategory $category
 * @property-read mixed $formatted_price
 */
	class POSProduct {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Rate
 *
 * @property integer $room_type_id
 * @property string $date
 * @property integer $price
 * @property-read \App\HostelPro\Models\RoomType $roomtype
 */
	class Rate {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Role
 *
 * @property integer $id
 * @property string $name
 * @property string $label
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Permission[] $permissions
 */
	class Role {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Room
 *
 * @property integer $id
 * @property string $name
 * @property integer $room_type_id
 * @property-read \App\HostelPro\Models\RoomType $roomtype
 */
	class Room {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\RoomType
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $gender
 * @property boolean $number_of_guests
 * @property integer $hostel_id
 * @property integer $myallocator_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Room[] $rooms
 */
	class RoomType {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\SeasonRate
 *
 * @property integer $id
 * @property integer $hostel_id
 * @property string $name
 * @property string $start_date
 * @property string $end_date
 */
	class SeasonRate {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\ShiftReportData
 *
 * @property integer $id
 * @property integer $shift_report_id
 * @property integer $amount_of_money
 * @property integer $quantity
 * @property boolean $is_start_shift
 * @property-read \App\HostelPro\Models\ShiftReport $shift
 */
	class ShiftReportData {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Stay
 *
 * @property integer $id
 * @property integer $booking_id
 * @property integer $room_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $number_of_guests
 * @property integer $price_per_night
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property boolean $temp
 * @property-read \App\HostelPro\Models\Booking $booking
 * @property-read \App\HostelPro\Models\Room $room
 * @property-read mixed $number_of_nights
 * @property-read mixed $pretty_start_date
 * @property-read mixed $pretty_end_date
 * @method static \Illuminate\Database\Query\Builder|\App\HostelPro\Models\Stay nonTemp()
 * @method static \Illuminate\Database\Query\Builder|\App\HostelPro\Models\Stay temp()
 */
	class Stay {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\Timezone
 *
 * @property integer $id
 * @property string $country_code
 * @property string $timezone
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Hostel[] $hostels
 */
	class Timezone {}
}

namespace App\HostelPro\Models{
/**
 * App\HostelPro\Models\User
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property integer $current_hostel
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Hostel[] $hostels
 * @property-read \App\HostelPro\Models\Hostel $currenthostel
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\HostelPro\Models\Role[] $roles
 */
	class User {}
}

