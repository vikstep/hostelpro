
// Set our code to run once the page is fully loaded
// This has the added bonus of giving us our own scope to work with
function loadDragAndDrop() {

  // ========
  // Settings
  // ========

  // jQuery selectors for the elements that should be draggable
  var draggableSelectors = [
    //'.calendar-table>tbody>tr>td.calendar-booking-paid',
    //'.calendar-table>tbody>tr>td.calendar-booking-checked-in-paid',
    //'.calendar-table>tbody>tr>td.calendar-booking-unpaid',
    //'.calendar-table>tbody>tr>td.calendar-booking-checked-in-unpaid',
    '.calendar-table>tbody>tr>td.calendar-draggable',
  ];

  // Distance before a draggable snaps to a grid location
  var snapDistance = 20;

  // Opacity when entry is held over a valid drop target
  var validOpacity = 0.9;

  // Opacity when entry is not held over a valid drop target
  var invalidOpacity = 0.5;

  // If autoscroll should be enabled
  var scroll = true;

  // How close to the edge user can get before autoscrolling
  var scrollSensitivity = 50;

  // How fast to move when autoscrolling
  var scrollSpeed = 30;

  var startRoomId = 0;

  // Because of jQuery UI's behavior when adding drag visuals to the page,
  // it's best to have a seperate div sitting somewhere on the page that can
  // be used to house the elements jQuery UI adds. This is to avoid
  // weirdness that happens when jQuery UI places divs in a table.
  //
  // helperContainerId is the id of this div. By default, the script will
  // first search for this id on the page. If it finds it, it will us it.
  // If it does not find it, it will create it and append it to the body.
  //
  // It should be perfectly safe to let the script create the div itself.
  // You should only need to create the div manually if you're having
  // some sort of weird styling issues that are causing visual glitches.
  var helperContainerId = 'dragScriptHelperContainer';

  // Exposes the makeDraggable function on the global namespace
  // This is so that it can be accessed manually with ease
  var createWindowMakeDraggable = false;

  // Sets the function name for the global version of makeDraggable
  // Only relevant if createWindowMakeDraggable is true
  var windowMakeDraggableName = 'makeDraggable';

  // =======
  // Storage
  // =======

  // Will contain our helper container once it's found or created
  var helperContainer = null;

  // Will store the helper itself
  var helper = null;

  // Will contain the node the user is currently dragging
  var currentTarget = null;

  // Will contain a list of all possible drop targets and their related data
  var dropTargets = [];

  // Will contain info regarding the location the draggable came from.
  // This allows us to return the draggable to its previous location
  // if the user does not drop it on a valid drop target.
  var currentTargetOrigin = null;

  // Tracks the colspan and rowspan of the object being dragged
  var colspan = null;
  var rowspan = null;

  // Tracks element indexs for the sake of allowing seperate helper functions
  var afterElemWithIndex = null;

  // Tracks the data-date of our draggable
  var originDate = null;

  // =========
  // Functions
  // =========

  // Locates or creates our helper container elem
  var findOrCreateContainer = function() {

    // First attempt to locate the element if it already exists
    helperContainer = $('#' + helperContainerId);

    // If it doesn't exist, create it and append it to the body
    if (!helperContainer.length) {
      helperContainer = $('<div id="' + helperContainerId + '"></div>');
      helperContainer.appendTo('body');
    }
  };

  // Helper function to copy over CSS styles
  var copyStyle = function(style) {
    helper.css(style, currentTarget.css(style));
  };

  // Provides the elem that the users sees moving on the screen as they drag
  var getHelper = function(eventData, extra) {

    // Save our drag target for later use
    currentTarget = $(eventData.currentTarget);

    // Create a new elem, as helpers are destroyed at the end of a drag
    helper = $('<div></div>');

    // Copy over relevant CSS styles to keep things looking the same.
    // This list is as verbose as it is as some browsers seem to like to
    // pick and choose if you give them the choice. This way, they can't.
    // TODO - I've tried to cover what's needed here, but it's possible
    // there might be more styles that should be changed or copied to
    // get everything looking exactly as desired.
    [
      'height',
      'width',
      'background-color',
      'background-image',
      'text-shadow',
      'color',
      'white-space',
      'overflow-x',
      'overflow-y',
      'text-overflow',
      'padding-top',
      'padding-bottom',
      'padding-left',
      'padding-right',
      'vertical-align',
      'line-height',
      'text-align',
      'box-sizing',
      'border-top-color',
      'border-top-style',
      'border-top-width',
      'border-right-color',
      'border-right-style',
      'border-right-width',
      'border-bottom-color',
      'border-bottom-style',
      'border-bottom-width',
      'border-left-color',
      'border-left-style',
      'border-left-width',
      'border-image-source',
      'border-image-slice',
      'border-image-width',
      'border-image-outset',
      'border-image-repeat',
    ].forEach(copyStyle);

    // This is a fix for the line-height. I have no idea why, but the text
    // does not want to center vertically. This is ugly, but it fixes it.
    var lineHeight = parseInt(helper.css('height')) - 3;
    helper.css('line-height', lineHeight + 'px');

    // Clone the draggable's contents so it can be inserted in to our helper
    var contents = currentTarget.contents().clone();
    helper.append(contents);

    // Appending the helper to our helper container helps avoid visual
    // glitches that occur when placing extra elements in to our table.
    // Otherwise, if the helper did not exist somewhere on the page,
    // jQuery UI would place it next to where the drag started...
    // In pretty much all cases, that would be our table. Not good.
    helperContainer.append(helper);

    return helper;
  };

  // Small helper for getting date objects from the current date format
  var getDateObject = function(dateString) {
    return moment(dateString);
  };

  // Creates an empty td node to replace our drag target with
  // originalDate represents the date that the drag target had
  // xOffset represents how many extra columns to the right this td is
  var createEmptyTd = function(originalDate, xOffset) {

    // Determine the date for this td
    var date = getDateObject(originalDate);

    // Get our strings
    var dateString = date.add(xOffset, 'days').format('YYYY-MM-DD');
    var day = date.format('D');

    // Create our element
    var newElem = $('<td class="text-center addStay" data-date="' + dateString + '">' + day + '</td>');

    return newElem;
  };

  var checkElemDatesForIndexing = function(index, elem) {
    // If we've already found a suitable index, this will be a non-null value
    if (afterElemWithIndex !== null) {
      return;
    }

    // If we don't have data-date on this td, there's nothing to compare
    var dateString = $(elem).attr('data-date');
    if (!dateString) {
      return;
    }

    // Check if this date falls after the date of our drag target.
    // If it does, we can safely assume it's the first node to its right.
    // As such, we want whatever node is directly to it's left, i.e. -1.
    if (originDate.isBefore(dateString)) {
      afterElemWithIndex = index - 1;
    }
  };

  // Adds our possible drop targets to the list. We do not check yet if they
  // are valid as that's slow, and would not be neccessiary to perform for
  // every last possible target. Instead, we set their 'valid' state to null.
  // We will later change it to true or false if and when we check it.
  // Also, having this as a seperate function ensures that we are not
  // needlessly recreating this function thousands of time per click.
  var getDropTargets = function(index, elem) {
    var jqueryElem = $(elem);

    dropTargets.push({
      'elem': jqueryElem,
      'valid': null,
      'relatedElems': [],
      'x': jqueryElem.offset().left,
      'y': jqueryElem.offset().top
    });
  };

  // Checks if there's enough open slots around a given drop target.
  // This function only fires once the user tries to move over a drop
  // target that they have not yet moused over. Likewise, it saves the
  // result of its calculation. Doing so significantly reduces the amount
  // of times this code is being run.
  var checkIfValidDropTarget = function(dropTarget) {

    // If we've already calculated this, no need to recheck
    if (dropTarget.valid !== null)
      return dropTarget.valid;

    // Grab our drop target element
    var jqueryElem = dropTarget.elem;

    // Save the index of our current row so we don't have to recheck
    var rowIndex = jqueryElem.parent().index();

    // Determine what dates need to be open for this drop target to be valid
    var requiredDates = [];
    var date = getDateObject(jqueryElem.attr('data-date'));
    requiredDates.push(date.format('YYYY-MM-DD'));
    for (var col = 1; col < colspan; col++) {
      requiredDates.push(date.add(1, 'days').format('YYYY-MM-DD'));
    }

    // Loop through each row we require elements on, and check that an
    // element for each of the required dates exists in that row
    for (var row = 0; row < rowspan; row++) {

      // Get a reference to the next row we want to check
      var currentRow = jqueryElem.parent().parent().children().eq(rowIndex + row);

      for (var dateIndex = 0; dateIndex < requiredDates.length; dateIndex++) {

        // Check if our row has an available opening on the date required
        var nextElem = currentRow.children('.addStay[data-date="' + requiredDates[dateIndex] + '"]');

        // If it does not, the drop target is invalid, as the entry
        // simply won't fit. Save the result and return it.
        if (!nextElem.length) {
          dropTarget.valid = false;
          return false;
        }

        // If it does, save it as being related to the drop target
        dropTarget.relatedElems.push(nextElem);
      }
    }

    // If every row has every date we need, the drop target is valid.
    // Save the result for later use, and return it.
    dropTarget.valid = true;
    return true;
  };

  // Fires when the the user starts a drag
  var onDragStart = function() {
    startRoomId = currentTarget.closest('tr').parent().find('th.room-name').data('roomid');

    // Keep track of the size of our draggable
    colspan = parseInt(currentTarget.attr('colspan'));
    rowspan = parseInt(currentTarget.attr('rowspan'));

    // Keep track of our row's index
    var yIndex = currentTarget.parent().index();

    // Keep track our draggable's current date
    originDate = getDateObject(currentTarget.attr('data-date'));

    // Empty out any lingering previous information about old draggables
    currentTargetOrigin = {
      'elem': null,
      'relatedElems': []
    };

    // Used to avoid a "magic number" later on. Basically just provides
    // a more meaningful name to an otherwise meaningless number.
    var invalid = -1;

    // Storage for the elements that we'll be adding to our table.
    // We store them so that we can add them all at once, which is
    // slightly better for performance, as it ensures the browser
    // doesn't have to keep recounting elements as we ask for indexes.
    var replacementElems = [];

    // Loop through rows first as that way it's possible to look up
    // each new row only once as opposed to every time for every element.
    for (var row = rowspan; row--; ) {

      // Get a reference to this row's children
      var rowChildren = currentTarget
        .parent()
        .parent()
        .children()
        .eq(yIndex + row)
        .children();

      // Reset afterElemWithIndex so that it can be used in the next loop
      afterElemWithIndex = null;
      for (var col = colspan; col--; ) {

        // Create the new element that we will be adding to the table
        var newTd = createEmptyTd(currentTarget.attr('data-date'), col);

        // Flag it as an element we've created when removing our draggable
        currentTargetOrigin.relatedElems.push(newTd);

        // If row and col are 0, then this is also the element that has
        // the same top left location as our draggable originally did.
        if (row == 0 && col == 0) {
          currentTargetOrigin.elem = newTd;
        }

        // Save the element as needing placement in the table
        var replacement = {
          'elem': newTd,
          'target': null,
        };

        // If this is null at this point, this is the first loop through
        // a new row, and we need to determine where we should be
        // dropping in any new elements that we create.
        if (afterElemWithIndex === null) {

          // Call a helper function to determine the index
          // we need to insert our elements after
          rowChildren.each(checkElemDatesForIndexing);

          // If it's still null, we didn't find anything.
          // Set as invalid so we know to simply use the very last element.
          if (afterElemWithIndex === null) {
            afterElemWithIndex = invalid;
          }
        }

        // Set the element we need to insert after as appropriate
        if (afterElemWithIndex !== invalid) {
          replacement.target = rowChildren.eq(afterElemWithIndex);
        } else {
          replacement.target = rowChildren.last();
        }

        // Add our replacement to the list
        replacementElems.push(replacement);
      }
    }

    // Loop through and add all of our replacement elems to the table
    for (var i = 0; i < replacementElems.length; i++) {
      var data = replacementElems[i];
      data.target.after(data.elem);
    }

    // === TODO ===
    // Any event handlers for td.addStay elements will need to be added
    // to these elements, as they are brand new. This can be done within
    // the loop, outside it, or even elsewhere, just as long as it happens.

    // Remove and save a reference to our draggable elem. Using jQuery's
    // detach method allows us to reattach the element later, and all
    // event listeners and related data should remain intact.
    currentTarget.detach();

    // Reset our list of possible drop target locations
    dropTargets = [];
    $('td.addStay[data-date]').each(getDropTargets);
  };

  // Returns drop target information if we are close enough to one.
  // Technically returns the first it finds rather than the closest,
  // though with a reasonable snapDistance, this shouldn't be an issue.
  // Doing so this way helps keep dragging smooth, as it would take
  // more time to calculate which of the points is the closest.
  var findNearbyDropTarget = function() {

    // Determine the location of our helper elem
    var left = helper.offset().left;
    var top = helper.offset().top;

    for (var i = 0; i < dropTargets.length; i++) {

      var target = dropTargets[i];

      // Check to see if we're within the required distance
      if (left > target.x - snapDistance && left < target.x + snapDistance &&
        top > target.y - snapDistance && top < target.y + snapDistance) {

        // Confirm our drop target is valid
        if (checkIfValidDropTarget(target)) {
          // If so, stop searching and return the result right away
          return target;
        }
      }
    }

    // If we don't find anything in range, return null
    return null;
  };

  // Performed every time the mouse moves while dragging
  var onDrag = function(eventData, dragElemData) {

    // Assume we are not at a valid drop location until we determine otherwise
    helper.css('opacity', invalidOpacity);

    // Set the helper's location immediately, as we need to perform
    // calculations based on its location. Normally jQuery UI handles
    // this later, but without doing so now, our math would be off.
    helper.css('left', dragElemData.position.left + 'px');
    helper.css('top', dragElemData.position.top + 'px');

    // Check if we are close enough to a drop target to snap to it
    var nearbyTarget = findNearbyDropTarget();

    // If we are, snap to it and set our opacity as appropriate
    if (nearbyTarget !== null) {
      dragElemData.position.left -= helper.offset().left - nearbyTarget.x;
      dragElemData.position.top -= helper.offset().top - nearbyTarget.y;
      helper.css('opacity', validOpacity);
    }
  };

  var onDragStop = function(eventData, dragElemData) {

    // Set the helper's location immediately, as we need to perform
    // calculations based on its location. Normally jQuery UI handles
    // this later, but without doing so now, our math would be off.
    helper.css('left', dragElemData.position.left + 'px');
    helper.css('top', dragElemData.position.top + 'px');

    // Check if we are close enough to a drop target to snap to it
    var nearbyTarget = findNearbyDropTarget();

    // If we are, set up our elem in its new location
    if (nearbyTarget !== null) {
      var oldDate = currentTarget.data('date');
      var newDate = nearbyTarget.elem.attr('data-date');

      var newRoomId = nearbyTarget.elem.closest('tr').parent().find('th.room-name').data('roomid');

      //if dropped into another date or roomtype user needs to confirm
      if ((oldDate != newDate || startRoomId != newRoomId)) {
        noty({
          text: 'Do you really want to change booking data?',
          theme: 'relax',
          layout: 'center',
          modal: true,
          animation: {
            open: {height: 'toggle'}, // jQuery animate function property object
            close: {height: 'toggle'}, // jQuery animate function property object
            easing: 'swing', // easing
            speed: 150 // opening & closing animation speed
          },
          buttons: [
            {
              addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
              $noty.close();
              drop(nearbyTarget, newDate);
            }
            },
            {
              addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
              $noty.close();
              revert();
            }
            }
          ]
        });
      }
      else {
        drop(nearbyTarget, newDate);
      }
    }
    // If we are not near a drop target, return the entry to it's old location
    else {
      revert();      
    }
  };
  
  function drop(nearbyTarget, newDate) {
    // Change the date to represent its new location on the grid
    currentTarget.attr('data-date', newDate);

    // Insert the saved elem back into the grid
    nearbyTarget.elem.before(currentTarget);

    // Remove all td elements that our entry is replacing
    for (var i = nearbyTarget.relatedElems.length; i--; ) {
      nearbyTarget.relatedElems[i].remove();
    }

    // === TODO ===
    // At this point, the entry has been moved to its new location
    // This needs to be connected to the backend so that this
    // information is saved to the database.

    var parentRow = currentTarget.closest('tr');
    var bookingId = currentTarget.data('bookingid');
    var stayId = currentTarget.data('stayid');
    var date = newDate;
    var bedNumber = parentRow.children('td.bednumber').html();
    var roomId = parentRow.parent().find('th.room-name').data('roomid');

    moveStay(bookingId, date, stayId, roomId, bedNumber);
  }
  
  function revert(){
    // Insert the saved elem back into the grid
    currentTargetOrigin.elem.before(currentTarget);

    // Remove all td elements that our entry is replacing
    for (var i = currentTargetOrigin.relatedElems.length; i--; ) {
      currentTargetOrigin.relatedElems[i].remove();
    }
  }

  // ===========
  // Final Setup
  // ===========

  // Combine all of our options and functions
  var draggableSettings = {

    // When the user first picks up an entry, they will be over top of
    // a valid space by default; you can always drop an entry back down
    // it came from, so start off with the opacity for valid drops.
    'opacity': validOpacity,

    // Copy our settings from the beginning of the script
    'scroll': scroll,
    'scrollSensitivity': scrollSensitivity,
    'scrollSpeed': scrollSpeed,

    // Set our functions
    'helper': getHelper,
    'start': onDragStart,
    'drag': onDrag,
    'stop': onDragStop
  };

  // Makes nodes draggable
  var makeDraggable = function(jqueryObject) {
    jqueryObject.draggable(draggableSettings);
  };

  // Adds the method to the global namespace if set
  if (createWindowMakeDraggable) {
    window[windowMakeDraggableName] = makeDraggable;
  }

  // Locate our helper container
  findOrCreateContainer();

  // Make our configured elements draggable
  for (var i = 0; i < draggableSelectors.length; i++) {
    makeDraggable($(draggableSelectors[i]));
  }

  // =======================================
  // BEGIN: Drag to create new functionality
  // =======================================

  var originRow = null;
  var originTable = null;
  var previousLocation = null;

  var newBookingDraggableSelectors = [
    '.calendar-table>tbody>tr>td.addStay',
  ];

  // Provides the elem that the users sees moving on the screen as they drag
  var getNewBookingHelper = function(eventData, extra) {

    // Save our drag target for later use
    currentTarget = $(eventData.currentTarget);

    // Create a new elem, as helpers are destroyed at the end of a drag
    // TODO - Style this as desired
    helper = $('<div class="newBookingDrag"></div>');

    // Appending the helper to our helper container helps avoid visual
    // glitches that occur when placing extra elements in to our table.
    // Otherwise, if the helper did not exist somewhere on the page,
    // jQuery UI would place it next to where the drag started...
    // In pretty much all cases, that would be our table. Not good.
    helperContainer.append(helper);

    return helper;
  };

  // Adds our possible drop targets to the list. We do not check yet if they
  // are valid as that's slow, and would not be neccessiary to perform for
  // every last possible target. Instead, we set their 'valid' state to null.
  // We will later change it to true or false if and when we check it.
  // Also, having this as a seperate function ensures that we are not
  // needlessly recreating this function thousands of time per click.
  var getNewBookingDropTargets = function(index, elem) {
    var jqueryElem = $(elem);

    dropTargets.push({
      'elem': jqueryElem,
      'valid': null,
      'relatedElems': [],
      'x': jqueryElem.offset().left,
      'y': jqueryElem.offset().top,
      'bottom': jqueryElem.offset().top + jqueryElem.outerHeight(),
      'right': jqueryElem.offset().left + jqueryElem.outerWidth(),
      'cols': 0,
      'rows': 0,
    });
  };

  // Fires when the the user starts a drag
  var onNewBookingDragStart = function() {

    previousLocation = null;
    resizeFallback = null;

    // Keep track of our row's index
    originRow = currentTarget.parent().index();
    originTable = currentTarget.parent().parent();

    // Keep track our draggable's current date
    originDate = getDateObject(currentTarget.attr('data-date'));

    // Empty out any lingering previous information about old draggables
    currentTargetOrigin = {
      'elem': null,
      'relatedElems': [],
    };

    // Reset our list of possible drop target locations
    dropTargets = [];
    $('td.addStay[data-date]').each(getNewBookingDropTargets);
  };

  // Performed every time the mouse moves while dragging
  var onNewBookingDrag = function(eventData, dragElemData) {

    // Set the helper's location immediately, as we need to perform
    // calculations based on its location. Normally jQuery UI handles
    // this later, but without doing so now, our math would be off.
    helper.css('left', dragElemData.position.left + 'px');
    helper.css('top', dragElemData.position.top + 'px');

    // Check if we are close enough to a drop target to snap to it
    var nearbyTarget = findNearbyNewBookingDropTarget(eventData.pageX, eventData.pageY);

    // If we are, snap to it and set our opacity as appropriate
    if (nearbyTarget !== null) {
      if (nearbyTarget.full) {
        dragElemData.position.left -= helper.offset().left - nearbyTarget.full.left;
        dragElemData.position.top -= helper.offset().top - nearbyTarget.full.top;

        var width = nearbyTarget.full.right - nearbyTarget.full.left;
        var height = nearbyTarget.full.bottom - nearbyTarget.full.top;
      } else {
        dragElemData.position.left -= helper.offset().left - Math.min(currentTarget.offset().left, nearbyTarget.x);
        dragElemData.position.top -= helper.offset().top - Math.min(currentTarget.offset().top, nearbyTarget.y);

        var width = Math.max(
            nearbyTarget.right - currentTarget.offset().left,
            (currentTarget.outerWidth() + currentTarget.offset().left) - nearbyTarget.x
        );

        var height = Math.max(
            nearbyTarget.bottom - currentTarget.offset().top,
            (currentTarget.outerHeight() + currentTarget.offset().top) - nearbyTarget.y
        );
      }

      helper.css('width', width + 'px');
      helper.css('height', height + 'px');

      previousLocation = {
        top: dragElemData.position.top,
        left: dragElemData.position.left,
        height: height,
        width: width,
        target: nearbyTarget,
        topLeft: nearbyTarget.topLeft,
      };
    } else if (previousLocation !== null) {
      helper.css('width', previousLocation.width + 'px');
      helper.css('height', previousLocation.height + 'px');
      dragElemData.position.left = previousLocation.left;
      dragElemData.position.top = previousLocation.top;
    }
  };

  var resizeFallback = null;

  // Returns drop target information if we are close enough to one.
  // Technically returns the first it finds rather than the closest,
  // though with a reasonable snapDistance, this shouldn't be an issue.
  // Doing so this way helps keep dragging smooth, as it would take
  // more time to calculate which of the points is the closest.
  var findNearbyNewBookingDropTarget = function(pageX, pageY) {

    // Determine the location of our helper elem
    var left = pageX ? pageX : helper.offset().left;
    var top = pageY ? pageY : helper.offset().top;
    // debugger;

    for (var i = 0; i < dropTargets.length; i++) {

      var target = dropTargets[i];

      // Check to see if we're within the required distance
      // if (left > target.x - snapDistance && left < target.x + snapDistance &&
      //   top > target.y - snapDistance && top < target.y + snapDistance) {

      if (left > target.x && left < target.right &&
          top > target.y && top < target.bottom) {

        // Confirm our drop target is valid
        if (checkIfValidNewBookingDropTarget(target)) {
          // If so, stop searching and return the result right away
          return target;
        }
      }
    }

    if (previousLocation && previousLocation.target) {
      return previousLocation.target;
    }

    if (resizeFallback) {
      for (var i = 0; i < dropTargets.length; i++) {
        var target = dropTargets[i];
        if (target.elem.is(resizeFallback) && checkIfValidNewBookingDropTarget(target)) {
          return target;
        }
      }
    }

    // If we don't find anything in range, return null
    return null;
  };

  // Checks if there's enough open slots around a given drop target.
  // This function only fires once the user tries to move over a drop
  // target that they have not yet moused over. Likewise, it saves the
  // result of its calculation. Doing so significantly reduces the amount
  // of times this code is being run.
  var checkIfValidNewBookingDropTarget = function(dropTarget) {

    // If we've already calculated this, no need to recheck
    if (dropTarget.valid !== null)
      return dropTarget.valid;

    // Grab our drop target element
    var jqueryElem = dropTarget.elem;

    // Save the index of our current row so we don't have to recheck
    var rowIndex = jqueryElem.parent().index();

    // Determine what dates need to be open for this drop target to be valid
    var requiredDates = [];
    var date = getDateObject(jqueryElem.attr('data-date'));

    if (!lock.left && !lock.right) {
      requiredDates.push(date.format('YYYY-MM-DD'));
      dropTarget.cols++;
      var days = date.isBefore(originDate) ? 1 : -1;
      dropTarget.date = date.isBefore(originDate) ? date.format('YYYY-MM-DD') : originDate.format('YYYY-MM-DD');
      while (date.format('YYYY-MM-DD') !== originDate.format('YYYY-MM-DD')) {
        requiredDates.push(date.add(days, 'days').format('YYYY-MM-DD'));
        dropTarget.cols++;
      }
    } else if (lock.left && lock.right) {
      date = getDateObject(lock.left.format('YYYY-MM-DD'));
      requiredDates.push(date.format('YYYY-MM-DD'));
      dropTarget.cols++;
      while (date.format('YYYY-MM-DD') !== lock.right.format('YYYY-MM-DD')) {
        requiredDates.push(date.add(1, 'days').format('YYYY-MM-DD'));
        dropTarget.cols++;
      }
    } else if (lock.left && !date.isBefore(lock.left)) {
      requiredDates.push(date.format('YYYY-MM-DD'));
      dropTarget.cols++;
      var days = date.isBefore(lock.left) ? 1 : -1;
      dropTarget.date = date.isBefore(lock.left) ? date.format('YYYY-MM-DD') : lock.left.format('YYYY-MM-DD');
      while (date.format('YYYY-MM-DD') !== lock.left.format('YYYY-MM-DD')) {
        requiredDates.push(date.add(days, 'days').format('YYYY-MM-DD'));
        dropTarget.cols++;
      }
    } else if (lock.right && !lock.right.isBefore(date)) {
      requiredDates.push(date.format('YYYY-MM-DD'));
      dropTarget.cols++;
      var days = 1;
      dropTarget.date = date.format('YYYY-MM-DD');
      while (date.format('YYYY-MM-DD') !== lock.right.format('YYYY-MM-DD')) {
        requiredDates.push(date.add(days, 'days').format('YYYY-MM-DD'));
        dropTarget.cols++;
      }
    } else {
      dropTarget.valid = false;
      return false;
    }

    requiredDates.sort();

    // Loop through each row we require elements on, and check that an
    // element for each of the required dates exists in that row
    var topRow = lock.top !== null ? lock.top : lock.bottom ? rowIndex : Math.min(originRow, rowIndex);
    var bottomRow = lock.bottom ? lock.bottom : lock.top !== null ? rowIndex : Math.max(originRow, rowIndex);

    if (topRow > bottomRow) {
      dropTarget.valid = false;
      return false;
    }

    for (var row = topRow; row <= bottomRow; row++) {

      dropTarget.rows++;

      // Get a reference to the next row we want to check
      var currentRow = jqueryElem.parent().parent().children().eq(row);

      for (var dateIndex = 0; dateIndex < requiredDates.length; dateIndex++) {

        // Check if our row has an available opening on the date required
        var nextElem = currentRow.children('.addStay[data-date="' + requiredDates[dateIndex] + '"]');

        // If it does not, the drop target is invalid, as the entry
        // simply won't fit. Save the result and return it.
        if (!nextElem.length) {
          dropTarget.valid = false;
          return false;
        }

        if (!nextElem.parent().parent().is(originTable)) {
          dropTarget.valid = false;
          return false;
        }

        // If it does, save it as being related to the drop target
        dropTarget.relatedElems.push(nextElem);
      }
    }

    dropTarget.topLeft = dropTarget.relatedElems[0];

    dropTarget.full = {
      top: dropTarget.relatedElems[0].offset().top,
      left: dropTarget.relatedElems[0].offset().left,
      bottom: dropTarget.relatedElems[dropTarget.relatedElems.length - 1].offset().top + dropTarget.relatedElems[dropTarget.relatedElems.length - 1].outerHeight(),
      right: dropTarget.relatedElems[dropTarget.relatedElems.length - 1].offset().left + dropTarget.relatedElems[dropTarget.relatedElems.length - 1].outerWidth(),
    };

    // If every row has every date we need, the drop target is valid.
    // Save the result for later use, and return it.
    dropTarget.valid = true;
    return true;
  };

  var onNewBookingDragStop = function(eventData, dragElemData) {

    if (!previousLocation || !previousLocation.target) { return; }
    nearbyTarget = previousLocation.target;

    // If we are, set up our elem in its new location
    if (nearbyTarget !== null) {

      var resizeTop = $('<div class="topDrag"></div>');
      makeResizeDraggable(resizeTop);

      var resizeBottom = $('<div class="bottomDrag"></div>');
      makeResizeDraggable(resizeBottom);

      var resizeRight = $('<div class="rightDrag"></div>');
      makeResizeDraggable(resizeRight);

      var resizeLeft = $('<div class="leftDrag"></div>');
      makeResizeDraggable(resizeLeft);

      var newElem = $('<td></td>')
          .addClass('resizable')
          .attr('colspan', nearbyTarget.cols)
          .attr('rowspan', nearbyTarget.rows)
          .append(resizeTop)
          .append(resizeBottom)
          .append(resizeRight)
          .append(resizeLeft);
      // === TODO ===
      // Any more changes to the new element that will be dropped in
      // when creating a new booking

      // Change the date to represent its new location on the grid
      var newDate = nearbyTarget.topLeft.attr('data-date');
      newElem.attr('data-date', newDate);

      // Insert the saved elem back into the grid
      nearbyTarget.topLeft.before(newElem);
      // currentTarget.before(newElem);

      // Remove all td elements that our entry is replacing
      for (var i = nearbyTarget.relatedElems.length; i--; ) {
        nearbyTarget.relatedElems[i].remove();
      }

      // === TODO ===
      // At this point, the new entry has been created OR resized
      // This needs to be connected to the backend so that this
      // information is saved to the database.

      var parentRow = newElem.closest('tr');

      //console.log(nearbyTarget);

      var bedNumber = parentRow.children('td.bednumber').html();
      var roomId = parentRow.parent().find('th.room-name').data('roomid');

      addStay(nearbyTarget.date, roomId, bedNumber, nearbyTarget.rows, nearbyTarget.cols);
    }

    lock = {
      top: null,
      bottom: null,
      left: null,
      right: null
    };
  };

  var newBookingDraggableSettings = {

    'opacity': validOpacity,

    // Copy our settings from the beginning of the script
    'scroll': scroll,
    'scrollSensitivity': scrollSensitivity,
    'scrollSpeed': scrollSpeed,

    // Set our functions
    'helper': getNewBookingHelper,
    'start': onNewBookingDragStart,
    'drag': onNewBookingDrag,
    'stop': onNewBookingDragStop
  };

  // Makes nodes draggable
  var makeNewBookingDraggable = function(jqueryObject) {
    jqueryObject.draggable(newBookingDraggableSettings);
  };

  // Make our configured elements draggable
  for (var i = 0; i < newBookingDraggableSelectors.length; i++) {
    makeNewBookingDraggable($(newBookingDraggableSelectors[i]));
  }

  ///////////////////////
  // RESIZE FUNCTIONALITY
  ///////////////////////

  // Provides the elem that the users sees moving on the screen as they drag
  var getResizeHelper = function(eventData, extra) {

    // Save our drag target for later use
    currentTarget = $(eventData.currentTarget);

    isTop = currentTarget.hasClass('topDrag');
    isBottom = currentTarget.hasClass('bottomDrag');
    isLeft = currentTarget.hasClass('leftDrag');
    isRight = currentTarget.hasClass('rightDrag');

    currentTarget = currentTarget.parent();

    // Create a new elem, as helpers are destroyed at the end of a drag
    // TODO - Style this as desired
    helper = $('<div class="newBookingDrag"></div>');

    // Appending the helper to our helper container helps avoid visual
    // glitches that occur when placing extra elements in to our table.
    // Otherwise, if the helper did not exist somewhere on the page,
    // jQuery UI would place it next to where the drag started...
    // In pretty much all cases, that would be our table. Not good.
    helperContainer.append(helper);

    return helper;
  };

  var isTop = false;
  var isBottom = false;
  var isLeft = false;
  var isRight = false;

  var lock = {
    top: null,
    bottom: null,
    left: null,
    right: null
  };

  var onResizeDragStart = function() {
    previousLocation = null;

    // Keep track of the size of our draggable
    colspan = parseInt(currentTarget.attr('colspan'));
    rowspan = parseInt(currentTarget.attr('rowspan'));

    // Keep track of our row's index
    originRow = currentTarget.parent().index();
    originTable = currentTarget.parent().parent();

    // Keep track of our row's index
    var yIndex = currentTarget.parent().index();

    // Keep track our draggable's current date
    originDate = getDateObject(currentTarget.attr('data-date'));

    // Empty out any lingering previous information about old draggables
    currentTargetOrigin = {
      'elem': null,
      'relatedElems': [],
    };

    // Used to avoid a "magic number" later on. Basically just provides
    // a more meaningful name to an otherwise meaningless number.
    var invalid = -1;

    // Storage for the elements that we'll be adding to our table.
    // We store them so that we can add them all at once, which is
    // slightly better for performance, as it ensures the browser
    // doesn't have to keep recounting elements as we ask for indexes.
    var replacementElems = [];

    // Loop through rows first as that way it's possible to look up
    // each new row only once as opposed to every time for every element.
    for (var row = rowspan; row--; ) {

      // Get a reference to this row's children
      var rowChildren = currentTarget
          .parent()
          .parent()
          .children()
          .eq(yIndex + row)
          .children();

      // Reset afterElemWithIndex so that it can be used in the next loop
      afterElemWithIndex = null;
      for (var col = colspan; col--; ) {

        // Create the new element that we will be adding to the table
        var newTd = createEmptyTd(currentTarget.attr('data-date'), col);
        makeNewBookingDraggable(newTd);

        // Flag it as an element we've created when removing our draggable
        currentTargetOrigin.relatedElems.push(newTd);

        // If row and col are 0, then this is also the element that has
        // the same top left location as our draggable originally did.
        if (row == 0 && col == 0) {
          currentTargetOrigin.elem = newTd;
        }

        // Save the element as needing placement in the table
        var replacement = {
          'elem': newTd,
          'target': null,
        };

        // If this is null at this point, this is the first loop through
        // a new row, and we need to determine where we should be
        // dropping in any new elements that we create.
        if (afterElemWithIndex === null) {

          // Call a helper function to determine the index
          // we need to insert our elements after
          rowChildren.each(checkElemDatesForIndexing);

          // If it's still null, we didn't find anything.
          // Set as invalid so we know to simply use the very last element.
          if (afterElemWithIndex === null) {
            afterElemWithIndex = invalid;
          }
        }

        // Set the element we need to insert after as appropriate
        if (afterElemWithIndex !== invalid) {
          replacement.target = rowChildren.eq(afterElemWithIndex);
        } else {
          replacement.target = rowChildren.last();
        }

        // Add our replacement to the list
        replacementElems.push(replacement);
      }
    }

    // Loop through and add all of our replacement elems to the table
    for (var i = 0; i < replacementElems.length; i++) {
      var data = replacementElems[i];
      data.target.after(data.elem);
    }

    lock.top = isBottom || isLeft || isRight ? replacementElems[replacementElems.length - 1].elem.parent().index() : null;
    lock.bottom = isTop || isLeft || isRight ? replacementElems[0].elem.parent().index() : null;
    lock.left = isTop || isBottom || isRight ? getDateObject(replacementElems[replacementElems.length - 1].elem.attr('data-date')) : null;
    lock.right = isTop || isBottom || isLeft ? getDateObject(replacementElems[0].elem.attr('data-date')) : null;

    if (isTop || isBottom || isLeft || isRight) {
      resizeFallback = isTop || isLeft ? replacementElems[replacementElems.length - 1].elem : replacementElems[0].elem;
    }

    // === TODO ===
    // Any event handlers for td.addStay elements will need to be added
    // to these elements, as they are brand new. This can be done within
    // the loop, outside it, or even elsewhere, just as long as it happens.

    // Remove and save a reference to our draggable elem. Using jQuery's
    // detach method allows us to reattach the element later, and all
    // event listeners and related data should remain intact.
    currentTarget.detach();

    currentTarget = replacementElems[replacementElems.length - 1].elem;

    // Reset our list of possible drop target locations
    dropTargets = [];
    $('td.addStay[data-date]').each(getNewBookingDropTargets);
  };

  var resizeDraggableSettings = {

    'opacity': validOpacity,

    // Copy our settings from the beginning of the script
    'scroll': scroll,
    'scrollSensitivity': scrollSensitivity,
    'scrollSpeed': scrollSpeed,

    // Set our functions
    'helper': getResizeHelper,
    'start': onResizeDragStart,
    'drag': onNewBookingDrag,
    'stop': onNewBookingDragStop,
  };

  // Makes nodes draggable
  var makeResizeDraggable = function(jqueryObject) {
    jqueryObject.draggable(resizeDraggableSettings);
  };
}


