@if (Session::has('flash_message'))
  <div class="row">
    <div class="col-lg-6 col-lg-offset-3 pt15">
      <div class="alert text-center {{ Session::get('alert-class', 'alert-info') }}">{!! Session::get('flash_message') !!}</div>
    </div>
  </div>
@endif