<footer id="site-foot" class="site-foot clearfix">
  <audio id="audioBell" controls preload="auto" class="hidden">
    <source src="{{ secure_asset('assets/audio/bell.mp3') }}" type="audio/mpeg">
  </audio>

  <p>© Copyright 2016 <strong>HostelPro</strong>, All rights reserved.

    Page generated in {!! sprintf('%0.3f',(microtime(true) - LARAVEL_START)) !!} seconds. Server:

    @if ($_SERVER['SERVER_ADDR'] == '192.168.10.10')
      Local
    @elseif ($_SERVER['SERVER_ADDR'] == '108.61.190.24')
      Staging (Frankfurt)
    @elseif ($_SERVER['SERVER_ADDR'] == '108.61.174.80')
      London
    @elseif ($_SERVER['SERVER_ADDR'] == '185.92.223.42')
      Amsterdam
    @elseif ($_SERVER['SERVER_ADDR'] == '45.63.50.167')
      Los Angeles
    @endif

  </p>
</footer>