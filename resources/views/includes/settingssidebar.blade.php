<div id="sidebar-wrapper" class="m-t-sm">
  <ul class="sidebar-nav">

        <li class="heading">Hostel</li>

        <li class="{!! setActive( 'settings.RoomsOverview') ? 'active' : '' !!}">
            <div class="pull-right text-success m-t-sm m-r-xs">
                <i class="fa fa-circle"></i>
            </div>
          <a href="{!! route('settings.RoomsOverview') !!}">
            <i class="fa fa-bed icon"></i>
            <span>Rooms</span>
          </a>
        </li>

        <li class="{!! setActive( 'settings.posOverview') ? 'active' : '' !!}">
            <div class="pull-right text-success m-t-sm m-r-xs">
                <i class="fa fa-circle"></i>
            </div>
          <a href="{!! route('settings.posOverview') !!}">
            <i class="fa fa-list-alt icon"></i>
            <span>POS</span>
          </a>
        </li>

        {{--<li>--}}
          {{--<a href="">--}}
            {{--<i class="fa fa-globe icon"></i>--}}
            {{--<span>Location</span>--}}
          {{--</a>--}}
        {{--</li>--}}

        <li class="{!! setActive( 'settings.paymenttypes.create') ? 'active' : '' !!}">
            <div class="pull-right text-success m-t-sm m-r-xs">
                <i class="fa fa-circle"></i>
            </div>
          <a href="{!! route('settings.paymenttypes.create') !!}">
            <i class="fa fa-money icon"></i>
            <span>Payment Types</span>
          </a>
        </li>

        <li class="{!! setActive( 'settings.money.create') ? 'active' : '' !!}">
            <div class="pull-right text-success m-t-sm m-r-xs">
                <i class="fa fa-circle"></i>
            </div>
          <a href="{!! route('settings.money.create') !!}">
            <i class="fa fa-euro icon"></i>
            <span>Currency/Money</span>
          </a>
        </li>

      <li class="{!! setActive( 'settings.email.create') ? 'active' : '' !!}">
          @if (Auth::user()->currenthostel->email)
              <div class="pull-right text-success m-t-sm m-r-xs">
                  <i class="fa fa-circle"></i>
              </div>
              @else
              <div class="pull-right text-muted m-t-sm m-r-xs">
                  <i class="fa fa-circle"></i>
              </div>
              @endif
          <a href="{!! route('settings.email.create') !!}">
              <i class="fa fa-envelope icon"></i>
              <span>Email</span>
          </a>
      </li>

        <li class="heading">Reports</li>

        <li class="{!! setActive( 'settings.expense.create') ? 'active' : '' !!}">
            <div class="pull-right text-success m-t-sm m-r-xs">
                <i class="fa fa-circle"></i>
            </div>
          <a href="{!! route('settings.expensecategory.create') !!}">
            <i class="fa fa-cogs icon"></i>
            <span>Expenses</span>
          </a>
        </li>

      <li class="{!! setActive( 'settings.competitor.index') ? 'active' : '' !!}">
          @if (Auth::user()->currenthostel->reviewURLs->count() > 0)
              <div class="pull-right text-success m-t-sm m-r-xs">
                  <i class="fa fa-circle"></i>
              </div>
          @else
              <div class="pull-right text-muted m-t-sm m-r-xs">
                  <i class="fa fa-circle"></i>
              </div>
          @endif
          <a href="{!! route('reports.reviews.create') !!}">
              <i class="fa fa-comments icon"></i>
              <span>Reviews</span>
          </a>
      </li>

        <li class="heading">Rates</li>

        <li class="{!! setActive( 'settings.competitor.index') ? 'active' : '' !!}">
            @if (Auth::user()->currenthostel->competitorURLs->count() > 0)
                <div class="pull-right text-success m-t-sm m-r-xs">
                    <i class="fa fa-circle"></i>
                </div>
            @else
                <div class="pull-right text-muted m-t-sm m-r-xs">
                    <i class="fa fa-circle"></i>
                </div>
            @endif
          <a href="{!! route('settings.competitor.index') !!}">
            <i class="fa fa-cogs icon"></i>
            <span>Competitors</span>
          </a>
        </li>

        <li class="heading">Channel Manager</li>

        <li class="{!! setActive( 'settings.myallocator.create') ? 'active' : '' !!}">
            @if (Auth::user()->currenthostel->myallocatorToken)
                <div class="pull-right text-success m-t-sm m-r-xs">
                    <i class="fa fa-circle"></i>
                </div>
            @else
                <div class="pull-right text-muted m-t-sm m-r-xs">
                    <i class="fa fa-circle"></i>
                </div>
            @endif
          <a href="{!! route('settings.myallocator.create') !!}">
            <i class="fa fa-cogs icon"></i>
            <span>MyAllocator</span>
          </a>
        </li>

      {{--<li class="heading">Billing</li>--}}

      {{--<li class="{!! setActive( 'settings.billing.create') ? 'active' : '' !!}">--}}
          {{--<a href="{!! route('settings.billing.create') !!}">--}}
              {{--<i class="fa fa-credit-card icon"></i>--}}
              {{--<span>Billing</span>--}}
          {{--</a>--}}
      {{--</li>--}}


        <li class="heading">
          <span>Accounts</span>
        </li>

        {{--<li>--}}
          {{--<a href="">--}}
            {{--<i class="fa fa-user icon"></i>--}}
            {{--<span>My Account</span>--}}
          {{--</a>--}}
        {{--</li>--}}

        <li class="{!! setActive( 'users.index') ? 'active' : '' !!}">
            <div class="pull-right text-success m-t-sm m-r-xs">
                <i class="fa fa-circle"></i>
            </div>
          <a href="{!! route('users.index') !!}">
            <i class="fa fa-users icon"></i>
            <span>Manage Users</span>
          </a>
        </li>

      </ul>
</div>