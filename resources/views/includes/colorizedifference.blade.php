@if ($value > 0)
  <span class="badge bg-success">{{ $value }}</span>
@elseif ($value < 0)
  <span class="badge bg-danger">{{ $value }}</span>
@else
  -
@endif