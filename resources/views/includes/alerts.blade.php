<div class="dropdown-toggle">
    <i class="fa fa-globe"></i>
    <span class="visible-xs-inline">Alerts</span>
    @if ($notification_count == 0)
        <span class="badge badge-sm up pull-right-xs bg-info">0</span>
    @else
        <span class="badge badge-sm up bg-danger pull-right-xs">{{ $notification_count }}</span>
    @endif
</div>
<!-- dropdown -->
<div class="dropdown-menu w-xxl">
    <div class="panel bg-white">
        <div class="panel-heading b-light bg-light">
            <span class="badge badge-sm">0</span>&nbsp; <strong>General Notifications</strong>
        </div>
        <div class="list-group m-l-sm m-r-sm">
            <span class="text-muted">None</span>
        </div>

    </div>
</div>
<!-- / dropdown -->