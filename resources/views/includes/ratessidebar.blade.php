<div id="sidebar-wrapper" class="m-t-sm">
    <ul class="sidebar-nav">

            <li class="{!! setActive( 'settings.rates.index') ? 'active' : '' !!}">
                <a href="{!! route('settings.rates.index') !!}">
                    <i class="fa fa-sun-o icon"></i>
                    <span>Season Rates</span>
                </a>
            </li>

            <li class="{!! setActive( 'settings.rates.competitor') ? 'active' : '' !!}">
                <a href="{!! route('settings.rates.competitor') !!}">
                    <i class="fa fa-bullseye icon"></i>
                    <span>Competitors</span>
                </a>
            </li>

            <li class="{!! setActive( 'settings.rates.daily') ? 'active' : '' !!}">
                <a href="{!! route('settings.rates.daily') !!}">
                    <i class="fa fa-gear icon"></i>
                     <span>Daily Rates</span>
                </a>
            </li>

            <li class="{!! setActive( 'settings.rates.discount') ? 'active' : '' !!}">
                <a href="{!! route('settings.rates.discount') !!}">
                    <i class="fa fa-dollar icon"></i>
                    <span>Discount Rates</span>
                </a>
            </li>

        </ul>

</div>