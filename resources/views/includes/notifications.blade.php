<div class="dropdown-toggle">
    <i class="fa fa-bell"></i>
    <span class="visible-xs-inline">Notifications</span>
    @if ($notification_count == 0)
        <span class="badge badge-sm up pull-right-xs bg-info">0</span>
        @else
    <span class="badge badge-sm up bg-danger pull-right-xs">{{ $notification_count }}</span>
    @endif
</div>
<!-- dropdown -->
<div class="dropdown-menu w-xxl">
<div class="panel bg-white">
        <div class="panel-heading b-light bg-light">
            <strong><span class="badge {{ $bookings_that_need_attention->count() > 0 ? 'bg-danger' : '' }} badge-sm">{{ $bookings_that_need_attention->count() }}</span>&nbsp; Bookings That Need Your Attention</strong>
        </div>
        <div class="list-group m-l-sm m-r-sm">
            <table class="table table-striped m-b-none">
                <thead>
                <tr>
                    <th style="width:75px">Date</th>
                    <th style="width:100px">Name</th>
                    <th style="width:50px"># Guests</th>
                    <th style="width:50px"># Nights</th>
                </tr>
                </thead>
                <tbody>
                @forelse($bookings_that_need_attention as $booking)
                    <tr>
                        <td>{{ $booking->min_date }}</td>
                        <td><a href="#" data-bookingid="{{ $booking->id }}" class="loadBooking loadBookingText text-primary">{{ $booking->lastname }}</a></td>
                        <td>{{ $booking->number_of_guests }}</td>
                        <td>{{ $booking->number_of_nights }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4"><span class="text-muted">No Bookings</span></td>
                    </tr>
                @endforelse

                </tbody>
            </table>
        </div>


    <div class="panel-heading b-light bg-light">
        <strong><span class="badge {{ $possible_no_shows->count() > 0 ? 'bg-danger' : '' }} badge-sm">{{ $possible_no_shows->count() }}</span>&nbsp; Bookings That Might Be No-Shows</strong>
    </div>
    <div class="list-group m-l-sm m-r-sm">
        <table class="table table-striped m-b-none">
            <thead>
            <tr>
                <th style="width:75px">Date</th>
                <th style="width:100px">Name</th>
                <th style="width:50px"># Guests</th>
                <th style="width:50px"># Nights</th>
            </tr>
            </thead>
            <tbody>
            @forelse($possible_no_shows as $booking)
                <tr>
                    <td>{{ $booking->date }}</td>
                    <td><a href="#" data-bookingid="{{ $booking->id }}" class="loadBooking loadBookingText text-primary">{{ $booking->lastname }}</a></td>
                    <td>{{ $booking->number_of_guests }}</td>
                    <td>{{ $booking->number_of_nights }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="4"><span class="text-muted">No Bookings</span></td>
                </tr>
            @endforelse

            </tbody>
        </table>
    </div>

    <div class="panel-heading b-light bg-light">
        <strong><span class="badge {{ $bookings_that_havent_checked_out->count() > 0 ? 'bg-danger' : '' }} badge-sm">{{ $bookings_that_havent_checked_out->count() }}</span>&nbsp; Bookings That Haven't Checked Out</strong>
    </div>
    <div class="list-group m-l-sm m-r-sm">
        <table class="table table-striped m-b-none">
            <thead>
            <tr>
                <th style="width:75px">Date</th>
                <th style="width:100px">Name</th>
                <th style="width:50px"># Guests</th>
                <th style="width:50px"># Nights</th>
            </tr>
            </thead>
            <tbody>
            @forelse($bookings_that_havent_checked_out as $booking)
                <tr>
                    <td>{{ $booking->date }}</td>
                    <td><a href="#" data-bookingid="{{ $booking->id }}" class="loadBooking loadBookingText text-primary">{{ $booking->lastname }}</a></td>
                    <td>{{ $booking->number_of_guests }}</td>
                    <td>{{ $booking->number_of_nights }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="4"><span class="text-muted">No Bookings</span></td>
                </tr>
            @endforelse

            </tbody>
        </table>
    </div>


    {{--<div class="panel-footer text-sm">--}}
        {{--<p></p>--}}
    {{--</div>--}}
</div>
</div>
<!-- / dropdown -->