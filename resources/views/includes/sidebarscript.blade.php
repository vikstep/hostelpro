<script type="text/javascript">

    function getCalendarVars() {
        var calendar = {};

        @if (Route::is('dashboard') || Route::is('calendar'))
        calendar['openbooking'] = $("#bookingDetails").data('currentbookingid');
        calendar['days'] = $(".calendar-selectdays").children(".active").attr('data-days');
        calendar['viewmode'] = $(".calendar-selectViewMode").children(".active").attr('data-viewtype');
        calendar['date'] = $('#datepicker-calendar').val();
        @endif
        return calendar;
    }

    function loadBooking($bookingid, $openGuestDetailsTab) {

        if ($("#bookingDetails").data('currentbookingid') == $bookingid) {
            //If user is trying to reload the booking but has closed the bookingdetails tab, then open it up for them.
            if (!$("#bookingDetails").hasClass('open')) {
                $("#bookingDetails").trigger("click");
            }
            //If the user is trying to reload their already open booking don't do anything.
            return;
        }

        if ($("#bookingDetails").hasClass('open')) {
//            $("#bookingDetailsContainer").html("Please Wait... Loading");
            $("#bookingDetailsContainer").html('<div class="loader">Loading...</div>');
        }

        @if (Route::is('dashboard') || Route::is('calendar'))
        NProgress.configure({
            parent: '.calendar-container',
            showSpinner: false,
            trickleRate: 0.9,
            trickleSpeed: 250
        }).start();
        @endif

        var calendar = JSON.stringify(getCalendarVars());

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: "{{  url('booking') }}" + "/" + $bookingid,
            data: {
                "calendar": calendar
            },
            success: function (data) {
                $("#bookingDetails").data('currentbookingid', $bookingid);
                processResponse(data);
                if ($openGuestDetailsTab !== undefined) {
                    $('#bookingDetailsContainer').find('.nav-tabs > li').removeClass('active').eq(0).addClass('active');
                    $('#bookingDetailsContainer').find('.tab-pane:not(#personal-details):not(#address-details)').removeClass('active').eq(0).addClass('active');
                }
                loadDragAndDrop();
            }
        });
    }

    function processResponse(data) {
        if (!jQuery.isEmptyObject(data.bookingdetails)) {
            $("#bookingDetailsContainer").html(data.bookingdetails);
            if (!$("#bookingDetails").hasClass('open')) {
                $("#bookingDetails").trigger("click");
            }
            runDatePicker();
        }

        if (!jQuery.isEmptyObject(data.cashregisteraccommodation)) {
            $("#cashRegisterAccommodationContainer").html(data.cashregisteraccommodation);
            updateTotals();
            runDatePicker();
        }

        if (!jQuery.isEmptyObject(data.calendar)) {
            $('.calendar-container').html(data.calendar);
            loadDragAndDrop();
        }

        if (!jQuery.isEmptyObject(data.notifications)) {
            $('#notificationsContainer').html(data.notifications);
        }

        $('[data-toggle="tooltip"]').tooltip();
        @if (Route::is('dashboard') || Route::is('calendar'))
        NProgress.done();
        @endif
    }

    function loadBooking2($bookingid, $refresh, $openGuestDetailsTab) {

        $.ajax({
            type: "GET",
            url: "{{  url('booking') }}" + "/" + $bookingid,
            data: {refresh: $refresh},
            success: function (data) {
                $("#bookingDetails").data('currentbookingid', $bookingid);
                $("#bookingDetailsContainer").html(data);
                if (!$("#bookingDetails").hasClass('open')) {
                    $("#bookingDetails").trigger("click");
                }
                if ($openGuestDetailsTab !== undefined) {
                    $('#bookingDetailsContainer').find('.nav-tabs > li').removeClass('active').eq(0).addClass('active');
                    $('#bookingDetailsContainer').find('.tab-pane:not(#personal-details):not(#address-details)').removeClass('active').eq(0).addClass('active');
                }
                runDatePicker();
                $('[data-toggle="tooltip"]').tooltip();
                @if (Route::is('dashboard') || Route::is('calendar'))
                reloadCalendar($bookingid);
                @endif
                reloadNotifications();

            }
        });
    }

    function closeBooking() {
        //event.preventDefault();
        $("#bookingDetails").data('currentbookingid', '');
        $("#bookingDetailsContainer").html('<h3 class="text-center">Please select a booking</h3>');
        @if (Route::is('dashboard') || Route::is('calendar'))
        reloadCalendar();
        @endif

    }

    function changeCalendarDateIfNecessary(dateDDMMYYYY) {
        var date1Object = convertDayMonthYearToDateObject($('#datepicker-calendar').val());
        var date2Object = convertDayMonthYearToDateObject(dateDDMMYYYY);
        if (getDifferenceInDays(date1Object, date2Object) > 7) {
            date2Object.setDate(date2Object.getDate() - 1);
            $('#datepicker-calendar').val(date2Object.getDate() + "-" + (date2Object.getMonth() + 1) + "-" + date2Object.getFullYear());
        }
    }

    function formatDate(date) {
        var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function convertDayMonthYearToDateObject(date) {
        var newDate = new Date();
        var dateArr = date.split('-');
        newDate.setDate(dateArr[0]);
        newDate.setMonth(parseInt(dateArr[1], 10) - 1);
        newDate.setFullYear(dateArr[2]);
        return newDate;
    }

    function convertYearMonthDayToDateObject(date) {
        var newDate = new Date();
        var dateArr = date.split('-');
        newDate.setDate(dateArr[2]);
        newDate.setMonth(parseInt(dateArr[1], 10) - 1);
        newDate.setFullYear(dateArr[0]);
        return newDate;
    }

    function getDifferenceInDays(dateObject1, dateObject2) {
        var timeDiff = Math.abs(dateObject2.getTime() - dateObject1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    }


    function updateAllTotals(bookingDetailsContainerObject) {
        $(bookingDetailsContainerObject).find('.subtotal').each(function () {
            updateSubTotal($(this));
        });
        updateTotal();
    }

    function updateTotal() {
        var sum = 0;
        $("#cashRegisterAccommodationContainer").find('.subtotal').each(function () {
            sum += Number($(this).val());
        });
        $("#total").val(sum.toFixed(2));
    }

    function updateSubTotal(subtotalObject) {
        $number_of_guests = $(subtotalObject).closest('.row').find('.number_of_guests').val();
        $price_per_night = $(subtotalObject).closest('.row').find('.price_per_night').val();
        $nights = $(subtotalObject).closest('.row').find('.number_of_nights').val();
        var total = $number_of_guests * $price_per_night * $nights;
        $(subtotalObject).val(total.toFixed(2));
        updateTotal();
    }

    function calculateBookingDeposit() {
        var total = 0;
        $('.bookingDepositRow').each(function () {
            total += parseFloat($(this).find('input.price').val());
        });
        return total;
    }

    function updateTotals() {
        updateAllTotals($("#cashRegisterAccommodationContainer"));

        var accommodationExpected = parseFloat($("#total").val()) + calculateBookingDeposit();

        var total = 0;
        $('.productRow').each(function () {
            var qty = $(this).find('.qty').val();
            var price = $(this).find('.price').val();
            var newsubtotal = (qty * price).toFixed(2);
            $(this).find('.subtotal').val(newsubtotal);
            total = parseFloat(total) + parseFloat(newsubtotal);
        });
        var paymenttotal = 0;
        $('.paymentRow').each(function () {
            if ($(this).find('.subtotal').val()) {
                paymenttotal += parseFloat($(this).find('.subtotal').val());
            }
        });

        var newPayment = $('.paymentRow').last().find('.price').val();
        if (!newPayment) {
            newPayment = 0;
        }
        console.log("new payment: " + newPayment);

        var totalExcludingDeposit = parseFloat(total) - parseFloat(paymenttotal) - calculateBookingDeposit();
        //total = parseFloat(accommodation) + parseFloat(total) - parseFloat(paymenttotal);

        var totalExcludingNewPayment = parseFloat(totalExcludingDeposit) + parseFloat(newPayment);

        if (totalExcludingDeposit >= 0) {
            //They owe us money
            $('#posTotal').html(totalExcludingDeposit.toFixed(2));
            $('#changeDue').html('0.00');
            $('#changeDueHiddenInput').val('0.00');
        } else {
            //We owe them money
            if ((newPayment > 0) && (totalExcludingNewPayment > 0)) {
                $('#posTotal').html('0.00');
                $('#changeDue').html(Math.abs(totalExcludingDeposit).toFixed(2));
                $('#changeDueHiddenInput').val(Math.abs(totalExcludingDeposit).toFixed(2));
            } else {
                $('#posTotal').html(totalExcludingDeposit.toFixed(2));
                $('#changeDue').html('0.00');
                $('#changeDueHiddenInput').val('0.00');
            }
        }

        var totalAccommodation = 0;
        $('.accommodationRow').each(function () {
            if ($(this).find('.subtotal').val()) {
                totalAccommodation += parseFloat($(this).find('.subtotal').val());
            }
        });

        var remaining = parseFloat(accommodationExpected) - parseFloat(totalAccommodation);

        $('#posRemaining').html(remaining.toFixed(2));

    }

    function deleteItem($type, $id, $requesttype) {
        $.ajax({
            url: "{{ url('') }}" + "/" + $type + "/" + $id,
            type: $requesttype,
            success: function (response) {
                createSuccessModal($type + " deleted successfully.");
                if ($type == 'booking') {
                    closeBooking();
                }
                @if (Route::is('dashboard') || Route::is('calendar'))
                reloadCalendar();
                @else
                location.reload();
                @endif
                reloadNotifications();
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    }

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function editBooking(bookingid, params) {
        var calendar = JSON.stringify(getCalendarVars());
        $.ajaxq('sidebar', {
            type: "GET",
            url: "{{ url('booking') }}" + "/" + bookingid + "/edit?" + params,
            data: {calendar: calendar},
            success: function (data) {
                processResponse(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                createErrorModal(xhr.responseText);
                //Reset the form so if they selected a new value it doesn't stay on the new value.
                $("#saveAllocateForm").trigger('reset');
                updateTotals();
            },
            statusCode: {
                599: function () {
                    window.setTimeout(function () {
                        window.location.reload()
                    }, 5000);
                }
            }
        });
    }

    function addStay($date, $roomId, $bedNumber, $beds = 1, $days = 1) {
        var date = new Date($date);
        date.setDate(date.getDate() + 1);

        var now = new Date();

        if(now > date)
        {
            confirmBookingForPastDate($date, $roomId, $bedNumber, $beds, $days);
        }
        else
        {
            addStayConfirmed($date, $roomId, $bedNumber, $beds, $days);
        }
    }

    function addStayConfirmed($date, $roomId, $bedNumber, $beds, $days)
    {
        $bookingid = $("#bookingDetails").data('currentbookingid');

        var calendar = JSON.stringify(getCalendarVars());

        if ($bookingid) {
            //Add to existing open booking
            $.ajaxq('savebooking', {
                method: "POST",
                url: "booking/" + $bookingid + "/addStayToExistingTemp",
                data: {
                    bednumber: $bedNumber,
                    roomid: $roomId,
                    date: $date,
                    calendar: calendar,
                    beds: $beds,
                    days: $days
                }
            }).done(function (data) {
                processResponse(data);
            });

        } else {

            if ($.ajaxq.isRunning('savebooking')) {
                return;
            }

            //Else we need to make a new guest AND a new booking then open that booking
            $.ajaxq('savebooking', {
                method: "POST",
                url: "booking/createNewBookingAndStay",
                data: {
                    bednumber: $bedNumber,
                    roomid: $roomId,
                    date: $date,
                    beds: $beds,
                    days: $days
                }
            }).done(function (data) {
                loadBooking(data, true);
            });
        }
    }

    function confirmBookingForPastDate($date, $roomId, $bedNumber, $beds, $days)
    {
        noty({
            text: 'Do you want to create a booking for past date?',
            theme: 'relax',
            layout: 'center',
            modal: true,
            animation: {
                open: {height: 'toggle'}, // jQuery animate function property object
                close: {height: 'toggle'}, // jQuery animate function property object
                easing: 'swing', // easing
                speed: 150 // opening & closing animation speed
            },
            buttons: [
                {
                    addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    addStayConfirmed($date, $roomId, $bedNumber, $beds, $days);
                }
                },
                {
                    addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    return false;
                }
                }
            ]
        });
    }


    $(document).ready(function () {

        $('#header-search').change(function () {
            if ($(this).val()) {
                loadBooking($(this).val());
                $('#header-search').val('').trigger('change');
            }
        });

        $('#bookingDetails').on('click', '.nav-tabs li', function (event) {
            event.preventDefault();
            $('.nav-tabs li').removeClass('active');
            $(this).addClass('active');
            $(this).parent().next().find('.tab-pane:not(#personal-details):not(#address-details):not(#passport-details)').removeClass('active').eq($(this).index()).addClass('active');
        });

        $('body').on('click', '.loadBooking', function (event) {
            event.preventDefault();
            loadBooking($(this).attr('data-bookingid'));
        });

        $('#bookingDetailsContainer').on('change', '.number_of_guests', function () {
            $bookingid = $(this).data('bookingid');
            $params = "stayid=" + $(this).data('stayid') + "&guests=" + $(this).val();
            editBooking($bookingid, $params);
        }).on('input', '.price_per_night', function () {
            //updateSubTotal($(this).closest('.row').find('.subtotal'));
            updateTotals();
        }).on('input', '.number_of_nights', function () {
            $bookingid = $(this).data('bookingid');
            $params = "stayid=" + $(this).data('stayid') + "&number-of-nights=" + $(this).val();
            delay(function () {
                editBooking($bookingid, $params);
            }, 250);
            //editBooking($bookingid, $params);
        }).on('change', '.room', function () {
            $bookingid = $(this).data('bookingid');
            $params = "stayid=" + $(this).data('stayid') + "&room=" + $(this).val();
            editBooking($bookingid, $params);
        }).on('change', '.startdate', function () {
            $bookingid = $(this).data('bookingid');
            $params = "stayid=" + $(this).data('stayid') + "&startdate=" + $(this).val();
            editBooking($bookingid, $params);
        }).on('change', '.enddate', function () {
            $bookingid = $(this).data('bookingid');
            $params = "stayid=" + $(this).data('stayid') + "&enddate=" + $(this).val();
            editBooking($bookingid, $params);
        }).on('click', '.deleteStay', function () {
            $bookingid = $(this).data('bookingid');
            $params = "stayid=" + $(this).data('stayid') + "&delete=true";
            editBooking($bookingid, $params);
        }).on('click', '.addStay', function () {
            addStay(null, null, null);
        }).on('click', '#cancelAllocate', function (event) {
            closeBooking();
        }).on('click', '#cancelGuest', function (event) {
            closeBooking();
        }).on('keyup change', '#guestDetailsForm input, #guestDetailsForm select, #guestDetailsForm textarea', function () {
            $('#guestDetailsForm').data('changed', true)
        }).on('click', '#markNoShow', function () {

            var bookingid = $(this).data('bookingid');
            var method = "POST";
            var url = "booking/" + bookingid + "/noshow";

            var callback = function () {
                closeBooking();
                createSuccessModal("Successfully marked as no show.");
            };

            createAreYouSureModal(method, url, {}, 'Mark as No Show?', "You won't be able to undo this.", callback);

        }).on('click', '#submitGuest', function (event) {
            event.preventDefault();
            var url = $(this).closest('form').attr('action');

            /*var method = $(this).closest('form').find("input[name='_method']").val();*/

            var bookingid = $("#bookingDetails").data('currentbookingid');
            var calendar = JSON.stringify(getCalendarVars());
            var data = $(this).closest('form').serializeArray();
            data.push({"name": "calendar", value: calendar});
            data.push({"name": "bookingid", value: bookingid});

            $.ajax({
                type: "POST",
                dataType: 'json',
                url: url,
                data: data,
                success: function (data) {
                    processResponse(data);
                    $('#guestDetailsForm').data('changed', false)
                }
            });

        }).on('click', '#addNote', function (event) {
            event.preventDefault();

            $.ajax({
                method: "POST",
                url: $('#addNoteForm').attr('action'),
                data: $(this).closest('form').serialize(),
                success: function (data) {
                    $('#bookingNoteContainer').html(data);
                }
            });

        }).on('click', '.deleteItem', function (event) {
            event.preventDefault();

            var row = $(this).closest('.row');
            var itemid = $(this).data('itemid');

            var method = "DELETE";
            var url = "{{ route('pos.deleteitem', null) }}" + "/" + itemid;
            var callback = function () {
                row.remove();
                createSuccessModal("Item successfully deleted.");
            };

            createAreYouSureModal(method, url, {}, 'Delete Item?', "You won't be able to undo this.", callback);

        }).on('click', '.deletePayment', function (event) {
            event.preventDefault();

            var row = $(this).closest('.row');
            var paymentid = $(this).data('paymentid');

            var method = "DELETE";
            var url = "{{ route('pos.deletepayment', null) }}" + "/" + paymentid;
            var callback = function () {
                row.remove();
                createSuccessModal("Payment successfully deleted.");
            };

            createAreYouSureModal(method, url, {}, 'Delete Payment?', "You won't be able to undo this.", callback);

        }).on('click', '.updateLabelButton', function (event) {
            event.preventDefault();

            label = $(this).data('label');
            checkSubmitAllocate(label);
        }).on('click', '.deleteBooking', function (event) {
            event.preventDefault();

            var method = "DELETE";
            var url = "{{ route('booking.destroy', null) }}" + "/" + $(this).data('bookingid');
            var reason = $(this).data('reason');
            var params = {'reason' : reason};

            var callback = function () {
                closeBooking();
                createSuccessModal("Booking successfully deleted.");
            };

            if (reason.length == 0) {
                swal({
                    title: 'Enter Cancellation Reason',
                    input: 'text',
                    type: 'warning',
                    showCancelButton: true,
                    inputValidator: function(value) {
                        return new Promise(function(resolve, reject) {
                            if (value) {
                                resolve();
                            } else {
                                reject('You need to write something!');
                            }
                        });
                    }
                }).then(function(result) {
                    var params = {'reason' : result};
                    createAreYouSureModal(method, url, params, 'Delete Booking?', "You won't be able to undo this.", callback);
                });

                return;
            }

            createAreYouSureModal(method, url, params, 'Delete Booking?', "You won't be able to undo this.", callback);

        }).on('click', '#submitAllocate', function (event) {
            event.preventDefault();

            checkSubmitAllocate();
        });

        function checkSubmitAllocate(label) {
            if ($.ajaxq.isRunning('sidebar')) {
                return;
            }

            if ($.ajaxq.isRunning('savebooking')) {
                return;
            }

            if ($('.deleteStay').length == 0) {
                createErrorModal("This booking has no dates selected. If you are trying to delete this booking, please use the delete button at the bottom left.");
                return;
            }

            var roomMissing = false;
            $('#saveAllocateForm').find('.room').each(function () {
                if (!$(this).val()) {
                    roomMissing = true;
                }
            });

            if (roomMissing) {
                createErrorModal("Please assign the guest into a room before pressing save");
                return;
            }

            /** Begin warning checks **/

            var warnings = [];
            var posRemaining = $('#posRemaining').html();
            var posTotal = $('#posTotal').html();

            var depositTotal = 0;
            $('#posForm').find('.depositRow').find('.subtotal').each(function () {
                depositTotal += ($(this).val() * 100);
            });

            if (depositTotal < 0) {
                warnings.push("You are returning more deposits than originally given.")
            }

            if (label == "checkout") {
                if ((posTotal != "0.00") || (posRemaining != "0.00")) {
                    createErrorModal("You are trying to check this person out but they have unpaid items. Please take payment from guest before checking them out.");
                    return;
                    //warnings.push("You are trying to check this person out but they have unpaid items.")
                }
                if (depositTotal > 0) {
                    warnings.push("You are checking out a booking but some deposits have not yet been returned.")
                }
            }

            if (posTotal != "0.00") {
                warnings.push("You have due charges but no matching payments.")
            }


            if (label == "checkin") {
                var dates = [];
                $('#bookingDetailsContainer').find('.startdate').each(function () {
                    dates.push(convertDayMonthYearToDateObject($(this).val()));
                });

                dates.sort(function (a, b) {
                    return Date.parse(a) - Date.parse(b);
                });

                mindate = dates[0];
                today = new Date();
                if (getDifferenceInDays(today, mindate) > 2) {

                    warnings.push("Today is " + formatDate(today) + ", but this booking isn't due until " + formatDate(mindate));
                }

                var paymentAmount = $('#paymentSubtotal').val();
                if (((paymentAmount == '') || (paymentAmount == 0)) && ($('#posRemaining').html() > 0)) {
                    warnings.push("You are checking in a guest but not taking any payment");
                }

            }

            /** End Warning Checks **/

            if (warnings.length > 0) {
                prettywarnings = warnings.join('<br><br>');
                swal({
                    title: "Are you sure?",
                    text: prettywarnings,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#23ad44',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                    buttonsStyling: true
                }).then(function() {
                    submitAllocate(label)
                });
                return;
            }

            submitAllocate(label);
        }

        function submitAllocate(label) {
            var guestdata, paymentdata, bookingdata;

            if ($('#guestDetailsForm').data('changed') == true) {
                guestdata = $('#guestDetailsForm').serialize();
            }

            if (($('.deletePOSRow').length > 0) || ($('#paymentSubtotal').val() != 0)) {
                paymentdata = $('#posForm').serialize();
            }

            var bookingid = $("#bookingDetails").data('currentbookingid');

            bookingdata = $("#saveAllocateForm").serialize();

            var url = $("#saveAllocateForm").attr('action');

            var calendar = JSON.stringify(getCalendarVars());

            /*var method = $(this).closest('form').find("input[name='_method']").val();*/
            $.ajaxq('savebooking', {
                method: "PUT",
                url: url,
                data: {
                    "guestdata": guestdata,
                    "paymentdata": paymentdata,
                    "bookingdata" : bookingdata,
                    "label" : label,
                    "calendar" : calendar
                },
                success: function (data) {
                    createSuccessModal2("Booking Successfully allocated.");
                    processResponse(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    createErrorModal(xhr.responseText);
                    window.setTimeout(function () {
                        window.location.reload()
                    }, 5000);
                }
            });
        }

        $('#navbar-right .dropdown').click(function (event) {

            if (($(event.target).parents('#bookingDetailsContainer').length) || ($(event.target).hasClass('deletePOSRow'))) {
                return;
            }

            if ($(event.target).closest('li').hasClass('open')) {
                $('#navbar-right .dropdown').removeClass('open');
                if ($(event.target).closest('li').attr('id') == 'bookingDetails') {
                    $("#dashboard").width("100%");
                    $('#dashboard-footer').width("100%");
                    closeBooking();
                }
            } else {
                $('#navbar-right .dropdown').removeClass('open');
                $(event.target).closest('li').addClass('open');
                if ($(event.target).closest('li').attr('id') == 'bookingDetails') {
                    $("#dashboard-sidebar").width(648);
                    $("#dashboard").width("calc(100% - 635px)");
                    $('#dashboard-footer').width("calc(100% - 635px");
                }
            }

        });

        $(window).on('scroll', function() {
            var y_scroll_pos = window.pageYOffset;
            var scroll_pos_test = 150;             // set to whatever you want it to be

            if (y_scroll_pos > 50) {
                y_scroll_pos = 50;
            }

            $('#bookingDetailsContainer').css('top', 50 - y_scroll_pos + 'px').css('height', $(window).height() + 'px');
        });

    });
</script>