@can('hostel_settings')
@if (Auth::user()->currenthostel->setup_steps_remaining > 0)
<div class="row" id="setupWidget">
    <div class="col-lg-6 col-lg-offset-3 pt15">
        <div class="alert text-center alert-info">
            @if (Route::currentRouteName() == 'settings.myallocator.create')
                <h4>Link your MyAllocator Account</h4>
            <p>Use the form below to import all your existing MyAllocator room types, rooms and bookings.</p>
                <br>
            <div class="row">
                <div class="col-xs-1 col-xs-offset-11">
                    <a class="btn m-b-xs btn-success pull-right" href="{{ route('setup.nextstep') }}">Next Step</a>
                </div>
            </div>
                @elseif (Route::currentRouteName() == 'settings.RoomsOverview')
                    <h4>Set your room names and room order.</h4>
                    <p>Edit your room names and you can drag and drop them around to change the order in the calendar.</p>
                    <br>
                    <div class="row">
                        <div class="col-xs-1">
                            <a class="btn btn-sm m-b-xs btn-default" href="{{ route('setup.previousstep') }}">Previous Step</a>
                        </div>
                        <div class="col-xs-1 col-xs-offset-10">
                            <a class="btn m-b-xs btn-success pull-right" href="{{ route('setup.nextstep') }}">Next Step</a>
                        </div>
                    </div>
            @elseif (Route::currentRouteName() == 'settings.rates.index')
                <h4>Set your rates</h4>
                <p>Add a season rate and then set your prices.</p>
                <p>Once you press save these will be automatically communicated to MyAllocator and then out to your channels.</p>
                <br>
                <div class="row">
                    <div class="col-xs-1">
                        <a class="btn btn-sm m-b-xs btn-default" href="{{ route('setup.previousstep') }}">Previous Step</a>
                    </div>
                    <div class="col-xs-1 col-xs-offset-10">
                        <a class="btn m-b-xs btn-success pull-right" href="{{ route('setup.nextstep') }}">Next Step</a>
                    </div>
                </div>
            @elseif (Route::currentRouteName() == 'users.index')
                <h4>Add your managers/receptionists</h4>
                <p>Once a user is added they will receive a welcome email inviting them to create an account.</p>
                <br>
                <div class="row">
                    <div class="col-xs-1">
                        <a class="btn btn-sm m-b-xs btn-default" href="{{ route('setup.previousstep') }}">Previous Step</a>
                    </div>
                    <div class="col-xs-1 col-xs-offset-10">
                        <a class="btn m-b-xs btn-success pull-right" href="{{ route('setup.nextstep') }}">Next Step</a>
                    </div>
                </div>
                @else
            <h4>Complete Setup</h4>
            <br>
            <div class="progress ng-isolate-scope">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" aria-valuetext="16%" style="transition: none; width: 70%;"><b class="ng-binding ng-scope">70%</b></div>
            </div>
            <div class="row">
                <div class="col-xs-1 col-xs-offset-11">
                    <a class="btn m-b-xs btn-success pull-right" href="{{ route('setup.currentstep') }}">Next Step</a>
                </div>
            </div>

            @endif

        </div>
    </div>
</div>
@endif
@endcan