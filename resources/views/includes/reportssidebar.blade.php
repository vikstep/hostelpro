<div id="sidebar-wrapper" class="m-t-sm">
  <ul class="sidebar-nav">

    @can('advanced_reports')
    <li class="heading">General</li>

    <li class="{!! setActive( 'reports.dashboard') ? 'active' : '' !!}">
      <a href="{!! route('reports.dashboard') !!}">
        <i class="fa fa-dashboard icon"></i>
        <span>Dashboard</span>
      </a>
    </li>

    <li class="{!! setActive( 'reports.bookinglist') ? 'active' : '' !!}">
      <a href="{!! route('reports.bookinglist') !!}">
        <i class="fa fa-book icon"></i>
        <span>Guest Book</span>
      </a>
    </li>

    <li class="{!! setActive( 'reports.reviews.index') ? 'active' : '' !!}">
      <a href="{!! route('reports.reviews.index') !!}">
        <i class="fa fa-comments icon"></i>
        <span>Reviews</span>
      </a>
    </li>

    <li class="heading">Financials</li>

    <li class="{!! setActive( 'reports.revenue') ? 'active' : '' !!}">
      <a href="{!! route('reports.revenue') !!}">
        <i class="fa fa-bar-chart-o icon"></i>
        <span>Revenue</span>
      </a>
    </li>

    <li class="{!! setActive( 'expense.index') ? 'active' : '' !!}">
      <a href="{!! route('expense.index') !!}" class="auto">
        <i class="fa fa-sort-amount-asc icon"></i>
        <span>Expenses</span>
      </a>
    </li>

    <li class="{!! setActive( 'reports.paymentview') ? 'active' : '' !!}">
      <a href="{!! route('reports.paymentview') !!}" class="auto">
        <i class="fa fa-credit-card icon"></i>
        <span>Payment View</span>
      </a>
    </li>

      <li class="{!! setActive( 'reports.month2month') ? 'active' : '' !!}">
        <a href="{!! route('reports.month2month') !!}" class="auto">
          <i class="fa fa-calendar icon"></i>
          <span>Month on Month</span>
        </a>
      </li>

    <li class="{!! setActive( 'reports.profitandloss') ? 'active' : '' !!}">
      <a href="{!! route('reports.profitandloss') !!}" class="auto">
        <i class="fa fa-line-chart icon"></i>
        <span>Profit/Loss</span>
      </a>
    </li>
    @endcan

    @can('basic_reports')
    <li class="heading">Shifts</li>
    @endcan

    @can('basic_reports')
    <li class="{!! setActive( 'reports.shift.create') ? 'active' : '' !!}">
      <a href="{!! route('reports.shift.create') !!}">
        <i class="fa fa-plus-circle icon"></i>
        <span>Create</span>
      </a>
    </li>
    @endcan

    @can('advanced_reports')
    <li class="{!! setActive( 'reports.shift.index') ? 'active' : '' !!}">
      <a href="{!! route('reports.shift.index') !!}">
        <i class="fa fa-list icon"></i>
        <span>View</span>
      </a>
    </li>
    @endcan

    @can('basic_reports')
    <li class="{!! setActive( 'reports.housekeeping') ? 'active' : '' !!}">
      <a href="{!! route('reports.housekeeping') !!}">
        <i class="fa fa-file-text-o icon"></i>
        <span>Housekeeping</span>
      </a>
    </li>
    @endcan

  </ul>
</div>