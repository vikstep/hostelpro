<form class="form-inline">
  <div class="input-group dayview-selectdate">
              <span class="input-group-btn" id="dayViewMinusOneMonth">
                <button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i></button>
              </span>

    <div class="form-group">
      <input type="text" name="date" class="form-control input-md datepicker" value="{{ $date }}">
    </div>
              <span class="input-group-btn" id="dayViewAddOneMonth">
                <button type="button" class="btn btn-default"><i class="fa fa-chevron-right"></i></button>
              </span>
  </div>
  <button type="submit" class="btn btn-sm btn-info">View</button>
</form>