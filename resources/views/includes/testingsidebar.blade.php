<div class="settings_sidebar_wrap">

  <nav class="navi clearfix">
    <ul class="nav">

      <li class="heading padder m-t m-b-sm text-muted text-xs">
        <span>Testing Options</span>
      </li>

      <li class="{!! setActive( 'testing') ? 'active' : '' !!}">
        <a href="{!! route('testing') !!}" class="auto">
          <i class="fa fa-bed icon"></i>
          <span class="font-bold">Add Booking</span>
        </a>
      </li>

    </ul>
  </nav>

</div>