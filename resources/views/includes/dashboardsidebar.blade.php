@if (1 == 2)
<aside id="dashboard-sidebar" data-spy="affix">
  <nav class="site-nav clearfix">

    <ul class="nav dashboardSidebar">

      <li id="openDashboardSidebar">
        <div class="row">
          <div class="col-sm-1">
            <i class="fa fa-2x fa-bars dashboard-sidebar-icon"></i>
          </div>
        </div>
      </li>

      <li id="bookingDetails" data-currentbookingid="">
        <div class="row">
            <div class="col-sm-1">
          <i class="fa fa-2x fa-money dashboard-sidebar-icon"></i>
          </div>
          <div class="col-sm-11">
          <h2 class="hidden dashboard-sidebar-text" id="bookingDetailsText">Cash Register</h2><i class="fa fa-caret-right icon hidden"></i>
          </div>
        </div>
       
      </li>
      <li id="bookingDetailsContainer" class="hidden sidebarContainer">
        <p>Please select a booking.</p>
      </li>

    </ul>

  </nav>
</aside>
@endif