<div class="app-header navbar">
  <div class="navbar-header">
    <button class="pull-right visible-xs dk">
      <i class="glyphicon glyphicon-cog"></i>
    </button>
    <button class="pull-right visible-xs">
      <i class="glyphicon glyphicon-align-justify"></i>
    </button>
    <!-- brand -->
    <a href="#" class="navbar-brand text-lt">
      <span id="header-logo" class="hidden-folded m-l-xs">&nbsp; &nbsp; &nbsp; &nbsp;</span>
    </a>
    {{--<img src="{{ asset('assets/images/logo_white.png') }}" class="img-responsive">--}}
    <!-- / brand -->
  </div>
  <!-- / navbar header -->

  <!-- navbar collapse -->
  <div class="collapse pos-rlt navbar-collapse">

    <ul class="nav navbar-nav list-unstyled navbar-left">

      <li>
        <div id="sidebar-search">
          <select id="header-search" class="form-control no-border" style="width: 100%">

          </select>
        </div>
      </li>
      <li class="{!! setActive( 'calendar') ? 'active' : '' !!}">
        <a href="{!! route('calendar') !!}">
          <span class="text hidden-xs hidden-sm hidden-md">Bookings</span>
        </a>
      </li>
      <li class="{!! setActive( 'reports') ? 'active' : '' !!}">
        <a href="{!! route('reports.shift.create') !!}">
          <span class="text hidden-xs hidden-sm hidden-md">Reports</span>
        </a>
      </li>
      @if (Gate::allows('rates'))
      <li class="{!! setActive( 'settings.rates.index') ? 'active' : '' !!}">
        <a href="{!! route('settings.rates.index') !!}" class="auto">
          <span class="font-bold hidden-xs hidden-sm hidden-md">Rates</span>
        </a>
      </li>
        <li class="">
          <a href="http://help.hostelprofessional.com" target="_blank">
            <span class="text">Help</span>
          </a>
        </li>
      @endif
      @if (1 == 2)
      <li class="{!! Request::segment(1) == 'testing' ? 'active' : '' !!}">
        <a href="{!! route('testing') !!}">
          <i class="fa fa-flask icon"></i>
          <span class="text">Testing</span>
        </a>
      </li>
      @endif
    </ul>

    <!-- nabar right -->
    <ul class="nav navbar-nav navbar-right" id="navbar-right">

      <li id="bookingDetails" class="dropdown">
        <div class="dropdown-toggle">
          <i class="fa fa-money"></i>
        </div>
            <div id="bookingDetailsContainer" class="dropdown-menu">
              <h3 class="text-center">Please select a booking</h3>
      </li>

      <li id="notificationsContainer" class="dropdown">

        @include('includes.notifications')

      </li>

      <li id="alertsContainer" class="dropdown">

        @include('includes.alerts')

      </li>

      <li id="settingsContainer" class="dropdown btn-group">
        <div class="dropdown-toggle clear">
          {{--<span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">--}}
                {{--<img src="{!! secure_asset('assets/images/default.png', array(), true) !!}" alt="...">--}}
                {{--<i class="on md b-white bottom"></i>--}}
              {{--</span>--}}
          <i class="fa fa-user icon"></i>&nbsp;
            <span class="hidden-sm hidden-md underline-dashed">{{ Auth::user()->firstname }}</span>
          {{--<b class="caret"></b>--}}
        </div>
        <!-- dropdown -->
        <ul class="dropdown-menu" role="menu">

          @if (1 == 2)
          <li>
            <a href=""><i class="fa fa-user icon"></i><span>&nbsp; User Settings</span></a>
          </li>
          <li class="divider"></li>
          @endif

          @can('hostel_settings')
          <li class="{!! Request::segment(1) == 'settings' ? 'active' : '' !!}">
            <a href="{!! route('settings') !!}">
              <i class="fa fa-cog icon"></i>
              <span class="text">&nbsp; Hostel Settings</span>
            </a>
          </li>
            @endcan

          <li class="divider"></li>
            <li>
              <a href="{!! route('register') !!}"><i class="fa fa-plus icon"></i><span>&nbsp; Add Hostel</span></a>
            </li>

            @if (count(Auth::user()->hostelArrayList()) > 1)
            @foreach(Auth::user()->hostelArrayList() as $hostel_id => $hostel_name)
              <li>
                <a href="{!! route('changeactivehostel', $hostel_id) !!}"><i class="fa fa-bed icon"></i><span>&nbsp; {{ $hostel_name }}</span></a>
              </li>
            @endforeach
              <li class="divider"></li>
            @endif

          <li>
            <a href="{!! route('logout') !!}"><i class="fa fa-sign-out icon"></i><span>&nbsp; Logout</span></a>
          </li>
        </ul>

        <!-- / dropdown -->

      </li>


    </ul>
    <!-- / navbar right -->

  </div>
  <!-- / navbar collapse -->
</div>