<script type="text/javascript">

  /** Begin Reload Functions **/
  window.setInterval(function () {
    reloadNotifications();
  }, 120000);

  function reloadNotifications() {
    $.post("{{ route('ajax.getnotifications') }}", function (data) {
      $('#notificationsContainer').html($(data));
    });
  }
  /** End Reload Functions **/

  function runDatePicker() {
    $('.datepicker').datepicker({
      inline: true,
      showOtherMonths: true,
      dateFormat: "dd-mm-yy",
      dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    }).each(function() {
      if ($(this).val() == '') {
        //Generate a new date
        var date = new Date();
        //Set to today
        date.setDate(date.getDate());
        //Set the datepicker to the date we just generated
        $(this).datepicker("setDate", date);
      }
    });

    $('.datepicker-extended').datepicker({
      inline: true,
      showOtherMonths: true,
      changeMonth: true,
      changeYear: true,
      minDate: '-80Y',
      maxDate: '0Y',
      yearRange: "-80:+0",
      dateFormat: "dd-mm-yy",
      dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    });


  }

  runDatePicker();

  $('#header-search').select2({
    placeholder: 'Search Bookings....',
    minimumInputLength: 3,
    multiple: true,
    maximumSelectionSize: 1,
    allowClear: true,
    ajax: {
      dataType: 'json',
      url: '{{ url("ajax/searchBookings") }}',
      delay: 0,
      data: function(params) {

        //Small hack below because the "please enter x characters wasn't disappearing
        if (params.term.length >= 3) {
          $('.select2-results__options li:nth-child(2)').remove();
        }

        return {
          term: params.term
        }
      },
      processResults: function (data, page) {
        return {
          results: data
        };
      }
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
//    $('.nav').on('click', '.dropdown', function () {
//      if ($(this).hasClass('open')) {
//        $(this).removeClass('open');
//      } else {
//        $(this).addClass('open');
//      }
//    });

    /* Add global csrf token so we don't have to add it for each call */
    $.ajaxSetup({
      data: {
        _token: '{!!  csrf_token() !!}'
      },
      error: function (xhr, textStatus, thrownError) {
        if (textStatus != "abort") {
          createErrorModal(xhr.responseText);
        }
      },
      statusCode: {
        401: function() {
          window.location.replace("{!! route('login') !!}");
        }
      }
    });
  });

  function createAreYouSureModal(method, url, params, title, text, callback) {
    swal({
      title: title,
      text: text,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#23ad44',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      buttonsStyling: true,
      preConfirm: function() {
        return $.ajax({
          method: method,
          url: url,
          data: params
        });
      },
      showLoaderOnConfirm: true
    }).then(callback);
  }

  function createSavingModal() {
    swal({
      title: 'Saving...',
      text: 'Please Wait.',
      type: 'info'
      //timer: 10000,
    })
  }

  function createSuccessModal(text) {
    swal({
      title: 'Success!',
      text: text,
      type: 'success',
      timer: 10000,
      showCloseButton: true,
      customClass: 'test'
    })
  }

  function createSuccessModal2($text) {
    noty({
      text        : 'Success: ' + $text,
      type        : 'success',
      dismissQueue: true,
      layout      : 'topRight',
      theme       : 'relax',
      closeWith   : ['button', 'click'],
      maxVisible  : 20,
      timeout     : 3000,
      modal       : false,
      animation: {
        open: {height: 'toggle'}, // jQuery animate function property object
        close: {height: 'toggle'}, // jQuery animate function property object
        easing: 'swing', // easing
        speed: 200 // opening & closing animation speed
      }
    });
  }

  function createErrorModal(text) {
    console.log(text);
    if (isJson(text)) {

      textobj = jQuery.parseJSON(text);

      text = '';
      $.each(textobj, function(key, value) {
        console.log(key, value);
        text += '<strong>' + key + '</strong>: ' + value + '<br>';
      });

    }

    swal({
      title: 'Error!',
      text: text,
      type: 'error',
      timer: 4000,
      showCloseButton: true
    })
  }

  function isJson(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  function createErrorModal2($text) {

    /*
     var parsed = JSON.parse($text);
     var arr = [];
     for(var x in parsed){
     arr.push(parsed[x]);
     }

     alert(arr);*/

    noty({
      text        : 'Error: ' + $text,
      type        : 'error',
      dismissQueue: true,
      layout      : 'center',
      theme       : 'relax',
      closeWith   : ['button', 'click'],
      maxVisible  : 20,
      timeout     : 20000,
      modal       : true,
      animation: {
        open: {height: 'toggle'}, // jQuery animate function property object
        close: {height: 'toggle'}, // jQuery animate function property object
        easing: 'swing', // easing
        speed: 150 // opening & closing animation speed
      }
    });
  }
</script>

<!-- Begin Intercom chat -->
<script>
  window.intercomSettings = {
    app_id: "spf8pp5u",
    name: '{{ Auth::user()->fullName() }}',
    email: '{{ Auth::user()->email }}',
    created_at: {{ Auth::user()->created_at->timestamp }},
    hostel_name: '{{ Auth::user()->currenthostel->name }}',
    hostel_role: '{{ Auth::user()->roleAtCurrentHostel() }}',
    hostel_country: '{{ Auth::user()->currenthostel->country->country_name }}',
    hostel_currency: '{{ Auth::user()->currenthostel->currency->currency_code }}'
  };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/spf8pp5u';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
<!-- End Intercom chat -->