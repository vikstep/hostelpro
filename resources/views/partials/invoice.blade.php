@extends('layouts.backend')

@section('meta-title', 'HostelPro - Dashboard')

@section('content')

<div id="invoice">
<h2>{{ $hostel->name }}</h2>
<h3>(061) 426-4994</h3>
<h3>37 O Utca</h3>

<p>Invoice Number: #<span id="invoiceNumber">{{ $invoice->id }}</span></p>
<p>Date / Time: <span id="dateTime">{{ $invoice->created_at }}</span></p>
<p>Employee: <span id="employee">Jeremiah Barro</span></p>

<table>
  <thead>
  <th width="50%">Item</th>
  <th width="10%">Qty</th>
  <th width="20%">Amount</th>
  <th width="20%">Subtotal</th>
  </thead>
  <tbody>
    @foreach ($invoice->items as $item)
      <tr>
      <td class="itemName">{{ $item->name }}</td>
      <td class="itemQuantity">{{ $item->units }}</td>
      <td class="itemPrice">{{ $item->price/100 }}</td>
      <td class="itemSubtotal">{{ $item->subtotal/100 }}</td>
      </tr>
    @endforeach
  </tbody>
  <tbody>
    <tr>
    <th colspan="3">Subtotal: </th>
    <td id="overallSubtotal">{{ $invoice->total }}</td>
    </tr>
    <tr>
    <th colspan="3">Tax: </th>
    <td id="overallTax">0.00</td>
    </tr>
    <tr>
      <th colspan="3">Total: </th>
      <td id="overallTotal">{{ $invoice->total }}</td>
    </tr>
  </tbody>
</table>
</div>
@stop