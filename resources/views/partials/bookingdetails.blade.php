<div class="tab-container">
  <ul class="nav nav-tabs nav-justified">
    <li>
      <a href="">{{ $booking->guest->lastname . ', ' . $booking->guest->firstname }}</a>
    </li>
    <li class="active">
      <a href="">Cash Register</a>
    </li>
    <li>
      <a href="">Log</a>
    </li>
    <li>
      @if ($booking->notes->count() > 0)
        <a href="">Notes &nbsp; <span class="badge bg-danger">{{ $booking->notes->count() }}</span></a>
      @else
        <a href="">Notes &nbsp; <span class="badge bg-light">0</span></a>
      @endif

    </li>
  </ul>
  <div class="tab-content" data-bookingid="{{ $booking->id }}">

    <div class="tab-pane">
        <div class="row">
            <div class="col-sm-12">
                <div id="cashRegisterAccommodationContainer">
                    @include('partials.cashregisteraccommodation')
                </div>
            </div>
        </div>

      {!! Form::model($booking->guest, array('route' => array('guest.update',$booking->guest->id), 'method' => 'PUT', 'id' => 'guestDetailsForm', 'data-changed' => 'false')) !!}
      <input type="hidden" name="guestid" value="{{ $booking->guest->id }}">

      <div class="row">

        <div class="col-sm-8">

          @if (1 == 2)
          <!-- Nav tabs -->
          <ul class="list-inline inlineActions" role="tablist">
            <li role="presentation" class="active"><a href="#personal-details" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Personal Details</a></li>
            <li role="presentation" class=""><a href="#address-details" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Address</a></li>
            <li role="presentation" class=""><a href="#passport-details" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Passport</a></li>
            <li class=""><a class="deleteItem" href="">Delete Guest</a></li>
          </ul>
          @endif

          <div class="guest-details-form">

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="personal-details">

                <h4>Personal Details</h4>

                <!-- Personal details -->
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('firstname', "First Name") !!}
                      {!! Form::text('firstname', null, array('class' => 'form-control input-sm', 'required' => 'required')) !!}
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('lastname', "Last Name") !!}
                      {!! Form::text('lastname', null, array('class' => 'form-control input-sm', 'required' => 'required')) !!}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('email', "Email") !!}
                      {!! Form::email('email', null, array('class' => 'form-control input-sm')) !!}
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('phone', "Phone") !!}
                      {!! Form::text('phone', null, array('class' => 'form-control input-sm')) !!}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('invoice_number', "Invoice Number") !!}
                      {!! Form::text('invoice_number', null, array('class' => 'form-control input-sm')) !!}
                    </div>
                  </div>
                </div>
                <!-- /Personal details -->


                <!-- Address details -->
                <h4>Address</h4>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('country_id', "Country") !!}
                      @include('partials.form.country', array('id' => 'country_id'))
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('address_line_1', "Address") !!}
                      {!! Form::text('address_line_1', null, array('class' => 'form-control input-sm')) !!}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('city_id', "City") !!}
                      {!! Form::text('cityzzz', $booking->guest->city_name, array('class' => 'form-control input-sm')) !!}
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('address_line_2', "Address (cont.)") !!}
                      {!! Form::text('address_line_2', null, array('class' => 'form-control input-sm')) !!}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('zip_code', "Zip Code") !!}
                      {!! Form::text('zip_code', null, array('class' => 'form-control input-sm')) !!}
                    </div>
                  </div>
                </div>
                <!-- /Address details -->



                <!-- Passport details -->
                <h4>Passport Details</h4>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('passport_number', "Passport #") !!}
                      {!! Form::text('passport_number', null, array('class' => 'form-control input-sm')) !!}
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('country_id', "Passport Country") !!}
                      @include('partials.form.country', array('id' => 'passport_country_id'))
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('date_of_birth', "Date of Birth") !!}
                      {!! Form::text('date_of_birth', null, array('class' => 'form-control input-sm datepicker-extended')) !!}
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('place_of_birth', "Place of Birth") !!}
                      {!! Form::text('place_of_birth', null, array('class' => 'form-control input-sm')) !!}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('passport_issue_date', "Issue Date") !!}
                      {!! Form::text('passport_issue_date', null, array('class' => 'form-control input-sm datepicker-extended')) !!}
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      {!! Form::Label('passport_expiry_date', "Expiry Date") !!}
                      {!! Form::text('passport_expiry_date', null, array('class' => 'form-control input-sm datepicker-extended')) !!}
                    </div>
                  </div>
                </div>
                <!-- /Passport details -->

              </div>

            </div>


          </div>

          {!! Form::submit('Save', array('id' => 'submitGuest', 'class' => 'btn btn-primary')) !!}
          <button id="cancelGuest" type="button" class="btn btn-link">Close</button>


        </div>

        <div class="col-sm-4">
          <!-- Guest List -->

          <ul class="list-inline inlineActions">
            <li>Guests</li>
            @if (1 == 2)
            <li><a href="" class="new-guest">+Add New</a></li>
            @endif
          </ul>

          <ul class="list-unstyled guest-list border-top-2">
            <li role="presentation" class="active"><label class=""><input checked="" type="radio" name="guest">
                {{ $booking->guest->firstname }} {{ $booking->guest->lastname }}
                <span class="booking-attribution">- Booker</span></label></li>
          </ul>
        </div>

      </div>
      {!! Form::close() !!}



    </div>



    <div class="tab-pane active" id="cashRegisterContainer">
      @include('pages.pos')
    </div>

    <div class="tab-pane" id="myallocatorLogContainer">
        @include('partials.transactions')
<!--      @if ($booking->myallocator_formatted_log->count() > 0)
        <div class="row">
          <div class="col-sm-12">
            <h4> {{ $booking->channel->name ?? '' }} </h4>
          </div>
          <div class="col-sm-12">
            <p>MyAllocator ID: <span>{{ $booking->myallocator_id }}</span></p>
            <p>Created At: <span>{{ $booking->created_at }}</span></p>
          </div>
        </div>
      <div class="row">
        <div class="col-sm-12">
          <h4>Booking Data</h4>
        </div>
      </div>
      <table class="table table-striped table-condensed">
        <thead>
        <tr>
          <th>Start Date</th>
          <th>End Date</th>
          <th># Guests</th>
          <th>Room Type</th>
          <th>Subtotal</th>
        </tr>
        </thead>
        <tbody>
        @foreach($booking->myallocator_formatted_log as $logEntry)
          <tr>
            <td>{{ $logEntry->start_date }}</td>
            <td>{{ $logEntry->end_date }}</td>
            <td>{{ $logEntry->number_of_guests }}</td>
            <td>{{ $logEntry->roomtype->name ?? '' }}</td>
            <td>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($logEntry->subtotal) }}</td>
          </tr>
        @endforeach
        </tbody>
      </table>
        <p></p>
        <p>Total: <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($booking->myallocator_formatted_log->sum('subtotal')) }}</strong></p>
        <p>Deposit: <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($booking->getDepositAmount()) }}</strong></p>
        @if ($booking->hasCommission())
          <p>Commission: <strong>{{ $booking->getCommissionPretty() }}</strong></p>
        @endif
        <p>Remaining: <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($booking->myallocator_formatted_log->sum('subtotal') - abs($booking->getDepositAmount())) }} </strong></p>
        @else
      No log data found.
        @endif-->
    </div>
    <div class="tab-pane">

      {!! Form::open(array('route' => array('booking.note.store',$booking->id), 'id' => 'addNoteForm', 'method' => 'POST')) !!}
      <textarea id="addNoteText" name="text" class="form-control input-md m-b-sm" placeholder="Enter your note here" rows="4"></textarea>
      {!! Form::submit('Add Note', array('class' => 'btn btn-info btn-md', 'id' => 'addNote')) !!}
      {!! Form::close() !!}

      <hr>

      <div id="bookingNoteContainer">
      @include('partials.bookingnote')
      </div>

    </div>
  </div>
</div>