{!! Form::open(array('route' => array('expense.store'), 'method' => 'POST', 'class' => 'form-inline', 'id' => 'addExpenseForm')) !!}
@if (!isset($hideDate))
<input type="text" name="date" class="form-control input-md datepicker" required>
@endif
{!! Form::select('category', $categories, null, ['placeholder' => 'Category', 'class' => 'form-control', 'required' => 'required']) !!}
{!! Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'eg. Lightbulbs', 'required' => '']) !!}
<div class="input-group" id="expensePrice">
  <span class="input-group-addon">$</span>
  <input type="number" name="price" class="form-control" placeholder="0.00" min="0.00" step="0.01" required>
</div>
@if (isset($hideDate))
{!! Form::select('taken_from_cash_register', array(true => 'Yes', false => 'No'), null, ['placeholder' => 'Taken from Cash Register', 'class' => 'form-control', 'required' => 'required']) !!}
@endif
<button type="submit" class="btn btn-sm btn-primary">Submit Expense</button>
{!! Form::close() !!}