<div class="report-dayview">

  <table class="table table-condensed table-hover dayView">
    <thead>
    <tr>
      <th colspan="6"><p class="text-center">Daily Summary - {{ $report->formattedDate }} &nbsp;-&nbsp; <a href="#" onClick="window.print();return false">Print</a></p></th>
    </tr>
    <tr>
      <th colspan="6">
        <h4>
          Departing
          @if ($report->totals['departing'] > 0)
            / <span class="smallerfont"> {{ $report->totals['departing'] }} Guests</span>
          @endif
        </h4>
      </th>
    </tr>
    <tr>
      <th>Name</th>
      <th>Status</th>
      <th>Room</th>
      <th>Bed</th>
      <th># Guests</th>
      <th>Balance Due</th>
    </tr>
    </thead>
    <tbody>
    @forelse($report->departing as $departing)
      @if ($departing->status == "Checked Out")
        <tr class="checkedout">
      @else
        <tr>
          @endif
          <td><a href="#" class="loadBooking" data-bookingid="{{ $departing->id }}">{{ $departing->id }} - {{ $departing->lastname }}</a></td>
          <td>{{ $departing->status }}</td>
          <td>{{ $departing->room_name }}</td>
          <td>{{ $departing->bed }}</td>
          <td>{{ $departing->number_of_guests }}</td>
          <td>{{ $departing->totalAmountOwingFormatted() }}</td>
        </tr>
        @empty
          <tr>
            <td colspan="6">No bookings are departing on this day.</td>
          </tr>
        @endforelse
    </tbody>
  </table>

  <table class="table table-condensed table-hover dayView">
    <thead>
    <tr>
      <th colspan="6">
        <h4>
          Changing Rooms
          @if ($report->totals['changing_rooms'] > 0)
            / <span class="smallerfont"> {{ $report->totals['changing_rooms'] }} Guests</span>
          @endif
        </h4>
      </th>
    </tr>
    <tr>
      <th>Name</th>
      <th>Status</th>
      <th>Room</th>
      <th>Bed</th>
      <th># Guests</th>
      <th>Balance Due</th>
    </tr>
    </thead>
    <tbody>
    @forelse($report->changing_rooms as $changing)
      @if ($changing->status == "Checked Out")
        <tr class="checkedout">
      @else
        <tr>
          @endif
          <td><a href="#" class="loadBooking" data-bookingid="{{ $changing->id }}">{{ $changing->id }} - {{ $changing->lastname }}</a></td>
          <td>{{ $changing->status }}</td>
          <td>{{ $changing->room_name }}</td>
          <td>{{ $changing->bed }}</td>
          <td>{{ $changing->number_of_guests }}</td>
          <td>{{ $changing->totalAmountOwingFormatted() }}</td>
        </tr>
        @empty
          <tr>
            <td colspan="6">No bookings are changing rooms on this day.</td>
          </tr>
        @endforelse
    </tbody>
  </table>

  <table class="table table-condensed table-hover dayView">
    <thead>
    <tr>
      <th colspan="6">
        <h4>
          Arriving
          @if ($report->totals['arriving'] > 0)
            / <span class="smallerfont"> {{ $report->totals['arriving'] }} Guests</span>
          @endif
        </h4>
      </th>
    </tr>
    <tr>
      <th>Name</th>
      <th>Status</th>
      <th>Room</th>
      <th>Bed</th>
      <th># Guests</th>
      <th>Balance Due</th>
    </tr>
    </thead>
    <tbody>
    @forelse($report->arriving as $arriving)
      @if ($arriving->status == "Checked in - Paid")
        <tr class="checkedout">
      @else
        <tr>
          @endif
          <td><a href="#" class="loadBooking" data-bookingid="{{ $arriving->id }}">{{ $arriving->id }} - {{ $arriving->lastname }}</a></td>
          <td>{{ $arriving->status }}</td>
          <td>{{ $arriving->room_name }}</td>
          <td>{{ $arriving->bed }}</td>
          <td>{{ $arriving->number_of_guests }}</td>
          <td>{{ $arriving->totalAmountOwingFormatted() }}</td>
        </tr>
        @empty
          <tr>
            <td colspan="6">No bookings are arriving on this day.</td>
          </tr>
        @endforelse
    </tbody>
  </table>

  <table class="table table-condensed table-hover dayView">
    <thead>
    <tr>
      <th colspan="6">
        <h4>
          Remaining
          @if ($report->totals['remaining'] > 0)
            / <span class="smallerfont"> {{ $report->totals['remaining'] }} Guests</span>
          @endif
        </h4>
      </th>
    </tr>
    <tr>
      <th>Name</th>
      <th>Status</th>
      <th>Room</th>
      <th>Bed</th>
      <th># Guests</th>
      <th>Balance Due</th>
    </tr>
    </thead>
    <tbody>
    @forelse($report->remaining as $remaining)
      @if ($remaining->status == "Checked Out")
        <tr class="checkedout">
      @else
        <tr>
          @endif
          <td><a href="#" class="loadBooking" data-bookingid="{{ $remaining->id }}">{{ $remaining->id }} - {{ $remaining->lastname }}</a></td>
          <td>{{ $remaining->status }}</td>
          <td>{{ $remaining->room_name }}</td>
          <td>{{ $remaining->bed }}</td>
          <td>{{ $remaining->number_of_guests }}</td>
          <td>{{ $remaining->totalAmountOwingFormatted() }}</td>
        </tr>
        @empty
          <tr>
            <td colspan="6">No bookings are remaining on this day.</td>
          </tr>
        @endforelse
    </tbody>
  </table>

  <table class="table table-condensed table-hover dayView">
    <thead>
    <tr>
      <th colspan="6">
        <h4>
          Blocked
          @if ($report->totals['blocked'] > 0)
            / <span class="smallerfont"> {{ $report->totals['blocked'] }} Beds</span>
          @endif
        </h4>
      </th>
    </tr>
    <tr>
      <th>Name</th>
      <th>Status</th>
      <th>Room</th>
      <th>Bed</th>
      <th># Beds</th>
      <th>Balance Due</th>
    </tr>
    </thead>
    <tbody>
    @forelse($report->blocked as $blocked)
      <tr>
        <td><a href="#" class="loadBooking" data-bookingid="{{ $blocked->id }}">{{ $blocked->id }} - {{ $blocked->lastname }}</a></td>
        <td>{{ $blocked->status }}</td>
        <td>{{ $blocked->room_name }}</td>
        <td>{{ $blocked->bed }}</td>
        <td>{{ $blocked->number_of_guests }}</td>
        <td>{{ $blocked->totalAmountOwingFormatted() }}</td>
      </tr>
    @empty
      <tr>
        <td colspan="6">No beds are blocked on this day.</td>
      </tr>
    @endforelse
    </tbody>
  </table>

</div>