  <td class="v-middle"> {{ $room['name'] }} </td>
  <td class="v-middle"> {{ $room['type'] }} </td>
  <td class="v-middle"> {{ $room['gender'] }} </td>
  <td class="v-middle"> {{ $room['number_of_guests'] }} </td>
  <td class="v-middle"> {!! $room['myallocator_id'] ? '<span class="label text-base bg-success pos-rlt m-r">Yes</span>' : '<span class="label text-base bg-light pos-rlt m-r">No</span>' !!}</td>
  <td class="v-middle">
    <button class="btn btn-sm btn-danger deleteRoomType" data-roomtypeid="{{ $room['id'] }}"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>
  </td>