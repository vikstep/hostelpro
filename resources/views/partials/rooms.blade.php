<table class="table table-condensed rooms">
  <thead>
  <th>Name</th>
  <th>Type</th>
  <th>Edit</th>
  </thead>
  <tbody id="roomTable">
  @forelse($rooms as $room)
    <tr id="rooms_{{ $room->id }}">@include('partials.room')</tr>
  @empty
    <tr id="norooms">
      <td>No Rooms exist.</td>
    </tr>
  @endforelse
  </tbody>
</table>