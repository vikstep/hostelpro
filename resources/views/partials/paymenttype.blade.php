@foreach($payment_types as $payment_type)
    <tr id="paymenttypes_{{ $payment_type->id }}">
        <td class="v-middle"> {{ $payment_type->name }} </td>
        <td class="v-middle"> {{ $payment_type->kept_in_till ? 'Yes' : 'No' }} </td>
        <td class="v-middle">
            <button class="btn btn-sm btn-danger deletePaymentType" data-id="{{ $payment_type->id }}"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>
        </td>
    </tr>
@endforeach