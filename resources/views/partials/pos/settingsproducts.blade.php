<table id="POSProductTable" class="table table-condensed POSProduct">
    <thead>
    <th>Name</th>
    <th>Category</th>
    <th>Price</th>
    <th>Fixed Price</th>
    <th>Deposit</th>
    <th>Edit</th>
    </thead>
    <tbody id="productTable">
    @if (count($products))
        @foreach($products as $product)
            <tr id="product_{{ $product->id }}">
                @include('partials.pos.settingsproduct')
            </tr>
        @endforeach
    @else
        <tr class="noresults">
            <td colspan="5">No Products exist.</td>
        </tr>
    @endif
    </tbody>
</table>