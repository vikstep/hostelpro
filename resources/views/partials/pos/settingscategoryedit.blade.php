<td class="v-middle"><input type="text" class="form-control name" name="name" value="{{ $category->name }}"></td>
<td class="v-middle">
    <button class="btn btn-sm btn-info saveEditCategory" data-categoryid="{{ $category->id }}"><i
                class="fa fa-floppy-o"></i>&nbsp; Save &nbsp;</button>
    <button class="btn btn-sm btn-danger deleteCategory" data-categoryid="{{ $category->id }}"><i
                class="fa fa-trash-o"></i>&nbsp; Delete
    </button>
</td>