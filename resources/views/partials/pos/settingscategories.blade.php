<table class="table table-condensed POSCategory">
    <thead>
    <th>Name</th>
    <th>Edit</th>
    </thead>
    <tbody id="categoryTable">
    @if (count($categories))
        @foreach($categories as $category)
            <tr id="category_{{ $category->id }}">
                @include('partials.pos.settingscategory')
            </tr>
        @endforeach
    @else
        <tr class="noresults">
            <td colspan="2">No Categories exist.</td>
        </tr>
    @endif
    </tbody>
</table>