<td class="v-middle"> {{ $category->name }} </td>
<td class="v-middle">
    <button class="btn btn-sm btn-info editCategory" data-categoryid="{{ $category->id }}"><i class="fa fa-pencil"></i>&nbsp; Edit &nbsp;</button>
    <button class="btn btn-sm btn-danger deleteCategory" data-categoryid="{{ $category->id }}"><i
                class="fa fa-trash-o"></i>&nbsp; Delete
    </button>
</td>