@forelse ($products as $product)
  <li class="list-group-item addProduct" data-productname="{{ $product->name }}" data-productid="{{ $product->id }}" data-formattedprice="{{ $product->formatted_price }}" data-fixedprice="{{ $product->is_fixed_price }}" data-deposit="{{ $product->deposit }}">{{  $product->name }}</li>
@empty
  <li class="list-group-item">No products in this category.</li>
@endforelse