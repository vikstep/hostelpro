<td class="v-middle"><input type="text" class="form-control name" name="name" value="{{ $product['name'] }}"></td>
<td class="v-middle">{!! Form::select("category_id", Auth::user()->currenthostel->posCategories->pluck("name", "id"), $product->category_id, array("class" => "form-control category_id")) !!}</td>
<td class="v-middle"><input type="text" class="form-control price" name="price" value="{{ $product->formatted_price }}"></td>
<td class="v-middle">{!! Form::select("is_fixed_price", [true => 'Yes', false => 'No'], $product->is_fixed_price, array("class" => "form-control is_fixed_price")) !!}</td>
<td class="v-middle">{!! Form::select("deposit", [true => 'Yes', false => 'No'], $product->deposit, array("class" => "form-control deposit")) !!}</td>
<td class="v-middle">
    <button class="btn btn-sm btn-info saveEditProduct" data-productid="{{ $product->id }}"><i
                class="fa fa-floppy-o"></i>&nbsp; Save &nbsp;</button>
    <button class="btn btn-sm btn-danger deleteProduct" data-productid="{{ $product->id }}"><i
                class="fa fa-trash-o"></i>&nbsp; Delete
    </button>
</td>