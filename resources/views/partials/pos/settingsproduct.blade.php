<td class="v-middle"> {{ $product['name'] }} </td>
<td class="v-middle"> {{ $product->category->name }} </td>
<td class="v-middle"> {{ $product->formatted_price }} </td>
<td class="v-middle"> {{ $product->is_fixed_price ? 'Yes' : 'No' }}</td>
<td class="v-middle"> {{ $product->deposit ? 'Yes' : 'No' }}</td>
<td class="v-middle">
    <button class="btn btn-sm btn-info editProduct" data-productid="{{ $product->id }}"><i class="fa fa-pencil"></i>&nbsp; Edit &nbsp;</button>
    <button class="btn btn-sm btn-danger deleteProduct" data-productid="{{ $product->id }}"><i
                class="fa fa-trash-o"></i>&nbsp; Delete
    </button>
</td>