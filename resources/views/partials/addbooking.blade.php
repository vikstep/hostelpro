<div class="tab-container ng-isolate-scope">
  <ul class="nav nav-tabs">
    <li class="active">
      <a href="">Booking Details</a>
    </li>
    <li>
      <a href="">Additional Info</a>
    </li>
  </ul>
  {!! Form::open(['route' => 'booking.store']) !!}

  <div class="tab-content">

    <div class="tab-pane active">

      <div class="row">
        <div class="col-lg-5 padding5">
          <div class="form-group">
            {!! Form::Label('start_date', "Start Date") !!}
            {!! Form::text('start_date', '', array('id' => 'start_date', 'class' => 'form-control datepicker', 'placeholder' => 'Date', 'required')) !!}
            {!! errors_for('start_date', $errors) !!}
          </div>
        </div>
        <div class="col-lg-5 padding5">
          <div class="form-group">
            {!! Form::Label('end_date', "End Date") !!}
            {!! Form::text('end_date', '', array('id' => 'end_date', 'class' => 'form-control datepicker', 'placeholder' => 'Date', 'required')) !!}
            {!! errors_for('end_date', $errors) !!}
          </div>
        </div>
        <div class="col-lg-2 padding5">
          <div class="form-group">
            {!! Form::Label('number_of_guests', "Guests") !!}
            {!! Form::text('number_of_guests', '', array('class' => 'form-control')) !!}
            {!! errors_for('number_of_guests', $errors) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6 padding5">
          <div class="form-group">
            {!! Form::Label('firstname', "First Name") !!}
            {!! Form::text('firstname', '', array('class' => 'form-control', 'required' => 'required')) !!}
            {!! errors_for('firstname', $errors) !!}
          </div>
        </div>
        <div class="col-lg-6 padding5">
          <div class="form-group">
            {!! Form::Label('lastname', "Last Name") !!}
            {!! Form::text('lastname', '', array('class' => 'form-control', 'required' => 'required')) !!}
            {!! errors_for('lastname', $errors) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-8 padding5">
          <div class="form-group">
            {!! Form::Label('room_type_id', "Room Type") !!}
            {!! Form::select('room_type_id', $roomtypes, false, array('class' => 'form-control')) !!}
            {!! errors_for('room_type_id', $errors) !!}
          </div>
        </div>
        <div class="col-lg-4 padding5">
          <div class="form-group">
            {!! Form::Label('price_per_night', "Price per Night") !!}
            {!! Form::text('price_per_night', '', array('class' => 'form-control')) !!}
            {!! errors_for('price_per_night', $errors) !!}
          </div>
        </div>
      </div>

    </div>

    <div class="tab-pane">

      <div class="row">
        <div class="col-lg-6 padding5">
          <div class="form-group">
            {!! Form::Label('email', "Email") !!}
            {!! Form::email('email', '', array('class' => 'form-control')) !!}
            {!! errors_for('email', $errors) !!}
          </div>
        </div>
        <div class="col-lg-6 padding5">
          <div class="form-group">
            {!! Form::Label('phone', "Phone") !!}
            {!! Form::text('phone', '', array('class' => 'form-control')) !!}
            {!! errors_for('phone', $errors) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6 padding5">
          <div class="form-group">
            {!! Form::Label('country_id', "Country") !!}
            {!! Form::select('country_id', $countries, false, array('class' => 'form-control')) !!}
            {!! errors_for('country_id', $errors) !!}
          </div>
        </div>
        <div class="col-lg-6 padding5">
          <div class="form-group">
            {!! Form::Label('city_id', "City") !!}
            {!! Form::select('city_id', array(), false, array('class' => 'form-control')) !!}
            {!! errors_for('city_id', $errors) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12 padding5">
          <div class="form-group">
            {!! Form::Label('address_line_1', "Address") !!}
            {!! Form::text('address_line_1', '', array('class' => 'form-control')) !!}
            {!! errors_for('address_line_1', $errors) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-9 padding5">
          <div class="form-group">
            {!! Form::Label('address_line_2', "Address (cont.)") !!}
            {!! Form::text('address_line_2', '', array('class' => 'form-control')) !!}
            {!! errors_for('address_line_2', $errors) !!}
          </div>
        </div>
        <div class="col-lg-3 padding5">
          <div class="form-group">
            {!! Form::Label('zip_code', "Zip Code") !!}
            {!! Form::text('zip_code', '', array('class' => 'form-control')) !!}
            {!! errors_for('zip_code', $errors) !!}
          </div>
        </div>
      </div>


    </div>
    {!! Form::submit('Add Booking', array('class' => 'btn btn-lg btn-primary btn-block')) !!}
    {!! Form::close() !!}
  </div>