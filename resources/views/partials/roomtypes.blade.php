<table class="table table-hover table-condensed roomtypes">
  <thead>
  <th>Name</th>
  <th>Type</th>
  <th>Gender</th>
  <th>Capacity</th>
  <th>MyAllocator</th>
  <th>Edit</th>
  </thead>
  <tbody>
  @forelse($roomtypes as $room)
    <tr>@include('partials.roomtype')</tr>
  @empty
    <tr id="noroomtypes">
      <td>No Room Types exist.</td>
    </tr>
  @endforelse
  </tbody>
</table>