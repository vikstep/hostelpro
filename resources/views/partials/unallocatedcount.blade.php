<div class="row">
  <div class="col-sm-1 badgefix">
    <i class="fa fa-2x fa-bell icon dashboard-sidebar-icon"></i>
    @if ($unallocated_bookings_count == 0)
      <b class="badge up-large bg-info">0</b>
    @else
      <b class="badge up-large bg-danger">{{ $unallocated_bookings_count }}</b>
    @endif
  </div>
  <div class="col-sm-11">
    <h2 class="hidden purple dashboard-sidebar-text">Bookings</h2><i class="fa fa-caret-right icon hidden"></i>
  </div>
</div>