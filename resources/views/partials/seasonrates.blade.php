<div class="row seasonRates">
  <ul class="nav nav-tabs">
    @foreach($seasonrates as $key => $seasonrate)
      <li class="{{ ($key == 0) ? 'active' : '' }}">
        <a href="">{{ $seasonrate->name }}<br><span class="small">{{ $seasonrate->getFormattedStartDate() }} - {{ $seasonrate->getFormattedEndDate() }}</span></a>

      </li>
    @endforeach
  </ul>
  <div class="tab-content col-sm-10">

    @foreach($seasonrates as $key => $seasonrate)
      <div class="tab-pane {{ ($key == 0) ? 'active' : ''}}">

        {!! Form::open(array('route' => array('settings.rates.update', $seasonrate->id), 'method' => 'PUT', 'class' => 'form-inline')) !!}
        <input type="hidden" name="modify" value="true">

        <div class="form-group">
          <input class="form-control datepicker" value="{{ $seasonrate->getFormattedStartDate() }}" required="required" name="startdate" type="text">
        </div>
        <div class="form-group">
          &nbsp; to &nbsp;
        </div>
        <div class="form-group">
          <input class="form-control datepicker" value="{{ $seasonrate->getFormattedEndDate() }}" required="required" name="enddate" type="text">
        </div>
        <div class="form-group">
          <button class="btn btn-sm btn-primary" type="submit">Modify Dates</button>
          {!! Form::close() !!}
        </div>

        {!! Form::open(array('route' => array('settings.rates.destroy', $seasonrate->id), 'method' => 'DELETE', 'class' => 'form-inline')) !!}
        <div class="form-group">
          &nbsp;or&nbsp;
          <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-trash"></i>&nbsp; Delete</button>
        </div>
        {!! Form::close() !!}

        <br>

        {!! Form::open(array('route' => array('settings.rates.update', $seasonrate->id), 'method' => 'PUT', 'class' => 'rates-form form-inline')) !!}
        <table class="table table-striped table-condensed">
          <thead>
          <tr>
            <th></th>
            <th></th>
            @foreach ($days_of_week[$seasonrate->id] as $i => $day_of_week)
              <th>{!! (count($days_of_week[$seasonrate->id]) < 7) ? App\HostelPro\Models\DateHelper::addDays($seasonrate->start_date, $i) . '<br>' : '' !!} {{ $day_of_week }}</th>
            @endforeach

            @foreach($rateData[$seasonrate->id] as $roomtype)
              @if (1 == 2)
                <th>{!! (count($dates[$seasonrate->id]) < 7) ? $date . '<br>' : '' !!} {{ $day_of_week[$seasonrate->id][$date] }}</th>
              @endif
            @endforeach
          </tr>
          </thead>
          <tbody>
          @foreach($roomtypes as $roomtype)
            <tr>
              <th rowspan="2">{{ $roomtype->name }}</th>
              <th>Price (per {{ $roomtype->type == "Private" ? ' room' : 'bed' }})</th>
              @foreach($rateData[$seasonrate->id][$roomtype->id] as $key => $data)
                <td>
                  <input type="text" name="prices[{{$roomtype->id}}][{{$key}}]" value="{{ $data['price'] }}" class="form-control input-sm" required="required">
                </td>
              @endforeach
            </tr>
            <tr>
              <th>Min. Nights</th>
              @foreach($rateData[$seasonrate->id][$roomtype->id] as $key => $data)
                <td>
                  <input type="text" name="nights[{{$roomtype->id}}][{{$key}}]" value="{{ $data['nights'] }}" class="form-control input-sm" required="required">
                </td>
              @endforeach
            </tr>
          @endforeach
          </tbody>
        </table>
        <p>
          <button class="btn m-b-xs btn-sm btn-success btn-addon" type="submit"><i class="fa fa-check"></i>Save Rates</button>
          {!! Form::close() !!}
        </p>

      </div>
    @endforeach
  </div>
</div>