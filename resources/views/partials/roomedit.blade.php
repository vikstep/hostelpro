<td class="v-middle"><input type="text" class="form-control name" name="name" value="{{ $room->name }}"></td>
<td class="v-middle">{{ $room->roomtype->name }} </td>
<td class="v-middle">
  <button class="btn btn-sm btn-info saveEditRoom" data-roomid="{{ $room->id }}"><i class="fa fa-floppy-o"></i>&nbsp; Save &nbsp;</button>
  <button class="btn btn-sm btn-danger deleteRoom" data-roomid="{{ $room->id }}"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>
</td>