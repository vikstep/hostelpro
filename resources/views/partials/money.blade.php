@foreach($money as $item)
  <tr>
    <td class="v-middle"> {{ $item->getAttributes()['amount'] / 100 }} </td>
    <td class="v-middle">
      <button class="btn btn-sm btn-danger deleteMoney" data-moneyid="{{ $item['id'] }}"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>
    </td>
  </tr>
@endforeach