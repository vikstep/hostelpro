<td class="v-middle"> {{ $room->name }} </td>
<td class="v-middle"> {{ $room->roomtype->name }} </td>
<td class="v-middle">
  <button class="btn btn-sm btn-info editRoom" data-roomid="{{ $room->id }}"><i class="fa fa-pencil"></i>&nbsp; Edit &nbsp;</button>
  <button class="btn btn-sm btn-danger deleteRoom" data-roomid="{{ $room->id }}"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>
</td>