@foreach($expenseCategories as $expenseCategory)
  <tr>
    <td class="v-middle"> {{ $expenseCategory['name'] }} </td>
    <td class="v-middle">
      <button class="btn btn-sm btn-danger deleteExpense" data-expenseid="{{ $expenseCategory['id'] }}"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>
    </td>
  </tr>
@endforeach