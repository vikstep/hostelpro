  <table class="table table-striped table-condensed calendar-table">

    <thead>

    <tr class="calendar-month">
      <td></td>
      <td class="calendar-beds"></td>
      @foreach($calendar->table_month_headings as $month)
        <th colspan="{{ $month['days'] }}" class="text-center month-border">{{ $month['month'] . " " . $month['year'] }}</th>
      @endforeach
    </tr>

    <tr class="calendar-date">
      <th></th>
      <th class="calendar-beds">Bed</th>
      @foreach($calendar->table_month_headings as $i => $month)
        @for ($x = 0; $x < $month['days']-1; $x++)
          <th class="text-center {{ ($calendar->table_headings[$i][$x][1] == 'Fri' || $calendar->table_headings[$i][$x][1] == 'Sat') ? 'weekend' : '' }}">{{ $calendar->table_headings[$i][$x][0] }}<br><span class="calendar-dayofweek">{{ $calendar->table_headings[$i][$x][1] }}</span></th>
        @endfor
        <th class="text-center month-border">{{ $calendar->table_headings[$i][$x][0] }}<br><span class="calendar-dayofweek">{{ $calendar->table_headings[$i][$x][1] }}</span></th>
      @endforeach
    </tr>

    </thead>

    @forelse($calendar->rooms as $room)
      <tbody>
      @for ($bed = 1; $bed <= $room->roomtype->number_of_guests; $bed++)
        <tr>
          @if ($bed == 1)
            <th rowspan="{{ $room->roomtype->number_of_guests }}" class="text-center room-name room-border" data-roomid="{{ $room->id }}">{{ $room->name }}<br>({{ $room->roomtype->number_of_guests }})</th>
          @endif

          <td class="bednumber text-center">{{ $bed }}</td>

          @foreach ($calendar->boardarray[$room->id][$bed] as $key => $date)
            @if (!empty($date) && (!isset($date['unallocated'])))

                <td id="{{ $date['bookingid'] }}" class="text-center booking-cell loadExistingBooking {{ $date['label'] }} {{ $date['draggable'] }}
                @if ($date['bookingid'] == $calendar->tempBooking_id)
                  calendar-booking-currently-selected
                @endif
                @if ($date['draggable'])
                        calendar-draggable
                @endif
                " data-date="{{ $key }}" data-firstname="{{ $date['firstname'] }}" data-lastname="{{ $date['lastname'] }}" data-bookingid="{{ $date['bookingid']  }}" data-stayid="{{ $date['stayid']  }}" data-locked="{{ $date['locked'] }}" data-possiblenoshow="{{ $date['possiblenoshow'] }}" rowspan="{{ $date['rowspan'] }}" colspan="{{ $date['colspan'] }}">

                    <a href="#">{{ $date['lastname'] }}</a>

                </td>
              @elseif (!empty($date) && (isset($date['unallocated'])))
              <td class="text-center calendar-booking-unpaid unallocated addStay" data-date="{{ $key }}">&nbsp;</td>

            @else
              <td class="text-center addStay" data-date="{{ $key }}">
              @if (substr($key,-2,-1) == 0)
                {{ substr($key,-1,1) }}
                @else
                {{ substr($key,-2) }}
                @endif
              </td>
            @endif
          @endforeach

        </tr>
      @endfor
      </tbody>
    @empty
      <tbody>
      <tr>
        <td colspan="31">No Rooms exist.</td>
      </tr>
      </tbody>
    @endforelse

  </table>