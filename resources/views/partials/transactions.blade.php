<div class="row">
  <div class="col-sm-2">
    <h4>Transactions</h4>
  </div>
  <div class="col-sm-1 col-sm-offset-3">
    <h4>Qty</h4>
    </div>
  <div class="col-sm-2">
    <h4>Price</h4>
  </div>
  <div class="col-sm-2">
    <h4>Amount</h4>
  </div>
    <div class="col-sm-2">
    <h4>Payment</h4>
  </div>
</div>
<div id="transactionsDiv">
  @foreach($booking->transactions() as $item)
    @if (class_basename($item) == "ActionLog")
      <div class="row">
        <div class="col-sm-2 text-right">
          <p data-toggle="tooltip" data-placement="right" title="{{ explode(' ',$item->created_at)[1] }} {{ $item->user ? 'by ' . $item->user->firstname : 'by system' }}">
            {{ strtok($item->created_at, " ") }}
          </p>
        </div>
        <div class="col-sm-8 nopadding">
          {!! $item->type->name !!}
        </div>
      </div>
    @elseif (class_basename($item) == "AdditionalItem")
      <div class="row productRow {{ ($item->name == "Accommodation") ? 'accommodationRow' : '' }} {{ ($item->name == "Booking Deposit") ? 'bookingDepositRow' : '' }} {{ $item->deposit ? 'depositRow' : '' }}">
        <div class="col-sm-2 text-right nopadding">
          <p data-toggle="tooltip" data-placement="right" title="{{ explode(' ',$item->created_at)[1] }} {{ $item->user ? 'by ' . $item->user->firstname : 'by system' }}">
            @if (Auth::user()->canDelete($item))
            <i class="fa fa-trash-o fa-fw text-muted deleteItem" data-itemid="{{ $item->id }}"></i>
            @endif
            {{ strtok($item->created_at, " ") }}
          </p>
        </div>
        <div class="col-sm-3 nopadding">
          {!! $item->name !!}
        </div>
        <div class="col-sm-1 nopadding">
          <input class="form-control input-sm qty" readonly="readonly" min="1" step="1" max="99" name="qty" type="number" value="{!! $item->units !!}">
        </div>
        <div class="col-sm-2 nopadding">
          <input class="form-control input-sm price" readonly="readonly" name="price" type="text" value="{{ ($item->price/100) }}">
        </div>
        <div class="col-sm-2 nopadding">
          <input class="form-control input-sm subtotal" readonly="readonly" name="subtotal" type="text" value="{!! ($item->subtotal/100) !!}">
        </div>
      </div>
      @else
      <div class="row paymentRow">
        <div class="col-sm-2 text-right nopadding">
          <p data-toggle="tooltip" data-placement="right" title="{{ explode(' ',$item->created_at)[1] }} {{ $item->user ? 'by ' . $item->user->firstname : 'by system' }}">
            @if (Auth::user()->canDelete($item))
            <i class="fa fa-trash-o fa-fw text-muted deletePayment" data-paymentid="{{ $item->id }}"></i>
            @endif
            {{ strtok($item->created_at, " ") }}
          </p>
        </div>
        <div class="col-sm-8 nopadding">
          {!! $item->name !!}
        </div>
        <div class="col-sm-2 nopadding">
          <input class="form-control input-sm subtotal" readonly="readonly" name="subtotal" type="text" value="{!! ($item->total/100) !!}">
        </div>
      </div>

      @endif
  @endforeach
   
</div>