{!! Form::open(array('route' => array('booking.update',$booking->id), 'method' => 'PUT', 'id' => 'saveAllocateForm')) !!}
@if ($booking->groupHasBeenSplit())
  <div class="row">
    <div class="col-sm-12">
      <div class="alert text-center alert-info">This group booking has been split into multiple rooms. Try to unallocate some other bookings before allocating this one.</div>
    </div>
  </div>
@endif

@if (Session::has('flash_message'))
  <div class="row">
    <div class="col-sm-12">
      <div class="alert text-center {{ Session::get('alert-class', 'alert-info') }}">{!! Session::get('flash_message') !!}</div>
    </div>
  </div>
@endif

<div class="row">
  <div class="col-sm-12">
    <h4>Accommodation</h4>
  </div>
</div>

<div class="row">
  <div class="col-sm-2">
    {!! Form::Label('start_date', "Arrive") !!}
  </div>
  <div class="col-sm-1 nopadding">
    {!! Form::Label('number_of_nights', "Nights") !!}
  </div>
  <div class="col-sm-2 nopadding">
    {!! Form::Label('end_date', "Depart") !!}
  </div>
  <div class="col-sm-1 nopadding">
    {!! Form::Label('number_of_guests', "Guests") !!}
  </div>
  <div class="col-sm-2 nopadding">
    {!! Form::Label('room_id', "Room") !!}
  </div>
  <div class="col-sm-2 nopadding">
    {!! Form::Label('price', "Price") !!}
  </div>
  <div class="col-sm-2 nopadding">
    {!! Form::Label('subtotal', "Subtotal") !!}
  </div>
</div>

@foreach($booking->stays as $stay)
  <div class="row">
    <div class="col-sm-2 nopadding">
      <div class="row">
        <div class="col-sm-1 nopadding">

        </div>
        <div class="col-sm-2 nopadding">
          <i class="fa fa-trash-o fa-fw text-muted deleteStay" data-bookingid="{{ $booking->id }}" data-stayid="{{ $stay->id }}"></i>
        </div>
        <div class="col-sm-9 nopadding">
          {!! Form::text('start_date['.$stay->id.']', $stay->pretty_start_date, array('id' => 'start_date['.$stay->id.']', 'data-bookingid' => $stay->booking->id, 'data-stayid' => $stay->id, 'class' => 'form-control input-sm datepicker startdate', 'required')) !!}
        </div>
      </div>
    </div>
    <div class="col-sm-1 nopadding">
      {!! Form::input('number', 'number_of_nights', \App\HostelPro\Models\DateHelper::getDaysBetween($stay->start_date, $stay->end_date)+1, array('data-bookingid' => $stay->booking->id, 'data-stayid' => $stay->id, 'class' => 'form-control input-sm number_of_nights', 'min' => '1', 'step' =>
      '1', 'max' => '31')) !!}
    </div>
    <div class="col-sm-2 nopadding">
      {!! Form::text('end_date['.$stay->id.']', $stay->pretty_end_date, array('id' => 'end_date['.$stay->id.']', 'data-bookingid' => $stay->booking->id, 'data-stayid' => $stay->id, 'class' => 'form-control input-sm datepicker enddate', 'required')) !!}
    </div>
    <div class="col-sm-1 nopadding">
      @if (isset($stay->room_id))
        @if ($stay->room->isPrivateRoom())
          {!! Form::selectRange('name', 1, $stay->room->roomtype->number_of_guests, $stay->number_of_guests, array('data-bookingid' => $stay->booking->id, 'data-stayid' => $stay->id, 'class' => 'form-control input-sm number_of_guests', 'disabled' => 'disabled')) !!}
          @else
        {!! Form::selectRange('name', 1, $stay->room->roomtype->number_of_guests, $stay->number_of_guests, array('data-bookingid' => $stay->booking->id, 'data-stayid' => $stay->id, 'class' => 'form-control input-sm number_of_guests')) !!}
        @endif
      @else
        {!! Form::input('number', 'number_of_guests['.$stay->id.']', $stay->number_of_guests, array('data-bookingid' => $stay->booking->id, 'data-stayid' => $stay->id, 'class' => 'form-control input-sm number_of_guests', 'min' => '1', 'step' => '1', 'max' => '99')) !!}
      @endif
    </div>
    <div class="col-sm-2 nopadding">
        @include('partials.form.selectroom')
    </div>
    <div class="col-sm-2 nopadding">
      {!! Form::text('price_per_night['.$stay->id.']', \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($stay->price_per_night), array('class' => 'form-control input-sm price_per_night')) !!}
    </div>
    <div class="col-sm-2 nopadding">
      {!! Form::text('subtotal', null, array('class' => 'form-control input-sm subtotal', 'readonly')) !!}
    </div>
  </div>
@endforeach

<div class="row">
  <div class="col-sm-1 nopadding">
    <i class="fa fa-plus addStay"></i>
  </div>

  <div class="col-sm-offset-7 col-sm-2 nopadding">
    {!! Form::Label('total', "Total:", array('id' => 'total_label')) !!}
  </div>

  <div class="col-sm-2 nopadding">
    {!! Form::text('total', null, array('class' => 'form-control', 'readonly')) !!}
  </div>

</div>
{!! Form::close() !!}