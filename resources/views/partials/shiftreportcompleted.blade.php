<div class="row">
  <div class="col-xs-4">
    <h4 class="text-center">Denomination</h4>
  </div>
  <div class="col-xs-5">
    <h4 class="text-center">Quantity</h4>
  </div>
  <div class="col-xs-3">
    <h4 class="text-center">Total</h4>
  </div>
</div>
@foreach ($items as $item)
  <div class="row">
    <div class="col-xs-5 text-center">
      {!! Form::Label('items[' . $item['amount_of_money'] . ']', $item['amount_of_money'], array('class' => 'denomination')) !!}
    </div>
    <div class="col-xs-3">
      {!! Form::input('number', 'items[' . $item['amount_of_money'] . ']', $item['quantity'], array('class' => 'form-control input-sm text-center m-b-xs', 'min' => '0', 'step' => '1', 'readonly' => 'readonly')) !!}
    </div>
    <div class="col-xs-1">
      <p>=</p>
    </div>
    <div class="col-xs-3">
      {!! Form::input('number', 'items[' . $item['amount_of_money'] . ']', ($item['amount_of_money'] * $item['quantity']), array('class' => 'form-control input-sm text-center m-b-xs', 'min' => '0', 'step' => '0.01', 'readonly' => 'readonly')) !!}
    </div>
  </div>
@endforeach
<div class="row">
  <div class="col-xs-offset-7 col-xs-5">
    <hr>
    {!! Form::input('string', 'grandtotal2', $total, array('class' => 'form-control input-md text-center m-b-xs', 'min' => '0', 'step' => '1', 'readonly' => 'readonly')) !!}
    <hr>
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <p>Saved:<strong> {{ $submitted }} </strong> by <strong> {{ $submittedBy }} </strong></p>
  </div>
</div>