<ul class="label-footer calendar-key">
  <li class="key-item booking-checked-in-paid"><span></span>Checked in, Paid</li>
  <li class="key-item booking-checked-in-unpaid"><span></span> Checked in, Unpaid</li>
  <li class="key-item booking-unpaid"><span></span> Arriving</li>
  <li class="key-item booking-multi-room"><span></span> Multi-Room Booking</li>
  <li class="key-item booking-checked-out"><span></span> Checked Out</li>
  <li class="key-item booking-blocked-room"><span></span> Blocked Room</li>
  <li class="key-item booking-currently-selected"><span></span> Currently Selected</li>
</ul>