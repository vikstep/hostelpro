{!! Form::open(array('route' => array('reports.shift.store'), 'method' => 'POST')) !!}

<div class="row">
  <div class="col-xs-4">
    <h4 class="text-center">Denomination</h4>
  </div>
  <div class="col-xs-5">
    <h4 class="text-center">Quantity</h4>
  </div>
  <div class="col-xs-3">
    <h4 class="text-center">Total</h4>
  </div>
</div>
@forelse ($money as $item)
  <div class="row">
    <div class="col-xs-5 text-center">
      {!! Form::Label('items[' . $item['amount'] . ']', $item['amount'], array('class' => 'denomination')) !!}
    </div>
    <div class="col-xs-3">
      {!! Form::input('number', 'items[' . $item['amount'] . ']', '', array('class' => 'form-control input-sm text-center m-b-xs amount', 'min' => '0', 'step' => '1', 'required' => 'required')) !!}
    </div>
    <div class="col-xs-1">
      <p>=</p>
    </div>
    <div class="col-xs-3">
      {!! Form::input('number', 'subtotal[' . $item['amount_of_money'] . ']', $item['quantity'], array('class' => 'form-control input-sm text-center m-b-xs subtotal', 'min' => '0', 'step' => '0.01', 'readonly' => 'readonly')) !!}
    </div>
  </div>
  @empty
    <div class="row">
      <div class="col-xs-12">
        Error: No denominations added! Click <a href="{{ route('settings.money.create') }}" class="text-primary font-bold">here</a> to add, or go to Hostel Settings --> Currency/Money.
      </div>
    </div>
@endforelse
<div class="row">
  <div class="col-xs-offset-7 col-xs-5">
    <hr>
    {!! Form::input('number', 'grandtotal', '', array('class' => 'form-control input-md text-center m-b-xs', 'id' => 'grandtotal', 'min' => '0', 'step' => '1', 'readonly' => 'readonly')) !!}
    <hr>
  </div>
</div>
<div class="row">
  <div class="col-xs-offset-7 col-xs-5">
    @if ($report)
      <input type="hidden" name="shiftreportid" value="{{ $report->id }}">
    @endif
    <button class="btn m-t-sm btn-block btn-success btn-addon" type="submit"><i class="fa fa-check"></i>Save</button>
 </div>
</div>
{!! Form::close() !!}