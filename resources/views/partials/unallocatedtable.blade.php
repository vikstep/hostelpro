@if ($unallocated_bookings_count > 0)
<h4>Bookings that need your attention</h4>
<table class="table table-striped m-b-none">
  <thead>
  <tr>
    <th style="width:50px;">Date</th>
    <th style="width:100px;">Name</th>
    <th style="width:50px"># Guests</th>
    <th style="width:50px"># Nights</th>
  </tr>
  </thead>
  <tbody>
  @forelse($bookings_that_need_attention as $booking)
    <tr>
      <td>{{ $booking->min_date }}</td>
      <td><span class="badge bg-danger">!</span>&nbsp;<a href="#" data-bookingid="{{ $booking->id }}" class="loadBooking loadBookingText">{{ $booking->lastname }}</a></td>
      <td>{{ $booking->number_of_guests }}</td>
      <td>{{ $booking->number_of_nights }}</td>
    </tr>
    @empty
    <tr>
      <td colspan="2">No unallocated bookings</td>
    </tr>
  @endforelse

  </tbody>
</table>
@endif

<h4>Possible No-Show Bookings</h4>
<table class="table table-striped m-b-none">
  <thead>
  <tr>
    <th style="width:50px;">Date</th>
    <th style="width:100px;">Name</th>
    <th style="width:50px"># Guests</th>
    <th style="width:50px"># Nights</th>
  </tr>
  </thead>
  <tbody>
  @forelse($possible_no_shows as $booking)
    <tr>
      <td>{{ $booking->date }}</td>
      <td><i class="fa fa-star text-danger text-active"></i>&nbsp;<a href="#" data-bookingid="{{ $booking->id }}" class="loadBooking loadBookingText">{{ $booking->lastname }}</a></td>
      <td>{{ $booking->number_of_guests }}</td>
      <td>{{ $booking->number_of_nights }}</td>
    </tr>
  @empty
    <tr>
      <td colspan="2">None</td>
    </tr>
  @endforelse

  </tbody>
</table>

<h4>Bookings that haven't checked out</h4>
<table class="table table-striped m-b-none">
  <thead>
  <tr>
    <th style="width:50px;">Date</th>
    <th style="width:100px;">Name</th>
    <th style="width:50px"># Guests</th>
    <th style="width:50px"># Nights</th>
  </tr>
  </thead>
  <tbody>
  @forelse($bookings_that_havent_checked_out as $booking)
    <tr>
      <td>{{ $booking->date }}</td>
      <td><i class="fa fa-star text-danger text-active"></i>&nbsp;<a href="#" data-bookingid="{{ $booking->id }}" class="loadBooking loadBookingText">{{ $booking->lastname }}</a></td>
      <td>{{ $booking->number_of_guests }}</td>
      <td>{{ $booking->number_of_nights }}</td>
    </tr>
  @empty
    <tr>
      <td colspan="2">None</td>
    </tr>
  @endforelse

  </tbody>
</table>