@if (count($booking->notes) > 0)

  @foreach ($booking->notes as $note)
    <div class="tl-content panel padder b-a w-md w-auto-xs">
      <span class="arrow left pull-up"></span>

      <div class="text-lt m-b-sm">{{ $note->user->firstname or 'Booking Note' }} {{ $note->user->lastname or '' }} ({{ $note->created_at }})</div>
      <div class="panel-body pull-in b-t b-light no-border">
        <div class="clear">
          <p class="text-primary">{!! $note->text !!}</p>
        </div>
      </div>
    </div>
    <br>
    <br>
  @endforeach
@else
  <p>No notes are attached to this booking.</p>
@endif
