
<table class="table table-striped table-condensed calendar-table" id="availability-table">
  <thead>

  <tr class="calendar-month">
    <td></td>
    <td class="calendar-beds"></td>
    @foreach($calendar->table_month_headings as $month)
      <th colspan="{{ $month['days'] }}" class="text-center month-border">{{ $month['month'] . " " . $month['year'] }}</th>
    @endforeach
  </tr>

  <tr class="calendar-date">
    <th></th>
    <th class="calendar-beds">Total</th>
    @foreach($calendar->table_month_headings as $i=>$month)
      @for ($x = 0; $x < $month['days']-1; $x++)
        <th class="text-center">{{ $calendar->table_headings[$i][$x][0] }}<br><span class="calendar-dayofweek">{{ $calendar->table_headings[$i][$x][1] }}</span></th>
      @endfor
      <th class="text-center month-border">{{ $calendar->table_headings[$i][$x][0] }}<br><span class="calendar-dayofweek">{{ $calendar->table_headings[$i][$x][1] }}</span></th>
    @endforeach
  </tr>

  </thead>

  @forelse($calendar->availability as $roomtype)
    <tbody>
      <tr>
        <th class="text-center room-name">{{ $roomtype['name'] }}</th>
        <td class="bednumber text-center">{{ $roomtype['max_per_day'] }}</td>
        @foreach($roomtype['availability'] as $date => $free)
          <td class="text-center availability-cell" data-date="{{ $date }}"><p data-toggle="tooltip" data-placement="top" title="{{ $date }}<br>{{ $roomtype['rates'][$date]['price'] or '0.00' }}<br>Min {{ $roomtype['rates'][$date]['min-stay'] or '0' }} nights">{{ $free }}</p></td>
        @endforeach
      </tr>
    <tr>
      <th class="text-center">Price</th>
      <td></td>
      @foreach($roomtype['availability'] as $date => $free)
        <td class="text-center">{{ $roomtype['rates'][$date]['price'] or '0.00' }}</td>
      @endforeach
    </tr>
    <tr>
      <th class="text-center">Min. Nights</th>
      <td></td>
      @foreach($roomtype['availability'] as $date => $free)
        <td class="text-center">{{ $roomtype['rates'][$date]['min-stay'] or '0' }}</td>
      @endforeach
    </tr>
    </tbody>
  @empty
    <tbody>
    <tr>
      <td colspan="31">No Room Types exist.</td>
    </tr>
    </tbody>
  @endforelse

</table>