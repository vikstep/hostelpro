
@extends('layouts.backend')

@section('meta-title', 'HostelPro - Competitor Settings')

@section('page-id', 'reports')

@section('content')

    @include('includes.settingssidebar')

    <div id="content-wrapper" class="m-t-md">

                            <div class="row">
                                <div class="col-md-8">

                                    @if (Auth::user()->currenthostel->competitorURLs()->count() < 5)
                                    {!! Form::open(['route' => 'settings.competitor.store', 'method' => 'POST']) !!}
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="url"
                                               placeholder="e.g http://www.booking.com/url-to-competitor-hostel-page"
                                               required>
                                        <span class="input-group-btn"><button class="btn btn-default" type="submit">Add
                                                URL
                                            </button></span>
                                    </div>
                                    {!! Form::close() !!}
                                    @else
                                        <p>You can only track up to <strong>5</strong> URLs, please delete one before adding more.</p>
                                    @endif

                                    <table class="table table-bordered table-striped table-responsive m-t-lg">
                                        <thead>
                                        <tr>
                                            <th width="80%">URL</th>
                                            <th width="20%">Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse (Auth::user()->currenthostel->competitorURLs as $url)
                                                <tr>
                                                    <td><a href="{{ $url->url }}" target="_blank" class="text-primary">{{ $url->url }}</a></td>
                                                    <td>
                                                        {!! Form::open(['route' => ['settings.competitor.destroy', $url->id], 'method' => 'DELETE']) !!}
                                                        <button class="btn-xs btn-danger" type="submit">Delete</button>
                                                        {!! Form::close() !!}
                                                    </td>
                                                </tr>
                                                @empty
                                                <tr>
                                                    <td colspan="2">No URLs added yet</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>

                                </div>
                                <div class="col-md-4">
                                    @if (Auth::user()->currenthostel->competitorURLs->count() == 0)
                                        <p><strong>You have not yet added any competitor URLs</strong></p>
                                    @endif
                                    <p>HostelPro can track competitor pricing from Booking.com and HostelWorld</p>

                                    <p>To make use of this functionality you will first need to tell HostelPro the
                                        URL to hostel you wish to track</p>

                                    <p>You can find the URL by going to Booking.com or HostelWorld and loading the hostel page, then copying and pasting the URL in the
                                        form.</p>

                                    <p>You only need to do this once, then HostelPro will automatically track pricing every single day.</p>

                                </div>
                            </div>

                        </div>
@endsection