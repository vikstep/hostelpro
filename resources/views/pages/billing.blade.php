@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings - Money')

@section('page-id', 'settings')

@section('content')

    @include('includes.settingssidebar')

    <div id="content-wrapper" class="m-t-md">

        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-lined mb30">
                    <div class="panel-heading font-bold">Plan Details</div>
                    <div class="panel-body">

                        <p>You currently have <strong>{{ Auth::user()->currenthostel->totalNumberOfBeds() }}</strong> beds</p>
                        <p>This puts you on the {{ $offer->min_beds }} to {{ $offer->max_beds }} bed plan for <strong>&euro; {{ $offer->price/100 }}</strong> per month.</p>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="panel panel-lined mb30">
                    <div class="panel-heading font-bold">Update Your Credit Card Details</div>
                    <div class="panel-body">

                        <div class="payment-errors"></div>

                        {!! Form::open(['route' => 'settings.billing.store', 'method' => 'POST', 'id' => 'payment-form']) !!}

                        <input type="hidden" name="amount" value="{{ $offer->price }}">
                        <input type="hidden" name="description" value="{{ Auth::user()->currenthostel->name }} / {{ Auth::user()->currenthostel->totalNumberOfBeds() }} beds">
                        <input type="hidden" name="email" value="{{ Auth::user()->email }}">

                        <div id="credit-card-fields">
                            <!-- Embedded credit card frame will load here -->
                        </div>

                        <button type="submit" class="btn btn-block btn-primary" id="payment-submit">Submit</button>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
@section('footer')
    @parent

    <script>

        $('#card').card({
            // a selector or DOM element for the container
            // where you want the card to appear
            container: '#card-wrapper', // *required*

            // all of the other options from above
        });
    </script>


    <script src="https://bridge.paymill.com/"></script>

    <script>

        var PAYMILL_PUBLIC_KEY = '48133911933aa6d012ec9957fe028e1c';

        // Callback for the PayFrame
        var payFrameCallback = function (error) {
            if (error) {
                // Frame could not be loaded, check error object for reason.
                console.log(error.apierror, error.message);
            } else {
                // Frame was loaded successfully and is ready to be used.
                console.log("PayFrame successfully loaded");
                $("#payment-form").show(300);
            }
        };

        $('#payment-submit').click(function () {
            paymill.createTokenViaFrame({
                amount_int: $('#payment-form input[name=amount]').val(),
                currency: 'EUR',
                description: $('#payment-form input[name=description]').val(),
                email: $('#payment-form input[name=email]').val()
            }, function(error, result) {
                // Handle error or process result.
                if (error) {
                    // Token could not be created, check error object for reason.
                    console.log(error.apierror, error.message);
                } else {
                    // Token was created successfully and can be sent to backend.
                    var form = $("#payment-form");
                    var token = result.token;
                    form.append("<input type='hidden' name='paymill_token' value='" + token + "'/>");
                    form.get(0).submit();
                }
            });

            return false;
        });

        $(document).ready(function () {
            paymill.embedFrame('credit-card-fields', {
                lang: 'en'
            }, payFrameCallback);
        });

    </script>
@endsection