@extends('layouts.backend')

@section('meta-title', 'HostelPro - Create Shift Report')

@section('page-id', 'reports')

@section('content')


    @include('includes.reportssidebar')

    <div id="content-wrapper" class="m-t-md">

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="panel panel-lined mb30">
                    <div class="panel-heading font-bold">Start of Shift Report</div>
                    <div class="panel-body">
                        @if ($report)
                            @include('partials.shiftreportcompleted', ['items' => $report->itemsStartOfShift, 'submitted' => $report->start_time, 'total' => $report->startTotal(), 'submittedBy' => $report->user->fullName()])
                        @else
                            @include('partials.shiftreporttable')
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="panel panel-lined mb30">
                    <div class="panel-heading font-bold">End of Shift Report</div>
                    <div class="panel-body">
                        @if ($report)
                            @if ($report->isCompleted())
                                @include('partials.shiftreportcompleted', ['items' => $report->itemsEndOfShift, 'submitted' => $report->end_time, 'total' => $report->endTotal(), 'submittedBy' => $report->user->fullName()])
                            @elseif ($report->user_id == Auth::user()->id)
                                @include('partials.shiftreporttable')
                            @else
                                <p>The person who submitted this report has not yet filled out an
                                    end-of-shift report.</p>
                            @endif
                        @else
                            <p>You must fill out a start-of-shift report before you can create an
                                end-of-shift report.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        @if ($report)
            <div class="row">


                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="panel panel-lined mb30">
                        <div class="panel-body">
                            <div class="row m-b-sm">
                                <div class="col-sm-12 p-l-lg">

                                    @can('advanced_reports')
                                    <strong>Charges</strong>
                                    @foreach($report->transactionTotalsWithoutDeposits() as $name => $total)
                                        <p>{{ $name }}:
                                            <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($total) }}</strong>
                                            @endforeach
                                            @endcan

                                            <br><br>
                                            <strong>Deposits</strong>
                                        @foreach($report->depositTotals() as $name => $total)
                                            <p>{{ $name }}:
                                                <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($total) }}</strong>
                                        @endforeach
                                        <p>
                                            <strong>Total: {{ $report->deposits()->sum('total') }}</strong>
                                        </p>


                                        <br><br>

                                        <strong>Expenses</strong>
                                        <p>Expenses from Til:
                                            <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($report->expenses->sum('price')) }}</strong>
                                        </p>

                                        <br>

                                        @can('advanced_reports')
                                        <strong>Revenue</strong>

                                        <p>End Count - Start Count:
                                            <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($report->totalDifference()) }}</strong>
                                        </p>

                                        <p>Shift Total:
                                            <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars((float) $report->totalDifference() + $report->expenses->sum('price')) }}</strong>
                                        </p>

                                        <br>
                                        @foreach($report->paymentTotals() as $name => $total)
                                            <p>{{ $name }}:
                                                <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($total) }}</strong>
                                        @endforeach
                                        <p>Total Payments:
                                            <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($report->payments->sum('total')) }}</strong>
                                        </p>

                                        <br>
                                        <p>Difference between Totals:
                                            <strong>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars(((float) $report->totalDifference() + $report->expenses->sum('price')) - ($report->payments->sum('total'))) }}</strong>
                                        </p>
                                        @endcan


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="panel panel-lined mb30">
                        <div class="panel-body">
                            <div class="row m-b-sm">
                                <div class="col-sm-4"><strong>Expense Category</strong></div>
                                <div class="col-sm-4"><strong>Expense Name</strong></div>
                                <div class="col-sm-4"><strong>Amount</strong></div>
                            </div>
                            @forelse ($report->expenses as $expense)
                                <div class="row m-b-sm">
                                    <div class="col-sm-4">{{ $expense->category }}</div>
                                    <div class="col-sm-4">{{ $expense->name }}</div>
                                    <div class="col-sm-4">{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($expense->price) }}</div>
                                </div>
                            @empty
                                <div class="row">
                                    <div class="col-sm-12">No expenses taken from cash register this
                                        shift.
                                    </div>
                                </div>
                            @endforelse
                            @if (($report->user_id == Auth::user()->id) && (!$report->isCompleted()))
                                <hr>
                                @include('partials.addexpense', ['hideDate' => true])
                            @endif

                            <hr>

                            @can('advanced_reports')
                            <strong>Transaction Summary</strong>

                            <p>
                                Created: {{ $report->logs()->where('action_log_type_id', '=', '3')->count() }}</p>

                            <p>Check
                                Ins: {{ $report->logs()->where('action_log_type_id', '=', '9')->count() }}</p>

                            <p>Check
                                Outs: {{ $report->logs()->where('action_log_type_id', '=', '12')->count() }}</p>

                            <p>
                                Modified: {{ $report->logs()->where('action_log_type_id', '=', '6')->count() }}</p>

                            <p>
                                Cancellations: {{ $report->logs()->where('action_log_type_id', '=', '15')->count() }}</p>

                            <p>
                                No-Shows: {{ $report->logs()->where('action_log_type_id', '=', '18')->count() }}</p>

                            @endcan


                        </div>
                    </div>
                </div>

            </div>

            @can('advanced_reports')
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-lined mb30">
                        <div class="panel-body">
                            <div class="panel-heading font-bold">Transactions</div>
                            <div class="row m-b-sm">
                                <div class="col-sm-2"><strong>Date Time</strong></div>
                                <div class="col-sm-3"><strong>Item</strong></div>
                                <div class="col-sm-1"><strong>Qty</strong></div>
                                <div class="col-sm-2"><strong>Price</strong></div>
                                <div class="col-sm-2"><strong>Total</strong></div>
                                <div class="col-sm-2"><strong>Payment</strong></div>
                            </div>
                            @forelse ($report->transactions() as $transaction)
                                @if (class_basename($transaction) == "ActionLog")
                                    <div class="row m-b-sm">
                                        <div class="col-sm-2">{{ $transaction->created_at }}</div>
                                        <div class="col-sm-3"><a class="loadBooking"
                                                                 data-bookingid="{{ $transaction->booking_id }}">{{ $transaction->type->name }}</a>
                                        </div>
                                    </div>
                                @elseif (class_basename($transaction) == "AdditionalItem")
                                    <div class="row m-b-sm">
                                        <div class="col-sm-2">{{ $transaction->created_at }}</div>
                                        <div class="col-sm-3"><a class="loadBooking"
                                                                 data-bookingid="{{ $transaction->booking_id }}">{{ $transaction->name }}</a>
                                        </div>
                                        <div class="col-sm-1">{{ $transaction->units }}</div>
                                        <div class="col-sm-2">{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($transaction->price) }}</div>
                                        <div class="col-sm-2">{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($transaction->total) }}</div>
                                    </div>
                                @else
                                    <div class="row m-b-sm">
                                        <div class="col-sm-2">{{ $transaction->created_at }}</div>
                                        <div class="col-sm-3"><a class="loadBooking"
                                                                 data-bookingid="{{ $transaction->booking_id }}">{{ $transaction->name }}</a>
                                        </div>
                                        <div class="col-sm-2 col-sm-offset-5">
                                            {{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars($transaction->total) }}
                                        </div>
                                    </div>
                                @endif
                            @empty
                                <div class="row">
                                    <div class="col-sm-12">No transactions during this shift.</div>
                                </div>
                            @endforelse

                        </div>
                    </div>
                </div>
            </div>
            @endcan

            <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="panel panel-lined mb30">
                    <div class="panel-body">

                        <div class="row m-b-sm">
                            <div class="col-sm-9"><strong>Booking Note</strong></div>
                            <div class="col-sm-3"><strong>Time</strong></div>
                        </div>
                        @forelse ($report->notes as $note)
                            <div class="row m-b-sm">
                                <div class="col-sm-9"><a class="loadBooking"
                                                         data-bookingid="{{ $note->booking_id }}">{{ $note->text }}</a>
                                </div>
                                <div class="col-sm-3">{{ $note->created_at }}</div>
                            </div>
                        @empty
                            <div class="row">
                                <div class="col-sm-12">No notes added to bookings this shift.</div>
                            </div>
                        @endforelse

                    </div>
                </div>
            </div>
            </div>
        @endif

    </div>

@endsection

@section('footer')
    @parent

    <script type="text/javascript">
        $(document).ready(function () {

            updateGrandTotal();

            $(document).on('change', '.amount', function () {
                var amount = $(this).val();
                var denomination = $(this).parent().parent().find('.denomination').html();
                $(this).parent().parent().find('.subtotal').val(amount * denomination);
                updateGrandTotal();
            });

            function updateGrandTotal() {
                var sum = 0;
                $('.subtotal').each(function () {
                    //alert($(this).text());
                    sum += Number($(this).val());
                });
                $("#grandtotal").val(sum);
            }

        });
    </script>
@endsection