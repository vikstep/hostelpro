@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings - Money')

@section('page-id', 'settings')

@section('content')

  @include('includes.settingssidebar')

  <div id="content-wrapper" class="m-t-md">

            <div class="row">

              <div class="col-lg-6">
                <div class="panel panel-lined mb30">
                  <div class="panel-heading font-bold">Money List</div>
                  <div class="panel-body">
                    <table class="table table-condensed money">
                      <thead>
                      <th width="50%">Amount</th>
                      <th width="50%">Remove</th>
                      </thead>
                      <tbody>
                      @if (count($money))
                        @include('partials.money')
                      @else
                        <tr id="nomoney">
                          <td>No money amounts have been added.</td>
                        </tr>
                      @endif
                      </tbody>
                    </table>
                    <p>
                      <button class="btn m-b-xs btn-sm btn-primary btn-addon" id="addmoney"><i class="fa fa-plus"></i>Add Amount</button>
                    </p>
                  </div>
                </div>
              </div>

            </div>

          </div>
@endsection
@section('footer')
  @parent

  <script type="text/javascript">
    $(document).ready(function () {

      var newMoneyBlock = '<tr>' +
              '<td class="v-middle"><input type="number" class="form-control amount" name="amount" placeholder="0.00" min="0" step="0.01"> </td>' +
              '<td>' +
              '<button class="btn btn-sm btn-info saveMoney"><i class="fa fa-floppy-o"></i>&nbsp; Save</button>' +
              '<button class="btn btn-sm btn-danger deleteRow"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>' +
              '</td>' +
              '</tr>';

      $("#addmoney").click(function () {
        $(".money tbody").append(newMoneyBlock);
        $("#nomoney").addClass("hidden");
      });

      $(document).on('click', '.saveMoney', function () {
        var data = {};
        data.amount = $(this).parent().parent().find('.amount').val();
        //var $data = [$name, $roomtype, $gender, $capacity];
        //console.log( JSON.stringify(data) );
        saveItem('{!! route('settings.money.store') !!}', data, $(this));
      });

      $(document).on('click', '.deleteRow', function () {
        $(this).closest('tr').remove();
      });

      $(document).on('click', '.deleteMoney', function () {
        confirmDeletePopup('{!! route('settings.money.destroy', '') !!}', $(this).data("moneyid"), 'DELETE');
      });

      function confirmDeletePopup($type, $id, $requesttype) {
        noty({
          text: 'Are you sure you want to delete this item?',
          theme: 'relax',
          layout: 'center',
          modal: true,
          animation: {
            open: {height: 'toggle'}, // jQuery animate function property object
            close: {height: 'toggle'}, // jQuery animate function property object
            easing: 'swing', // easing
            speed: 150 // opening & closing animation speed
          },
          buttons: [
            {
              addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
              $noty.close();
              deleteItem($type, $id, $requesttype);
            }
            },
            {
              addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
              $noty.close();
            }
            }
          ]
        });
      };

      function deleteItem($type, $id, $requesttype) {
        $.ajax({
          url: $type + "/" + $id,
          type: $requesttype,
          success: function (response) {
            $("table.money").html(response);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            createErrorModal(xhr.responseText);
          }
        });
      };

      function saveItem($type, $data, $row) {
        $.ajax({
          url: $type,
          type: 'POST',
          data: $data,
          success: function (response) {
            $row.parent().parent().replaceWith(response);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            createErrorModal(xhr.responseText);
          }
        });
      };


    });
  </script>


@endsection