@if (isset($booking))

{!! Form::open(['route' => 'savePayment', 'class' => 'form-horizontal', 'id' => 'posForm']) !!}
<p id="bookingName" class="hidden">{{ $booking->guest->lastname }}, {{ $booking->guest->firstname }}</p>
<input type="hidden" name="bookingid" value="{{ $booking->id }}">
<input id="changeDueHiddenInput" type="hidden" name="changedue" value="0">
@endif

<div class="row">
    <div class="col-sm-6">
        <h4>Categories</h4>

        <ul class="list-group">
            <li class="list-group-item loadPOSCategory" data-categoryid="accommodation">Accommodation</li>
            @forelse ($booking->hostel->posCategories as $category)
            <li class="list-group-item loadPOSCategory" data-categoryid="{!! $category->id !!}">{!! $category->name !!}</li>
            @empty
            <li class="list-group-item">No categories - please add some in hostel settings.</li>
            @endforelse
        </ul>
    </div>
    <div class="col-sm-6">
        <h4>Products</h4>
        <ul class="list-group" id="POSCategory_Container">

        </ul>
    </div>
</div>

<div class="row">
  <div class="col-sm-2">
    
  </div>
  <div class="col-sm-1 col-sm-offset-3">
    <h4>Qty</h4>
    </div>
  <div class="col-sm-2">
    <h4>Price</h4>
  </div>
  <div class="col-sm-2">
    <h4>Amount</h4>
  </div>
  <div class="col-sm-2">
    <h4>Payment</h4>
  </div>
</div>
<div id="newPaymentRow" class="row paymentRow newPaymentRow">
    <div class="col-sm-2 text-right nopadding">{{ Auth::user()->currenthostel->today_at_hostel }}</div>
    <div class="col-sm-3 nopadding">
        {!! Form::select('paymenttype', Auth::user()->currenthostel->paymentTypes->pluck('name', 'name')->all(), null, array('class' => 'form-control input-sm', 'required' => 'required')) !!}
    </div>
    <div class="col-sm-2 col-sm-offset-5 nopadding">
        <input id="paymentSubtotal" class="form-control input-sm price subtotal" name="payment" min="0.01" step="0.01" max="99999999999" type="number">
    </div>
</div>

<div class="row">
    <div class="col-xs-4">


        <h3>Remaining: <strong><span id="posRemaining">0.00</span></strong></h3>


    </div>
    <div class="col-xs-8">


        <div class="pull-right">
            <h3 class="inline">Due Now: <strong><span id="posTotal">0.00</span></strong></h3>
            &nbsp; &nbsp;
            <h3 class="inline">Change Due: <strong><span id="changeDue">0.00</span></strong></h3>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-sm-12">

        <div id="POSButtonRow" class="row">

            <div class="col-sm-2">

                <!-- Single button -->
                <div id="deleteButtonGroup" class="btn-group dropup">
                    <button type="button" class="btn btn-danger dropdown-toggle" id="deleteBooking" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-trash-o"></i>
                        Delete &nbsp; <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="deleteButtonsMenuContainer">
                        @if ($booking->isPossibleNoShow())
                        <li><a href="#" id="markNoShow" data-bookingid="{{ $booking->id }}">Mark as No Show</a></li>
                        @endif
                        <li><a href="#" class="deleteBooking" data-bookingid="{{ $booking->id }}" data-reason="Cancelled by Email">Cancelled by Email</a></li>
                        <li><a href="#" class="deleteBooking" data-bookingid="{{ $booking->id }}" data-reason="Cancelled by Email">Cancelled by Phone</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#" class="deleteBooking" data-bookingid="{{ $booking->id }}" data-reason="">Other Reason</a></li>
                    </ul>
                </div>


            </div>

            <div class="col-sm-2">
                <button type="button" class="btn" id="cancelAllocate">
                    <i class="fa fa-times"></i>
                    Close
                </button>
            </div>

            <div class="col-sm-2 col-sm-offset-3">
                <button type="button" class="btn btn-success" id="submitAllocate" form="saveAllocateForm">
                    <i class="fa fa-save"></i>
                    Save
                </button>
            </div>

            <div class="col-sm-3 text-center">
                @if (($booking->getLabelFormattedAttribute() != "Checked in - Paid") && ($booking->getLabelFormattedAttribute() != "Checked in - Unpaid"))
                <button type="button" class="btn btn-success updateLabelButton" data-label="checkin" data-bookingid="{{ $booking->id }}">
                    <i class="fa fa-sign-in"></i>
                    Check In
                </button>
                @else
                <button type="button" class="btn btn-primary updateLabelButton" data-label="checkout" data-bookingid="{{ $booking->id }}">
                    <i class="fa fa-sign-out"></i>
                    Check Out
                </button>
                @endif
            </div>

        </div>

    </div>
</div>

{!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function () {

        updateTotals();

        $('.loadPOSCategory').click(function () {
            if ($(this).data('categoryid') == "accommodation") {
                addAccommodationLine();
            } else {
                $('.loadPOSCategory').removeClass('active');
                $(this).addClass('active');
                $('#POSCategory_Container').load("{{ url('getProductsByCategoryId') }}/" + $(this).data('categoryid'));
            }
        });

        $('#POSCategory_Container').on('click', '.addProduct', function () {
            $("#noresults").remove();
            addProduct($(this).data('productid'), $(this).data('productname'), $(this).data('formattedprice'), $(this).data('fixedprice'), $(this).data('deposit'));
            updateTotals();
        });

        $('#transactionsDiv').on('input', '.qty', function () {
            updateTotals();
        }).on('input', '.price', function () {
            updateTotals();
        }).on('click', '.deletePOSRow', function () {
            $(this).closest('.row').remove();
            updateTotals();
        });

        $('#paymentContainer').on('input', '.price', function () {
            updateTotals();
        }).on('click', '.deletePOSRow', function () {
            $(this).closest('.row').remove();
            updateTotals();
        });

        function addProduct($productId, $productName, $formattedprice, $fixedprice, $deposit) {
            var found = false;
            $('.productRow').each(function () {
                if ($(this).data('productid') == $productId) {
                    found = true;
                    var units = parseInt($(this).find('.qty').val()) + 1;
                    var price = parseFloat($(this).find('td.price').html()).toFixed(2);
                    $(this).find('.qty').val(units);
                    $(this).find('.subtotal').val((units * price).toFixed(2));
                    updateTotals();
                }
            });
            if (found == false) {

                var date = new Date().toISOString().slice(0, 10);

                $('<div class="row productRow' + ($deposit ? ' depositRow' : '') + '" data-productid="' + $productId + '"><div class="col-sm-2 nopadding"><i class="fa fa-trash-o fa-fw text-muted deletePOSRow"></i>' + date + '</div><div class="col-sm-3 nopadding">' + $productName + '</div>'
                        + '<div class="col-sm-1 nopadding"><input class="form-control input-sm qty" min="1" step="1" max="99" name="items[' + $productId + ']qty" type="number" value="1"></div>'
                        + '<div class="col-sm-2 nopadding"><input class="form-control input-sm price" name="prices[' + $productId + ']" ' + ($fixedprice ? "readonly=\"readonly\"" : '') + ' type="text" value="' + $formattedprice + '"></div>'
                        + '<div class="col-sm-2 nopadding"><input class="form-control input-sm subtotal" readonly="readonly" name="subtotal" type="text" value="' + $formattedprice + '"></div></div>'
                        ).insertBefore('#newPaymentRow');
            }
        }

        function addAccommodationLine() {
            $('.newAccommodationRow').remove();
            var date = new Date().toISOString().slice(0, 10);

            var $productId = '0';
            var $formattedprice = calculateRemainingAccommodation();

            $('<div class="row productRow accommodationRow newAccommodationRow" data-productid="' + $productId + '"><div class="col-sm-2 nopadding"><i class="fa fa-trash-o fa-fw text-muted deletePOSRow"></i>' + date + '</div><div class="col-sm-3 nopadding">Accommodation</div>'
                    + '<div class="col-sm-1 nopadding"><input class="form-control input-sm qty" min="1" step="1" max="99" name="items[accommodation]qty" readonly="readonly" type="number" value="1"></div>'
                    + '<div class="col-sm-2 nopadding"><input class="form-control input-sm price" name="prices[accommodation]" type="text" value="' + $formattedprice + '"></div>'
                    + '<div class="col-sm-2 nopadding"><input class="form-control input-sm subtotal" readonly="readonly" name="subtotal" type="text" value="' + $formattedprice + '"></div></div>'
                    ).insertBefore('#newPaymentRow');
            updateTotals();
        }

        function calculateRemainingAccommodation() {
            var accommodationTotal = parseFloat($("#total").val());
            var paidAccommodation = parseFloat(calculatePaidAccommodation());
            var deposit = calculateBookingDeposit();

            return (accommodationTotal - paidAccommodation + deposit).toFixed(2);
        }

        function calculatePaidAccommodation() {
            var total = 0;
            $('.accommodationRow').each(function () {
                total += parseFloat($(this).find('input.price').val());
            });
            return total;
        }

    })
</script>