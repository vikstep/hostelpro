@extends('layouts.backend')

@section('meta-title', 'HostelPro - Shift Reports')

@section('page-id', 'reports')

@section('content')


    @include('includes.reportssidebar')

    <div id="content-wrapper" class="m-t-md">

        <table class="table table-condensed table-hover">
            <thead>
            <tr>
                <th>View</th>
                <th>Status</th>
                <th>Created By</th>
                <th>$ Diff</th>
                <th>Created At</th>
                <th>Completed At</th>
                <th>$ Start</th>
                <th>$ End</th>
            </tr>
            </thead>
            <tbody>
            @if ($reports)
                @foreach ($reports as $report)
                    <tr>
                        <td>
                            <a href="{{ route('reports.shift.show', ['id' => $report->id]) }}">
                                <button class="btn btn-default btn-xs">View Report</button>
                            </a>
                        </td>
                        @if ($report->isCompleted())
                            <td><span class="label bg-success">Completed</span></td>
                        @else
                            <td><span class="label bg-danger">Not Completed</span></td>
                        @endif
                        <td>{{ $report->user->fullName() }}</td>
                        @if ($report->isCompleted())
                            <td>{{ \App\HostelPro\Models\MoneyHelper::convertCentsToDollars(($report->totalDifference() + $report->expenses->sum('price')) - ($report->payments->sum('total'))) }}</td>
                        @else
                            <td></td>
                        @endif
                        <td>{{ $report->start_time }}</td>
                        @if ($report->isCompleted())
                            <td>{{ $report->end_time }}</td>
                        @else
                            <td></td>
                        @endif
                        <td>{{ $report->startTotal() }}</td>
                        <td>{{ $report->endTotal() }}</td>
                    </tr>
                @endforeach

            @else
                <tr>
                    <td colspan="5">There are currently no shift reports in the system.</td>
                </tr>
            @endif

            </tbody>
        </table>
    </div>
@endsection