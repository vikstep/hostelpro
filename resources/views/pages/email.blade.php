@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings - Money')

@section('page-id', 'settings')

@section('content')

    @include('includes.settingssidebar')

    <div id="content-wrapper" class="m-t-md">

        <div class="row">
            <div class="col-xs-12">

                <div id="emailTabs" class="tab-container">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="">SMTP Settings</a>
                        </li>
                        <li class="">
                            <a href="">Before Arrival Email</a>
                        </li>
                        <li class="">
                            <a href="">After Departure Email</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active">


                            {!! Form::open(array('route' => array('settings.email.store'), 'method' => 'POST', 'class' => 'form-horizontal')) !!}

                            <div class="form-group">
                                <label class="col-lg-2 control-label">SMTP Server</label>

                                <div class="col-lg-10">
                                    {!! Form::text('host', $email->host, ['class' => 'form-control', 'placeholder' => 'eg smtp.gmail.com', 'required' => 'required']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Port</label>

                                <div class="col-lg-10">
                                    {!! Form::text('port', $email->port, ['class' => 'form-control', 'placeholder' => 'eg 465', 'required' => 'required']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Encryption</label>

                                <div class="col-lg-10">
                                    {!! Form::select('encryption', ['' => 'none', 'ssl' => 'SSL', 'tls' => 'TLS'], $email->encryption, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Username</label>

                                <div class="col-lg-10">
                                    {!! Form::text('username', $email->username, ['class' => 'form-control', 'placeholder' => 'eg john.doe@gmail.com', 'required' => 'required']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Password</label>

                                <div class="col-lg-10">
                                    {!! Form::text('password', $email->password, ['class' => 'form-control', 'placeholder' => 'eg secret123', 'required' => 'required']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn m-b-xs btn-sm btn-primary" type="submit">Update Settings</button>
                                </div>
                            </div>

                            {!! Form::close() !!}


                            <div class="row">
                                <div class="col-lg-12 col-lg-offset-2">
                                    <a href="{{ route('settings.email.test', 'pre') }}" class="btn btn-default">Send
                                        Test Email (Before Arrival)</a>
                                    <a href="{{ route('settings.email.test', 'post') }}" class="btn btn-default">Send
                                        Test Email (After Departure)</a>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane">
                            {!! Form::open(array('route' => array('settings.email.store'), 'method' => 'POST', 'class' => 'form-horizontal')) !!}
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Send Email Before Arrival</label>

                                <div class="col-lg-10">
                                    {!! Form::select('pre_email_days', [
                                    '' => 'never',
                                    '1' => '1 day',
                                    '2' => '2 days',
                                    '3' => '3 days',
                                    '4' => '4 days',
                                    '7' => '1 week',
                                    '14' => '2 weeks'], $email->pre_email_days, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <textarea name="pre_email_text" class="summernote">{{ $email->pre_email_text }}</textarea>
                            <button class="btn btn-primary" type="submit">Save</button>
                            {!! Form::close() !!}
                        </div>
                        <div class="tab-pane">
                            {!! Form::open(array('route' => array('settings.email.store'), 'method' => 'POST', 'class' => 'form-horizontal')) !!}
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Send Email After Departure</label>

                                <div class="col-lg-10">
                                    {!! Form::select('post_email_days', [
                                    '' => 'never',
                                    '1' => '1 day',
                                    '2' => '2 days',
                                    '3' => '3 days',
                                    '4' => '4 days',
                                    '7' => '1 week',
                                    '14' => '2 weeks'], $email->post_email_days, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <textarea name="post_email_text" class="summernote">{{ $email->post_email_text }}</textarea>
                            <button class="btn btn-primary" type="submit">Save</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
@endsection
@section('footer')
    @parent

    <script type="text/javascript">
        $(document).ready(function () {

            $('#emailTabs').on('click', '.nav-tabs li', function (event) {
                event.preventDefault();
                $('.nav-tabs li').removeClass('active');
                $(this).addClass('active');
                $(this).closest('#emailTabs').find('.tab-pane').removeClass('active').eq($(this).index()).addClass('active');
            });

            var FirstNameButton = function (context) {
                var ui = $.summernote.ui;

                // create button
                var button = ui.button({
                    contents: '<i class="fa fa-child"/>GuestFirstName',
                    tooltip: 'Insert a placeholder for the first name of the guest',
                    click: function () {
                        // invoke insertText method with 'hello' on editor module.
                        context.invoke('editor.insertText', '[GuestFirstName]');
                    }
                });

                return button.render();   // return button as jquery object
            };

            var LastNameButton = function (context) {
                var ui = $.summernote.ui;

                // create button
                var button = ui.button({
                    contents: '<i class="fa fa-child"/>GuestLastName',
                    tooltip: 'Insert a placeholder for the last name of the guest',
                    click: function () {
                        // invoke insertText method with 'hello' on editor module.
                        context.invoke('editor.insertText', '[GuestLastName]');
                    }
                });

                return button.render();   // return button as jquery object
            };

            $('.summernote').summernote({
                minHeight: 300,

                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ol', 'ul', 'paragraph', 'height']],
                    ['insert', ['link']],
                    ['mybutton', ['firstname']],
                    ['mybutton2', ['lastname']]
                ],

                buttons: {
                    firstname: FirstNameButton,
                    lastname: LastNameButton
                }
            });

        });
    </script>


@endsection