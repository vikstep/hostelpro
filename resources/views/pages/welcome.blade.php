<html>
<head>
  <link href='https://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

  <style>
    body {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      color: #0C0C0C;
      display: table;
      font-weight: 100;
      font-family: 'Lato';
    }

    .container {
      text-align: center;
      display: table-cell;
      vertical-align: middle;
    }

    .content {
      text-align: center;
      display: inline-block;
    }

    .title {
      font-size: 72px;
      margin-bottom: 40px;
    }

    .subtitle {
      font-size: 36px;
      margin-bottom: 15px;
    }
  </style>
</head>
<body>
<div class="container">
  <div class="content">
    <div class="title">HostelPro</div>
    <div class="subtitle">We are currently in closed beta.</div>

    If you already have an account, <a href="login">log in here</a>.
  </div>
</div>
</body>
</html>
