@extends('layouts.backend')

@section('meta-title', 'HostelPro - Dashboard')

@section('page-id', 'dashboard')

@section('content')

    <div class="col-lg-12" id="dashboard-content">

        <div class="panel panel-default">

            <div id="calendar-control-row" class="panel-heading calendar-panel-heading">
                <div class="btn-group">
                    <label class="btn btn-info" id="setCalendarToToday">Today</label>
                </div>
                <div class="btn-group">

                    <div class="input-group calendar-panel-selectdate">

              <span class="input-group-btn" id="calendarMinusOneMonth">
                <button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i></button>
              </span>
                        <input class="form-control" type="text" id="datepicker-calendar" placeholder="Date">

              <span class="input-group-btn" id="calendarAddOneMonth">
                <button type="button" class="btn btn-default"><i class="fa fa-chevron-right"></i></button>
              </span>
                    </div>
                </div>

                <div class="btn-group calendar-selectViewMode">
                    <label class="btn btn-sm btn-primary active" data-viewtype="day">Day</label>
                    <label class="btn btn-sm btn-primary" data-viewtype="occupancy">Calendar</label>
                    <label class="btn btn-sm btn-primary" data-viewtype="availability">Availability</label>
                </div>

                <div class="pull-right">
                    <div class="btn-group calendar-selectdays">
                        <label class="btn btn-sm btn-primary" data-days="7">7 Days</label>
                        <label class="btn btn-sm btn-primary" data-days="14">14 Days</label>
                        <label class="btn btn-sm btn-primary active" data-days="31">31 Days</label>
                    </div>
                </div>
            </div>

            <div class="calendar-container">
                @include('partials.dayview')
                @if (1== 2)
                    <div class="alert alert-warning" role="alert">
                        <p>You need to add some room types and rooms before the calendar can be generated.</p>
                    </div>
                @endif
            </div>

        </div>

        <div id="dashboard-footer" data-spy="affix">
            @include('partials.labellist')
        </div>
        @endsection

        @section('footer')
            @parent

            @if (Session::has('show-intro'))
            <script>
                function startIntro() {
                    var intro = introJs();
                    intro.setOptions({
                        steps: [
                            {
                                intro: "<h3><center>Welcome to the Day View!</center></h3>This is your main screen, the <strong>Day View</strong>. It shows a list of all people departing, arriving or changing rooms. You can click on a name here to open a booking.",
                            },
                            {
                                element: '.calendar-selectViewMode',
                                intro: "You can click here to change your view.<br><br> We are currently on the <strong>Day View</strong>." +
                                "<br><br><strong>Calendar View</strong> will show all bookings in a calendar format. You can left click on a booking to open it or right click to access advanced functions. " +
                                "You can also drag and drop bookings.<br><br>  <strong>Availability View</strong> is useful for seeing your availability at a glance, ie how many free beds in each room you have and what the price is set to for each day.",
                            },
                            {
                                element: '#sidebar-search',
                                intro: "You can also open a booking by simply typing in the name here.<br><br> Bookings are sorted by arrival date so you will see the most likely matches first."
                            },
                            {
                                element: '#bookingDetails',
                                intro: "<h3><center>POS</center></h3>This is your POS. When you open a booking it'll pop out from here.<br><br> The POS box allows you to see all aspects of the booking and allows you to make changes, check in guests and add payments."
                            },
                            {
                                element: '#notificationsContainer',
                                intro: "<h3><center>Notifications</center></h3>The notification area will list any bookings that might be no-shows or bookings that haven't checked out today."
                            },
                            {
                              element: '#alertsContainer',
                                intro: "<h3><center>Alerts</center></h3>The alerts area will contain any HostelProfessional-related alerts. For example, billing problems or missing information."
                            },
                            {
                                element: "#settingsContainer",
                                intro: "<h3><center>Settings</center></h3>Here you can change your hostel settings, add users, setup your rooms and setup your POS.<br><br> You can also later add multiple hostels and switch between them without having to log out."
                            }

                        ],
                        exitOnOverlayClick: false,
                        showProgress: true
                    });

                    intro.start();
                }


                startIntro();
            </script>
            @endif

            <script type="text/javascript">
                function reloadCalendar() {
                    $bookingid = $("#bookingDetails").data('currentbookingid');
                    console.log("bookingid: " + $bookingid);
                    NProgress.configure({
                        parent: '.calendar-container',
                        showSpinner: false,
                        trickleRate: 0.9,
                        trickleSpeed: 250
                    }).start();
                    var $days = $(".calendar-selectdays").children(".active").attr('data-days');
                    var $viewmode = $(".calendar-selectViewMode").children(".active").attr('data-viewtype');
                    $('.calendar-container').load("ajax/calendar", {
                                viewmode: $viewmode,
                                days: $days,
                                bookingid: $bookingid,
                                date: $('#datepicker-calendar').val()
                            },
                            function () {
                                //alert( "Load was performed." );
                                if ($viewmode == "availability") {
                                    colorAllAvailabilityCells();
                                    $('[data-toggle="tooltip"]').tooltip({html: true})
                                } else if ($viewmode == "occupancy") {
                                    loadDragAndDrop();
                                }
                                $('.calendar-table').stickyTableHeaders();
                                NProgress.done();
                            });
                }

                function bookingAction($bookingId, $action) {

                    NProgress.configure({
                        parent: '.calendar-container',
                        showSpinner: false,
                        trickleRate: 0.9,
                        trickleSpeed: 250
                    }).start();

                    var calendar = JSON.stringify(getCalendarVars());

                    $.ajax({
                        type: 'POST',
                        url: "booking/" + $bookingId + "/" + $action,
                        data: {calendar: calendar},
                        success: function (data) {
                            processResponse(data);
                        }
                    });
                }

                function blockFromContext($date, $roomId, $bedNumber) {
                    $.post("booking/createNewBookingAndBlock", {bednumber: $bedNumber, roomid: $roomId, date: $date})
                            .done(function (data) {
                                loadBooking(data);
                            });
                }

                function moveStay($bookingid, $date, $stayId, $roomId, $bedNumber) {
                    var $openbookingid = $("#bookingDetails").data('currentbookingid');

                    var calendar = JSON.stringify(getCalendarVars());

                    $.ajaxq('savebooking', {
                        method: "POST",
                        url: "booking/" + $bookingid + "/movestay",
                        data: {
                            date: $date,
                            stayid: $stayId,
                            bednumber: $bedNumber,
                            roomid: $roomId,
                            openbookingid: $openbookingid,
                            calendar: calendar
                        }
                    }).done(function (data) {
                        processResponse(data);
                    });
                }

                function closeContextMenu() {
                    $('.context-menu').remove();
                }

                function hsl_col_perc(percent, start, end) {

                    var a = percent / 100,
                            b = end * a;
                    c = b + start;

                    //Return a CSS HSL string
                    return 'hsla(' + c + ',100%,50%,0.6)';
                }

                function colorAllAvailabilityCells() {
                    $('.availability-cell').each(function () {
                        $beds_free = $(this).children().html();
                        if ($beds_free < 0) {
                            $beds_free = 0;
                        }
                        $total_beds = $(this).closest('tr').find('.bednumber').html();
                        $percent = ($beds_free / $total_beds) * 100;
                        if ($percent > 60) {
                            return;
                        }
                        $(this).css('background-color', hsl_col_perc($percent, 0, 100));
                    });
                }


            </script>

            <script type="text/javascript">
                $(document).ready(function () {

                    if ($(window).width() < 1400) {
                        $('.calendar-selectdays').find('*').removeClass('active');
                        $('.calendar-selectdays').find('label[data-days=14]').addClass('active');
                    }

                    $('.calendar-table').stickyTableHeaders();

                    $('#datepicker-calendar').datepicker({
                        inline: true,
                        showOtherMonths: true,
                        dateFormat: "dd-mm-yy",
                        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                    });
                    //Generate a new date
                    var date = new Date();
                    //Set to the day before today
                    //date.setDate(date.getDate() - 1);
                    //Set the datepicker to the date we just generated
                    $('#datepicker-calendar').datepicker("setDate", date);

                    /* Begin Calendar Functions */
                    $('.calendar-selectdays').click(function (event) {
                        $(this).find('*').removeClass('active');
                        $(event.target).addClass('active');
                        reloadCalendar();
                    });

                    $('.calendar-selectViewMode').click(function (event) {
                        $(this).find('*').removeClass('active');
                        $(event.target).addClass('active');
                        reloadCalendar();
                    });

                    $('#datepicker-calendar').change(function () {
                        reloadCalendar();
                    });
                    /* End Calendar Functions */

                    $('accordion .panel-heading').click(function (event) {
                        event.preventDefault();
                        if ($(this).next().hasClass('collapse')) {
                            $(this).next().removeClass('collapse');
                            $(this).find("i:first-of-type").removeClass('fa-angle-right').addClass('fa-angle-down');
                        } else {
                            $(this).next().addClass('collapse');
                            $(this).find("i:first-of-type").removeClass('fa-angle-down').addClass('fa-angle-right');
                        }
                    });

                    /* Begin Context Menu */

                    $('body').click(function (event) {
                        var bookingid = $(event.target).data('bookingid');
                        if ($(event.target).hasClass('unallocate-booking')) {
                            bookingAction(bookingid, "unallocate");
                            if ($("#bookingDetails").data('currentbookingid').length) {
                                closeBooking();
                            }
                        }
                        if ($(event.target).hasClass('block-booking')) {
                            bookingAction(bookingid, "block");
                        }
                        if ($(event.target).hasClass('unblock-booking')) {
                            bookingAction(bookingid, "unblock");
                        }
                        if ($(event.target).hasClass('lock-booking')) {
                            bookingAction(bookingid, "lock");
                        }
                        if ($(event.target).hasClass('unlock-booking')) {
                            bookingAction(bookingid, "unlock");
                        }
                        if ($(event.target).hasClass('noshow-booking')) {
                            bookingAction(bookingid, "noshow");
                        }
                        if ($(event.target).hasClass('loadBookingFromContext')) {
                            loadBooking($(event.target).data('bookingid'));
                        }
                        if ($(event.target).hasClass('addStayFromContext')) {
                            $date = $(event.target).data('date');
                            $roomId = $(event.target).data('roomid');
                            $bedNumber = $(event.target).data('bednumber');

                            addStay($date, $roomId, $bedNumber);
                        }
                        if ($(event.target).hasClass('blockFromContext')) {
                            $date = $(event.target).data('date');
                            $roomId = $(event.target).data('roomid');
                            $bedNumber = $(event.target).data('bednumber');

                            blockFromContext($date, $roomId, $bedNumber);
                        }

                        closeContextMenu();
                    });

                    $('.calendar-container').on('contextmenu', '.booking-cell', function (event) {
                        event.preventDefault();
                        closeContextMenu();
                        var lastname = $(this).data('lastname');
                        var firstname = $(this).data('firstname');
                        var bookingid = $(this).data('bookingid');

                        var rightClickMenu = '<div class="context-menu"><div class="btn-group-vertical m-b-sm">' +
                                '<button type="button" class="btn btn-primary btn-addon no-hover">' + lastname + ' , ' + firstname + '</button>' +
                                '<button type="button" class="btn btn-primary btn-addon loadBookingFromContext" data-bookingid="' + bookingid + '"><i class="fa fa-eye"></i>View Details</button>';

                        if ($(this).data('possiblenoshow') && ($(this).hasClass('calendar-booking-unpaid'))) {
                            rightClickMenu += '<button type="button" class="btn btn-primary btn-addon noshow-booking" data-bookingid="' + bookingid + '"><i class="fa fa-minus-circle"></i>Mark as No Show</button>';
                        }

                        if ($(this).hasClass('calendar-booking-blocked-room')) {
                            rightClickMenu += '<button type="button" class="btn btn-primary btn-addon unblock-booking" data-bookingid="' + bookingid + '"><i class="fa fa-square-o"></i>UnBlock</button>';
                        } else {
                            rightClickMenu += '<button type="button" class="btn btn-primary btn-addon block-booking" data-bookingid="' + bookingid + '"><i class="fa fa-square-o"></i>Block</button>';
                        }

                        if ($(this).data('locked')) {
                            rightClickMenu += '<button type="button" class="btn btn-primary btn-addon unlock-booking" data-bookingid="' + bookingid + '"><i class="fa fa-unlock"></i>Unlock</button>';
                        } else {
                            rightClickMenu += '<button type="button" class="btn btn-primary btn-addon lock-booking" data-bookingid="' + bookingid + '"><i class="fa fa-lock"></i>Lock</button>';
                        }

                        rightClickMenu += '<button type="button" class="btn btn-primary btn-addon unallocate-booking" data-bookingid="' + bookingid + '"><i class="fa fa-chain-broken"></i>Unallocate</button>'
                                + '</div></div>';

                        $(rightClickMenu)
                                .appendTo("body")
                                .css({position: "absolute", top: event.pageY + "px", left: event.pageX + "px"});
                    }).on('contextmenu', '.addStay', function (event) {
                        event.preventDefault();
                        closeContextMenu();

                        $bedNumber = $(this).closest('tr').find('.bednumber').html();
                        $roomId = $(this).closest('tbody').find('th').data('roomid');
                        $date = $(this).data('date');

                        var rightClickMenu = '<div class="context-menu"><div class="btn-group-vertical m-b-sm">'
                                + '<button type="button" class="btn btn-primary btn-addon addStayFromContext" data-bednumber="' + $bedNumber + '" data-roomid="' + $roomId + '" data-date="' + $date + '"><i class="fa fa-plus"></i>Create Booking</button>'
                                + '<button type="button" class="btn btn-primary btn-addon blockFromContext" data-bednumber="' + $bedNumber + '" data-roomid="' + $roomId + '" data-date="' + $date + '"><i class="fa fa-square-o"></i>Block</button>'
                                + '</div></div>';

                        $(rightClickMenu)
                                .appendTo("body")
                                .css({position: "absolute", top: event.pageY + "px", left: event.pageX + "px"});
                    }).on('mouseenter', '.addStay', function () {
                        $(this).addClass('hover');
                    }).on('mouseleave', '.addStay', function () {
                        $(this).removeClass('hover');
                    }).on('click', '.addStay', function () {

                        $date = $(this).data('date');
                        $roomId = $(this).closest('tbody').find('th').data('roomid');
                        $bedNumber = $(this).closest('tr').find('.bednumber').html();

                        addStay($date, $roomId, $bedNumber);

                    });

                    $("#setCalendarToToday").click(function () {
                        var date = new Date();
                        //date.setDate(date.getDate() - 1);
                        $('#datepicker-calendar').datepicker("setDate", date);
                        reloadCalendar();
                    });

                    $("#calendarAddOneMonth").click(function () {
                        var viewmode = $(".calendar-selectViewMode").children(".active").attr('data-viewtype');

                        var originaldate = $("#datepicker-calendar").val().split("-");
                        //remember that javascript date function uses '0' for january and '11' for december, so minus 1 from the month to account for this
                        var date = new Date(originaldate[2], originaldate[1] - 1, originaldate[0]);

                        if (viewmode == "day") {
                            date.setDate(date.getDate() + 1);
                        } else {
                            date.setMonth(date.getMonth() + 1);
                        }

                        $('#datepicker-calendar').datepicker("setDate", date);
                        reloadCalendar();
                    });

                    $("#calendarMinusOneMonth").click(function () {
                        var viewmode = $(".calendar-selectViewMode").children(".active").attr('data-viewtype');

                        var originaldate = $("#datepicker-calendar").val().split("-");
                        //remember that javascript date function uses '0' for january and '11' for december, so minus 1 from the month to account for this
                        var date = new Date(originaldate[2], originaldate[1] - 1, originaldate[0]);

                        if (viewmode == "day") {
                            date.setDate(date.getDate() - 1);
                        } else {
                            date.setMonth(date.getMonth() - 1);
                        }

                        $('#datepicker-calendar').datepicker("setDate", date);
                        reloadCalendar();
                    });


                    $('.calendar-container').on('click', '.loadExistingBooking', function (event) {
                        event.preventDefault();
                        $bookingid = $(this).attr('data-bookingid');
                        loadBooking($bookingid);
                    });

                });
            </script>
@endsection