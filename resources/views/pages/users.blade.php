@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings - Money')

@section('page-id', 'settings')

@section('content')

  @include('includes.settingssidebar')

  <div id="content-wrapper" class="m-t-md">


            <div class="row">

              <div class="col-lg-12">
                <div class="panel panel-lined mb30">
                  <div class="panel-heading font-bold">User List</div>
                  <div class="panel-body">
                    <table id="userTable" class="table table-condensed">
                      <thead>
                      <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Access Level</th>
                        <th>Edit</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($users as $user)
                        <tr>
                          <td>{{ $user->firstname }}</td>
                          <td>{{ $user->lastname }}</td>
                          <td>{{ $user->email }}</td>
                          <td>{{ $user->roles()->first()->name }}</td>
                          <td><a href="{{ route('users.show', $user->id) }}">
                              <button class="btn btn-default btn-xs">Edit</button>
                            </a>
                          </td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                    <p>
                      <button id="addUserButton" class="btn m-b-xs btn-sm btn-primary btn-addon"><i class="fa fa-plus"></i>Add User</button>
                    </p>
                  </div>
                </div>
              </div>

            </div>

          </div>
@endsection

@section('footer')
  @parent

  <script type="text/javascript">
    $(document).ready(function () {

      var newUserBlock = '<tr>' +
              '<td class="v-middle"><input type="text" class="form-control" name="firstname" placeholder="First Name" required> </td>' +
              '<td class="v-middle"><input type="text" class="form-control" name="lastname" placeholder="Last Name" required> </td>' +
              '<td class="v-middle"><input type="text" class="form-control" name="email" placeholder="Email Address" required> </td>' +
              '<td class="v-middle">{!! Form::select("role_id", $roles, "", array("class" => "form-control")) !!}</td>' +
              '<td class="v-middle">' +
              '<button class="btn btn-sm btn-info saveUser"><i class="fa fa-floppy-o"></i>&nbsp; Save</button>' +
              '<button class="btn btn-sm btn-danger deleteRow"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>' +
              '</td>' +
              '</tr>';

      $("#addUserButton").click(function () {
        $("#userTable tbody").append(newUserBlock);
      });

      $(document).on('click', '.saveUser', function () {
        var data = {};
        data.firstname = $(this).parent().parent().find("input[name=firstname]").val();
        data.lastname = $(this).parent().parent().find("input[name=lastname]").val();
        data.email = $(this).parent().parent().find("input[name=email]").val();
        data.role_id = $(this).parent().parent().find("select[name=role_id]").val();
        saveItem('users', data, $(this));
      });

      $(document).on('click', '.deleteRow', function () {
        $(this).closest('tr').remove();
      });

      function saveItem($type, $data, $row) {
        $.ajax({
          url: $type,
          //context: document.body,
          type: 'POST',
          data: $data,
          success: function (response) {
            location.reload();
          },
          error: function (xhr, ajaxOptions, thrownError) {
            createErrorModal(xhr.responseText);
          }
        });
      }
    });
  </script>
@endsection