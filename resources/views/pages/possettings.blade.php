@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings - POS')

@section('page-id', 'settings')

@section('content')

    @include('includes.settingssidebar')

    <div id="content-wrapper" class="m-t-md">

        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-lined mb30">
                    <div class="panel-heading"><i>Point of Sale Category List</i></div>
                    <div class="panel-body">
                        @include('partials.pos.settingscategories')
                        <p>
                            <button class="btn m-b-xs btn-sm btn-primary btn-addon" id="addcategory"><i
                                        class="fa fa-plus"></i>Add Category
                            </button>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="panel panel-lined mb30">
                    <div class="panel-heading"><i>Point of Sale Product List</i></div>
                    <div class="panel-body">
                        @include('partials.pos.settingsproducts')
                        <p>
                            <button class="btn m-b-xs btn-sm btn-primary btn-addon" id="addproduct"><i
                                        class="fa fa-plus"></i>Add Product
                            </button>
                        </p>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
@section('footer')
    @parent

    <script type="text/javascript">
        $(document).ready(function () {

            var newCategoryBlock = '<tr>' +
                    '<td class="v-middle"><input type="text" class="form-control name" name="name" placeholder="Name"> </td>' +
                    '<td class="v-middle">' +
                    '<button class="btn btn-sm btn-info saveCategory"><i class="fa fa-floppy-o"></i>&nbsp; Save</button>' +
                    '<button class="btn btn-sm btn-danger deleteRow"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>' +
                    '</td>' +
                    '</tr>';

            categoryList = '{!! Form::select("category_id", Auth::user()->currenthostel->posCategories->pluck("name", "id"), "", array("class" => "form-control category_id")) !!}';

            $("#addcategory").click(function () {
                $(".POSCategory tbody").append(newCategoryBlock);
                $(".noresults").addClass("hidden");
                $(".POSCategory thead").removeClass("hidden");
            });

            $("#addproduct").click(function () {
                $(".POSProduct tbody").append('<tr>' +
                        '<td class="v-middle"><input type="text" class="form-control name" name="name" placeholder="Name"> </td>' +
                        '<td class="v-middle">' + categoryList + '</td>' +
                        '<td class="v-middle"><input type="text" class="form-control price" name="price" placeholder="$0.00"> </td>' +
                        '<td class="v-middle">{!! Form::select("is_fixed_price", [true => 'Yes', false => 'No'], 1, array("class" => "form-control is_fixed_price")) !!}</td>' +
                        '<td class="v-middle">{!! Form::select("deposit", [true => 'Yes', false => 'No'], 0, array("class" => "form-control deposit")) !!}</td>' +
                        '<td class="v-middle">' +
                        '<button class="btn btn-sm btn-info saveProduct"><i class="fa fa-floppy-o"></i>&nbsp; Save</button>' +
                        '<button class="btn btn-sm btn-danger deleteRow"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>' +
                        '</td>' +
                        '</tr>');
                $(".noresults").addClass("hidden");
                $(".POSProduct thead").removeClass("hidden");
            });

            $(document).on('click', '.saveCategory', function () {
                var data = {};
                data.name = $(this).parent().parent().find('.name').val();
                saveItem('POSCategory', data, $(this));
            });

            $(document).on('click', '.saveProduct', function () {
                var data = {};
                data.name = $(this).parent().parent().find('.name').val();
                data.category_id = $(this).parent().parent().find('.category_id').val();
                data.price = $(this).parent().parent().find('.price').val();
                data.is_fixed_price = $(this).parent().parent().find('.is_fixed_price').val();
                data.deposit = $(this).parent().parent().find('.deposit').val();
                saveItem('POSProduct', data, $(this));
            });

            $(document).on('click', '.deleteRow', function () {
                $(this).closest('tr').remove();
            });

            $(document).on('click', '.deleteProduct', function () {
                confirmDeletePopup('POSProduct', $(this).attr("data-productid"), 'DELETE');
            });

            $(document).on('click', '.deleteCategory', function () {
                confirmDeletePopup('POSCategory', $(this).attr("data-categoryid"), 'DELETE');
            });

            $(document).on('click', '.editCategory', function () {
                var categoryid = $(this).data("categoryid");
                $(this).closest('tr').load('POSCategory/' + categoryid + '/edit');
            });

            $(document).on('click', '.editProduct', function () {
                var productid = $(this).data("productid");
                $(this).closest('tr').load('POSProduct/' + productid + '/edit');
            });

            $(document).on('click', '.saveEditCategory', function () {
                var data = {};
                data.name = $(this).parent().parent().find('.name').val();
                data.id = $(this).data("categoryid");
                $.ajax({
                    url: "POSCategory/" + data.id,
                    type: "PUT",
                    data: data,
                    context: this,
                    success: function (response) {
                        $(this).closest('tr').html(response);
                        updateCategoryList();
                    }
                });
            });

            $(document).on('click', '.saveEditProduct', function () {
                var data = {};
                data.name = $(this).parent().parent().find('.name').val();
                data.id = $(this).data("productid");
                data.price = $(this).parent().parent().find('.price').val();
                data.category_id = $(this).parent().parent().find('.category_id').val();
                data.is_fixed_price = $(this).parent().parent().find('.is_fixed_price').val();
                data.deposit = $(this).parent().parent().find('.deposit').val();
                $.ajax({
                    url: "POSProduct/" + data.id,
                    type: "PUT",
                    data: data,
                    context: this,
                    success: function (response) {
                        updateProductTable();
                    }
                });
            });

            function confirmDeletePopup($type, $id, $requesttype) {
                noty({
                    text: 'Are you sure you want to delete this item?',
                    theme: 'relax',
                    layout: 'center',
                    modal: true,
                    animation: {
                        open: {height: 'toggle'}, // jQuery animate function property object
                        close: {height: 'toggle'}, // jQuery animate function property object
                        easing: 'swing', // easing
                        speed: 150 // opening & closing animation speed
                    },
                    buttons: [
                        {
                            addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            deleteItem($type, $id, $requesttype);
                        }
                        },
                        {
                            addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                            $noty.close();
                        }
                        }
                    ]
                });
            }

            function deleteItem($type, $id, $requesttype) {
                $.ajax({
                    url: $type + "/" + $id,
                    type: $requesttype,
                    data: {_token: '{!!  csrf_token() !!}'},
                    success: function (response) {
                        $("table." + $type).html(response);
                        if ($type == 'POSCategory') {
                            updateCategoryList();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        createErrorModal(xhr.responseText);
                    }
                });
            }

            function saveItem($type, $data, $row) {
                $.ajax({
                    url: $type,
                    //context: document.body,
                    type: 'POST',
                    data: $data,
                    success: function (response) {
                        $row.parent().parent().replaceWith(response);
                        if ($type == 'POSCategory') {
                            updateCategoryList();
                        } else {
                            updateProductTable();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        createErrorModal(xhr.responseText);
                    }
                });
            }

            function updateCategoryList() {

                $.get('{{ route('settings.POSCategory.index') }}', function (data) {
                    categoryList = data;
                    updateProductTable();
                });
            }

            function updateProductTable() {
                $.get('{{ route('settings.POSProduct.index') }}', function (data) {
                    $('#POSProductTable').replaceWith(data);
                    makeProductTableSortable();
                });
            }

            $('#categoryTable').sortable({
                axis: 'y',
                update: function (event, ui) {
                    var data = $(this).sortable('serialize');
                    data += '&_token={{ csrf_token() }}';

                    console.log(data);
                    $.ajax({
                        url: '{{ route('settings.POSCategory.sort') }}',
                        type: 'POST',
                        data: data,
                        success: function (response) {
                            updateProductTable();
                        }
                    });

                }
            });

            makeProductTableSortable();
            function makeProductTableSortable() {
                $('#productTable').sortable({
                    axis: 'y',
                    update: function (event, ui) {
                        var data = $(this).sortable('serialize');
                        data += '&_token={{ csrf_token() }}';

                        console.log(data);
                        $.ajax({
                            url: '{{ route('settings.POSProduct.sort') }}',
                            type: 'POST',
                            data: data,
                            success: function (response) {
                                updateProductTable();
                            }
                        });

                    }
                });
            }

        });
    </script>
@endsection