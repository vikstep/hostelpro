@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings')

@section('content')

  @include('includes.standardheader')



  <div class="main-container clearfix" id="settings">
    @include('includes.settingssidebar')


    @include('includes.footer')
  </div>
@stop