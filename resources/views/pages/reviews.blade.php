@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings - Reviews')

@section('page-id', 'settings')

@section('content')

    @include('includes.settingssidebar')

    <div id="content-wrapper" class="m-t-md">

        <div class="row">

            <div class="col-lg-8">

                {!! Form::open(['route' => 'reports.reviews.store', 'method' => 'POST']) !!}
                <div class="input-group">
                    <input type="text" class="form-control" name="url"
                           placeholder="e.g http://www.booking.com/url-to-your-hostel-page"
                           required>
                                        <span class="input-group-btn"><button class="btn btn-default" type="submit">Add
                                                URL
                                            </button></span>
                </div>
                {!! Form::close() !!}

                <div class="m-b m-t-sm m-l-sm">
                    <span class="text-muted">Tracking reviews from: &nbsp;</span>
                    <span class="label text-base {{ Auth::user()->currenthostel->reviewURLFor('boo') ? 'bg-success' : 'bg-danger' }} pos-rlt m-r">Booking.com</span>
                    <span class="label text-base {{ Auth::user()->currenthostel->reviewURLFor('hw2') ? 'bg-success' : 'bg-danger' }} pos-rlt m-r">HostelWorld</span>
                    <span class="label text-base {{ Auth::user()->currenthostel->reviewURLFor('hb2') ? 'bg-success' : 'bg-danger' }} pos-rlt m-r">HostelBookers</span>
                </div>

            </div>

        </div>

    </div>
@endsection