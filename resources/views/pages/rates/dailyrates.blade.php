@extends('layouts.backend')

@section('meta-title', 'HostelPro - Daily Rates')

@section('page-id', 'rates')

@section('content')


    @include('includes.ratessidebar')

    <div class="row">
        <div class="main-content col-lg-10 col-md-9">

            <div class="daily-date">
                <button class="btn btn-success btn-today">Today</button>
                <input class="date" type="text" value="{{ array_keys($datesFormatted)[0] }}"/>
            </div>

            <div class="daily-pricing">
                <h2>Daily Pricing</h2>
                <table class="pricing-table table-responsive">
                    <thead>
                    <th></th>
                    @php $day = 0; @endphp
                    @foreach ($datesFormatted as $key => $data)
                        <th>
                            <div class="day-week day{{$day}}">{{$data['dayOfWeek']}}</div>
                            <div class="date-week date{{$day}}">{{$data['day']}}</div>
                        </th>@php ++$day; @endphp
                    @endforeach
                    </thead>
                    <tbody>
                    @foreach ($occupancies as $roomname => $data)
                        <tr>
                            <td class="first-cell">
                                <div class="top-half">{{$roomname}}</div>
                                <div class="bottom-half">Available</div>
                            </td>
                            @php $day = 0; @endphp
                            @foreach ($data as $date => $value)
                                <td class="{{$value['color']}}-cell" id="roomtype{{$roomTypes[$roomname]}}-{{$day}}">
                                    <div class="top-half rate"
                                         data-roomtype="{{$roomTypes[$roomname]}}"
                                         data-date="{{$date}}"
                                         contenteditable="true">{{$value['rate']}}</div>
                                    <div class="bottom-half">{{$value['available']}}</div>
                                </td>
                                @php ++$day; @endphp
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="form-group pull-right">
                    <button class="form-submit btn btn-success">
                        <img src="/assets/images/i-save.png"/>&nbsp;&nbsp;Save Rates
                    </button>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('footer')
    @parent
    <script type="text/javascript">
        $(document).ready(function (event) {
            var dateToday = new Date();
            $("input.date").datepicker({
                dateFormat: "yy-mm-dd",
                numberOfMonths: 3,
                minDate: dateToday
            }).on("change", function (e) {
                updateTable();
            });

            function updateTable(){
                var datePicker = $('input.date');
                var startDate = datePicker.val();

                var currentDate = datePicker.datepicker( 'getDate' );
                updateHeader(currentDate);

                $.ajax({
                    type: "POST",
                    url: '{{route('settings.daily.get')}}',
                    data: {startDate:startDate},
                    success: function (res) {
                        var items = res.items;
                        for(var key in items)
                        {
                            //key is ID of roomtype
                            //value is array with rate data
                            let value = items[key];

                            for(var i=0; i<value.length; i++)
                            {
                                var data = value[i];

                                var cellId = '#roomtype'+key+'-'+i;

                                var cell = $(cellId);

                                cell.html(
                                    "<div class='top-half rate' data-roomtype='"+key+
                                    "' data-date='"+data.date+"' contenteditable='true'>"+
                                    data.rate+"</div><div class='bottom-half'>"+data.available+"</div>"
                                );
                                cell.attr('class','');
                                cell.addClass('cell-'+data.color);
                            }
                        }
                    }
                })
            }

            function updateHeader(startDate)
            {
                for(var i=0; i< 7; i++)
                {
                    var day = getDayOfWeek(startDate.getDay());
                    var date = startDate.getDate();

                    //update th values
                    $('.day'+i).html(day);
                    $('.date'+i).html(date);

                    //jump to next day
                    var ms = startDate.getTime() + 86400000;
                    startDate = new Date(ms);
                }
            }

            function getDayOfWeek(index)
            {
                var weekday = new Array(7);
                weekday[0] =  "Sun";
                weekday[1] = "Mon";
                weekday[2] = "Tue";
                weekday[3] = "Wed";
                weekday[4] = "Thu";
                weekday[5] = "Fri";
                weekday[6] = "Sat";

                return weekday[index];
            }

            $('.btn-today').on('click', function(e){
                $("input.date").datepicker("setDate", dateToday);
                updateTable();
            });

            //save daily rates
            $('.form-submit').on('click', function(){
                var items = $( ".rate-changed" );

                var rates = [];

                items.each(function() {
                    var rate = $( this ).html();
                    var date = $( this ).data('date');
                    var roomtype = $( this ).data('roomtype');

                    var obj = [roomtype, date, rate];
                    rates.push(obj);
                });

                var start = $("input.date").val();

                $.ajax({
                    type: "POST",
                    url: '{{route('settings.daily.store')}}',
                    data: {rates:rates,start:start},
                    success: function (res) {

                        var msg = res.message;
                        createSuccessModal(msg+". MyAllocator users please allow a few minutes for this to reach booking channels.");

                        $('.pricing-table .top-half[contenteditable=true]').removeClass('rate-changed');

                    }
            })});

            /*********************************/

            /* Editable Prices - Daily Rates */
            $(document).on('keyup', '.pricing-table .top-half[contenteditable=true]', function () {
                $( this ).addClass('rate-changed');

                var limit = 10;
                var minlimit = 1;
                var text = $(this).html();
                var chars = text.length;
                if (chars >= limit) {
                    var new_text = text.substr(0, limit);
                    $(this).html(new_text);
                }
            });
        });
    </script>
@endsection