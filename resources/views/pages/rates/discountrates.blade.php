@extends('layouts.backend')

@section('meta-title', 'HostelPro - Discount Settings')

@php
$seasonId = null;
@endphp

@section('page-id', 'rates')
@section('content')
    @include('includes.ratessidebar')
    @if (count($seasonrates) == 0)
        <div id="content-wrapper" class="m-t-md">
            <p>Please Fill Season Rates Data</p>
        </div>
    @elseif(count($roomtypes) == 0)
        <div id="content-wrapper" class="m-t-md">
            <p>No Rooms found/p>
        </div>
    @else
    <div id="content-wrapper" class="m-t-md">
        {!! Form::open() !!}
        <div class="row seasonRates form-group">
            <ul class="nav nav-tabs">
                @foreach($seasonrates as $key => $seasonrate)
                    <li class="{{ ($key == 0) ? 'active' : '' }}" data-seasonId="{{$seasonrate->id}}">
                        <a href="">{{ $seasonrate->name }}<br><span class="small">{{ $seasonrate->getFormattedStartDate() }}
                                - {{ $seasonrate->getFormattedEndDate() }}</span></a>
                        @php
                            if(is_null($seasonId))
                            {
                                $seasonId = $seasonrate->id;
                            }
                        @endphp
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="row form-group">
            <div class="col-xs-2">
                <select id="rooms" name="rooms" class="form-control">
                    @foreach($roomtypes as $room)
                        <option value="{{ $room['id'] }}">{{ $room['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-2">
                Early Bird Discount
            </div>
            <div class="col-xs-2">
                <input class="form-control discount-text" type="text" placeholder="Discount %" id="earlyBirdPercent"
                       name="earlyBirdPercent"
                       value="@isset($currentDiscount) {{$currentDiscount->earlybird_discount}} @endisset"/>
            </div>
            <div class="col-xs-2">
                <input class="form-control discount-text" type="text" placeholder="Days in Advance" id="earlyBirdDays"
                       name="earlyBirdDays"
                       value="@isset($currentDiscount) {{$currentDiscount->earlybird_days}} @endisset"
                />
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-2">
                Last Minute Discount
            </div>
            <div class="col-xs-2">
                <input class="form-control discount-text" type="text" placeholder="Discount %" id="lastMinutePercent"
                       name="lastMinutePercent"
                       value="@isset($currentDiscount) {{$currentDiscount->lastminute_discount}} @endisset"
                />
            </div>
            <div class="col-xs-2">
                <input class="form-control discount-text" type="text" placeholder="Days in Advance" id="lastMinuteHours"
                       name="lastMinuteHours"
                       value="@isset($currentDiscount) {{$currentDiscount->lastminute_days}} @endisset"
                />
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-2">
                Progressive Pricing
            </div>
            <div class="col-xs-2">
                <input class="form-control discount-text" type="text" placeholder="Low Price" id="progressiveLowPrice"
                       name="progressiveLowPrice"
                       value="@isset($currentDiscount) {{$currentDiscount->progressive_low_price}} @endisset"
                />
            </div>
            <div class="col-xs-2">
                <input class="form-control discount-text" type="text" placeholder="High Price" id="progressiveHighPrice"
                       name="progressiveHighPrice"
                       value="@isset($currentDiscount) {{$currentDiscount->progressive_high_price}} @endisset"
                />
            </div>
        </div>
        <input type="hidden" id="seasonId" name="seasonId" value="{{$seasonId}}"/>
        <input type="hidden" id="discountRateId" name="discountRateId"
               value="@isset($currentDiscount) {{$currentDiscount->id}} @endisset"/>
        <p>
            <button class="btn m-b-xs btn-sm btn-success btn-addon" type="submit"><i class="fa fa-check"></i>Save
            </button>

        </p>
        {!! Form::close() !!}
        @endif
    </div>
@endsection
@section('footer')
    @parent
    <script type="text/javascript">
        $(document).ready(function (event) {
            $('.seasonRates').on('click', '.nav-tabs li', function (event) {
                event.preventDefault();
                $('.nav-tabs li').removeClass('active');
                $(this).addClass('active');

                var seasonid = $(this).data('seasonid');
                $('#seasonId').val(seasonid);

                $(this).closest('.seasonRates').find('.tab-pane').removeClass('active').eq($(this).index()).addClass('active');

                changeDiscountData();
            });

            $( "#rooms" ).on('change', function(e){
                changeDiscountData();
            });

            function changeDiscountData(){
                var data = $( "form" ).first().serializeArray();

                $.post('{{ route('settings.discount.find') }}', data,
                        function(res){
                            if(res.discountRate)
                            {
                                var obj = JSON.parse(res.discountRate);

                                $('#earlyBirdPercent').val(obj.earlybird_discount);
                                $('#earlyBirdDays').val(obj.earlybird_days);
                                $('#lastMinutePercent').val(obj.lastminute_discount);
                                $('#lastMinuteHours').val(obj.lastminute_days);
                                $('#progressiveLowPrice').val(obj.progressive_low_price);
                                $('#progressiveHighPrice').val(obj.progressive_high_price);

                                $('#discountRateId').val(obj.id);
                            }
                            else
                            {
                                $("input.discount-text:text").val("");
                                $('#discountRateId').val('');
                            }
                        }
                );
            }

            $('.btn-success').on('click', function(e){
                e.preventDefault();
                saveDiscountRateData();
            });


            function saveDiscountRateData() {
                var data = $( "form" ).first().serializeArray();
                $.ajax({
                    type: "POST",
                    url: '{{ route('settings.discount.save') }}',
                    data: data,
                    success: function (res) {
                        var id = res.id;

                        if(id)
                        {
                            $('#discountRateId').val(id);
                        }

                        var msg = res.message;
                        createSuccessModal(msg+". MyAllocator users please allow a few minutes for this to reach booking channels.");
                    }
                });
            }
        });
    </script>
@endsection