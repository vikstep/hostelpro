@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings')

@section('page-id', 'rates')

@section('content')

    @include('includes.ratessidebar')

    <div id="content-wrapper" class="m-t-md">


        <div class="row seasonRates">
            <ul class="nav nav-tabs">
                @foreach($rates->room_types as $key => $room_type)
                    <li class="{{ ($key == 0) ? 'active' : '' }}">
                        <a href="">{{ $room_type->name }}</a>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content col-sm-10">

                @foreach($rates->room_types as $key => $room_type)
                    <div class="tab-pane {{ ($key == 0) ? 'active' : ''}}">
                        <table class="table table-striped table-condensed">

                            @foreach ($rates->calendar as $i => $week)
                                <thead>
                                <tr>
                                    <th></th>
                                    @foreach ($week as $date)
                                        <th>{!! $i == 0 ? $date->format('D'). '<br>' : '' !!} {{ $date->format('d M Y') }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><strong>{{ Auth::user()->currenthostel->name }}</strong></td>
                                    @foreach ($week as $date)
                                        <td>{{ $calendar->availability[$key]['rates'][$date->toDateString()]['price'] ?? null }}</td>
                                    @endforeach
                                </tr>

                                @foreach ($rates->competitor_urls as $competitor_url )
                                    <tr>
                                        <td><strong>{{ $competitor_url->name }}</strong></td>
                                        @foreach ($week as $date)
                                            <td>{{ $rates->rates[$room_type->id][$competitor_url->id][$date->toDateString()] ?? null }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            @endforeach

                        </table>
                    </div>
                @endforeach
            </div>


        </div>


    </div>
@endsection
@section('footer')
    @parent
    <script type="text/javascript">
        $(document).ready(function (event) {
            $('.seasonRates').on('click', '.nav-tabs li', function (event) {
                event.preventDefault();
                $('.nav-tabs li').removeClass('active');
                $(this).addClass('active');
                $(this).closest('.seasonRates').find('.tab-pane').removeClass('active').eq($(this).index()).addClass('active');
            });

//            $('.nav-tabs')
//                    .scrollingTabs({
//                        scrollToTabEdge: true,
//                        disableScrollArrowsOnFullyScrolled: true
//                    })
//                    .on('ready.scrtabs', function () {
//                        $('.tab-content').show();
//                        var width = $('.report-right').width() - 40;
//                        $('.scrtabs-tabs-fixed-container').width(width);
//                    });

        });
    </script>
@endsection