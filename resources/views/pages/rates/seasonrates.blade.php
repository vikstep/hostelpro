@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings')

@section('page-id', 'rates')

@section('content')

    @include('includes.ratessidebar')

    <div id="content-wrapper" class="m-t-md">

        <div class="row">
            <div class="col-sm-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(array('route' => array('settings.rates.store'), 'method' => 'POST', 'class' => 'form-inline m-l-xs')) !!}
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Season Name" name="name">
                </div>
                <div class="form-group">
                    <input class="form-control datepicker" placeholder="Date" required="required"
                           name="startdate" type="text" value="">
                </div>
                <div class="form-group">
                    &nbsp; to &nbsp;
                </div>
                <div class="form-group">
                    <input class="form-control datepicker" placeholder="Date" required="required" name="enddate"
                           type="text" value="">
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-primary btn-addon" type="submit"><i class="fa fa-plus"></i>Add
                        New Season
                    </button>
                </div>
                {!! Form::close() !!}

                <hr>

                @if ($seasonrates->count() > 0)
                    @include('partials.seasonrates')
                @else
                    <p>You have not yet added any Season Rates.</p>
                @endif

            </div>

        </div>
    </div>
@endsection
@section('footer')
    @parent
    <script type="text/javascript">
        $(document).ready(function (event) {
            $('.seasonRates').on('click', '.nav-tabs li', function (event) {
                event.preventDefault();
                $('.nav-tabs li').removeClass('active');
                $(this).addClass('active');
                $(this).closest('.seasonRates').find('.tab-pane').removeClass('active').eq($(this).index()).addClass('active');
            }).on('submit', '.rates-form', function (event) {
                event.preventDefault();
                var alert = false;
                $(this).closest('.tab-pane').find('input').each(function () {
                    if (parseInt($(this).val()) == 0) {
                        alert = true;
                    }
                });
                if (alert) {
                    //confirmSeasonRatePopup($(this));
                    createAreYouSureModal('POST',
                            $(this).attr('action'),
                            $(this).serialize(),
                            "Are you sure?",
                            "Some values are 0. A 0 value means the room cannot be booked on that day.", submitSeasonRateForm())
                } else {
                    submitSeasonRateForm();
                }
            });

//            $('.nav-tabs')
//                    .scrollingTabs({
//                        scrollToTabEdge: true,
//                        disableScrollArrowsOnFullyScrolled: true
//                    })
//                    .on('ready.scrtabs', function () {
//                        $('.tab-content').show();
//                        var width = $('.report-right').width() - 40;
//                        $('.scrtabs-tabs-fixed-container').width(width);
//                    });

            function submitSeasonRateForm() {
                //form.submit();
                var form = $('.tab-pane.active .rates-form');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function () {
                        createSuccessModal("MyAllocator users please allow a few minutes for this to reach booking channels.");
                    }
                });
            }

            function confirmSeasonRatePopup(form) {
                noty({
                    text: 'Some values are 0. A 0 value means the room cannot be booked on that day.<br><br> Are you sure you wish to proceed?',
                    theme: 'relax',
                    layout: 'center',
                    modal: true,
                    animation: {
                        open: {height: 'toggle'}, // jQuery animate function property object
                        close: {height: 'toggle'}, // jQuery animate function property object
                        easing: 'swing', // easing
                        speed: 150 // opening & closing animation speed
                    },
                    buttons: [
                        {
                            addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            submitSeasonRateForm(form);
                        }
                        },
                        {
                            addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                            $noty.close();
                        }
                        }
                    ]
                });
            }

        });
    </script>
@endsection