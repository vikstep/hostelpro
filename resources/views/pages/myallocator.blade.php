@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings - MyAllocator')

@section('page-id', 'settings')

@section('content')

  @include('includes.settingssidebar')

  <div id="content-wrapper" class="m-t-md">


            <div class="row">

              @if (isset($properties))
                <div class="row">
                  <div class="col-lg-8 col-lg-offset-2 pt15">
                    <div class="panel panel-lined">
                      <div class="panel-heading"><i><strong>One more step remaining: Select MyAllocator Property</strong></i></div>
                      <div class="panel-body">
                        <p>You have multiple properties setup on MyAllocator.</p>

                        <p>Please select the myallocator property that corresponds to your current HostelPro property (<strong>{!! Auth::user()->currenthostel->name !!})</strong></p>

                        <table class="table table-condensed rooms">
                          <thead>
                          <th>MyAllocator Property Name</th>
                          <th>Select</th>
                          </thead>
                          <tbody>
                          {!! Form::open(['route' => 'settings.myallocator.store']) !!}
                          {!! Form::hidden('UserId', $UserId) !!}
                          {!! Form::hidden('UserPassword', $UserPassword) !!}
                          @foreach($properties as $property)
                            <tr>
                              <td class="v-middle">{!! $property['name'] !!}</td>
                              <td class="v-middle">
                                <button class="btn btn-sm btn-success" name="PropertyId" value="{!! $property['id'] !!}"><i class="fa fa-link"></i>&nbsp; Link this Property &nbsp;</button>
                              </td>
                            </tr>
                          @endforeach
                          {!! Form::close() !!}
                          </tbody>
                        </table>

                        <p>Note: To link up your other properties with HostelPro, you will first need to switch to those properties in HostelPro, then repeat this linking process.</p>

                      </div>
                    </div>
                  </div>
                </div>
              @else

                <div class="col-lg-3">
                  <div class="panel panel-lined">
                    <div class="panel-heading"><i><strong>Link your MyAllocator account to HostelPro</strong></i></div>
                    <div class="panel-body">

                      @if (Auth::user()->currenthostel->myallocatorToken()->count() == 0)
                      {!! Form::open(['route' => 'settings.myallocator.store']) !!}

                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            {!! Form::Label('UserId', "MyAllocator Username:") !!}
                            {!! Form::text('UserId', null, array('class' => 'form-control', 'placeholder' => 'Username', 'required' => 'required')) !!}
                            {!! errors_for('UserId', $errors) !!}
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            {!! Form::Label('UserPassword', "MyAllocator Password:") !!}
                            {!! Form::password('UserPassword', array('class' => 'form-control', 'placeholder' => 'Password', 'required' => 'required')) !!}
                            {!! errors_for('UserPassword', $errors) !!}
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        {!! Form::submit('Link with MyAllocator', array('class' => 'btn btn-lg btn-primary btn-block')) !!}
                        {!! Form::close() !!}
                      </div>

                        @else
                      <p>You have already linked your myallocator account.</p>

                      @endif

                    </div>
                  </div>
                </div>

                <div class="col-lg-9">
                  <div class="panel panel-lined">
                    <div class="panel-heading"><i><strong>What does this do?</strong></i></div>
                    <div class="panel-body">
                      <p><strong>WARNING: All existing rooms and bookings in HostelProfessional will be deleted once you submit this form.</strong></p>

                      <p>Once successfully linked with MyAllocator, all future bookings from MyAllocator will appear in HostelPro.</p>

                      <p>Any existing MyAllocator bookings will be automatically imported by HostelPro.</p>

                      <p>For further information or if you have any questions or concerns do not hesitate to contact support.</p>

                    </div>
                  </div>
                </div>

              @endif

            </div>

          </div>

@endsection