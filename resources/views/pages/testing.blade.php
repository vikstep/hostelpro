@extends('layouts.backend')

@section('meta-title', 'HostelPro - Testing')

@section('content')

  @include('includes.standardheader')

  @include('includes.testingsidebar')
  <div class="content-container" id="settings">


    <div class="row pt15">

      @if (Session::has('flash_message'))
        <div class="row">
          <div class="col-lg-6 col-lg-offset-3 pt15">
            <div class="alert text-center {{ Session::get('alert-class', 'alert-info') }}">{!! Session::get('flash_message') !!}</div>
          </div>
        </div>
      @endif

      <div class="col-lg-6">
        <div class="panel panel-lined mb30">
          <div class="panel-heading"><i>Add Guest</i></div>
          <div class="panel-body">

            {!! Form::open(['route' => 'testing.addGuest']) !!}

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  {!! Form::Label('firstname', "First Name") !!}
                  {!! Form::text('firstname', '', array('class' => 'form-control', 'required' => 'required')) !!}
                  {!! errors_for('firstname', $errors) !!}
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  {!! Form::Label('lastname', "Last Name") !!}
                  {!! Form::text('lastname', '', array('class' => 'form-control', 'required' => 'required')) !!}
                  {!! errors_for('lastname', $errors) !!}
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  {!! Form::Label('email', "Email") !!}
                  {!! Form::email('email', '', array('class' => 'form-control')) !!}
                  {!! errors_for('email', $errors) !!}
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  {!! Form::Label('phone', "Phone") !!}
                  {!! Form::text('phone', '', array('class' => 'form-control')) !!}
                  {!! errors_for('phone', $errors) !!}
                </div>
              </div>
            </div>

            <div class="form-group">
              {!! Form::Label('country_id', "Country") !!}
              {!! Form::select('country_id', $countries, false, array('class' => 'form-control')) !!}
              {!! errors_for('country_id', $errors) !!}
            </div>
            <div class="form-group">
              {!! Form::Label('city_id', "City") !!}
              {!! Form::select('city_id', array(), false, array('class' => 'form-control')) !!}
              {!! errors_for('city_id', $errors) !!}
            </div>
            <div class="form-group">
              {!! Form::Label('address_line_1', "Address") !!}
              {!! Form::text('address_line_1', '', array('class' => 'form-control')) !!}
              {!! errors_for('address_line_1', $errors) !!}
            </div>
            <div class="form-group">
              {!! Form::Label('address_line_2', "Address (cont.)") !!}
              {!! Form::text('address_line_2', '', array('class' => 'form-control')) !!}
              {!! errors_for('address_line_2', $errors) !!}
            </div>
            <div class="form-group">
              {!! Form::Label('zip_code', "Zip Code") !!}
              {!! Form::text('zip_code', '', array('class' => 'form-control')) !!}
              {!! errors_for('zip_code', $errors) !!}
            </div>

            {!! Form::submit('Add Guest', array('class' => 'btn btn-lg btn-primary btn-block')) !!}
            </form>

          </div>
        </div>
      </div>


      <div class="col-lg-6">
        <div class="panel panel-lined mb30">
          <div class="panel-heading"><i>Add Local Booking</i></div>
          <div class="panel-body">

            {!! Form::open(['route' => 'testing.addBooking']) !!}

            <div class="row">

              <div class="col-lg-6">
                <div class="form-group">
                  {!! Form::Label('guest_id', "Select Guest") !!}
                  {!! Form::select('guest_id', $guests, false, array('class' => 'form-control')) !!}
                  {!! errors_for('guest_id', $errors) !!}
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  {!! Form::Label('room_type_id', "Select Room Type") !!}
                  {!! Form::select('room_type_id', $roomtypes, false, array('class' => 'form-control')) !!}
                  {!! errors_for('room_type_id', $errors) !!}
                </div>
              </div>

            </div>

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  {!! Form::Label('start_date', "Start Date") !!}
                  {!! Form::text('start_date', '', array('id' => 'start_date', 'class' => 'form-control', 'required')) !!}
                  {!! errors_for('start_date', $errors) !!}
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  {!! Form::Label('end_date', "End Date") !!}
                  {!! Form::text('end_date', '', array('id' => 'end_date', 'class' => 'form-control', 'placeholder' => 'Date', 'required')) !!}
                  {!! errors_for('end_date', $errors) !!}
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  {!! Form::Label('number_of_guests', "Number of Guests") !!}
                  {!! Form::text('number_of_guests', '', array('class' => 'form-control')) !!}
                  {!! errors_for('number_of_guests', $errors) !!}
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  {!! Form::Label('price_per_night', "Price per Night") !!}
                  {!! Form::text('price_per_night', '', array('class' => 'form-control')) !!}
                  {!! errors_for('price_per_night', $errors) !!}
                </div>
              </div>
            </div>


            {!! Form::submit('Add Local Booking', array('name' => 'submit', 'value' => 'local', 'class' => 'btn btn-lg btn-primary btn-block')) !!}
            {!! Form::submit('Add MyAllocator Booking', array('name' => 'submit', 'value' => 'myallocator', 'class' => 'btn btn-lg btn-primary btn-block')) !!}
            </form>




          </div>
        </div>
      </div>




    </div>

    @include('includes.footer')
  </div>

  <script type="text/javascript">
    $(function(){
      $('#start_date').datepicker({
        inline: true,
        showOtherMonths: true,
        dateFormat: "dd-mm-yy",
        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      });
      //Generate a new date
      var date = new Date();
      //Set to today
      date.setDate(date.getDate());
      //Set the datepicker to the date we just generated
      $('#start_date').datepicker("setDate", date);
    });
  </script>
  <script type="text/javascript">
    $(function(){
      $('#end_date').datepicker({
        inline: true,
        showOtherMonths: true,
        dateFormat: "dd-mm-yy",
        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      });
      //Generate a new date
      var date = new Date();
      //Set to 3 days from today
      date.setDate(date.getDate() + 3);
      //Set the datepicker to the date we just generated
      $('#end_date').datepicker("setDate", date);
    });
  </script>

@stop