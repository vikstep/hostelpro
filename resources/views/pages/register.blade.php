@extends('layouts.backend')

@section('meta-title', 'HostelPro - Register')

@section('content')

  <div class="app ng-scope app-header-fixed app-aside-fixed vertical-center-container">
    <div class="container w-xxl w-auto-xs vertical-center">

      <a href="" class="navbar-brand block m-t ng-binding">Hostel<span>&nbsp; Pro</span></a>

      <div class="m-b-lg">
        <div class="wrapper text-center">
          @if (Auth::check())
            <p>Create and add a new hostel to this account</p>
            @else
            <p>Create an account</p>
          @endif
        </div>

        @if (Session::has('flash_message'))
          <div class="text-center alert {{ Session::get('alert-class', 'alert-info') }}" role="alert">{!! Session::get('flash_message') !!}</div>
        @endif

        {!! Form::open(['route' => 'registration.store']) !!}
        <div class="list-group list-group-sm">

          @if (!Auth::check())
          <div class="list-group-item">
            <div class="row">
              <div class="col-lg-6 small-padding border-right">
                {!! Form::text('firstname', '', array('class' => 'form-control no-border', 'placeholder'=>'First Name', 'required'=> 'required')) !!}
                {!! errors_for('firstname', $errors) !!}
              </div>
              <div class="col-lg-6 small-padding">
                {!! Form::text('lastname', '', array('class' => 'form-control no-border', 'placeholder'=>'Last Name', 'required' => 'required')) !!}
                {!! errors_for('lastname', $errors) !!}
              </div>
            </div>
          </div>
          @endif

          <div class="list-group-item">
            {!! Form::text('hostelname', '', array('class' => 'form-control no-border', 'placeholder'=>'Hostel Name', 'required'=> 'required')) !!}
          </div>
          {!! errors_for('hostelname', $errors) !!}

            <div class="list-group-item">
              {!! Form::select('country', $country_options, null, array('id' => 'registerCountry', 'class' => 'form-control no-border', 'required'=> 'required')) !!}
            </div>
            {!! errors_for('country', $errors) !!}

          @if (!Auth::check())
          <div class="list-group-item">
            {!! Form::email('email', '', array('class' => 'form-control no-border', 'placeholder' => 'email@address.com', 'required' => 'required')) !!}
          </div>
          {!! errors_for('email', $errors) !!}

          <div class="list-group-item">
            {!! Form::password('password', array('class' => 'form-control no-border', 'placeholder' => 'Password', 'required' => 'required')) !!}
          </div>
          {!! errors_for('password', $errors) !!}
            @endif

        </div>
        <div class="checkbox m-b-md m-t-none">
          <label class="i-checks">
            {!! Form::checkbox('terms_and_conditions', 'yes') !!}<i></i> I agree to the terms and conditions
          </label>
          {!! errors_for('terms_and_conditions', $errors) !!}
        </div>
        {!! Form::submit(Auth::check() ? 'Add Hostel' : 'Register', array('class' => 'btn btn-lg btn-primary btn-block')) !!}

        @if (!Auth::check())
        <div class="line line-dashed"></div>
        <p class="text-center">
          <small>Already have an account?</small>
        </p>
        {!! link_to('login', "Log in", array('class' => 'btn btn-lg btn-default btn-block'), $secure = true) !!}
        @endif
        {!! Form::close() !!}
      </div>

    </div>
  </div>
@stop