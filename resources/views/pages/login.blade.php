@extends('layouts.backend')

@section('meta-title', 'HostelPro - Login')

@section('page-id', 'login')

@section('content')

  <div class="app ng-scope app-header-fixed app-aside-fixed vertical-center-container">
    <div class="container w-xxl w-auto-xs vertical-center">

        {{--<a href="" class="navbar-brand block m-t ng-binding">Hostel<span>&nbsp; Pro</span></a>--}}
        <div class="m-b-lg">
          <div class="wrapper text-center">
            <img src="{{ asset('assets/images/logo_above_text.png') }}">
          </div>

          {!! Form::open(['route' => 'sessions.store']) !!}
            <div class="list-group list-group-sm">
              <div class="list-group-item">
                {!! Form::email('email', '', array('class' => 'form-control no-border', 'placeholder' => 'email@address.com', 'required' => 'required')) !!}
                {!! errors_for('email', $errors) !!}
              </div>
              <div class="list-group-item">
                {!! Form::password('password', array('class' => 'form-control no-border', 'placeholder' => 'Password', 'required' => 'required')) !!}
                {!! errors_for('password', $errors) !!}
              </div>
            </div>
          {!! Form::submit('Log in', array('class' => 'btn btn-lg btn-primary btn-block')) !!}


            <div class="text-center m-t m-b"><a href="{{ url('/password/reset') }}">Forgot password?</a></div>
          @if (1 == 2)
            <div class="line line-dashed"></div>

            <p class="text-center"><small>Do not have an account?</small></p>
            {!! link_to('register', "Register Now", array('class' => 'btn btn-lg btn-default btn-block'), $secure = true) !!}
            <p class="text-center"><strong>Note:</strong>If you want to be added to an existing account you need to ask your Admin to create an account for you via the user settings page.</p>
            @endif
          {!! Form::close() !!}
        </div>

      </div>
  </div>
@endsection