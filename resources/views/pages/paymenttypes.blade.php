@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings - Money')

@section('page-id', 'settings')

@section('content')

    @include('includes.settingssidebar')

    <div id="content-wrapper" class="m-t-md">

                        <div class="row">

                            <div class="col-sm-12">
                                <div class="panel panel-lined mb30">
                                    <div class="panel-heading font-bold">Payment Types List</div>
                                    <div class="panel-body">
                                        <table class="table table-condensed paymenttype">
                                            <thead>
                                            <th>Payment Type</th>
                                            <th>Kept in Till</th>
                                            <th>Remove</th>
                                            </thead>
                                            <tbody id="paymentTypeTable">
                                            @if (count($payment_types))
                                                @include('partials.paymenttype')
                                            @else
                                                <tr id="nopaymenttype">
                                                    <td>No payment types have been added.</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <p>
                                            <button class="btn m-b-xs btn-sm btn-primary btn-addon" id="addpaymenttype"><i class="fa fa-plus"></i>Add Payment Type</button>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
@endsection
@section('footer')
    @parent

    <script type="text/javascript">
        $(document).ready(function () {

            var newPaymentTypeBlock = '<tr>' +
                    '<td class="v-middle"><input type="text" class="form-control" name="name" placeholder="eg Cash Payment" required> </td>' +
                    '<td class="v-middle">{!! Form::select("kept_in_till", [true => 'Yes', false => 'No'], 1, array("class" => "form-control")) !!}</td>' +
                    '<td>' +
                    '<button class="btn btn-sm btn-info savePaymentType"><i class="fa fa-floppy-o"></i>&nbsp; Save</button>' +
                    '<button class="btn btn-sm btn-danger deleteRow"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>' +
                    '</td>' +
                    '</tr>';

            $("#addpaymenttype").click(function () {
                $(".paymenttype tbody").append(newPaymentTypeBlock);
                $("#nopaymenttype").addClass("hidden");
            });

            $(document).on('click', '.savePaymentType', function () {
                var data = {};
                data.name = $(this).parent().parent().find('[name="name"]').val();
                data.keptintill = $(this).parent().parent().find('[name="kept_in_till"]').val();
                saveItem('{!! route('settings.paymenttypes.store') !!}', data, $(this));
            });

            $(document).on('click', '.deleteRow', function () {
                $(this).closest('tr').remove();
            });

            $(document).on('click', '.deletePaymentType', function () {
                confirmDeletePopup('{!! route('settings.paymenttypes.destroy', '') !!}', $(this).data("id"), 'DELETE');
            });

            $('#paymentTypeTable').sortable({
                axis: 'y',
                update: function (event, ui) {
                    var data = $(this).sortable('serialize');
                    data += '&_token={{ csrf_token() }}';

                    console.log(data);
                    $.ajax({
                        url: '{{ route('settings.paymenttypes.sort') }}',
                        type: 'POST',
                        data: data
                    });

                }
            });

            function confirmDeletePopup($type, $id, $requesttype) {
                noty({
                    text: 'Are you sure you want to delete this item?',
                    theme: 'relax',
                    layout: 'center',
                    modal: true,
                    animation: {
                        open: {height: 'toggle'}, // jQuery animate function property object
                        close: {height: 'toggle'}, // jQuery animate function property object
                        easing: 'swing', // easing
                        speed: 150 // opening & closing animation speed
                    },
                    buttons: [
                        {
                            addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            deleteItem($type, $id, $requesttype);
                        }
                        },
                        {
                            addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                            $noty.close();
                        }
                        }
                    ]
                });
            }

            function deleteItem($type, $id, $requesttype) {
                $.ajax({
                    url: $type + "/" + $id,
                    type: $requesttype,
                    success: function (response) {
                        $("table.paymenttype").html(response);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        createErrorModal(xhr.responseText);
                    }
                });
            }

            function saveItem($type, $data, $row) {
                $.ajax({
                    url: $type,
                    type: 'POST',
                    data: $data,
                    success: function (response) {
                        $row.parent().parent().replaceWith(response);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        createErrorModal(xhr.responseText);
                    }
                });
            }


        });
    </script>


@endsection