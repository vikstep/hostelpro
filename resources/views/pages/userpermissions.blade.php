@extends('layouts.backend')

@section('meta-title', 'HostelPro - User Management - Money User')

@section('page-id', 'settings')

@section('content')
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="panel panel-default mb30">
                <div class="panel-heading font-bold"></div>
                <div class="panel-body">

                    @include('includes.settingssidebar')

                    <div class="report-right">


                        <div class="row">

                            <div class="col-lg-12">
                                <div class="panel panel-lined mb30">
                                    <div class="panel-heading font-bold">User Permissions
                                        for {{ $user->fullName() }}</div>
                                    <div class="panel-body">
                                        <table class="table table-condensed money">
                                            <thead>
                                            <tr>
                                                <th>Permission Name</th>
                                                <th>Allowed</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Basic Reports</td>
                                                <td>@if($user->hasPermission('basic_reports'))<i
                                                            class="fa fa-check text-success"></i>@endif</td>
                                            </tr>
                                            <tr>
                                                <td>Advanced Reports</td>
                                                <td>@if($user->hasPermission('advanced_reports'))<i
                                                            class="fa fa-check text-success"></i>@endif</td>
                                            </tr>
                                            <tr>
                                                <td>Hostel Settings</td>
                                                <td>@if($user->hasPermission('hostel_settings'))<i
                                                            class="fa fa-check text-success"></i>@endif</td>
                                            </tr>
                                            <tr>
                                                <td>User Management</td>
                                                <td>@if($user->hasPermission('user_management'))<i
                                                            class="fa fa-check text-success"></i>@endif</td>
                                            </tr>
                                            <tr>
                                                <td>Rates</td>
                                                <td>@if($user->hasPermission('rates'))<i
                                                            class="fa fa-check text-success"></i>@endif</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        @if (Auth::user()->canDeleteUser($user))
                                            {!! Form::open(array('route' => array('users.destroy', $user->id), 'method' => 'DELETE', 'id' => 'deleteUserForm'))  !!}
                                            <button class="btn btn-sm btn-danger" type="submit"><i
                                                        class="fa fa-trash-o"></i>&nbsp; Delete
                                            </button>
                                            {!! Form::close() !!}
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @parent

    <script type="text/javascript">

        $(function() { //shorthand document.ready function
            $('#deleteUserForm').on('submit', function(e) {
                e.preventDefault();

                noty({
                    text: 'Are you sure you want to delete this users?',
                    theme: 'relax',
                    layout: 'center',
                    modal: true,
                    animation: {
                        open: {height: 'toggle'}, // jQuery animate function property object
                        close: {height: 'toggle'}, // jQuery animate function property object
                        easing: 'swing', // easing
                        speed: 150 // opening & closing animation speed
                    },
                    buttons: [
                        {
                            addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            //Bypass jquery bound event and manually submit
                            $('#deleteUserForm')[0].submit();
                        }
                        },
                        {
                            addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                            $noty.close();
                        }
                        }
                    ]
                });

                //var data = $("#deleteUserForm :input").serializeArray();
                //console.log(data); //use the console for debugging, F12 in Chrome, not alerts
            });
        });
    </script>

@endsection