@extends('layouts.backend')

@section('meta-title', 'HostelPro - Month on Month Report')

@section('page-id', 'reports')

@section('content')

    @include('includes.reportssidebar')

    <div id="content-wrapper" class="m-t-md">
        <ul class="btn-group">
            <li id='lastMonth' class="btn btn-sm btn-primary active">Last Month</li>
            <li id='lastYear' class="btn btn-sm btn-primary">Last Year</li>
        </ul>

        <ul class="btn-group">
            <li id='listView' class="btn btn-sm btn-primary active">List</li>
            <li id='graphView' class="btn btn-sm btn-primary">Graph</li>
        </ul>

        <div class="row" id="monthTable">
            <table class="table">
                <tr style="background-color: #e0f3ff;">
                    <td></td>
                    <td>{{ $monthData['prev'] }}</td>
                    <td>{{ $monthData['current'] }}</td>
                    <td>Change</td>
                </tr>
                <tr>
                    <td><b>TOTAL REVENUE</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr  style="background-color: lightpink; color:orangered">
                    <td></td>
                    <td>{{ $reportPrevMonth['revenue']['prev'] }}</td>
                    <td>{{ $reportPrevMonth['revenue']['current'] }}</td>
                    <td>{{ $reportPrevMonth['revenue']['change'] }}</td>
                </tr>
                <tr>
                    <td><b>TOTAL EXPENSE</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="background-color: #e0f3ff;  color:orangered">
                    <td></td>
                    <td>{{ $reportPrevMonth['expense']['prev'] }}</td>
                    <td>{{ $reportPrevMonth['expense']['current'] }}</td>
                    <td>{{ $reportPrevMonth['expense']['change'] }}</td>
                </tr>
                @foreach ($reportPrevMonth['expense']['categories'] as $key => $data)
                    <tr>
                        <td>{{$key}}</td>
                        <td>{{  $data['prev'] }}</td>
                        <td>{{  $data['current'] }}</td>
                        <td>{{  $data['change'] }}</td>
                    </tr>
                @endforeach
            </table>
        </div>

        <div class="row hidden" id="yearTable">
            <table class="table">
                <tr style="background-color: #e0f3ff;">
                    <td></td>
                    <td>{{ $yearData['prev'] }}</td>
                    <td>{{ $yearData['current'] }}</td>
                    <td>Change</td>
                </tr>
                <tr>
                    <td><b>TOTAL REVENUE</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr  style="background-color: lightpink; color:orangered">
                    <td></td>
                    <td>{{ $reportPrevYear['revenue']['prev'] }}</td>
                    <td>{{ $reportPrevYear['revenue']['current'] }}</td>
                    <td>{{ $reportPrevYear['revenue']['change'] }}</td>
                </tr>
                <tr>
                    <td><b>TOTAL EXPENSE</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="background-color: #e0f3ff;  color:orangered">
                    <td></td>
                    <td>{{ $reportPrevYear['expense']['prev'] }}</td>
                    <td>{{ $reportPrevYear['expense']['current'] }}</td>
                    <td>{{ $reportPrevYear['expense']['change'] }}</td>
                </tr>
                @foreach ($reportPrevYear['expense']['categories'] as $key => $data)
                    <tr>
                        <td>{{$key}}</td>
                        <td>{{  $data['prev'] }}</td>
                        <td>{{  $data['current'] }}</td>
                        <td>{{  $data['change'] }}</td>
                    </tr>
                @endforeach
            </table>
        </div>

        <div class="row hidden" id="monthlyChart">
            <div class="col-lg-12">
                <div id="chartMonthly"></div>
            </div>
        </div>

        <div class="row hidden" id="yearlyChart">
            <div class="col-lg-12">
                <div id="chartYearly"></div>
            </div>
        </div>

    </div>
@endsection
@section('footer')
    @parent

    <script type="text/javascript">
        $(function () {
            $('#lastMonth').on('click', function(){
                $('#lastYear').removeClass('active');
                $(this).addClass('active');

                $('#yearlyChart').addClass('hidden');
                $('#yearTable').addClass('hidden');

                if($('#listView').hasClass('active'))
                {
                    $('#monthlyChart').addClass('hidden');
                    $('#monthTable').removeClass('hidden');
                }
                else
                {
                    $('#monthlyChart').removeClass('hidden');
                    $('#monthTable').addClass('hidden');
                }
            });

            $('#lastYear').on('click', function(){
                $(this).addClass('active');
                $('#lastMonth').removeClass('active');

                $('#monthlyChart').addClass('hidden');
                $('#monthTable').addClass('hidden');

                if($('#listView').hasClass('active'))
                {
                    $('#yearlyChart').addClass('hidden');
                    $('#yearTable').removeClass('hidden');
                }
                else
                {
                    $('#yearlyChart').removeClass('hidden');
                    $('#yearTable').addClass('hidden');
                }
            });

            $('#listView').on('click', function(){
                $('#graphView').removeClass('active');
                $(this).addClass('active');

                $('#monthlyChart').addClass('hidden');
                $('#yearlyChart').addClass('hidden');

                if($('#lastMonth').hasClass('active'))
                {
                    $('#monthTable').removeClass('hidden');
                    $('#yearTable').addClass('hidden');
                }
                else
                {
                    $('#monthTable').addClass('hidden');
                    $('#yearTable').removeClass('hidden');
                }
            });

            $('#graphView').on('click', function(){
                $(this).addClass('active');
                $('#listView').removeClass('active');

                $('#monthTable').addClass('hidden');
                $('#yearTable').addClass('hidden');

                if($('#lastMonth').hasClass('active'))
                {
                    $('#monthlyChart').removeClass('hidden');
                    $('#yearlyChart').addClass('hidden');
                }
                else
                {
                    $('#monthlyChart').addClass('hidden');
                    $('#yearlyChart').removeClass('hidden');
                }
            });
        });

        var chartMonthly = c3.generate({
            bindto: '#chartMonthly',
            data: {
                columns: [
                    ['Revenue',
                        {{$reportPrevMonth['raw']['revenuePrev']}},
                        {{$reportPrevMonth['raw']['revenueCurrent']}}
                    ],
                    ['Expenses',
                        {{$reportPrevMonth['raw']['expensesPrev']}},
                        {{$reportPrevMonth['raw']['expensesCurrent']}}
                    ],
                    ['Profit',
                        {{$reportPrevMonth['raw']['grossprofitPrev']}},
                        {{$reportPrevMonth['raw']['grossprofitCurrent']}}
                    ]
                ],
                types: {
                    Revenue: 'bar',
                    Expenses: 'bar',
                    Profit: 'line'
                },
                colors: {
                    //data1: '#dcdcdc',
                    Revenue: '#2ca02c',
                    Expenses: '#d62728',
                    Profit: '#36404a'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: [
                        {{$monthData['prev']}}, {{$monthData['current']}}
                    ]
                }
            }
        });

        var chartYearly = c3.generate({
            bindto: '#chartYearly',
            data: {
                columns: [
                    ['Revenue',
                        {{$reportPrevYear['raw']['revenuePrev']}},
                        {{$reportPrevYear['raw']['revenueCurrent']}}
                    ],
                    ['Expenses',
                        {{$reportPrevYear['raw']['expensesPrev']}},
                        {{$reportPrevYear['raw']['expensesCurrent']}}
                    ],
                    ['Profit',
                        {{$reportPrevYear['raw']['grossprofitPrev']}},
                        {{$reportPrevYear['raw']['grossprofitCurrent']}}
                    ]
                ],
                types: {
                    Revenue: 'bar',
                    Expenses: 'bar',
                    Profit: 'line'
                },
                colors: {
                    //data1: '#dcdcdc',
                    Revenue: '#2ca02c',
                    Expenses: '#d62728',
                    Profit: '#36404a'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: [
                        {{$yearData['prev']}}, {{$yearData['current']}}
                    ]
                }
            }
        });

    </script>

@endsection