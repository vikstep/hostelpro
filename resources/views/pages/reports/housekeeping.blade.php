@extends('layouts.backend')

@section('meta-title', 'HostelPro - HouseKeeping')

@section('page-id', 'reports')

@section('content')

    <div class="row hidden-print m-t-md">
        <div class="col-sm-12">

            {!! Form::open(['route' => 'reports.housekeepingdownload', 'class' => 'form-inline', 'id' => 'housekeeping-form', 'method' => 'GET']) !!}
            <div class="input-group">
                <input id="datepicker-housekeeping" name="date" type="text"
                       class="form-control input-sm" value="{{ $date }}">
                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            </div>
            <div class="input-group">
                <button class="btn btn-info" id="printButton">Print</button>
            </div>
            {!! Form::close() !!}

        </div>
    </div>


    @include('includes.reportssidebar')

    <div id="content-wrapper" class="m-t-md">

        <h1 class="text-center">Housekeeping for {{ Auth::user()->currenthostel->name }}</h1>

        <h3 class="text-center">{{ $report->formatted_date }}</h3>

        <table width="100%" class="table table-condensed m-t-lg">
            <thead>
            <tr>
                <th width="35%">Room</th>
                <th width="7%" class="text-center">Arr</th>
                <th width="7%" class="text-center">Dep</th>
                <th width="7%" class="text-center">Rem</th>
                <th width="7%" class="text-center">NiU</th>
                <th width="7%" class="text-center">Free</th>
                <th width="30%" class="text-center">Notes</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($report->rooms as $room)
                <tr>
                    <td>{{ $room->name }}</td>
                    <td class="text-center">{{ $report->data[$room->id]['arr'] }}</td>
                    <td class="text-center">{{ $report->data[$room->id]['dep'] }}</td>
                    <td class="text-center">{{ $report->data[$room->id]['rem'] }}</td>
                    <td class="text-center">{{ $report->data[$room->id]['niu'] }}</td>
                    <td class="text-center">{{ $report->data[$room->id]['free'] }}</td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <p class="text-center">Arr = Arrivals, Dep = Departures, Rem = Remainings, NiU = Not in Use or
            Blocked</p>

        <table class="table table-condensed">
            <thead>
            <tr>
                <th colspan="5">
                    <h4>
                        Departing
                        @if ($dayreport->totals['departing'] > 0)
                            / <span class="smallerfont"> {{ $dayreport->totals['departing'] }} Guests</span>
                        @endif
                    </h4>
                </th>
            </tr>
            <tr>
                <th width="20%">Name</th>
                <th width="20%">Status</th>
                <th width="20%">Room</th>
                <th width="20%">Bed</th>
                <th width="20%"># Guests</th>
            </tr>
            </thead>
            <tbody>
            @forelse($dayreport->departing as $departing)
                @if ($departing->status == "Checked Out")
                    <tr class="checkedout">
                @else
                    <tr>
                        @endif
                        <td><a href="#" class="loadBooking" data-bookingid="{{ $departing->id }}">{{ $departing->id }}
                                - {{ $departing->lastname }}</a></td>
                        <td>{{ $departing->status }}</td>
                        <td>{{ $departing->room_name }}</td>
                        <td>{{ $departing->bed }}</td>
                        <td>{{ $departing->number_of_guests }}</td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="5">No bookings are departing on this day.</td>
                        </tr>
                    @endforelse
            </tbody>
        </table>

        <table class="table table-condensed">
            <thead>
            <tr>
                <th colspan="5">
                    <h4>
                        Changing Rooms
                        @if ($dayreport->totals['changing_rooms'] > 0)
                            / <span class="smallerfont"> {{ $dayreport->totals['changing_rooms'] }} Guests</span>
                        @endif
                    </h4>
                </th>
            </tr>
            <tr>
                <th width="20%">Name</th>
                <th width="20%">Status</th>
                <th width="20%">Room</th>
                <th width="20%">Bed</th>
                <th width="20%"># Guests</th>
            </tr>
            </thead>
            <tbody>
            @forelse($dayreport->changing_rooms as $changing)
                @if ($changing->status == "Checked Out")
                    <tr class="checkedout">
                @else
                    <tr>
                        @endif
                        <td><a href="#" class="loadBooking" data-bookingid="{{ $changing->id }}">{{ $changing->id }}
                                - {{ $changing->lastname }}</a></td>
                        <td>{{ $changing->status }}</td>
                        <td>{{ $changing->room_name }}</td>
                        <td>{{ $changing->bed }}</td>
                        <td>{{ $changing->number_of_guests }}</td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="5">No bookings are changing rooms on this day.</td>
                        </tr>
                    @endforelse
            </tbody>
        </table>

    </div>

@endsection
@section('footer')
    @parent

    <script type="text/javascript">
        $(function () {
            $('#datepicker-housekeeping').datepicker({
                inline: true,
                showOtherMonths: true,
                dateFormat: "dd-mm-yy",
                dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                onSelect: function () {
                    $(this).closest('form').submit();
                }
            });
            //Generate a new date
            var date = new Date();
            //Set the datepicker to the date we just generated
            $('#datepicker-houseKeeping').datepicker("setDate", date);
        });

        $('#printButton').click(function () {
            window.print();
        });
    </script>

@endsection