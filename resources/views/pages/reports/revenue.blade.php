@extends('layouts.backend')

@section('meta-title', 'HostelPro - Revenue Report')

@section('page-id', 'reports')

@section('content')

  <div class="row m-t-md">
    <div class="col-lg-12">
          @include('partials.daterangepicker-inline')
        </div>
    </div>

  @include('includes.reportssidebar')

  <div id="content-wrapper" class="m-t-md">


            <table id="revenue-table" class="table table-condensed dayView">
              <thead>
              <tr>
                <th rowspan="2">Date</th>
                <th colspan="3">Accommodation</th>
                <th colspan="2">Deposits</th>
                <th rowspan="2">Other</th>
                <th rowspan="2">Total</th>
              </tr>
              <tr>
                <th>Expected</th>
                <th>Actual</th>
                <th>Difference</th>
                <th>Taken</th>
                <th>Returned</th>
              </tr>
              </thead>
              <tbody>
              @foreach ($report->totals as $day)
                <tr class="dateRow">
                  <td>{{ $day['date'] }} {!! count($report->bookingdata[$day['date']]) ? '<i class="icon fa fa-caret-right"></i>' : '' !!}</td>
                  <td>{{ $day['accommodation-expected'] }}</td>
                  <td>{{ $day['accommodation-actual'] }}</td>
                  <td>
                    @if ($day['accommodation-actual'] == $day['accommodation-expected'])
                      -
                    @elseif ($day['accommodation-actual'] >= $day['accommodation-expected'])
                      <span class="badge bg-success">{{ ( (float)$day['accommodation-actual'] - (float)$day['accommodation-expected']) }}</span>
                    @else
                      <span class="badge bg-danger">{{ ( (float)$day['accommodation-expected'] - (float)$day['accommodation-actual']) }}</span>
                    @endif
                  </td>
                  <td>{{ $day['deposits-taken'] }}</td>
                  <td>{{ $day['deposits-returned'] }}</td>
                  <td>{{ $day['other'] }}</td>
                  <td>{{ $day['accommodation-actual'] }}</td>
                </tr>
                @foreach ($report->bookingdata[$day['date']] as $data)
                  @if (isset($data['bookingid']))
                    <tr class="subdata hidden">
                      <td><a class="loadBooking" data-bookingid="{{ $data['bookingid'] }}">&nbsp; {{ $data['bookingid'] }} - {{ $data['lastname'] }}</a></td>
                      <td>{{ $data['accommodation-expected'] }}</td>
                      <td>{{ $data['accommodation-actual'] }}</td>
                      <td>
                        @if ($data['accommodation-actual'] == $data['accommodation-expected'])
                          -
                        @elseif ($data['accommodation-actual'] >= $data['accommodation-expected'])
                          <span class="badge bg-success">{{ ( (float)$data['accommodation-actual'] - (float)$data['accommodation-expected']) }}</span>
                        @else
                          <span class="badge bg-danger">{{ ( (float)$data['accommodation-expected'] - (float)$data['accommodation-actual']) }}</span>
                        @endif
                      </td>
                      <td>{{ $data['deposits-taken'] }}</td>
                      <td>{{ $data['deposits-returned'] }}</td>
                      <td>{{ $data['other'] }}</td>
                      <td>{{ ( (float)$data['accommodation-actual'] + (float)$data['other']) }}</td>
                    </tr>
                  @endif
                @endforeach
              @endforeach
              <tr class="totalRow dateRow">
                <th class="smallcaps">Totals:</th>
                <td>{{ $report->grandtotals['accommodation-expected'] }}</td>
                <td>{{ $report->grandtotals['accommodation-actual'] }}</td>
                <td>
                  @if ($report->grandtotals['accommodation-actual'] == $report->grandtotals['accommodation-expected'])
                    -
                  @elseif ($report->grandtotals['accommodation-actual'] >= $report->grandtotals['accommodation-expected'])
                    <span class="badge bg-success">{{ ($report->grandtotals['accommodation-actual'] - $report->grandtotals['accommodation-expected']) }}</span>
                  @else
                    <span class="badge bg-danger">{{ ($report->grandtotals['accommodation-expected'] - $report->grandtotals['accommodation-actual']) }}</span>
                  @endif
                </td>
                <td>{{ $report->grandtotals['deposits-taken'] }}</td>
                <td>{{ $report->grandtotals['deposits-returned'] }}</td>
                <td>-</td>
                <td>{{ ($report->grandtotals['accommodation-actual'] + $report->grandtotals['deposits-taken'] - $report->grandtotals['deposits-returned']) }}</td>
              </tr>
              </tbody>
            </table>


          </div>
  @endsection
@section('footer')
  @parent

  <script type="text/javascript">
    $(function () {
      $('.datepicker').datepicker({
        inline: true,
        showOtherMonths: true,
        dateFormat: "dd-mm-yy",
        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
      });
    });

    $("#dayViewAddOneMonth").click(function () {
      var originaldate = $("#datepicker-dayview").val().split("-");
      //remember that javascript date function uses '0' for january and '11' for december, so minus 1 from the month to account for this
      var date = new Date(originaldate[2], originaldate[1] - 1, originaldate[0]);
      date.setMonth(date.getMonth() + 1);
      $('#datepicker-dayview').datepicker("setDate", date);
    });

    $("#dayViewMinusOneMonth").click(function () {
      var originaldate = $("#datepicker-dayview").val().split("-");
      //remember that javascript date function uses '0' for january and '11' for december, so minus 1 from the month to account for this
      var date = new Date(originaldate[2], originaldate[1] - 1, originaldate[0]);
      date.setMonth(date.getMonth() - 1);
      $('#datepicker-dayview').datepicker("setDate", date);
    });

    $(".dateRow td").click(function () {
      if ($(this).hasClass('expanded')) {
        $(this).children('.icon').addClass('fa-caret-right').removeClass('fa-caret-down');
        $(this).parent().nextUntil('.dateRow').addClass('hidden');
        $(this).removeClass('expanded');
      } else {
        $(this).children('.icon').removeClass('fa-caret-right').addClass('fa-caret-down');
        $(this).parent().nextUntil('.dateRow').removeClass('hidden');
        $(this).addClass('expanded');
      }
    });
  </script>

@endsection