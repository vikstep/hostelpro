@extends('layouts.backend')

@section('meta-title', 'HostelPro - Revenue Report')

@section('page-id', 'reports')

@section('content')

    @include('includes.reportssidebar')

    <div id="content-wrapper" class="m-t-md">


        <div class="row">

            <div class="col-md-6 col-lg-8">
                @if (Auth::user()->currenthostel->reviewURLs->count() == 0)
                    <p><strong>You have not yet added any review URLs</strong></p>
                @endif
                <p>HostelPro can track reviews from Booking.com, HostelWorld and
                    HostelBookers.</p>

                <p>To make use of this functionality you will first need to tell HostelPro the
                    URL to your hostel</p>

                <p>You can find the URL by going to Booking.com, HostelWorld or HostelBookers,
                    loading your public hostel page and then copying and pasting the URL in the
                    form.</p>

                <p>You only need to do this once, then HostelPro will automatically check for
                    new reviews every single day.</p>

                <p><strong>Green</strong> means a URL is already added, <strong>red</strong>
                    means a url is not yet added.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">

                @foreach ($reviews as $review)
                    <div class="panel review">
                        <div class="panel-heading">

                            <label class="label label-lg {{ $review->overall_score >= 75 ? 'bg-success' : ($review->overall_score >= 50 ? 'bg-warning' : 'bg-danger') }}">{{ $review->overall_score }}</label>

                            {{ $review->url->channel->name }}
                        </div>

                        <div class="panel-body">
                            @if ((!$review->liked) && (!$review->disliked) && (!$review->overall))
                                <small class="text-muted">No text written.</small>
                            @endif
                            @if ($review->overall)
                                <p>{{ $review->overall }}</p>
                            @endif
                            @if ($review->liked)
                                <p>
                                    <strong>Positive: </strong>{{ $review->liked }}
                                </p>
                            @endif
                            @if ($review->disliked)
                                <p>
                                    <strong>Negative:</strong>
                                    {{ $review->disliked }}
                                </p>
                            @endif
                        </div>

                        @if ($review->value_score)
                            <div class="panel-footer small">
                                <p>
                                    Atmosphere: <strong>{{ $review->atmosphere_score }}</strong>&nbsp;&nbsp;
                                    Cleanliness: <strong>{{ $review->cleanliness_score }}</strong>&nbsp;&nbsp;
                                    Facilities: <strong>{{ $review->facilities_score }}</strong>&nbsp;&nbsp;
                                    Location: <strong>{{ $review->location_score }}</strong>&nbsp;&nbsp;
                                    Safety: <strong>{{ $review->safety_score }}</strong>&nbsp;&nbsp;
                                    Staff: <strong>{{ $review->staff_score }}</strong>&nbsp;&nbsp;
                                    Value: <strong>{{ $review->value_score }}</strong>&nbsp;&nbsp;
                                </p>
                            </div>
                        @endif
                    </div>

                @endforeach
            </div>
        </div>


    </div>
@endsection