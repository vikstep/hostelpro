@extends('layouts.backend')

@section('meta-title', 'HostelPro - Payment Report')

@section('page-id', 'reports')

@section('content')

    <div class="row m-t-md">
        <div class="col-sm-12">

            <form class="form-inline">

                <div class="input-group dayview-selectdate">

              <span class="input-group-btn" id="dayViewMinusOneMonth">
                <button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i></button>
              </span>

                    <div class="form-group">
                        <input type="text" id="datepicker-start" name="startdate"
                               class="form-control input-md datetimepicker" value="{{ $startdate }}">
                    </div>


              <span class="input-group-btn" id="dayViewAddOneMonth">
                <button type="button" class="btn btn-default"><i class="fa fa-chevron-right"></i></button>
              </span>
                </div>

                &nbsp; to &nbsp;

                <div class="input-group dayview-selectdate">

              <span class="input-group-btn" id="dayViewMinusOneMonth">
                <button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i></button>
              </span>

                    <div class="form-group">
                        <input type="text" id="datepicker-end" name="enddate"
                               class="form-control input-md datetimepicker" value="{{ $enddate }}">
                    </div>

              <span class="input-group-btn" id="dayViewAddOneMonth">
                <button type="button" class="btn btn-default"><i class="fa fa-chevron-right"></i></button>
              </span>
                </div>

                <button type="submit" class="btn btn-sm btn-info">View</button>

            </form>


        </div>
    </div>

    @include('includes.reportssidebar')

    <div id="content-wrapper" class="m-t-md">

        <table class="table table-condensed dayView">
            <thead>
            <tr>
                <th width="15%">Date</th>
                <th width="10%">Description</th>
                <th width="10%">Qty</th>
                <th width="10%">Price</th>
                <th width="20%">Amount</th>
                <th width="10%">Payment</th>
                <th width="15%">User</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($report->data as $date => $data)
                <tr class="dateRow">
                    <td>{{ $date }} @if ($data->count() > 0)<i class="icon fa fa-caret-right"></i>@endif</td>
                    <td></td>
                    <td></td>
                    <td><strong class="headingAmount">{{ $report->totals[$date] }}</strong></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @forelse($data as $transaction)
                    @if (class_basename($transaction) == "AdditionalItem")
                        <tr class="subdata hidden">
                            @if ($transaction->booking_id)
                                <td><a class="loadBooking" data-bookingid="{{ $transaction->booking_id }}">
                                        &nbsp; {{ $transaction->created_at->toTimeString() }}</a></td>
                            @else
                                <td>&nbsp; {{ $transaction->created_at->toTimeString() }}</td>
                            @endif
                            <td>{{ $transaction->name }}</td>
                            <td>{{ $transaction->units }}</td>
                            <td>{{ $transaction->price/100 }}</td>
                            <td>{{ $transaction->total/100 }}</td>
                            <td></td>
                            <td>@if ($transaction->user) {{ $transaction->user->fullName() }}@endif</td>
                        </tr>
                    @elseif (class_basename($transaction) == "ActionLog")
                        <tr class="subdata hidden">
                            @if ($transaction->booking_id)
                                <td><a class="loadBooking" data-bookingid="{{ $transaction->booking_id }}">
                                        &nbsp; {{ $transaction->created_at->toTimeString() }}</a></td>
                            @else
                                <td>&nbsp; {{ $transaction->created_at->toTimeString() }}</td>
                            @endif
                            <td>{{ $transaction->type->name }}</td>
                            <td colspan="4"></td>
                            <td>@if ($transaction->user) {{ $transaction->user->fullName() }}@endif</td>
                        </tr>
                    @else
                        <tr class="subdata hidden">
                            @if ($transaction->booking_id)
                                <td><a class="loadBooking" data-bookingid="{{ $transaction->booking_id }}">
                                        &nbsp; {{ $transaction->created_at->toTimeString() }}</a></td>
                            @else
                                <td>&nbsp; {{ $transaction->created_at->toTimeString() }}</td>
                            @endif
                            <td>{{ $transaction->name }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{{ $transaction->total/100 }}</td>
                            <td>@if ($transaction->user) {{ $transaction->user->fullName() }}@endif</td>
                        </tr>
                    @endif
                @empty

                @endforelse

            @endforeach
            <tr class="totalRow dateRow">
                <th>Totals:</th>
                <td></td>
                <td></td>
                <td><strong class="grandtotal">{{ $report->grandtotal }}</strong></td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
@section('footer')
    @parent

    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').datetimepicker({
                inline: true,
                showOtherMonths: true,
                dateFormat: "dd-mm-yy",
                dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
            });
        });

        $("#dayViewAddOneMonth").click(function () {
            var originaldate = $("#datepicker-dayview").val().split("-");
            //remember that javascript date function uses '0' for january and '11' for december, so minus 1 from the month to account for this
            var date = new Date(originaldate[2], originaldate[1] - 1, originaldate[0]);
            date.setMonth(date.getMonth() + 1);
            $('#datepicker-dayview').datepicker("setDate", date);
        });

        $("#dayViewMinusOneMonth").click(function () {
            var originaldate = $("#datepicker-dayview").val().split("-");
            //remember that javascript date function uses '0' for january and '11' for december, so minus 1 from the month to account for this
            var date = new Date(originaldate[2], originaldate[1] - 1, originaldate[0]);
            date.setMonth(date.getMonth() - 1);
            $('#datepicker-dayview').datepicker("setDate", date);
        });

        $(".dateRow td").click(function () {
            if ($(this).hasClass('expanded')) {
                $(this).children('.icon').addClass('fa-caret-right').removeClass('fa-caret-down');
                $(this).parent().nextUntil('.dateRow').addClass('hidden');
                $(this).removeClass('expanded');
            } else {
                $(this).children('.icon').removeClass('fa-caret-right').addClass('fa-caret-down');
                $(this).parent().nextUntil('.dateRow').removeClass('hidden');
                $(this).addClass('expanded');
            }
        });

        $('.payment-mode').click(function (event) {
            $(this).find('*').removeClass('active');
            $(event.target).addClass('active');
            $("#mode").val($(event.target).data('mode'));
        });
    </script>
@endsection