@extends('layouts.backend')

@section('meta-title', 'HostelPro - Revenue Report')

@section('page-id', 'reports')

@section('content')

    <div class="row m-t-md">
        <div class="col-lg-12">
            @include('partials.daterangepicker-inline')
        </div>
    </div>

    @include('includes.reportssidebar')

    <div id="content-wrapper" class="m-t-md">

        <div class="row">
            <div class="col-sm-12">

                <table class="table table-condensed dayView" id="expenseTable">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Category</th>
                        <th>Expense</th>
                        <th>Amount</th>
                        <th>Added By</th>
                        <th>Added At</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($expenses as $expense)
                        <tr id="{{ $expense['id'] }}">
                            <td>{{ $expense['date'] }}</td>
                            <td>{{ $expense['category'] }}</td>
                            <td>{{ $expense['name'] }}</td>
                            <td>{{ $expense->prettyPrice }}</td>
                            <td>{{ $expense->user->fullName() }}</td>
                            <td>{{ $expense['created_at'] }}</td>
                            <td>
                                <button class="btn btn-xs btn-danger deleteExpense"
                                        data-expenseid="{{ $expense['id'] }}"><i class="fa fa-trash-o"></i>&nbsp; Delete
                                </button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">There are no expenses for the dates you specified.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>


            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                @include('partials.addexpense')
            </div>
        </div>


    </div>
@endsection
@section('footer')
    @parent

    <script type="text/javascript">
        $(document).ready(function () {

            $(document).on('click', '.deleteExpense', function () {
                confirmDeletePopup('{!! route('expense.destroy', '') !!}', $(this).data("expenseid"), 'DELETE');
            });

            function confirmDeletePopup($type, $id, $requesttype) {
                noty({
                    text: 'Are you sure you want to delete this item?',
                    theme: 'relax',
                    layout: 'center',
                    modal: true,
                    animation: {
                        open: {height: 'toggle'}, // jQuery animate function property object
                        close: {height: 'toggle'}, // jQuery animate function property object
                        easing: 'swing', // easing
                        speed: 150 // opening & closing animation speed
                    },
                    buttons: [
                        {
                            addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            deleteItem($type, $id, $requesttype);
                        }
                        },
                        {
                            addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                            $noty.close();
                        }
                        }
                    ]
                });
            };

            function deleteItem($type, $id, $requesttype) {
                $.ajax({
                    url: $type + "/" + $id,
                    type: $requesttype,
                    success: function (response) {
                        var tr = $("#" + $id);
                        tr.css("background-color", "#EF5C5C");
                        tr.fadeOut(400, function () {
                            tr.remove();
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        createErrorModal(xhr.responseText);
                    }
                });
            };

        });
    </script>

@endsection