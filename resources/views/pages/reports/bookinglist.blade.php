@extends('layouts.backend')

@section('meta-title', 'HostelPro - Booking List Report')

@section('page-id', 'reports')

@section('content')

  @include('includes.reportssidebar')

  <div id="content-wrapper" class="m-t-md">

    <table id="booking-list-table" class="table table-striped" width="100%">
      <thead>
      <tr>
        <th>ID</th>
        <th>Date</th>
        <th>Status</th>
        <th>Last Name</th>
        <th>First Name</th>
        <th>Email</th>
        <th>Address</th>
        <th>Invoice #</th>
        <th>Country</th>
        <th>Passport Number</th>
        <th>Passport Country</th>
        <th>Place of Birth</th>
        <th>Passport Issued</th>
        <th>Passport Expired</th>
        <th># Guests</th>
        <th># Nights</th>
        <th>Actual $</th>
        <th>Commission</th>
        <th>Channel</th>
        <th>Created</th>
        <th>Updated</th>
      </tr>
      </thead>
      <tbody>

      </tbody>
      <tfoot>

      </tfoot>
    </table>

    <div id="custom-toolbar">
      <div class="input-group">
        <input id="booking-list-startdate" type="text" class="form-control input-sm datepicker" value="{{ $start }}">
        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
      </div>
      &nbsp; to &nbsp;
      <div class="input-group">
        <input id="booking-list-enddate" type="text" class="form-control input-sm datepicker" value="{{ $end }}">
        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
      </div>
      {!! Form::select('booking-list-status', $statuses, null, array('id' => 'booking-list-status', 'multiple' => 'multiple', 'size' => '1', 'class' => 'form-control')) !!}
      {!! Form::select('booking-list-channel', $channels, null, array('id' => 'booking-list-channel', 'multiple' => 'multiple', 'size' => '1', 'class' => 'form-control')) !!}
    </div>



    </div>
@endsection
@section('footer')
  @parent

  <script type="text/javascript">
    $(document).ready(function() {
      $('#booking-list-table').DataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        scrollX: true,
        dom: '<"#booking-list-top-buttons"B>l<"datatable-custom-toolbar">frtBip',
        order: [
          [19, "desc"]
        ],
        fixedHeader: true,
        ajax: {
          url: '{{ route('reports.bookinglistdata') }}',
          data: function (d) {
            d.startdate = $('#booking-list-startdate').val();
            d.enddate = $('#booking-list-enddate').val();
            d.channel = $('#booking-list-channel').val();
            d.status = $('#booking-list-status').val();
          }
        },
        columns: [
          {data: 'id', name: 'booking.id', searchable: false},
          {data: 'date', name: 'date', searchable: false},
          {data: 'status', name: 'booking.status', searchable: false},
          {data: 'lastname', name: 'guest.lastname', searchable: true},
          {data: 'firstname', name: 'guest.firstname', searchable: true},
          {data: 'email', name: 'guest.email', searchable: true, visible: false},
          {data: 'address_line_1', name: 'guest.address_line_1', searchable: true, visible: false},
          {data: 'invoice_number', name: 'guest.invoice_number', searchable: true, visible: false},
          {data: 'guestcountry', name: 'guestcountry', searchable: false, visible: false},
          {data: 'passport_number', name: 'guest.passport_number', searchable: true, visible: false},
          {data: 'passportcountry', name: 'passportcountry', searchable: false, visible: false},
          {data: 'place_of_birth', name: 'guest.place_of_birth', searchable: true, visible: false},
          {data: 'passport_issue_date', name: 'guest.passport_issue_date', searchable: true, visible: false},
          {data: 'passport_expiry_date', name: 'guest.passport_expiry_date', searchable: true, visible: false},
          {data: 'number_of_guests', name: 'number_of_guests', searchable: false},
          {data: 'nights', name: 'nights', searchable: false},
          {data: 'payment_actual', name: 'payment_actual', searchable: false},
          {data: 'commission_amount', name: 'commission_amount', searchable: false},
          {data: 'myallocator_channel_id', name: 'booking.myallocator_channel_id', searchable: false},
          {data: 'created_at', name: 'booking.created_at', searchable: false},
          {data: 'updated_at', name: 'booking.updated_at', searchable: false}

        ],
        buttons: [
          {
            extend: 'copyHtml5',
            className: 'btn btn-default',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdfHtml5',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'csvHtml5',
            text: 'Excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          'colvis'
        ],
        lengthMenu: [10, 25, 50, 100, 250, 1000],
        pageLength: 100,
        pagingType: "full_numbers"
      });

      $('#custom-toolbar').appendTo('div.datatable-custom-toolbar');

      $('.datepicker').datepicker({
        inline: true,
        showOtherMonths: true,
        dateFormat: "dd-mm-yyyy",
        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
      });

      $('#booking-list-startdate').on('change', function (e) {
        e.preventDefault();
        $('#booking-list-table').DataTable().draw();
      });

      $('#booking-list-enddate').on('change', function (e) {
        e.preventDefault();
        $('#booking-list-table').DataTable().draw();
      });

      $('#booking-list-status').on('change', function (e) {
        e.preventDefault();
        $('#booking-list-table').DataTable().draw();
      }).select2({
        placeholder: "Any Status"
      });

      $('#booking-list-channel').on('change', function (e) {
        e.preventDefault();
        $('#booking-list-table').DataTable().draw();
      }).select2({
        placeholder: "Any Channel"
      })


    });
  </script>

@endsection