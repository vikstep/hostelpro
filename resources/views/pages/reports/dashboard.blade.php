@extends('layouts.backend')

@section('meta-title', 'HostelPro - Dashboard')

@section('page-id', 'reports')

@section('content')

    <div class="row m-t-md">
        <div class="col-lg-12">
            @include('partials.daterangepicker-inline')
        </div>
    </div>

    @include('includes.reportssidebar')

    <div id="content-wrapper" class="m-t-md">

        <div class="panel-footer bg-light lter wrapper">
            <div class="row">
                <div class="col-xs-4">
                    <small class="text-muted text-center block">Bookings</small>
                    <span class="text-lg text-center block">{{ $report->grand_totals['Allocated'] }}</span>
                </div>
                <div class="col-xs-4">
                    <small class="text-muted text-center block">Cancellations</small>
                    <span class="text-lg text-center block">{{ $report->grand_totals['Cancelled'] }}</span>
                </div>
                <div class="col-xs-4">
                    <small class="text-muted text-center block">No-Shows</small>
                    <span class="text-lg text-center block">{{ $report->grand_totals['NoShow'] }}</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-4">
                <h3 class="text-center">Bookings</h3>

                <div id="bookingsourcechart"></div>
            </div>
            <div class="col-sm-12 col-md-4">
                <h3 class="text-center">Accommodation Revenue</h3>

                <div id="revenuepiechart"></div>
            </div>
            <div class="col-sm-12 col-md-4">
                <h3 class="text-center">Revenue vs. Commission</h3>

                <div id="revenuecommissionchart"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-4">
                <h3 class="text-center">Lead Time</h3>

                <div id="leadtimechart"></div>
            </div>
            <div class="col-sm-12 col-md-4">
                <h3 class="text-center">Occupancy</h3>

                <div id="occupancychart"></div>
            </div>
            <div class="col-sm-12 col-md-4">
                <h3 class="text-center">Bookings by Channel</h3>

                <div id="bookingsbychannelchart"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-4">
                <h3 class="text-center"># Bookings per day</h3>

                <div id="bookingsperdaychart"></div>
            </div>
        </div>

        <div class="row m-t-lg">
            <div class="col-sm-12">
                <table class="table table-hover" id="dashboard-table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Bookings</th>
                        <th>Cancellations</th>
                        <th>NoShows</th>
                        <th>Total Bed Nights</th>
                        <th>Avg. Stay</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($report->channel_totals as $channel => $totals)
                        <tr>
                            <th>{{ $channel }}</th>
                            <td>{{ $totals['Allocated'] }} <span
                                        class="dashboard-percentage">({{ $report->channel_percentages[$channel]['Allocated'] }}
                                    %)</span></td>
                            <td>{{ $totals['Cancelled'] }} <span
                                        class="dashboard-percentage">({{ $report->channel_percentages[$channel]['Cancelled'] }}
                                    %)</span></td>
                            <td>{{ $totals['NoShow'] }} <span
                                        class="dashboard-percentage">({{ $report->channel_percentages[$channel]['NoShow'] }}
                                    %)</span></td>
                            <td>{{ $totals['TotalBedNights'] }}</td>
                            <td>{{ $totals['AvgStay'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <hr>

        <div class="row m-t-lg">
            <div class="col-sm-12">
                <table class="table table-hover" id="dashboard-table">
                    <thead>
                    <tr>
                        <th>Lead Time</th>
                        <th>Same Day</th>
                        <th>1-7 Days</th>
                        <th>8-14 Days</th>
                        <th>15-30 Days</th>
                        <th>31-60 Days</th>
                        <th>61+ Days</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($report->channels as $channel)
                        <tr>
                            <th>{{ $channel }}</th>
                            <td>{{ $report->lead_time_average[$channel]['Same Day']}}%</td>
                            <td>{{ $report->lead_time_average[$channel]['1-7 Days']}}%</td>
                            <td>{{ $report->lead_time_average[$channel]['8-14 Days']}}%</td>
                            <td>{{ $report->lead_time_average[$channel]['15-30 Days']}}%</td>
                            <td>{{ $report->lead_time_average[$channel]['31-60 Days']}}%</td>
                            <td>{{ $report->lead_time_average[$channel]['61+ Days']}}%</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
@section('footer')
    @parent

    <script type="text/javascript">
        var chart = c3.generate({
            bindto: '#bookingsourcechart',
            data: {
                columns: [
                        @foreach($report->booking_sources as $channel => $value)
                        ['{{ $channel }}', {{ $value }}],
                    @endforeach
            ],
                type: 'pie'
            },
            pie: {
                label: {
                    format: function (value, ratio, id) {
                        return value;
                    }
                }
            }
        });

        var chart2 = c3.generate({
            bindto: '#revenuepiechart',
            data: {
                columns: [
                        @foreach($report->accommodation_revenue as $channel => $value)
                        ['{{ $channel }}', {{ $value }}],
                    @endforeach
            ],
                type: 'pie'
            },
            pie: {
                label: {
                    format: function (value, ratio, id) {
                        return ("$" + value / 100);
                    }
                }
            }
        });

        var chart3 = c3.generate({
            bindto: '#revenuecommissionchart',
            data: {
                columns: [
                    ['Revenue',
                        @foreach($report->accommodation_revenue as $revenue)
                        {{ $revenue }},
                        @endforeach
                            ],
                    ['Commission',
                        @foreach($report->commission as $commission)
                        {{ $commission }},
                        @endforeach
                    ]
                ],
                type: 'bar',
                colors: {
                    Revenue: '#2ca02c',
                    Commission: '#d62728'
                },
                groups: [
                    ['Revenue', 'Commission']
                ]
            },
            axis: {
                x: {
                    type: 'category',
                    categories: [
                        @foreach($report->channels as $channel)
                         '{{ $channel }}',
                        @endforeach
                    ]
                },
                y: {
                    fit: true,
                    culling: {
                        max: 4
                    },
                    tick: {
                        rotate: 75,
                        //format: d3.format("$")
                        format: function (d) {
                            return ("$" + d / 100);
                        }
                    }
                }
            }
        });

        var chart4 = c3.generate({
            bindto: '#leadtimechart',
            data: {
                columns: [
                        @foreach($report->lead_times as $channel => $days)
                         [' {{ $channel }} '
                        @foreach($days as $value)
                        , {{ $value }}
                            @endforeach
                        ],
                    @endforeach
            ],
                type: 'spline'
            },
            spline: {
                interpolation: {
                    type: 'monotone'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                },
                y: {
                    min: 1,
                    label: 'number of days'
                }
            }
        });

        var chart5 = c3.generate({
            bindto: '#occupancychart',
            data: {
                columns: [
                    ['Occupancy'
                        @foreach($report->occupancy as $room_type => $percentage)
                        , {{  $percentage }}
                    @endforeach
                    ]
                ],
                type: 'bar',
                colors: {
                    Occupancy: '#A5668B'
                }
            },
            axis: {
                rotated: true,
                x: {
                    type: 'category',
                    categories: [
                        @foreach($report->occupancy as $room_type => $percentage)
                        '{{ $room_type }}',
                        @endforeach
                    ]
                },
                y: {
                    fit: true,
                    min: 0,
                    max: 100,
                    padding: {top: 0, bottom: 0},
                    tick: {
                        format: function (d) {
                            return (d + "%");
                        }
                    }
                }
            }
        });


        var chart6 = c3.generate({
            bindto: '#bookingsbychannelchart',
            data: {
                columns: [
                    ['Bookings',
                        @foreach($report->channels as $channel)
                        {{ $report->channel_percentages[$channel]['Allocated'] }},
                        @endforeach
                            ],
                    ['Cancellations',
                        @foreach($report->channels as $channel)
                        {{ $report->channel_percentages[$channel]['Cancelled'] }},
                        @endforeach
                            ],
                    ['NoShows',
                        @foreach($report->channels as $channel)
                        {{ $report->channel_percentages[$channel]['NoShow'] }},
                        @endforeach
                            ]
                ],
                type: 'bar',
                colors: {
                    Bookings: '#003049',
                    Cancellations: '#d62728',
                    NoShows: '#E35F60'
                },
                groups: [
                    ['Bookings', 'Cancellations', 'NoShows']
                ]
            },
            axis: {
                x: {
                    type: 'category',
                    categories: [
                        @foreach($report->channels as $channel)
                         '{{ $channel }}',
                        @endforeach
                    ]
                },
                y: {
                    fit: true,
                    min: 0,
                    max: 100,
                    padding: {top: 0, bottom: 0},
                    tick: {
                        format: function (d) {
                            return (d + "%");
                        }
                    }
                }
            }
        });

        var chart7 = c3.generate({
            bindto: '#bookingsperdaychart',
            data: {
                columns: [
                        @foreach($report->bookings_per_day as $channel => $days)
                         ['{{ $channel }}'
                        @foreach($days as $value)
                        , {{ $value }}
                            @endforeach
                        ],
                    @endforeach
            ],
                type: 'area',
                groups: [
                    [
                        @foreach($report->channels as $channel)
                        '{{ $channel }}',
                        @endforeach
                    ]
                ]
            },
            spline: {
                interpolation: {
                    type: 'monotone'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                },
                y: {
                    label: 'number of bookings'
                }
            }
        });

    </script>

@endsection