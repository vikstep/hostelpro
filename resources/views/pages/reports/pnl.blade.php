@extends('layouts.backend')

@section('meta-title', 'HostelPro - Profit and Loss Report')

@section('page-id', 'reports')

@section('content')

    <div class="row m-t-md">
        <div class="col-sm-12">
            <div class="btn-group dropdown">
                <button class="btn btn-default" data-toggle="dropdown" aria-expanded="false">{{ $years[0] }} <span
                            class="caret"></span></button>
                <ul class="dropdown-menu">
                    @foreach ($years as $year)
                        <li><a href="?year={{ $year }}">{{ $year }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    @include('includes.reportssidebar')

    <div id="content-wrapper" class="m-t-md">

        <div class="row">
            <div class="col-lg-12">

                <table class="table table-condensed dayView">
                    <col style="width:10%">
                    <thead>
                    <tr>
                        <th></th>
                        @foreach ($report->months as $month)
                            <th>{{ $month }}</th>
                        @endforeach
                        <th>YTD</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="dateRow noCursor">
                        <th><p class="pnlTableHeading">Revenue</p></th>
                        @foreach ($report->revenue as $amount)
                            <td><p class="pnlTableHeading">{{ $amount }}</p></td>
                        @endforeach
                        <td><p class="pnlTableHeading">{{ $report->yearToDate['revenue'] }}</p></td>
                    </tr>
                    <tr class="dateRow noCursor">
                        <th><p class="pnlTableHeading">Expenses</p></th>
                        @foreach ($report->expenseTotals as $amount)
                            <td><p class="pnlTableHeading">{{ $amount }}</p></td>
                        @endforeach
                        <td><p class="pnlTableHeading">{{ $report->yearToDate['expenses'] }}</p></td>
                    </tr>
                    @foreach ($report->expenses as $category => $expenses)
                        <tr class="dateRow noCursor">
                            <th> {{ $category }}</th>
                            @foreach ($expenses as $expense)
                                <td>{{ $expense }}</td>
                            @endforeach
                            <td></td>
                        </tr>
                    @endforeach

                    <tr class="dateRow noCursor">
                        <th><p class="pnlTableHeading">Gross Profit</p></th>
                        @foreach ($report->grossprofit as $amount)
                            <td><p class="pnlTableHeading">{{ $amount }}</p></td>
                        @endforeach
                        <td><p class="pnlTableHeading">{{ $report->yearToDate['gross'] }}</p></td>

                    </tr>

                    </tbody>
                </table>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div id="pnlchart"></div>
            </div>
        </div>

    </div>
@endsection
@section('footer')
    @parent

    <script type="text/javascript">
        $(function () {
            $('.datepicker').datepicker({
                inline: true,
                showOtherMonths: true,
                dateFormat: "dd-mm-yy",
                dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
            });
        });

        var chart = c3.generate({
            bindto: '#pnlchart',
            data: {
                columns: [
                    ['Revenue',
                        @foreach($report->revenueRaw as $amount)
                        {{ $amount }},
                        @endforeach
                        ],
                    ['Expenses',
                        @foreach($report->expensetotalsRaw as $amount)
                        {{ $amount }},
                        @endforeach
                        ],
                    ['Profit',
                        @foreach($report->grossprofitRaw as $amount)
                        {{ $amount }},
                        @endforeach
                      ]
                ],
                types: {
                    Revenue: 'bar',
                    Expenses: 'bar',
                    Profit: 'line'
                },
                colors: {
                    //data1: '#dcdcdc',
                    Revenue: '#2ca02c',
                    Expenses: '#d62728',
                    Profit: '#36404a'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: [
                        @foreach($report->months as $month)
                        '{{ $month }}',
                        @endforeach
                          ]
                }
            }
        });

        $("#dayViewAddOneMonth").click(function () {
            var originaldate = $("#datepicker-dayview").val().split("-");
            //remember that javascript date function uses '0' for january and '11' for december, so minus 1 from the month to account for this
            var date = new Date(originaldate[2], originaldate[1] - 1, originaldate[0]);
            date.setMonth(date.getMonth() + 1);
            $('#datepicker-dayview').datepicker("setDate", date);
        });

        $("#dayViewMinusOneMonth").click(function () {
            var originaldate = $("#datepicker-dayview").val().split("-");
            //remember that javascript date function uses '0' for january and '11' for december, so minus 1 from the month to account for this
            var date = new Date(originaldate[2], originaldate[1] - 1, originaldate[0]);
            date.setMonth(date.getMonth() - 1);
            $('#datepicker-dayview').datepicker("setDate", date);
        });

        $(".dateRow td").click(function () {
            if ($(this).hasClass('expanded')) {
                $(this).children('.icon').addClass('fa-caret-right').removeClass('fa-caret-down');
                $(this).parent().nextUntil('.dateRow').addClass('hidden');
                $(this).removeClass('expanded');
            } else {
                $(this).children('.icon').removeClass('fa-caret-right').addClass('fa-caret-down');
                $(this).parent().nextUntil('.dateRow').removeClass('hidden');
                $(this).addClass('expanded');
            }
        });
    </script>

@endsection