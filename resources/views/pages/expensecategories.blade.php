@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings - Expense Categories')

@section('page-id', 'settings')

@section('content')
    @include('includes.settingssidebar')

    <div id="content-wrapper" class="m-t-md">


        <div class="row">
            <div class="col-lg-6">
                <table class="table table-condensed category">
                    <thead>
                    <th width="50%">Name</th>
                    <th width="50%">Remove</th>
                    </thead>
                    <tbody>
                    @if (count($expenseCategories))
                        @include('partials.expensecategory')
                    @else
                        <tr id="nocategory">
                            <td>No categories have been added.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <p>
                    <button class="btn m-b-xs btn-sm btn-primary btn-addon" id="addcategory"><i class="fa fa-plus"></i>Add
                        Category
                    </button>
                </p>

            </div>
        </div>

    </div>
@endsection
@section('footer')
    @parent

    <script type="text/javascript">
        $(document).ready(function () {

            var newMoneyBlock = '<tr>' +
                    '<td class="v-middle"><input type="text" class="form-control categoryname" name="categoryname" placeholder="eg Utilities"> </td>' +
                    '<td>' +
                    '<button class="btn btn-sm btn-info saveExpense"><i class="fa fa-floppy-o"></i>&nbsp; Save</button>' +
                    '<button class="btn btn-sm btn-danger deleteRow"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>' +
                    '</td>' +
                    '</tr>';

            $("#addcategory").click(function () {
                $(".category tbody").append(newMoneyBlock);
                $("#nocategory").addClass("hidden");
            });

            $(document).on('click', '.saveExpense', function () {
                var data = {};
                data.categoryname = $(this).parent().parent().find('.categoryname').val();
                //var $data = [$name, $roomtype, $gender, $capacity];
                //console.log( JSON.stringify(data) );
                saveItem('{!! route('settings.expensecategory.store') !!}', data, $(this));
            });

            $(document).on('click', '.deleteRow', function () {
                $(this).closest('tr').remove();
            });

            $(document).on('click', '.deleteExpense', function () {
                confirmDeletePopup('{!! route('settings.expensecategory.destroy', '') !!}', $(this).data("expenseid"), 'DELETE');
            });

            function confirmDeletePopup($type, $id, $requesttype) {
                noty({
                    text: 'Are you sure you want to delete this item?',
                    theme: 'relax',
                    layout: 'center',
                    modal: true,
                    animation: {
                        open: {height: 'toggle'}, // jQuery animate function property object
                        close: {height: 'toggle'}, // jQuery animate function property object
                        easing: 'swing', // easing
                        speed: 150 // opening & closing animation speed
                    },
                    buttons: [
                        {
                            addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            deleteItem($type, $id, $requesttype);
                        }
                        },
                        {
                            addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                            $noty.close();
                        }
                        }
                    ]
                });
            };

            function deleteItem($type, $id, $requesttype) {
                $.ajax({
                    url: $type + "/" + $id,
                    type: $requesttype,
                    success: function (response) {
                        $("table.category tbody").html(response);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        createErrorModal(xhr.responseText);
                    }
                });
            };

            function saveItem($type, $data, $row) {
                $.ajax({
                    url: $type,
                    type: 'POST',
                    data: $data,
                    success: function (response) {
                        $row.parent().parent().replaceWith(response);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        createErrorModal(xhr.responseText);
                    }
                });
            };


        });
    </script>
@endsection