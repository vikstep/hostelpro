@extends('layouts.backend')

@section('meta-title', 'HostelPro - Settings - Rooms')

@section('page-id', 'settings')

@section('content')

    @include('includes.settingssidebar')

    <div id="content-wrapper" class="m-t-md">

        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-lined mb30">
                    <div class="panel-heading font-bold">Room Type List</div>
                    <div class="panel-body">
                        @include('partials.roomtypes')
                        <p>
                            <button class="btn m-b-xs btn-sm btn-primary btn-addon" id="addroomtype"><i
                                        class="fa fa-plus"></i>Add Room Type
                            </button>

                            <a class="btn m-b-xs btn-sm btn-default btn-addon" href="{{ route('settings.myallocator.resync') }}"><i class="fa fa-refresh"></i>MyAllocator ReSync</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="panel panel-lined mb30">
                    <div class="panel-heading font-bold">Room List</div>
                    <div class="panel-body">
                        @include('partials.rooms')
                        <p>
                            <button class="btn m-b-xs btn-sm btn-primary btn-addon" id="addroom"><i
                                        class="fa fa-plus"></i>Add Room
                            </button>
                        </p>
                    </div>
                </div>
            </div>

        </div>


    </div>
@endsection
@section('footer')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {

            var newRoomTypeBlock = '<tr>' +
                    '<td class="v-middle"><input type="text" class="form-control name" name="name" placeholder="Name"> </td>' +
                    '<td class="v-middle"><select class="form-control type" name="type">' +
                    '<option value="Dorm">Dorm</option>' +
                    '<option value="Private">Private</option>' +
                    '</select> </td>' +
                    '<td class="v-middle"><select class="form-control gender" name="gender">' +
                    '<option value="Female">Female</option>' +
                    '<option value="Male">Male</option>' +
                    '<option value="Mixed">Mixed</option>' +
                    '</select></td>' +
                    '<td class="v-middle"> {!!  Form::selectRange("number_of_guests", 1, 30, 1, array("class" => "form-control number_of_guests")) !!} </td>' +
                    '<td class="v-middle"></td>' +
                    '<td class="v-middle">' +
                    '<button class="btn btn-sm btn-info saveRoomType"><i class="fa fa-floppy-o"></i>&nbsp; Save</button>' +
                    '<button class="btn btn-sm btn-danger deleteRow"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>' +
                    '</td>' +
                    '</tr>';

            roomTypeList = '{{ Form::select("room_type_id", Auth::user()->currenthostel->roomtypes->pluck("name", "id"), "", array("class" => "form-control room_type_id")) }}';

            $("#addroomtype").click(function () {
                $(".roomtypes tbody").append(newRoomTypeBlock);
                $("#noroomtypes").addClass("hidden");
                $(".roomtypes thead").removeClass("hidden");
            });

            $("#addroom").click(function () {
                $(".rooms tbody").append('<tr>' +
                        '<td class="v-middle"><input type="text" class="form-control name" name="name" placeholder="Name"> </td>' +
                        '<td class="v-middle">' + roomTypeList + '</td>' +
                        '<td class="v-middle">' +
                        '<button class="btn btn-sm btn-info saveRoom"><i class="fa fa-floppy-o"></i>&nbsp; Save</button>' +
                        '<button class="btn btn-sm btn-danger deleteRow"><i class="fa fa-trash-o"></i>&nbsp; Delete</button>' +
                        '</td>' +
                        '</tr>');
                $("#norooms").addClass("hidden");
                $(".rooms thead").removeClass("hidden");
            });

            $('#roomTable').sortable({
                axis: 'y',
                update: function (event, ui) {
                    var data = $(this).sortable('serialize');
                    data += '&_token={{ csrf_token() }}';

                    console.log(data);
                    $.ajax({
                        url: '{{ route('settings.RoomsSort') }}',
                        type: 'POST',
                        data: data
                    });

                }
            });
            //$('#roomTable').sortable().disableSelection();

            $(document).on('click', '.saveRoomType', function () {
                var data = {};
                data.name = $(this).parent().parent().find('.name').val();
                data.type = $(this).parent().parent().find('.type').val();
                data.gender = $(this).parent().parent().find('.gender').val();
                data.number_of_guests = $(this).parent().parent().find('.number_of_guests').val();
                //var $data = [$name, $roomtype, $gender, $capacity];
                //console.log( JSON.stringify(data) );
                saveItem('roomtypes', data, $(this));
            });

            $(document).on('click', '.saveRoom', function () {
                var data = {};
                data.name = $(this).parent().parent().find('.name').val();
                data.room_type_id = $(this).parent().parent().find('.room_type_id').val();
                //var $data = [$name, $roomtype, $gender, $capacity];
                //console.log( JSON.stringify(data) );
                saveItem('rooms', data, $(this));
            });

            $(document).on('click', '.saveEditRoom', function () {
                var data = {};
                data.name = $(this).parent().parent().find('.name').val();
                data.room_type_id = $(this).parent().parent().find('.room_type_id').val();
                $.ajax({
                    url: "rooms/" + $(this).data("roomid"),
                    type: "PUT",
                    data: data,
                    context: this,
                    success: function (response) {
                        $(this).closest('tr').html(response);
                    }
                });
            });


            $(document).on('click', '.deleteRow', function () {
                $(this).closest('tr').remove();
            });

            $(document).on('click', '.deleteRoomType', function () {
                confirmDeletePopup('roomtypes', $(this).data("roomtypeid"), 'DELETE');
            });

            $(document).on('click', '.deleteRoom', function () {
                confirmDeletePopup('rooms', $(this).data("roomid"), 'DELETE');
            });

            $(document).on('click', '.editRoom', function () {

                var roomid = $(this).data("roomid");

                $(this).closest('tr').load('rooms/' + roomid + '/edit');


                //confirmDeletePopup('rooms', $(this).data("roomid"), 'DELETE');
            });

            function confirmDeletePopup($type, $id, $requesttype) {
                noty({
                    text: 'Are you sure you want to delete this item?',
                    theme: 'relax',
                    layout: 'center',
                    modal: true,
                    animation: {
                        open: {height: 'toggle'}, // jQuery animate function property object
                        close: {height: 'toggle'}, // jQuery animate function property object
                        easing: 'swing', // easing
                        speed: 150 // opening & closing animation speed
                    },
                    buttons: [
                        {
                            addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            deleteItem($type, $id, $requesttype);
                        }
                        },
                        {
                            addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                            $noty.close();
                        }
                        }
                    ]
                });
            }

            function deleteItem($type, $id, $requesttype) {
                $.ajax({
                    url: $type + "/" + $id,
                    type: $requesttype,
                    data: {_token: '{!!  csrf_token() !!}'},
                    success: function (response) {
                        $("table." + $type).html(response);
                        if ($type == 'roomtypes') {
                            $.get('{{ route('settings.roomtypes.index') }}', function (data) {
                                updateRoomTypeList(data);
                            });
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        createErrorModal(xhr.responseText);
                    }
                });
            }

            function saveItem($type, $data, $row) {
                $.ajax({
                    url: $type,
                    type: 'POST',
                    data: $data,
                    success: function (response) {
                        $row.parent().parent().html(response);
                        if ($type == 'roomtypes') {
                            $.get('{{ route('settings.roomtypes.index') }}', function (data) {
                                updateRoomTypeList(data);
                            });
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        createErrorModal(xhr.responseText);
                    }
                });
            }

            function updateRoomTypeList(data) {
                roomTypeList = data;
            }


        });
    </script>


@endsection