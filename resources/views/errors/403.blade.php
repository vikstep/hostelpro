<html>
<head>
  <link href='https://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

  <style>
    body {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      color: #0C0C0C;
      display: table;
      font-weight: 100;
      font-family: 'Lato';
    }

    .container {
      text-align: center;
      display: table-cell;
      vertical-align: middle;
    }

    .content {
      text-align: center;
      display: inline-block;
    }

    .title {
      font-size: 72px;
      margin-bottom: 40px;
    }

    .subtitle {
      font-size: 36px;
      margin-bottom: 15px;
    }
  </style>
</head>
<body>
<div class="container">
  <div class="content">
    <div class="title">Permission Denied</div>
    <div class="subtitle">Sorry, you do not have access to the resource you requested.</div>
  </div>
</div>
</body>
</html>
