
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>HostelPro - Property Management System for Hostels</title>

    <meta name="description" content="HostelPro is an affordable Property Management System PMS, Point of Sale POS, and Channel Management with accounting and reports tailor made for hostels.">
    <meta name="author" content="HostelPro">

    <link href="{{ url('assets_landing_old/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('assets_landing_old/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ url('assets_landing_old/css/style.css') }}" rel="stylesheet">

</head>
<body>

<div class="main-container">

    <header class="header">
        <div class="row">
            <div class="container">
                <nav id="navbar-scrolling" class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                        </button>
                        <div class="logo">Hostel<strong>Pro</strong></div>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#features">Features</a></li>
                            <li><a href="#pricing">Pricing</a></li>
                            <li><a href="#switching">Switching</a></li>
                            <li><a href="#contact">Contact</a></li>
                            <li><a href="{{ route('login') }}" class="btn btn-default">Login</a></li>
                            <li><a href="{{ route('register') }}" class="btn btn-info">Free trial</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <div class="main-banner row">
        <div class="container text-center">
            <h1>The smarter way to manage your hostel.</h1>
            <p>Still using slow, clunky old software to run your hostel?</p>
            <p>HostelPro is new cloud-based software that you'll love to use.</p>
            <a href="#features" class="btn btn-lg btn-default">Learn more</a>
            <a href="{{ route('register') }}" type="button" class="btn btn-lg btn-info">Try for free</a>
            {{--<img class="img-responsive" src="{{ url('assets_landing/images/hostelpro.jpg') }}">--}}
            <div id="gif-container">
                <img id="demo-gif-image" class="img-responsive" src="{{ url('assets_landing/images/demo-gif-static.jpg') }}"
                     onmouseover="this.src='{{ url('assets_landing_old/images/demo-gif-static-hover.jpg') }}';" onmouseout="this.src='{{ url('assets_landing/images/demo-gif-static.jpg') }}'">
                <img id="demo-gif" class="img-responsive hidden" src="{{ url('assets_landing_old/images/demo.gif') }}">
            </div>
        </div>
    </div>

    <div id="features" class="streamline row">
        <div class="container">
            <h2 class="text-center">Streamline hostel management and optimise your bookings.</h2>
            <div class="well row">
                <div class="col-md-4">
                    <div class="title"><i class="fa fa-check-circle" aria-hidden="true"></i> Custom-built for hostels</div>
                    <p>HostelPro provides complete control with guest profiles, CRM, easy room changes, fast booking view/edit, and the ability to move beds or reservations just where you want them. You can even sell the same room as different room types.</p>
                </div>
                <div class="col-md-4">
                    <div class="title"><i class="fa fa-bolt" aria-hidden="true"></i> A next-generation solution</div>
                    <p>Don't spend 100s of euro on ancient software that takes days of training. Running from the cloud, HostelPro is a fraction of the price of completing products. You'll love the advantage of faster check-in times, and reports that give you a business overview in just a few clicks.</p>
                </div>
                <div class="col-md-4">
                    <div class="title"><i class="fa fa-refresh" aria-hidden="true"></i> MyAllocator.com integration</div>
                    <p>Most hostels are listed on multiple websites in order to maximise bookings. HostelPro is integrated with MyAllocator channel management, so you'll automatically receive bookings from any website your hostel is listed.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="features row">
        <div class="container">
            <h3 class="text-center">Features for the professional hostel</h3>
            <div class="row">
                <div class="col-md-4">
                    <div class="title"><i class="fa fa-bed" aria-hidden="true"></i> Bed Allocation</div>
                    <p>No more trying to fit bookings in only to have to move them later. HostelPro's Smart Allocation keep groups together and fill rooms in the most optimal way. Keep rooms full, and minimise the number of guests changing beds.</p>
                </div>
                <div class="col-md-4">
                    <div class="title"><i class="fa fa-money" aria-hidden="true"></i> Integrated Point of Sale</div>
                    <p>HostelPro has integrated Point of Sale (POS) and will work with thermal printers and cash drawers for improved money handling, from reception to management.</p>
                </div>
                <div class="col-md-4">
                    <div class="title"><i class="fa fa-smile-o" aria-hidden="true"></i> Customer Relationship Management</div>
                    <p>Welcome guests prior to their arrival with an automated email that provides useful local information. When guests check out, a second email prompts guests to rate your hostel on the website they booked through.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="title"><i class="fa fa-cloud" aria-hidden="true"></i> Works Anywhere</div>
                    <p>Login from anywhere on your desktop PC, laptop or tablet, and instantly see what is happening at any number of hostel locations. Your data is secure and backed up automatically.</p>
                </div>
                <div class="col-md-4">
                    <div class="title"><i class="fa fa-bar-chart" aria-hidden="true"></i> Comprehensive Reports</div>
                    <p>Easy to understand reports allow you to track Yield, Profit & Loss, Booking Frequency per Booking Website, and much more.</p>
                </div>
                <div class="col-md-4">
                    <div class="title"><i class="fa fa-comments" aria-hidden="true"></i> Great Support</div>
                    <p>Our tech support is available by chat or phone. We really listen - your feedback helps us to improve and further develop HostelPro.</p>
                </div>
            </div>

            <div class="carousel slide" id="carousel-892244">
                <ol class="carousel-indicators">
                    <li data-slide-to="0" data-target="#carousel-892244" class="active">
                    </li>
                    <li data-slide-to="1" data-target="#carousel-892244">
                    </li>
                    <li data-slide-to="2" data-target="#carousel-892244">
                    </li>
                    <li data-slide-to="3" data-target="#carousel-892244">
                    </li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active left">
                        <img alt="Carousel Bootstrap First" src="{{ url('assets_landing_old/images/slide1.png') }}">
                    </div>
                    <div class="item next left">
                        <img alt="Carousel Bootstrap Second" src="{{ url('assets_landing_old/images/slide2.png') }}">
                    </div>
                    <div class="item">
                        <img alt="Carousel Bootstrap Third" src="{{ url('assets_landing_old/images/slide3.png') }}">
                    </div>
                    <div class="item">
                        <img alt="Carousel Bootstrap Fourth" src="{{ url('assets_landing_old/images/slide4.png') }}">
                    </div>
                </div> <a class="left carousel-control" href="#carousel-892244" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-892244" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
    </div>
    <div id="pricing" class="plans row">
        <div class="container text-center">
            <h2>Simple Monthly Pricing</h2>
            <h3>No Contract</h3>

            <div class="row">
                <div class="col-sm-4 col-sm-offset-2">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th class="text-center">Number of Beds</th>
                        <th class="text-center">Price (Euros/Month)</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>0-5</td>
                            <td>&euro; 10.00</td>
                        </tr>
                        <tr>
                            <td>6-10</td>
                            <td>&euro; 12.00</td>
                        </tr>
                        <tr>
                            <td>11-15</td>
                            <td>&euro; 14.00</td>
                        </tr>
                        <tr>
                            <td>16-20</td>
                            <td>&euro; 16.00</td>
                        </tr>
                        <tr>
                            <td>21-30</td>
                            <td>&euro; 21.00</td>
                        </tr>
                        <tr>
                            <td>31-40</td>
                            <td>&euro; 24.00</td>
                        </tr>
                        <tr>
                            <td>41-50</td>
                            <td>&euro; 27.00</td>
                        </tr>
                        <tr>
                            <td>51-60</td>
                            <td>&euro; 30.00</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-4">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th class="text-center">Number of Beds</th>
                        <th class="text-center">Price (Euros/Month)</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>61-70</td>
                            <td>&euro; 33.00</td>
                        </tr>
                        <tr>
                            <td>71-80</td>
                            <td>&euro; 36.00</td>
                        </tr>
                        <tr>
                            <td>81-100</td>
                            <td>&euro; 40.00</td>
                        </tr>
                        <tr>
                            <td>101-150</td>
                            <td>&euro; 55.00</td>
                        </tr>
                        <tr>
                            <td>151-200</td>
                            <td>&euro; 70.00</td>
                        </tr>
                        <tr>
                            <td>201-300</td>
                            <td>&euro; 90.00</td>
                        </tr>
                        <tr>
                            <td>301-500</td>
                            <td>&euro; 125.00</td>
                        </tr>
                        <tr>
                            <td>501-1000</td>
                            <td>&euro; 155.00</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>


            <a href="{{ route('register') }}" class="btn btn-info btn-lg">Try HostelPro for free</a>
            <p>30 Day free trial - No credit card required.</p>
        </div>
    </div>
    <div id="switching" class="switching row">
        <div class="container small-container">
            <h2 class="text-center">Switching to HostelPro is easy</h2>
            <dl>
                <dt><i class="fa fa-check" aria-hidden="true"></i> Work from your web browser</dt>
                <dd>HostelPro is cloud-based software. That means there is nothing to install and you simply login through your web browser. Your booking data is automatically backed-up on our secure servers.</dd>
                <dt><i class="fa fa-check" aria-hidden="true"></i> Get great Support</dt>
                <dd>We're more than happy to help if you need a hand setting up your account, or moving across bookings from another application. Typically, hostels make the transition to HostelPro with very little training or admin.</dd>
                <dt><i class="fa fa-check" aria-hidden="true"></i> Quick to learn, easy to use</dt>
                <dd>HostelPro is based on years of industry experience, and is so intuitive that receptionists, supervisors and managers will become proficient within minutes. Ease of use means reduced human error, and HostelPro's operational transparency makes employee theft nearly impossible.</dd>
                <dt><i class="fa fa-check" aria-hidden="true"></i> Online Training Videos</dt>
                <dd>Get your staff up to speed with HostelPro's video tutorials. The video content is easy to follow, and provides a clear walkthrough of common hostel activities, e.g. managing bookings, checking guests in/out, point of sale, and tracking keycards.</dd>
            </dl>
            <div class="learn row well hidden">
                <div class="container">
                    <h2>Learn how to streamline your hostel.</h2>
                    <p>Enter your email to receive HostelPro's free Product Brochure.</p>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <input type="email" class="form-control" id="inputEmail" placeholder="Your Email*">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="contact" class="question row">
        <div class="container small-container">
            <h2 class="text-center">Questions? We'd love to talk.</h2>
            {!! Form::open(['route' => 'landingpage.contact', 'id' => 'contactForm']) !!}
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Your Name *" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Your Email *" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="phone" placeholder="Your Phone">
                </div>
                <div class="form-group">
                    <textarea name="message" class="form-control" placeholder="Your Message *" required></textarea>
                </div>

                <button id="contactFormButton" type="submit" class="btn btn-info">Send Message</button>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="footer row">
        <div class="container">
            <div class="row">
                <div class="col-md-4">&copy; HostelPro 2016.</div>
                <div class="socials text-center col-md-4">
                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-4 text-right">
                    {{--<a href="#">Privacy Policy</a>--}}
                    {{--<a href="#">Terms of use</a>--}}
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ url('assets_landing_old/js/jquery.min.js') }}"></script>
<script src="{{ url('assets_landing_old/js/bootstrap.min.js') }}"></script>
<script src="{{ url('assets_landing_old/js/scripts.js') }}"></script>
</body>
</html>