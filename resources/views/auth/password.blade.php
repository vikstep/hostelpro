@extends('layouts.backend')

@section('meta-title', 'HostelPro - Reset Password')

@section('page-id', 'login')

@section('content')

    <div class="app ng-scope app-header-fixed app-aside-fixed vertical-center-container">
        <div class="container w-xxl w-auto-xs vertical-center">

            <a href="" class="navbar-brand block m-t ng-binding">Hostel<span>&nbsp; Pro</span></a>

            <div class="m-b-lg">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="/password/email">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="m-b-lg">
                        <div class="wrapper text-center">
                            <strong>Input your email to reset your password</strong>
                        </div>
                        <div class="list-group list-group-sm">
                            <div class="list-group-item">
                                <input type="email" name="email" placeholder="Email"
                                       class="form-control no-border ng-pristine ng-valid-email ng-invalid ng-invalid-required ng-touched"
                                       required="" aria-required="true" aria-invalid="true" style="">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-lg btn-primary btn-block">Send Password Reset Link</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
@endsection