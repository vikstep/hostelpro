<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="HostelPro">
  <meta name="author" content="HostelPro">
  <!-- <link rel="icon" href="favicon.ico"> -->
  <title>@yield('meta-title', 'HostelPro')</title>
  <link rel="stylesheet" href="{{ url(elixir('assets/css/all.css')) }}">
</head>
@if (Route::currentRouteName() != "calendar")
<body>
@else
  <body style="background-color: #f0f3f4">
@endif
@section('header')
  @if (Auth::user())
    @include('includes.standardheader')
    @include('includes.dashboardsidebar')
    @include('includes.modaldiv')
  @endif
@show
<div class="content-container" id="@yield('page-id')">
  @include('includes.flashmessagediv')
  @include('includes.setupwidget')
  @yield('content')
  @include('includes.footer')
</div>
@section('footer')
  @if (Auth::user())
    <script src="{{ url(elixir('assets/js/all.js')) }}"></script>
    @include('includes.sidebarscript')
    @include('includes.footerscripts')
  @endif
@show
</body>
</html>