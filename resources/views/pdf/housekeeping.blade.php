<html>
<head>
  <link rel="stylesheet" href="{{ url(elixir('assets/css/all.css')) }}">
</head>
<body class="pdf">


<h1 class="text-center">Housekeeping for {{ Auth::user()->currenthostel->name }}</h1>
<p class="text-center">{{ $report->formatted_date }}</p>

<table width="100%" class="pdftable m-t-lg">
  <thead>
  <tr>
    <td width="35%">Room</td>
    <td width="7%" class="text-center">Arr</td>
    <td width="7%" class="text-center">Dep</td>
    <td width="7%" class="text-center">Rem</td>
    <td width="7%" class="text-center">NiU</td>
    <td width="7%" class="text-center">Free</td>
    <td width="30%" class="text-center">Notes</td>
  </tr>
  </thead>
  <tobdy>
    @foreach ($report->rooms as $room)
      <tr>
        <td>{{ $room->name }}</td>
        <td class="text-center">{{ $report->data[$room->id]['arr'] }}</td>
        <td class="text-center">{{ $report->data[$room->id]['dep'] }}</td>
        <td class="text-center">{{ $report->data[$room->id]['rem'] }}</td>
        <td class="text-center">{{ $report->data[$room->id]['niu'] }}</td>
        <td class="text-center">{{ $report->data[$room->id]['free'] }}</td>
        <td></td>
      </tr>
    @endforeach
  </tobdy>
</table>

<p class="text-center">Arr = Arrivals, Dep = Departures, Rem = Remainings, NiU = Not in Use or Blocked</p>


</body>
</html>