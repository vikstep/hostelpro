-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2015 at 04:19 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hostelpro`
--

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(3) NOT NULL,
  `currency_name` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=164 ;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_code`, `currency_name`) VALUES
(1, 'AED', 'Dirham'),
(2, 'AFN', 'Afghani'),
(3, 'ALL', 'Lek'),
(4, 'AMD', 'Dram'),
(5, 'ANG', 'Guilder'),
(6, 'AOA', 'Kwanza'),
(7, 'ARS', 'Peso'),
(8, 'AUD', 'Dollar'),
(9, 'AWG', 'Guilder'),
(10, 'AZN', 'Manat'),
(11, 'BAM', 'Marka'),
(12, 'BBD', 'Dollar'),
(13, 'BDT', 'Taka'),
(14, 'BGN', 'Lev'),
(15, 'BHD', 'Dinar'),
(16, 'BIF', 'Franc'),
(17, 'BMD', 'Dollar'),
(18, 'BND', 'Dollar'),
(19, 'BOB', 'Boliviano'),
(20, 'BRL', 'Real'),
(21, 'BSD', 'Dollar'),
(22, 'BTN', 'Ngultrum'),
(23, 'BWP', 'Pula'),
(24, 'BYR', 'Ruble'),
(25, 'BZD', 'Dollar'),
(26, 'CAD', 'Dollar'),
(27, 'CDF', 'Franc'),
(28, 'CHF', 'Franc'),
(29, 'CLP', 'Peso'),
(30, 'CNY', 'Yuan Renminbi'),
(31, 'COP', 'Peso'),
(32, 'CRC', 'Colon'),
(33, 'CUP', 'Peso'),
(34, 'CVE', 'Escudo'),
(35, 'CYP', 'Pound'),
(36, 'CZK', 'Koruna'),
(37, 'DJF', 'Franc'),
(38, 'DKK', 'Krone'),
(39, 'DOP', 'Peso'),
(40, 'DZD', 'Dinar'),
(41, 'EEK', 'Kroon'),
(42, 'EGP', 'Pound'),
(43, 'ERN', 'Nakfa'),
(44, 'ETB', 'Birr'),
(45, 'EUR', 'Euro'),
(46, 'FJD', 'Dollar'),
(47, 'FKP', 'Pound'),
(48, 'GBP', 'Pound'),
(49, 'GEL', 'Lari'),
(50, 'GGP', 'Pound'),
(51, 'GHC', 'Cedi'),
(52, 'GIP', 'Pound'),
(53, 'GMD', 'Dalasi'),
(54, 'GNF', 'Franc'),
(55, 'GTQ', 'Quetzal'),
(56, 'GYD', 'Dollar'),
(57, 'HKD', 'Dollar'),
(58, 'HNL', 'Lempira'),
(59, 'HRK', 'Kuna'),
(60, 'HTG', 'Gourde'),
(61, 'HUF', 'Forint'),
(62, 'IDR', 'Rupiah'),
(63, 'ILS', 'Shekel'),
(64, 'IMP', 'Pound'),
(65, 'INR', 'Rupee'),
(66, 'IQD', 'Dinar'),
(67, 'IRR', 'Rial'),
(68, 'ISK', 'Krona'),
(69, 'JEP', 'Pound'),
(70, 'JMD', 'Dollar'),
(71, 'JOD', 'Dinar'),
(72, 'JPY', 'Yen'),
(73, 'KES', 'Shilling'),
(74, 'KGS', 'Som'),
(75, 'KHR', 'Riels'),
(76, 'KMF', 'Franc'),
(77, 'KPW', 'Won'),
(78, 'KRW', 'Won'),
(79, 'KWD', 'Dinar'),
(80, 'KYD', 'Dollar'),
(81, 'KZT', 'Tenge'),
(82, 'LAK', 'Kip'),
(83, 'LBP', 'Pound'),
(84, 'LKR', 'Rupee'),
(85, 'LRD', 'Dollar'),
(86, 'LSL', 'Loti'),
(87, 'LTL', 'Litas'),
(88, 'LVL', 'Lat'),
(89, 'LYD', 'Dinar'),
(90, 'MAD', 'Dirham'),
(91, 'MDL', 'Leu'),
(92, 'MGA', 'Ariary'),
(93, 'MKD', 'Denar'),
(94, 'MMK', 'Kyat'),
(95, 'MNT', 'Tugrik'),
(96, 'MOP', 'Pataca'),
(97, 'MRO', 'Ouguiya'),
(98, 'MTL', 'Lira'),
(99, 'MUR', 'Rupee'),
(100, 'MVR', 'Rufiyaa'),
(101, 'MWK', 'Kwacha'),
(102, 'MXN', 'Peso'),
(103, 'MYR', 'Ringgit'),
(104, 'MZM', 'Meticail'),
(105, 'NAD', 'Dollar'),
(106, 'NGN', 'Naira'),
(107, 'NIO', 'Cordoba'),
(108, 'NOK', 'Krone'),
(109, 'NPR', 'Rupee'),
(110, 'NZD', 'Dollar'),
(111, 'OMR', 'Rial'),
(112, 'PAB', 'Balboa'),
(113, 'PEN', 'Sol'),
(114, 'PGK', 'Kina'),
(115, 'PHP', 'Peso'),
(116, 'PKR', 'Rupee'),
(117, 'PLN', 'Zloty'),
(118, 'PYG', 'Guarani'),
(119, 'QAR', 'Rial'),
(120, 'RON', 'Leu'),
(121, 'RSD', 'Dinar'),
(122, 'RUB', 'Ruble'),
(123, 'RWF', 'Franc'),
(124, 'SAR', 'Rial'),
(125, 'SBD', 'Dollar'),
(126, 'SCR', 'Rupee'),
(127, 'SDD', 'Dinar'),
(128, 'SEK', 'Kronoa'),
(129, 'SGD', 'Dollar'),
(130, 'SHP', 'Pound'),
(131, 'SKK', 'Koruna'),
(132, 'SLL', 'Leone'),
(133, 'SOS', 'Shilling'),
(134, 'SRD', 'Dollar'),
(135, 'STD', 'Dobra'),
(136, 'SYP', 'Pound'),
(137, 'SZL', 'Lilangeni'),
(138, 'THB', 'Baht'),
(139, 'TJS', 'Somoni'),
(140, 'TMM', 'Manat'),
(141, 'TND', 'Dinar'),
(142, 'TOP', 'Pa''anga'),
(143, 'TRY', 'Lira'),
(144, 'TTD', 'Dollar'),
(145, 'TWD', 'Dollar'),
(146, 'TZS', 'Shilling'),
(147, 'UAH', 'Hryvnia'),
(148, 'UGX', 'Shilling'),
(149, 'USD', 'Dollar'),
(150, 'UYU', 'Peso'),
(151, 'UZS', 'Som'),
(152, 'VEB', 'Bolivar'),
(153, 'VND', 'Dong'),
(154, 'VUV', 'Vatu'),
(155, 'WST', 'Tala'),
(156, 'XAF', 'Franc'),
(157, 'XCD', 'Dollar'),
(158, 'XOF', 'Franc'),
(159, 'XPF', 'Franc'),
(160, 'YER', 'Rial'),
(161, 'ZAR', 'Rand'),
(162, 'ZMK', 'Kwacha'),
(163, 'ZWD', 'Dollar');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
