-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2015 at 11:43 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `do_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE IF NOT EXISTS `timezones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_code` char(2) CHARACTER SET utf8 NOT NULL,
  `timezone` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `timezone` (`timezone`),
  KEY `country_code` (`country_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=386 ;

--
-- Dumping data for table `timezones`
--

INSERT INTO `timezones` (`id`, `country_code`, `timezone`) VALUES
(1, 'CI', 'Africa/Abidjan'),
(2, 'GH', 'Africa/Accra'),
(3, 'ET', 'Africa/Addis_Ababa'),
(4, 'DZ', 'Africa/Algiers'),
(5, 'ER', 'Africa/Asmara'),
(6, 'ML', 'Africa/Bamako'),
(7, 'CF', 'Africa/Bangui'),
(8, 'GM', 'Africa/Banjul'),
(9, 'GW', 'Africa/Bissau'),
(10, 'MW', 'Africa/Blantyre'),
(11, 'CG', 'Africa/Brazzaville'),
(12, 'BI', 'Africa/Bujumbura'),
(13, 'EG', 'Africa/Cairo'),
(14, 'MA', 'Africa/Casablanca'),
(15, 'ES', 'Africa/Ceuta'),
(16, 'GN', 'Africa/Conakry'),
(17, 'SN', 'Africa/Dakar'),
(18, 'TZ', 'Africa/Dar_es_Salaam'),
(19, 'DJ', 'Africa/Djibouti'),
(20, 'CM', 'Africa/Douala'),
(21, 'SL', 'Africa/Freetown'),
(22, 'BW', 'Africa/Gaborone'),
(23, 'ZW', 'Africa/Harare'),
(24, 'ZA', 'Africa/Johannesburg'),
(25, 'UG', 'Africa/Kampala'),
(26, 'SD', 'Africa/Khartoum'),
(27, 'RW', 'Africa/Kigali'),
(28, 'CD', 'Africa/Kinshasa'),
(29, 'NG', 'Africa/Lagos'),
(30, 'GA', 'Africa/Libreville'),
(31, 'TG', 'Africa/Lome'),
(32, 'AO', 'Africa/Luanda'),
(33, 'CD', 'Africa/Lubumbashi'),
(34, 'ZM', 'Africa/Lusaka'),
(35, 'GQ', 'Africa/Malabo'),
(36, 'MZ', 'Africa/Maputo'),
(37, 'LS', 'Africa/Maseru'),
(38, 'SZ', 'Africa/Mbabane'),
(39, 'SO', 'Africa/Mogadishu'),
(40, 'LR', 'Africa/Monrovia'),
(41, 'KE', 'Africa/Nairobi'),
(42, 'TD', 'Africa/Ndjamena'),
(43, 'NE', 'Africa/Niamey'),
(44, 'MR', 'Africa/Nouakchott'),
(45, 'BF', 'Africa/Ouagadougou'),
(46, 'BJ', 'Africa/Porto-Novo'),
(47, 'ST', 'Africa/Sao_Tome'),
(48, 'LY', 'Africa/Tripoli'),
(49, 'TN', 'Africa/Tunis'),
(50, 'NA', 'Africa/Windhoek'),
(51, 'US', 'America/Adak'),
(52, 'US', 'America/Anchorage'),
(53, 'AI', 'America/Anguilla'),
(54, 'AG', 'America/Antigua'),
(55, 'BR', 'America/Araguaina'),
(56, 'AR', 'America/Argentina/Buenos_Aires'),
(57, 'AR', 'America/Argentina/Catamarca'),
(58, 'AR', 'America/Argentina/Cordoba'),
(59, 'AR', 'America/Argentina/Jujuy'),
(60, 'AR', 'America/Argentina/La_Rioja'),
(61, 'AR', 'America/Argentina/Mendoza'),
(62, 'AR', 'America/Argentina/Rio_Gallegos'),
(63, 'AR', 'America/Argentina/Salta'),
(64, 'AR', 'America/Argentina/San_Juan'),
(65, 'AR', 'America/Argentina/San_Luis'),
(66, 'AR', 'America/Argentina/Tucuman'),
(67, 'AR', 'America/Argentina/Ushuaia'),
(68, 'AW', 'America/Aruba'),
(69, 'PY', 'America/Asuncion'),
(70, 'CA', 'America/Atikokan'),
(71, 'BR', 'America/Bahia'),
(72, 'BB', 'America/Barbados'),
(73, 'BR', 'America/Belem'),
(74, 'BZ', 'America/Belize'),
(75, 'CA', 'America/Blanc-Sablon'),
(76, 'BR', 'America/Boa_Vista'),
(77, 'CO', 'America/Bogota'),
(78, 'US', 'America/Boise'),
(79, 'CA', 'America/Cambridge_Bay'),
(80, 'BR', 'America/Campo_Grande'),
(81, 'MX', 'America/Cancun'),
(82, 'VE', 'America/Caracas'),
(83, 'GF', 'America/Cayenne'),
(84, 'KY', 'America/Cayman'),
(85, 'US', 'America/Chicago'),
(86, 'MX', 'America/Chihuahua'),
(87, 'CR', 'America/Costa_Rica'),
(88, 'BR', 'America/Cuiaba'),
(89, 'AN', 'America/Curacao'),
(90, 'GL', 'America/Danmarkshavn'),
(91, 'CA', 'America/Dawson'),
(92, 'CA', 'America/Dawson_Creek'),
(93, 'US', 'America/Denver'),
(94, 'US', 'America/Detroit'),
(95, 'DM', 'America/Dominica'),
(96, 'CA', 'America/Edmonton'),
(97, 'BR', 'America/Eirunepe'),
(98, 'SV', 'America/El_Salvador'),
(99, 'BR', 'America/Fortaleza'),
(100, 'CA', 'America/Glace_Bay'),
(101, 'GL', 'America/Godthab'),
(102, 'CA', 'America/Goose_Bay'),
(103, 'TC', 'America/Grand_Turk'),
(104, 'GD', 'America/Grenada'),
(105, 'GP', 'America/Guadeloupe'),
(106, 'GT', 'America/Guatemala'),
(107, 'EC', 'America/Guayaquil'),
(108, 'GY', 'America/Guyana'),
(109, 'CA', 'America/Halifax'),
(110, 'CU', 'America/Havana'),
(111, 'MX', 'America/Hermosillo'),
(112, 'US', 'America/Indiana/Indianapolis'),
(113, 'US', 'America/Indiana/Knox'),
(114, 'US', 'America/Indiana/Marengo'),
(115, 'US', 'America/Indiana/Petersburg'),
(116, 'US', 'America/Indiana/Tell_City'),
(117, 'US', 'America/Indiana/Vevay'),
(118, 'US', 'America/Indiana/Vincennes'),
(119, 'US', 'America/Indiana/Winamac'),
(120, 'CA', 'America/Inuvik'),
(121, 'CA', 'America/Iqaluit'),
(122, 'JM', 'America/Jamaica'),
(123, 'US', 'America/Juneau'),
(124, 'US', 'America/Kentucky/Louisville'),
(125, 'US', 'America/Kentucky/Monticello'),
(126, 'BO', 'America/La_Paz'),
(127, 'PE', 'America/Lima'),
(128, 'US', 'America/Los_Angeles'),
(129, 'BR', 'America/Maceio'),
(130, 'NI', 'America/Managua'),
(131, 'BR', 'America/Manaus'),
(132, 'MQ', 'America/Martinique'),
(133, 'MX', 'America/Matamoros'),
(134, 'MX', 'America/Mazatlan'),
(135, 'US', 'America/Menominee'),
(136, 'MX', 'America/Merida'),
(137, 'MX', 'America/Mexico_City'),
(138, 'PM', 'America/Miquelon'),
(139, 'CA', 'America/Moncton'),
(140, 'MX', 'America/Monterrey'),
(141, 'UY', 'America/Montevideo'),
(142, 'CA', 'America/Montreal'),
(143, 'MS', 'America/Montserrat'),
(144, 'BS', 'America/Nassau'),
(145, 'US', 'America/New_York'),
(146, 'CA', 'America/Nipigon'),
(147, 'US', 'America/Nome'),
(148, 'BR', 'America/Noronha'),
(149, 'US', 'America/North_Dakota/Center'),
(150, 'US', 'America/North_Dakota/New_Salem'),
(151, 'MX', 'America/Ojinaga'),
(152, 'PA', 'America/Panama'),
(153, 'CA', 'America/Pangnirtung'),
(154, 'SR', 'America/Paramaribo'),
(155, 'US', 'America/Phoenix'),
(156, 'HT', 'America/Port-au-Prince'),
(157, 'BR', 'America/Porto_Velho'),
(158, 'TT', 'America/Port_of_Spain'),
(159, 'PR', 'America/Puerto_Rico'),
(160, 'CA', 'America/Rainy_River'),
(161, 'CA', 'America/Rankin_Inlet'),
(162, 'BR', 'America/Recife'),
(163, 'CA', 'America/Regina'),
(164, 'CA', 'America/Resolute'),
(165, 'BR', 'America/Rio_Branco'),
(166, 'BR', 'America/Santarem'),
(167, 'MX', 'America/Santa_Isabel'),
(168, 'CL', 'America/Santiago'),
(169, 'DO', 'America/Santo_Domingo'),
(170, 'BR', 'America/Sao_Paulo'),
(171, 'GL', 'America/Scoresbysund'),
(172, 'US', 'America/Shiprock'),
(173, 'CA', 'America/St_Johns'),
(174, 'KN', 'America/St_Kitts'),
(175, 'LC', 'America/St_Lucia'),
(176, 'VI', 'America/St_Thomas'),
(177, 'VC', 'America/St_Vincent'),
(178, 'CA', 'America/Swift_Current'),
(179, 'HN', 'America/Tegucigalpa'),
(180, 'GL', 'America/Thule'),
(181, 'CA', 'America/Thunder_Bay'),
(182, 'MX', 'America/Tijuana'),
(183, 'CA', 'America/Toronto'),
(184, 'VG', 'America/Tortola'),
(185, 'CA', 'America/Vancouver'),
(186, 'CA', 'America/Whitehorse'),
(187, 'CA', 'America/Winnipeg'),
(188, 'US', 'America/Yakutat'),
(189, 'CA', 'America/Yellowknife'),
(190, 'SJ', 'Arctic/Longyearbyen'),
(191, 'YE', 'Asia/Aden'),
(192, 'KZ', 'Asia/Almaty'),
(193, 'JO', 'Asia/Amman'),
(194, 'RU', 'Asia/Anadyr'),
(195, 'KZ', 'Asia/Aqtau'),
(196, 'KZ', 'Asia/Aqtobe'),
(197, 'TM', 'Asia/Ashgabat'),
(198, 'IQ', 'Asia/Baghdad'),
(199, 'BH', 'Asia/Bahrain'),
(200, 'AZ', 'Asia/Baku'),
(201, 'TH', 'Asia/Bangkok'),
(202, 'LB', 'Asia/Beirut'),
(203, 'KG', 'Asia/Bishkek'),
(204, 'BN', 'Asia/Brunei'),
(205, 'MN', 'Asia/Choibalsan'),
(206, 'CN', 'Asia/Chongqing'),
(207, 'LK', 'Asia/Colombo'),
(208, 'SY', 'Asia/Damascus'),
(209, 'BD', 'Asia/Dhaka'),
(210, 'TL', 'Asia/Dili'),
(211, 'AE', 'Asia/Dubai'),
(212, 'TJ', 'Asia/Dushanbe'),
(213, 'CN', 'Asia/Harbin'),
(214, 'HK', 'Asia/Hong_Kong'),
(215, 'MN', 'Asia/Hovd'),
(216, 'VN', 'Asia/Ho_Chi_Minh'),
(217, 'RU', 'Asia/Irkutsk'),
(218, 'ID', 'Asia/Jakarta'),
(219, 'ID', 'Asia/Jayapura'),
(220, 'IL', 'Asia/Jerusalem'),
(221, 'AF', 'Asia/Kabul'),
(222, 'RU', 'Asia/Kamchatka'),
(223, 'PK', 'Asia/Karachi'),
(224, 'CN', 'Asia/Kashgar'),
(225, 'NP', 'Asia/Kathmandu'),
(226, 'IN', 'Asia/Kolkata'),
(227, 'RU', 'Asia/Krasnoyarsk'),
(228, 'MY', 'Asia/Kuala_Lumpur'),
(229, 'MY', 'Asia/Kuching'),
(230, 'KW', 'Asia/Kuwait'),
(231, 'MO', 'Asia/Macau'),
(232, 'RU', 'Asia/Magadan'),
(233, 'ID', 'Asia/Makassar'),
(234, 'PH', 'Asia/Manila'),
(235, 'OM', 'Asia/Muscat'),
(236, 'CY', 'Asia/Nicosia'),
(237, 'RU', 'Asia/Novokuznetsk'),
(238, 'RU', 'Asia/Novosibirsk'),
(239, 'RU', 'Asia/Omsk'),
(240, 'KZ', 'Asia/Oral'),
(241, 'KH', 'Asia/Phnom_Penh'),
(242, 'ID', 'Asia/Pontianak'),
(243, 'KP', 'Asia/Pyongyang'),
(244, 'QA', 'Asia/Qatar'),
(245, 'KZ', 'Asia/Qyzylorda'),
(246, 'MM', 'Asia/Rangoon'),
(247, 'SA', 'Asia/Riyadh'),
(248, 'RU', 'Asia/Sakhalin'),
(249, 'UZ', 'Asia/Samarkand'),
(250, 'KR', 'Asia/Seoul'),
(251, 'CN', 'Asia/Shanghai'),
(252, 'SG', 'Asia/Singapore'),
(253, 'TW', 'Asia/Taipei'),
(254, 'UZ', 'Asia/Tashkent'),
(255, 'GE', 'Asia/Tbilisi'),
(256, 'IR', 'Asia/Tehran'),
(257, 'BT', 'Asia/Thimphu'),
(258, 'JP', 'Asia/Tokyo'),
(259, 'MN', 'Asia/Ulaanbaatar'),
(260, 'CN', 'Asia/Urumqi'),
(261, 'LA', 'Asia/Vientiane'),
(262, 'RU', 'Asia/Vladivostok'),
(263, 'RU', 'Asia/Yakutsk'),
(264, 'RU', 'Asia/Yekaterinburg'),
(265, 'AM', 'Asia/Yerevan'),
(266, 'PT', 'Atlantic/Azores'),
(267, 'BM', 'Atlantic/Bermuda'),
(268, 'ES', 'Atlantic/Canary'),
(269, 'CV', 'Atlantic/Cape_Verde'),
(270, 'FO', 'Atlantic/Faroe'),
(271, 'PT', 'Atlantic/Madeira'),
(272, 'IS', 'Atlantic/Reykjavik'),
(273, 'FK', 'Atlantic/Stanley'),
(274, 'SH', 'Atlantic/St_Helena'),
(275, 'AU', 'Australia/Adelaide'),
(276, 'AU', 'Australia/Brisbane'),
(277, 'AU', 'Australia/Broken_Hill'),
(278, 'AU', 'Australia/Currie'),
(279, 'AU', 'Australia/Darwin'),
(280, 'AU', 'Australia/Eucla'),
(281, 'AU', 'Australia/Hobart'),
(282, 'AU', 'Australia/Lindeman'),
(283, 'AU', 'Australia/Lord_Howe'),
(284, 'AU', 'Australia/Melbourne'),
(285, 'AU', 'Australia/Perth'),
(286, 'AU', 'Australia/Sydney'),
(287, 'NL', 'Europe/Amsterdam'),
(288, 'AD', 'Europe/Andorra'),
(289, 'GR', 'Europe/Athens'),
(290, 'RS', 'Europe/Belgrade'),
(291, 'DE', 'Europe/Berlin'),
(292, 'SK', 'Europe/Bratislava'),
(293, 'BE', 'Europe/Brussels'),
(294, 'RO', 'Europe/Bucharest'),
(295, 'HU', 'Europe/Budapest'),
(296, 'MD', 'Europe/Chisinau'),
(297, 'DK', 'Europe/Copenhagen'),
(298, 'IE', 'Europe/Dublin'),
(299, 'GI', 'Europe/Gibraltar'),
(300, 'GG', 'Europe/Guernsey'),
(301, 'FI', 'Europe/Helsinki'),
(302, 'IM', 'Europe/Isle_of_Man'),
(303, 'TR', 'Europe/Istanbul'),
(304, 'JE', 'Europe/Jersey'),
(305, 'RU', 'Europe/Kaliningrad'),
(306, 'UA', 'Europe/Kiev'),
(307, 'PT', 'Europe/Lisbon'),
(308, 'SI', 'Europe/Ljubljana'),
(309, 'GB', 'Europe/London'),
(310, 'LU', 'Europe/Luxembourg'),
(311, 'ES', 'Europe/Madrid'),
(312, 'MT', 'Europe/Malta'),
(313, 'AX', 'Europe/Mariehamn'),
(314, 'BY', 'Europe/Minsk'),
(315, 'MC', 'Europe/Monaco'),
(316, 'RU', 'Europe/Moscow'),
(317, 'NO', 'Europe/Oslo'),
(318, 'FR', 'Europe/Paris'),
(319, 'ME', 'Europe/Podgorica'),
(320, 'CZ', 'Europe/Prague'),
(321, 'LV', 'Europe/Riga'),
(322, 'IT', 'Europe/Rome'),
(323, 'RU', 'Europe/Samara'),
(324, 'SM', 'Europe/San_Marino'),
(325, 'BA', 'Europe/Sarajevo'),
(326, 'UA', 'Europe/Simferopol'),
(327, 'MK', 'Europe/Skopje'),
(328, 'BG', 'Europe/Sofia'),
(329, 'SE', 'Europe/Stockholm'),
(330, 'EE', 'Europe/Tallinn'),
(331, 'AL', 'Europe/Tirane'),
(332, 'UA', 'Europe/Uzhgorod'),
(333, 'LI', 'Europe/Vaduz'),
(334, 'VA', 'Europe/Vatican'),
(335, 'AT', 'Europe/Vienna'),
(336, 'LT', 'Europe/Vilnius'),
(337, 'RU', 'Europe/Volgograd'),
(338, 'PL', 'Europe/Warsaw'),
(339, 'HR', 'Europe/Zagreb'),
(340, 'UA', 'Europe/Zaporozhye'),
(341, 'CH', 'Europe/Zurich'),
(342, 'MG', 'Indian/Antananarivo'),
(343, 'CX', 'Indian/Christmas'),
(344, 'CC', 'Indian/Cocos'),
(345, 'KM', 'Indian/Comoro'),
(346, 'SC', 'Indian/Mahe'),
(347, 'MV', 'Indian/Maldives'),
(348, 'MU', 'Indian/Mauritius'),
(349, 'YT', 'Indian/Mayotte'),
(350, 'RE', 'Indian/Reunion'),
(351, 'WS', 'Pacific/Apia'),
(352, 'NZ', 'Pacific/Auckland'),
(353, 'NZ', 'Pacific/Chatham'),
(354, 'CL', 'Pacific/Easter'),
(355, 'VU', 'Pacific/Efate'),
(356, 'KI', 'Pacific/Enderbury'),
(357, 'TK', 'Pacific/Fakaofo'),
(358, 'FJ', 'Pacific/Fiji'),
(359, 'TV', 'Pacific/Funafuti'),
(360, 'EC', 'Pacific/Galapagos'),
(361, 'PF', 'Pacific/Gambier'),
(362, 'SB', 'Pacific/Guadalcanal'),
(363, 'GU', 'Pacific/Guam'),
(364, 'US', 'Pacific/Honolulu'),
(365, 'KI', 'Pacific/Kiritimati'),
(366, 'FM', 'Pacific/Kosrae'),
(367, 'MH', 'Pacific/Kwajalein'),
(368, 'MH', 'Pacific/Majuro'),
(369, 'PF', 'Pacific/Marquesas'),
(370, 'NR', 'Pacific/Nauru'),
(371, 'NU', 'Pacific/Niue'),
(372, 'NF', 'Pacific/Norfolk'),
(373, 'NC', 'Pacific/Noumea'),
(374, 'AS', 'Pacific/Pago_Pago'),
(375, 'PW', 'Pacific/Palau'),
(376, 'PN', 'Pacific/Pitcairn'),
(377, 'FM', 'Pacific/Ponape'),
(378, 'PG', 'Pacific/Port_Moresby'),
(379, 'CK', 'Pacific/Rarotonga'),
(380, 'MP', 'Pacific/Saipan'),
(381, 'PF', 'Pacific/Tahiti'),
(382, 'KI', 'Pacific/Tarawa'),
(383, 'TO', 'Pacific/Tongatapu'),
(384, 'FM', 'Pacific/Truk'),
(385, 'WF', 'Pacific/Wallis');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `timezones`
--
ALTER TABLE `timezones`
  ADD CONSTRAINT `timezones_ibfk_1` FOREIGN KEY (`country_code`) REFERENCES `country` (`country_code`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
