<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rates', function ($table) {
			#$table->increments('id');
			$table->integer('room_type_id')->unsigned()->index();
			$table->foreign('room_type_id')->references('id')->on('room_types')->onDelete('cascade');
			$table->date('date');
			$table->integer('price')->unsigned();
			$table->primary(array('room_type_id', 'date'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rates');
	}

}
