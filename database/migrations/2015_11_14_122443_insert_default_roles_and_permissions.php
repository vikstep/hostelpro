<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\HostelPro\Models\User;
use App\HostelPro\Models\Role;
use App\HostelPro\Models\Permission;

class InsertDefaultRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $admin = new Role();
        $admin->name = 'admin';
        $admin->save();

        $manager = new Role();
        $manager->name = 'manager';
        $manager->save();

        $staff = new Role();
        $staff->name = 'staff';
        $staff->save();

        $receptionist = new Role();
        $receptionist->name = 'receptionist';
        $receptionist->save();


        //For all our current legacy users, we want to assign them as admin
        $users = User::all();
        foreach ($users as $user) {
            $user->assignRole('admin');
        }

        $rates_view = new Permission();
        $rates_view->name = 'rates_view';
        $rates_view->save();

        $rates_create = new Permission();
        $rates_create->name = 'rates_create';
        $rates_create->save();

        $rates_edit = new Permission();
        $rates_edit->name = 'rates_edit';
        $rates_edit->save();

        $rates_create = new Permission();
        $rates_create->name = 'rates_create';
        $rates_create->save();

        $rates_delete = new Permission();
        $rates_delete->name = 'rates_delete';
        $rates_delete->save();

        $basicReports_view = new Permission();
        $basicReports_view->name = 'basicreports_view';
        $basicReports_view->save();

        $revenueReports_view = new Permission();
        $revenueReports_view->name = 'revenuereports_view';
        $revenueReports_view->save();

        $expenseReports_view = new Permission();
        $expenseReports_view->name = 'expensereports_view';
        $expenseReports_view->save();

        $expenseReports_create = new Permission();
        $expenseReports_create->name = 'expensereports_create';
        $expenseReports_create->save();

        $expenseReports_delete = new Permission();
        $expenseReports_delete->name = 'expensereports_delete';
        $expenseReports_delete->save();

        $pnlReport = new Permission();
        $pnlReport->name = 'pnlreport_view';
        $pnlReport->save();

        $shiftReports_view = new Permission();
        $shiftReports_view->name = 'shiftreports_view';
        $shiftReports_view->save();

        $shiftReports_create = new Permission();
        $shiftReports_create->name = 'shiftreports_create';
        $shiftReports_create->save();

        $shiftReports_delete = new Permission();
        $shiftReports_delete->name = 'shiftreports_delete';
        $shiftReports_delete->save();

        $hostelSettings_view = new Permission();
        $hostelSettings_view->name = 'hostelsettings_view';
        $hostelSettings_view->save();

        $hostelSettings_edit = new Permission();
        $hostelSettings_edit->name = 'hostelsettings_edit';
        $hostelSettings_edit->save();

        $users_view = new Permission();
        $users_view->name = 'users_view';
        $users_view->save();

        $users_create = new Permission();
        $users_create->name = 'users_create';
        $users_create->save();

        $users_edit = new Permission();
        $users_edit->name = 'users_edit';
        $users_edit->save();

        $users_delete = new Permission();
        $users_delete->name = 'users_delete';
        $users_delete->save();


        $admin->givePermissionTo($rates_view);
        $admin->givePermissionTo($rates_create);
        $admin->givePermissionTo($rates_edit);
        $admin->givePermissionTo($rates_delete);

        $admin->givePermissionTo($basicReports_view);
        $admin->givePermissionTo($revenueReports_view);

        $admin->givePermissionTo($expenseReports_view);
        $admin->givePermissionTo($expenseReports_create);
        $admin->givePermissionTo($expenseReports_delete);

        $admin->givePermissionTo($pnlReport);

        $admin->givePermissionTo($shiftReports_view);
        $admin->givePermissionTo($shiftReports_create);
        $admin->givePermissionTo($shiftReports_delete);

        $admin->givePermissionTo($hostelSettings_view);
        $admin->givePermissionTo($hostelSettings_edit);

        $admin->givePermissionTo($users_view);
        $admin->givePermissionTo($users_create);
        $admin->givePermissionTo($users_edit);
        $admin->givePermissionTo($users_delete);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('permission_role')->truncate();
        DB::table('role_user')->truncate();
        DB::table('roles')->truncate();
        DB::table('permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
