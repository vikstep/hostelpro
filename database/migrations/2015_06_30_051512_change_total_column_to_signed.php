<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\HostelPro\Models\Payment;

class ChangeTotalColumnToSigned extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE payment modify total int signed');

		$payments = Payment::with('items')->get();

		foreach ($payments as $payment) {
			$total = 0;
			foreach ($payment->items as $item) {
				$total += $item->subtotal;
			}
			$payment->total = $total;
			$payment->save();
		}


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE payment modify total int unsigned');
	}

}
