<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAndRecreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
        public function up()
    {
        Schema::drop('payment_data');
        Schema::drop('payment');

        Schema::create('payment', function ($table) {
            $table->increments('id');

            $table->string('name', 100)->index();

            $table->integer('total');

            $table->integer('hostel_id')->unsigned()->index();
            $table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');

            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')->references('id')->on('user');

            $table->integer('booking_id')->unsigned()->nullable()->index();
            $table->foreign('booking_id')->references('id')->on('booking')->onDelete('cascade');

            $table->integer('currency_id')->unsigned();
            $table->foreign('currency_id')->references('id')->on('currency');

            $table->timestamps();

            $table->index('created_at');
        });
    }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down() {
        Schema::drop('payment');
    }
}
