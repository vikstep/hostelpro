<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('board', function($table) {
			$table->date('date');
			$table->integer('room_id')->unsigned()->index();
			$table->foreign('room_id')->references('id')->on('room');
			$table->tinyInteger('bed_number')->unsigned()->index();
			$table->primary(array('date', 'room_id', 'bed_number'));
			$table->integer('stay_id')->unsigned()->index();
			$table->foreign('stay_id')->references('id')->on('stay')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('board');
	}

}
