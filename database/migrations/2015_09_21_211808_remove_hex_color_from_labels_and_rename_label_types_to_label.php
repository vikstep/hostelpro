<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveHexColorFromLabelsAndRenameLabelTypesToLabel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking', function($table)
		{
			$table->integer('label_type_id')->unsigned()->index()->nullable()->after('label_id');
			$table->foreign('label_type_id')->references('id')->on('label_types')->onDelete('SET NULL');
		});

		#DB::statement('')
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('booking', function($table)
		{
			$table->dropForeign('booking_label_type_id_foreign');
			$table->dropColumn('label_type_id');
		});
	}

}
