<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPosDepositCategory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hostel', function($table)
		{
			$table->integer('pos_deposit_category')->unsigned()->index()->nullable();
			$table->foreign('pos_deposit_category')->references('id')->on('pos_categories')->onDelete('SET NULL');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hostel', function($table)
		{
			$table->dropForeign('hostel_pos_deposit_category_foreign');
			$table->dropColumn('pos_deposit_category');
		});
	}

}
