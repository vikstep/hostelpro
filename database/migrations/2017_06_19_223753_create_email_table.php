<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hostel_id')->unsigned()->index();
            $table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');

            $table->string('host');
            $table->string('port');
            $table->enum('encryption', ['', 'tls', 'ssl'])->default('');
            $table->string('username')->nullable();
            $table->string('password')->nullable();

            $table->unsignedTinyInteger('pre_email_days')->default(0);
            $table->text('pre_email_text')->nullable();

            $table->unsignedTinyInteger('post_email_days')->default(0);
            $table->text('post_email_text')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email');
    }
}
