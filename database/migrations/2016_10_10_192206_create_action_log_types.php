<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionLogTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('action_log_types')->insert([
            ['name' => 'Booking Created'],
            ['name' => 'Booking Modified'],
            ['name' => 'Checked In'],
            ['name' => 'Checked Out'],
            ['name' => 'Booking Cancelled'],
            ['name' => 'No Show']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('action_log_types')->truncate();
    }
}
