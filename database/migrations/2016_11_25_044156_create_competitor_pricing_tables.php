<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitorPricingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitor_url', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hostel_id')->unsigned()->index();
            $table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');
            $table->string('myallocator_channel_id')->index();
            $table->foreign('myallocator_channel_id')->references('id')->on('myallocator_channel')->onDelete('cascade');
            $table->string('name');
            $table->string('url');
            $table->date('last_crawled')->nullable();
            $table->timestamps();
        });

        Schema::create('competitor_pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('competitor_url_id')->unsigned()->index();
            $table->foreign('competitor_url_id')->references('id')->on('competitor_url')->onDelete('cascade');
            $table->date('date')->index();
            $table->unsignedInteger('price');
            $table->integer('currency_id')->unsigned();
            $table->foreign('currency_id')->references('id')->on('currency');
            $table->string('room_description');
            $table->string('bed_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('competitor_pricing');
        Schema::drop('competitor_url');
    }
}
