<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function($table) {
			$table->increments('id');
			$table->string('firstname', 20)->required();
			$table->string('lastname', 20);
			$table->string('email', 100)->unique();
			$table->string('password', 60);
			$table->integer('current_hostel')->unsigned()->index()->nullable();
			$table->foreign('current_hostel')->references('id')->on('hostel')->onDelete('set null');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}
