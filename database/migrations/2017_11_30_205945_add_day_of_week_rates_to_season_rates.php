<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDayOfWeekRatesToSeasonRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('day_of_week_rate', function (Blueprint $table) {
            $table->integer('room_type_id')->unsigned()->index();
            $table->foreign('room_type_id')->references('id')->on('room_types')->onDelete('cascade');

            $table->integer('season_id')->unsigned()->index();
            $table->foreign('season_id')->references('id')->on('season_rates')->onDelete('cascade');
            
            //serialized array of rates: Mon:10, Tue:15 etc.
            $table->string('rates');
            //serialized array of min stay nights: Mon:1, Tue:2 etc.
            $table->string('min_stay');

            $table->primary(array('room_type_id', 'season_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('day_of_week_rate');
    }
}
