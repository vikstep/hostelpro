<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\HostelPro\Models\Board;
use App\HostelPro\Models\Stay;
use App\HostelPro\Models\Booking;

class AddHostelIdColumnToBoardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::table('board', function($table)
		{
			$table->integer('hostel_id')->unsigned()->index();
			$table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');
		});

		$booking_ids = Booking::select('id')->where('hostel_id', '=', '6')->groupBy('id')->pluck('id');
		$stay_ids = Stay::select('id')->whereIn('booking_id', $booking_ids)->groupBy('id')->pluck('id');

		Board::whereIn('stay_id', $stay_ids)->update(['hostel_id' => 6]);

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('board', function($table)
		{
			//Add in code here to drop Foreign key before dropping column
			$table->dropColumn('hostel_id');
		});
	}

}
