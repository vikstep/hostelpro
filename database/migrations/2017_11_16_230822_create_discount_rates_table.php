<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_rates', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('roomtype_id')->unsigned()->index();
            $table->foreign('roomtype_id')->references('id')->on('room_types')->onDelete('cascade');

            $table->integer('season_rates')->unsigned()->index();
            $table->foreign('season_rates')->references('id')->on('season_rates')->onDelete('cascade');


            $table->double('earlybird_discount', 8, 2);
            $table->integer('earlybird_days')->length(10)->unsigned();

            $table->double('lastminute_discount', 8, 2);
            $table->integer('lastminute_days')->length(10)->unsigned();

            $table->double('progressive_low_price', 8, 2);
            $table->double('progressive_high_price', 8, 2);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_rates');
    }
}
