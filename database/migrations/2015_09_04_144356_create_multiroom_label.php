<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\HostelPro\Models\Label;
use App\HostelPro\Models\LabelType;
use App\HostelPro\Models\Hostel;

class CreateMultiroomLabel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$labeltype = new LabelType();
		$labeltype->type = "Multi-Room Booking";
		$labeltype->save();

		$hostels = Hostel::all();
		foreach ($hostels as $hostel) {
			$label = new Label();
			$label->hostel_id = $hostel->id;
			$label->label_type_id = $labeltype->id;
			$label->hex_color = "7450D7";
			$label->save();
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
