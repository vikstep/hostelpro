<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseAndExpensecategoryTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('expense_category', function ($table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('hostel_id')->unsigned()->index();
			$table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');
		});

		Schema::create('expense', function ($table) {
			$table->increments('id');
			$table->integer('hostel_id')->unsigned()->index();
			$table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');
			$table->integer('user_id')->unsigned()->nullable()->index();
			$table->foreign('user_id')->references('id')->on('user')->onDelete('SET NULL');
			$table->date('date');
			$table->string('category');
			$table->string('name');
			$table->integer('price');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('expense');
		Schema::drop('expense_category');
	}

}
