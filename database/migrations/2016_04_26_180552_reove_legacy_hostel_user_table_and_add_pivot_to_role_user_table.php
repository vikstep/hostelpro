<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\HostelPro\Models\User;

class ReoveLegacyHostelUserTableAndAddPivotToRoleUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('hostel_user');
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('role_user');

        Schema::create('role_user', function ($table) {
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');

            $table->integer('hostel_id')->unsigned()->index();
            $table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');

            $table->primary(['role_id', 'user_id', 'hostel_id']);
        });

        $users = User::all();
        foreach ($users as $user) {
            $user->assignRole('admin', $user->current_hostel);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
