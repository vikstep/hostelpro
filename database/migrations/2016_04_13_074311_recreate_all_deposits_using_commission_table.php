<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateAllDepositsUsingCommissionTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::table('additional_item')->truncate();

        DB::beginTransaction();
        $all_booking_ids = \App\HostelPro\Models\Booking::where('hostel_id', '=', '13')->pluck('id')->all();
        $allPayments = \App\HostelPro\Models\Commission::whereIn('booking_id', $all_booking_ids)->where('is_deposit', '=', true)->get();
        foreach ($allPayments as $payment) {
            $newItem = new \App\HostelPro\Models\AdditionalItem();
            $newItem->name = 'Booking Deposit';
            if ($payment->converted_amount) {
                $newItem->price = 0 - \App\HostelPro\Models\MoneyHelper::convertToCents($payment->converted_amount);
            } else {
                $newItem->price = 0 - \App\HostelPro\Models\MoneyHelper::convertToCents($payment->original_amount);
            }
            $newItem->units = 1;
            if ($payment->converted_amount) {
                $newItem->total = 0 - \App\HostelPro\Models\MoneyHelper::convertToCents($payment->converted_amount);
            } else {
                $newItem->total = 0 - \App\HostelPro\Models\MoneyHelper::convertToCents($payment->original_amount);
            }
            $newItem->deposit = false;
            $newItem->hostel_id = 13;
            $newItem->booking_id = $payment->booking_id;
            $newItem->currency_id = 61;
            $newItem->save();
        }
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
}
