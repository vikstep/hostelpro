<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCancelledToBookingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE booking CHANGE status status ENUM('Unallocated', 'Allocated','Cancelled') DEFAULT 'Unallocated'");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement("ALTER TABLE booking CHANGE status status ENUM('Unallocated', 'Allocated') DEFAULT 'Unallocated'");
	}

}
