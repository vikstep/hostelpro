<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hostel', function($table)
		{
			$table->increments('id');
			$table->string('name', 50);
			$table->integer('country_id')->unsigned();
			$table->foreign('country_id')->references('id')->on('country');
			$table->integer('city')->unsigned()->nullable();
			$table->foreign('city')->references('geonameid')->on('city');
			$table->integer('timezone_id')->unsigned();
			$table->foreign('timezone_id')->references('id')->on('timezones');
			$table->integer('currency_id')->unsigned();
			$table->foreign('currency_id')->references('id')->on('currency');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hostel');
	}

}
