<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSortColumnToPosTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pos_categories', function ($table) {
            $table->tinyInteger('sort')->unsigned()->nullable();
        });
        Schema::table('pos_products', function ($table) {
            $table->tinyInteger('sort')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pos_categories', function ($table) {
            $table->dropColumn('sort');
        });
        Schema::table('pos_products', function ($table) {
            $table->dropColumn('sort');
        });
    }
}
