<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftReportTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shift_report', function ($table) {
			$table->increments('id');
			$table->integer('hostel_id')->unsigned()->index();
			$table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');
			$table->integer('user_id')->unsigned()->nullable()->index();
			$table->foreign('user_id')->references('id')->on('user')->onDelete('SET NULL');
			$table->dateTime('start_time');
			$table->dateTime('end_time');
		});

		Schema::create('shift_report_data', function ($table) {
			$table->increments('id');
			$table->integer('shift_report_id')->unsigned()->index();
			$table->foreign('shift_report_id')->references('id')->on('shift_report')->onDelete('cascade');
			$table->integer('amount_of_money');
			$table->integer('quantity');
			$table->boolean('is_start_shift')->default(true);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shift_report_data');
		Schema::drop('shift_report');
	}

}
