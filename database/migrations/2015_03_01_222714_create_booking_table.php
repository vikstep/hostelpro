<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking', function($table) {
			$table->increments('id');
			$table->enum('status', array('Unallocated', 'Allocated'))->default('Unallocated');
			$table->integer('hostel_id')->unsigned()->index();
			$table->foreign('hostel_id')->references('id')->on('hostel');
			$table->integer('guest_id')->unsigned()->index();
			$table->foreign('guest_id')->references('id')->on('guest');
			#$table->integer('room_type_id')->unsigned()->index();
			#$table->foreign('room_type_id')->references('id')->on('room_types');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking');
	}

}
