<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned()->index();
            $table->foreign('booking_id')->references('id')->on('booking')->onDelete('cascade');
            $table->boolean('is_deposit')->default(true);
            $table->integer('original_amount')->unsigned();
            $table->integer('original_currency_id')->unsigned();
            $table->foreign('original_currency_id')->references('id')->on('currency');
            $table->integer('converted_amount')->unsigned()->nullable();
            $table->integer('converted_currency_id')->unsigned()->nullable();
            $table->foreign('converted_currency_id')->references('id')->on('currency');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commission');
    }
}
