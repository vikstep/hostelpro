<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_url', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hostel_id')->unsigned()->index();
            $table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');
            $table->string('myallocator_channel_id')->index();
            $table->foreign('myallocator_channel_id')->references('id')->on('myallocator_channel')->onDelete('cascade');
            $table->string('url');
            $table->timestamps();
        });

        Schema::create('review', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('review_url_id')->unsigned()->index();
            $table->foreign('review_url_id')->references('id')->on('review_url')->onDelete('cascade');
            $table->date('date')->index();
            $table->unsignedTinyInteger('overall_score');
            $table->unsignedTinyInteger('atmosphere_score')->nullable();
            $table->unsignedTinyInteger('cleanliness_score')->nullable();
            $table->unsignedTinyInteger('facilities_score')->nullable();
            $table->unsignedTinyInteger('location_score')->nullable();
            $table->unsignedTinyInteger('safety_score')->nullable();
            $table->unsignedTinyInteger('staff_score')->nullable();
            $table->unsignedTinyInteger('value_score')->nullable();
            $table->text('liked')->nullable();
            $table->text('disliked')->nullable();
            $table->text('overall')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('review');
        Schema::drop('review_url');
    }
}
