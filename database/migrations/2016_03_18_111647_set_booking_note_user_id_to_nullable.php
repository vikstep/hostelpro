<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetBookingNoteUserIdToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_notes', function ($table) {
            $table->dropForeign(['user_id']);
        });

        Schema::table('booking_notes', function ($table) {
            $table->integer('user_id')->nullable()->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
