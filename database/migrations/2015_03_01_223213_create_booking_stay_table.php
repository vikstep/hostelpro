<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingStayTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stay', function($table) {
			$table->increments('id');
			$table->integer('booking_id')->unsigned()->index();
			$table->foreign('booking_id')->references('id')->on('booking');
			$table->integer('room_id')->unsigned()->index()->nullable();
			$table->foreign('room_id')->references('id')->on('room')->onDelete('set null');
			$table->date('start_date')->index();
			$table->date('end_date')->index();
			$table->integer('number_of_guests')->unsigned()->default(1);
			$table->integer('price_per_night')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stay');
	}

}
