<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_rate', function (Blueprint $table) {
            $table->integer('room_type_id')->unsigned()->index();
            $table->foreign('room_type_id')->references('id')->on('room_types')->onDelete('cascade');

            $table->date('date');
            $table->integer('price')->length(12)->unsigned();

            $table->primary(array('room_type_id', 'date'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_rate');
    }
}
