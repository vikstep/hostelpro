<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeRoomsCascadeDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room', function ($table) {
            $table->dropForeign('room_room_type_id_foreign');
            $table->foreign('room_type_id')
                ->references('id')->on('room_types')
                ->onDelete('cascade');
        });

        Schema::table('board', function ($table) {
            $table->dropForeign('board_room_id_foreign');
            $table->foreign('room_id')
                ->references('id')->on('room')
                ->onDelete('cascade');
        });

        Schema::table('board_temp', function ($table) {
            $table->dropForeign('board_temp_room_id_foreign');
            $table->foreign('room_id')
                ->references('id')->on('room')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
