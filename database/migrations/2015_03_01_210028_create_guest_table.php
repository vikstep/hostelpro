<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guest', function($table) {
			$table->increments('id');
			$table->string('firstname', 20);
			$table->string('lastname', 20);
			$table->string('email', 100)->nullable();
			$table->string('phone', 30)->nullable();

			$table->integer('hostel_id')->unsigned()->index();
			$table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');

			$table->integer('country_id')->unsigned()->index()->nullable();
			$table->foreign('country_id')->references('id')->on('country')->onDelete('set null');

			$table->integer('city_id')->unsigned()->index()->nullable();
			$table->foreign('city_id')->references('geonameid')->on('city')->onDelete('set null');

			$table->string('address_line_1', 50)->nullable();
			$table->string('address_line_2', 50)->nullable();
			$table->string('zip_code', 20)->nullable();

			$table->enum('gender', array('Female', 'Male', 'Mixed'))->nullable();

			$table->string('guest_notes', 250)->nullable();

			$table->timestamps();
		});

		DB::statement('ALTER TABLE guest ADD FULLTEXT search(firstname, lastname)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guest');
	}

}
