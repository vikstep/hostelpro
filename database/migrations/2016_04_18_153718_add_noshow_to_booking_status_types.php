<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoshowToBookingStatusTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE booking CHANGE COLUMN status status ENUM('Unallocated', 'Allocated', 'Cancelled', 'NoShow')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE booking CHANGE COLUMN status status ENUM('Unallocated', 'Allocated', 'Cancelled')");
    }
}
