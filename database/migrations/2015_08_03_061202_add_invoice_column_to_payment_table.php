<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceColumnToPaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payment', function($table)
		{
			$table->string('invoice_number')->nullable()->unique()->index();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payment', function($table)
		{
			//Add in code here to drop Foreign key before dropping column
			$table->dropColumn('invoice_number');
		});
	}

}
