<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionLogTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_log_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('action_log', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('action_log_type_id')->unsigned()->index();
            $table->foreign('action_log_type_id')->references('id')->on('action_log_types')->onDelete('cascade');

            $table->integer('hostel_id')->unsigned()->index();
            $table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');

            $table->integer('booking_id')->unsigned()->index()->nullable();
            $table->foreign('booking_id')->references('id')->on('booking')->onDelete('cascade');

            $table->text('detailed_text');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('action_log');
        Schema::drop('action_log_types');
    }
}
