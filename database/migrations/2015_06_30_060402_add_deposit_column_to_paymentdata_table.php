<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepositColumnToPaymentdataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payment_data', function($table)
		{
			$table->boolean('deposit')->default(false)->index();
		});

		DB::statement('UPDATE payment_data SET deposit=true where name like \'%deposit%\'');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payment_data', function($table)
		{
			$table->dropColumn('deposit');
		});
	}

}
