<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLabelColumnToBookingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking', function($table)
		{
			$table->integer('label_id')->unsigned()->index()->nullable();
			$table->foreign('label_id')->references('id')->on('label')->onDelete('SET NULL');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('booking', function($table)
		{
			$table->dropForeign('booking_label_id_foreign');
			$table->dropColumn('label_id');
		});
	}

}
