<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('season_rates', function ($table) {
			$table->increments('id');
			$table->integer('hostel_id')->unsigned()->index();
			$table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');
			$table->string('name', 50);
			$table->date('start_date');
			$table->date('end_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('season_rates');
	}
}
