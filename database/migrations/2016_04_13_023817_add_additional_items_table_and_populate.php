<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalItemsTableAndPopulate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_item', function ($table) {
            $table->increments('id');

            $table->string('name', 100)->index();

            $table->integer('price');

            $table->integer('units')->unsigned();

            $table->integer('total');

            $table->boolean('deposit')->default(false)->index();

            $table->integer('hostel_id')->unsigned()->index();
            $table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');

            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')->references('id')->on('user');

            $table->integer('booking_id')->unsigned()->nullable()->index();
            $table->foreign('booking_id')->references('id')->on('booking')->onDelete('cascade');

            $table->integer('currency_id')->unsigned();
            $table->foreign('currency_id')->references('id')->on('currency');

            $table->timestamps();

            $table->index('created_at');
        });

        /*
        DB::beginTransaction();
        $allPayments = \App\HostelPro\Models\Payment::where('hostel_id', '=', '13')->get();
        foreach ($allPayments as $onePayment) {
            foreach ($onePayment->items as $payment) {
                if ($payment->name == 'Booking Deposit') {
                    $newItem = new \App\HostelPro\Models\AdditionalItem();
                    $newItem->name = $payment->name;
                    $newItem->price = $payment->price;
                    $newItem->units = 1;
                    $newItem->total = $payment->price;
                    $newItem->deposit = false;
                    $newItem->hostel_id = 13;
                    $newItem->booking_id = $onePayment->booking_id;
                    $newItem->currency_id = 61;
                    $newItem->save();
                }
            }
        }
        DB::commit();*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('additional_item');
    }
}
