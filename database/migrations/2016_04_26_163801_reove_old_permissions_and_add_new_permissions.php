<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\HostelPro\Models\Role;
use App\HostelPro\Models\Permission;

class ReoveOldPermissionsAndAddNewPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('permission_role')->truncate();
        DB::table('permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $staff = Role::find(8);
        $staff->delete();

        $admin = Role::find(2);
        $manager = Role::find(5);
        $receptionist = Role::find(11);

        $basic_reports = new Permission();
        $basic_reports->name = 'basic_reports';
        $basic_reports->save();

        $advanced_reports = new Permission();
        $advanced_reports->name = 'advanced_reports';
        $advanced_reports->save();

        $hostel_settings = new Permission();
        $hostel_settings->name = 'hostel_settings';
        $hostel_settings->save();

        $user_management = new Permission();
        $user_management->name = 'user_management';
        $user_management->save();

        $rates = new Permission();
        $rates->name = 'rates';
        $rates->save();

        $admin->givePermissionTo($basic_reports);
        $admin->givePermissionTo($advanced_reports);
        $admin->givePermissionTo($hostel_settings);
        $admin->givePermissionTo($user_management);
        $admin->givePermissionTo($rates);

        $manager->givePermissionTo($basic_reports);
        $manager->givePermissionTo($advanced_reports);
        $manager->givePermissionTo($hostel_settings);
        $manager->givePermissionTo($user_management);
        $manager->givePermissionTo($rates);

        $receptionist->givePermissionTo($basic_reports);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
