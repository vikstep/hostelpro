<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('room_types', function($table)
		{
			$table->increments('id');
			$table->string('name', 50);
			$table->enum('type', array('Private', 'Dorm'));
			$table->enum('gender', array('Female', 'Male', 'Mixed'));
			$table->tinyInteger('number_of_guests')->unsigned();
			$table->integer('hostel_id')->unsigned();
			$table->foreign('hostel_id')->references('id')->on('hostel');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('room_types');
	}

}
