<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('label', function($table) {
			$table->increments('id');
			$table->integer('hostel_id')->unsigned()->index();
			$table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');
			$table->integer('label_type_id')->unsigned()->index();
			$table->foreign('label_type_id')->references('id')->on('label_types')->onDelete('cascade');
			$table->string('hex_color', 6);
		});

		$id = DB::table('label_types')->insertGetId(['type' => 'Booking - Paid']);
		DB::table('label')->insert(['hostel_id' => '1', 'label_type_id' => $id, 'hex_color' => '21C1ED']);

		$id = DB::table('label_types')->insertGetId(['type' => 'Booking - Unpaid']);
		DB::table('label')->insert(['hostel_id' => '1', 'label_type_id' => $id, 'hex_color' => 'DEFEFF']);

		$id = DB::table('label_types')->insertGetId(['type' => 'Checked in - Paid']);
		DB::table('label')->insert(['hostel_id' => '1', 'label_type_id' => $id, 'hex_color' => '10E894']);

		$id = DB::table('label_types')->insertGetId(['type' => 'Checked in - Unpaid']);
		DB::table('label')->insert(['hostel_id' => '1', 'label_type_id' => $id, 'hex_color' => 'A7FBBC']);

		$id = DB::table('label_types')->insertGetId(['type' => 'Checked Out']);
		DB::table('label')->insert(['hostel_id' => '1', 'label_type_id' => $id, 'hex_color' => '0C5E6B']);

		$id = DB::table('label_types')->insertGetId(['type' => 'Blocked Room']);
		DB::table('label')->insert(['hostel_id' => '1', 'label_type_id' => $id, 'hex_color' => 'DDE86C']);

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('label');
	}

}
