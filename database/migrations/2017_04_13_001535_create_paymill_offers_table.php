<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymillOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paymill_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('min_beds');
            $table->unsignedSmallInteger('max_beds');
            $table->unsignedSmallInteger('price');
            $table->string('paymill_id')->unique();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        DB::table('paymill_offers')->insert([
            ['min_beds' => 0, 'max_beds' => 5, 'price' => 1000, 'paymill_id' => 'offer_7618ff0d72327a42a798'],
            ['min_beds' => 6, 'max_beds' => 10, 'price' => 1200, 'paymill_id' => 'offer_bd7737ee034fa2936e80'],
            ['min_beds' => 11, 'max_beds' => 15, 'price' => 1400, 'paymill_id' => 'offer_611c945f0e3d645ec2e1'],
            ['min_beds' => 16, 'max_beds' => 20, 'price' => 1600, 'paymill_id' => 'offer_915f24c3e5f10a5eb50d'],
            ['min_beds' => 21, 'max_beds' => 30, 'price' => 2100, 'paymill_id' => 'offer_b4b8ecff9eb6f30bac33'],
            ['min_beds' => 31, 'max_beds' => 40, 'price' => 2400, 'paymill_id' => 'offer_e92fd85f55556c66951c'],
            ['min_beds' => 41, 'max_beds' => 50, 'price' => 2700, 'paymill_id' => 'offer_69ae11e18461dee19722'],
            ['min_beds' => 51, 'max_beds' => 60, 'price' => 3000, 'paymill_id' => 'offer_92125916a7487e73a300'],
            ['min_beds' => 61, 'max_beds' => 70, 'price' => 3300, 'paymill_id' => 'offer_c590073e0108c4e7930a'],
            ['min_beds' => 71, 'max_beds' => 80, 'price' => 3600, 'paymill_id' => 'offer_f6ab279d6991538c81df'],
            ['min_beds' => 81, 'max_beds' => 100, 'price' => 4000, 'paymill_id' => 'offer_252b8f42c9892fc21419'],
            ['min_beds' => 101, 'max_beds' => 150, 'price' => 5500, 'paymill_id' => 'offer_dae584aa7c484f9ae718'],
            ['min_beds' => 151, 'max_beds' => 200, 'price' => 7000, 'paymill_id' => 'offer_cc851a5c70d4ca819db2'],
            ['min_beds' => 201, 'max_beds' => 300, 'price' => 9000, 'paymill_id' => 'offer_17ff5962194897f4b32c'],
            ['min_beds' => 301, 'max_beds' => 500, 'price' => 12500, 'paymill_id' => 'offer_c243a8e5f27bd3b0102f'],
            ['min_beds' => 501, 'max_beds' => 1000, 'price' => 15500, 'paymill_id' => 'offer_395b15f904cd5426f25b']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paymill_offers');
    }
}
