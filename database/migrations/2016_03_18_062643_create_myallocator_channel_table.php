<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyallocatorChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('myallocator_channel', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name');
        });

        DB::unprepared(file_get_contents(base_path().'/database/seeds/myallocator_channel.sql'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('myallocator_channel');
    }
}
