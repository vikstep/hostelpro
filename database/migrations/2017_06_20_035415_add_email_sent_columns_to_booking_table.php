<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailSentColumnsToBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking', function ($table) {
            $table->boolean('pre_email_sent')->default(false);
            $table->boolean('post_email_sent')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking', function ($table) {
            $table->dropColumn('pre_email_sent');
            $table->dropColumn('post_email_sent');
        });
    }
}
