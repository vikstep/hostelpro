<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixStatusColumnInBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("ALTER TABLE booking CHANGE COLUMN status status ENUM('Unallocated', 'Allocated', 'Cancelled', 'NoShow') NOT NULL DEFAULT 'Unallocated'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE booking CHANGE COLUMN status status ENUM('Unallocated', 'Allocated', 'Cancelled', 'NoShow')");
    }
}
