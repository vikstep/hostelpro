<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPassportColumnsToGuestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guest', function ($table) {
            $table->string('passport_number')->index()->nullable();

            $table->integer('passport_country_id')->unsigned()->index()->nullable();
            $table->foreign('passport_country_id')->references('id')->on('country')->onDelete('set null');

            $table->date('date_of_birth')->nullable();

            $table->string('place_of_birth')->nullable();

            $table->date('passport_issue_date')->nullable();

            $table->date('passport_expiry_date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guest', function ($table) {
            $table->dropColumn(['passport_number', 'passport_country_id', 'date_of_birth', 'place_of_birth', 'passport_issue_date', 'passport_expiry_date']);
        });
    }
}
