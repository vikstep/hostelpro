<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardUnallocatedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('board_unallocated', function($table) {
			$table->date('date');
			$table->integer('room_type_id')->unsigned()->index();
			$table->foreign('room_type_id')->references('id')->on('room_types');
			$table->tinyInteger('number_of_guests')->unsigned()->index();
			$table->integer('booking_id')->unsigned()->index();
			$table->foreign('booking_id')->references('id')->on('booking')->onDelete('cascade');
			$table->integer('price_per_night')->unsigned();
			$table->primary(array('date', 'room_type_id', 'booking_id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('board_unallocated');
	}

}
