<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMyallocatorChannelIdToBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking', function ($table) {
            $table->string('myallocator_channel_id')->nullable();
            $table->foreign('myallocator_channel_id')->references('id')->on('myallocator_channel')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking', function ($table) {
            $table->dropColumn('myallocator_channel_id');
        });
    }
}
