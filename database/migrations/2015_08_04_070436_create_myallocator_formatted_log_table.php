<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyallocatorFormattedLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		#DB::statement('ALTER TABLE room_types ADD UNIQUE (myallocator_id)');

		Schema::create('myallocator_formatted_log', function($table) {
			$table->integer('booking_id')->unsigned()->nullable()->index();
			$table->foreign('booking_id')->references('id')->on('booking')->onDelete('cascade');
			$table->date('start_date');
			$table->date('end_date');
			$table->integer('number_of_guests')->unsigned();
			$table->integer('myallocator_room_type_id')->unsigned()->nullable()->index();
			#$table->foreign('myallocator_room_type_id')->references('myallocator_id')->on('room_types');
			$table->integer('subtotal');
			$table->integer('currency_id')->unsigned();
			$table->foreign('currency_id')->references('id')->on('currency');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('myallocator_formatted_log');
	}

}
