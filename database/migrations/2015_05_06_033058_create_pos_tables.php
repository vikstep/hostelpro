<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('pos_categories', function ($table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->integer('hostel_id')->unsigned()->index();
            $table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');
        });


        Schema::create('pos_products', function ($table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('pos_categories')->onDelete('cascade');
            $table->string('name', 100);
            $table->integer('price');
        });

        Schema::create('payment', function ($table) {
            $table->increments('id');

            $table->integer('total')->unsigned();

            $table->integer('hostel_id')->unsigned()->index();
            $table->foreign('hostel_id')->references('id')->on('hostel')->onDelete('cascade');

            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('set null');

            $table->integer('booking_id')->unsigned()->nullable()->index();
            $table->foreign('booking_id')->references('id')->on('booking')->onDelete('cascade');

            $table->integer('currency_id')->unsigned();
            $table->foreign('currency_id')->references('id')->on('currency');
            $table->timestamps();
        });

        Schema::create('payment_data', function ($table) {
            $table->increments('id');
            $table->integer('payment_id')->unsigned()->index();
            $table->foreign('payment_id')->references('id')->on('payment');
            $table->string('name', 100);
            $table->integer('price');
            $table->integer('units')->unsigned();
            $table->integer('subtotal');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('payment_data');
        Schema::drop('payment');
        Schema::drop('pos_products');
        Schema::drop('pos_categories');
    }

}
